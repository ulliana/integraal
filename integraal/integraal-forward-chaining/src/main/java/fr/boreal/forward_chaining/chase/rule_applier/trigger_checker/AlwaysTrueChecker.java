package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * Always accept the given trigger
 * For test purposes or when you are sure the trigger is new
 */
public class AlwaysTrueChecker implements TriggerChecker {

	@Override
	public boolean check(FORule rule, Substitution substitution, FactBase fb) {
		return true;
	}

}
