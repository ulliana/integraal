package fr.boreal.forward_chaining.chase.halting_condition;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * This halting condition stops the chase if no facts were created at the last step
 */
public class CreatedFactsAtPreviousStep implements HaltingCondition {
	
	private Chase c;

	@Override
	public boolean check() {
		// first step
		if(c.getLastStepResults().created_facts() == null) {
			return true;
		}
		return ! c.getLastStepResults().created_facts().isEmpty();
	}

	@Override
	public void init(Chase c) {
		this.c = c;
	}

}
