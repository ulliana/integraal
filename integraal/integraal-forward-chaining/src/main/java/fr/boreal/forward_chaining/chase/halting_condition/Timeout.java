package fr.boreal.forward_chaining.chase.halting_condition;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * This halting condition stops the chase if more than a given amount of time passed
 */
public class Timeout implements HaltingCondition {
	
	private long timeout;
	private long start_time;
	
	public Timeout(long ms) {
		this.timeout = ms * 1000000l;
	}

	@Override
	public boolean check() {
		long time = System.nanoTime();
		return (time - this.start_time) < timeout;
	}

	@Override
	public void init(Chase c) {
		this.start_time = System.nanoTime();
	}

}
