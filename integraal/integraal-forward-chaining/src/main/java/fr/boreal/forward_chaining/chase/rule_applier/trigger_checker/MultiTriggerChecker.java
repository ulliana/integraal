package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import java.util.Arrays;
import java.util.Collection;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * Combines multiple checkers and checks that all of them accept the given trigger
 */
public class MultiTriggerChecker implements TriggerChecker {

	Collection<TriggerChecker> checkers;
	
	public MultiTriggerChecker (TriggerChecker... checkers) {
		this.checkers = Arrays.asList(checkers);
	}
	
	public MultiTriggerChecker (Collection<TriggerChecker> checkers) {
		this.checkers = checkers;
	}

	@Override
	public boolean check(FORule rule, Substitution substitution, FactBase fb) {
		return this.checkers.stream().map(checker -> checker.check(rule, substitution, fb)).allMatch(b -> b);
	}

}
