package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler;

import java.util.Collection;
import java.util.stream.Collectors;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;

/**
 * Keep the new facts and delay the addition in the factbase
 */
public class DelegatedApplication implements FactsHandler {
	
	@Override
	public FOFormula<Atom> add(FOFormula<Atom> new_facts, FactBase fb) {		
		Collection<FOFormula<Atom>> atoms_to_add = new_facts.flatten().stream()
			.filter(a -> !fb.contains(a))
			.map(a -> FOFormulaFactory.instance().createOrGetAtomic(a).get())
			.collect(Collectors.toSet());
		
		if(atoms_to_add.isEmpty()) {
			return null;
		}
		return FOFormulaFactory.instance().createOrGetConjunction(atoms_to_add).get();
	}


}
