package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier;

import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.FactsHandler;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.TriggerRenamer;
import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * Applies the given triggers by
 * * renaming the existentials
 * * and creating the image of the rule's head with the renamed homomorphism
 * * then add the images to the facts
 */
public class TriggerApplierImpl implements TriggerApplier {
	
	private TriggerRenamer tr;
	private FactsHandler fh;
	
	public TriggerApplierImpl(TriggerRenamer tr, FactsHandler fh) {
		this.tr = tr;
		this.fh = fh;
	}

	@Override
	public FOFormula<Atom> apply(FORule rule, Substitution substitution, FactBase fb) {
		Substitution full_substitution = this.tr.renameExitentials(rule, substitution);
		FOFormula<Atom> new_facts = FOFormulaUtil.createImageWith(rule.getHead(), full_substitution);
		return this.fh.add(new_facts, fb);
	}

}