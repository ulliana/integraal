package fr.boreal.forward_chaining.chase.treatment;

/**
 * These treatments can be applied at the start of a step of the chase
 */
public interface Pretreatment extends Treatment {

}
