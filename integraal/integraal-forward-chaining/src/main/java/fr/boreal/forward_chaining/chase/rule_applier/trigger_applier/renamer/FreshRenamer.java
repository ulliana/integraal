package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.rule.api.FORule;

/**
 * Rename with a fresh variable each time
 */
public class FreshRenamer implements TriggerRenamer {

	private TermFactory tf;
	
	public FreshRenamer(TermFactory tf) {
		this.tf = tf;
	}

	@Override
	public Substitution renameExitentials(FORule rule, final Substitution substitution) {
		Substitution s = new SubstitutionImpl();
		for(Variable v : rule.getExistentials()) {
			s.add(v, this.tf.createOrGetFreshVariable());
		}
		
		// In case an existential have a value associated in the initial substitution
		// Could be the case during rewriting
		for(Variable v : substitution.keys()) {
			s.remove(v);
		}
		
		return s.merged(substitution).get();
	}
}
