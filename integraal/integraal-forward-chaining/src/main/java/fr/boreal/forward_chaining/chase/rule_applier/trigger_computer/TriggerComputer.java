package fr.boreal.forward_chaining.chase.rule_applier.trigger_computer;

import java.util.Iterator;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;

/**
 * Computes the triggers from a rule's body on a factbase
 */
public interface TriggerComputer {
	
	/**
	 * Initialize the trigger computer for the given chase
	 */
	public void init(Chase c);

	/**
	 * Computes and returns the triggers of the body on the factbase
	 */
	public Iterator<Substitution> compute(FOQuery body, FactBase fb);

	

}
