package fr.boreal.forward_chaining.chase.rule_scheduler;

import java.util.Collection;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.rule.api.FORule;

/**
 * Computes the rules that need to be applied according to previously applied rules
 */
public interface RuleScheduler {
	
	/**
	 * Initialize the rule applier for the given chase
	 */
	public void init(RuleBase rb);

	/**
	 * @return the rules to apply at the next step
	 */
	public Collection<FORule> getRulesToApply(Collection<FORule> last_applied_rules);	
	
}
