package fr.boreal.forward_chaining.chase.halting_condition;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * This halting condition stops the chase if it reaches a given number a steps
 */
public class LimitNumberOfStep implements HaltingCondition {

	private Chase c;
	
	private int max;
	
	public LimitNumberOfStep(int max) {
		this.max = max;
	}
	
	@Override
	public boolean check() {
		return c.getStepCount() < max;
	}

	@Override
	public void init(Chase c) {
		this.c = c;
	}

}
