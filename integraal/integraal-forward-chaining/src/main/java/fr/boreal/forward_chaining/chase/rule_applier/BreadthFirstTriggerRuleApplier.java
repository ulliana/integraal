package fr.boreal.forward_chaining.chase.rule_applier;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.TriggerChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;

/**
 * The breadth first rule application applies the rules by
 * * Computing the homomorphisms from the body of the rules to the factbase
 * * Checking if the homomorphism is applicable
 * * Applying the homomorphism to the head of the rule and
 * * Adding the resulting atoms to the factbase
 */
public class BreadthFirstTriggerRuleApplier extends AbstractRuleApplier {
	
	public BreadthFirstTriggerRuleApplier(TriggerComputer computer, TriggerChecker checker, TriggerApplier applier) {
		super(computer, checker, applier);
	}
	
	@Override
	public RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb) {
		Map<FOQuery, Collection<FORule>> rules_by_body = this.groupRulesByBody(rules);
		Map<FOQuery, Collection<Substitution>> substitutions_by_body = new HashMap<FOQuery, Collection<Substitution>>();
		
		// Evaluate all rule bodies and store the substitution
		for(FOQuery body : rules_by_body.keySet()) {
			Iterator<Substitution> res = this.computer.compute(body, fb);
			substitutions_by_body.put(body, new HashSet<Substitution>());
			while(res.hasNext()) {
				substitutions_by_body.get(body).add(res.next());
			}
		}
		
		// Apply the substitutions to rule heads and add the new facts
		Collection<FORule> applied_rules = new HashSet<FORule>();
		Collection<FOFormula<Atom>> created_facts = new HashSet<FOFormula<Atom>>();
		for(FOQuery body : rules_by_body.keySet()) {
			for(Substitution substitution : substitutions_by_body.get(body)) {
				for(FORule rule : rules_by_body.get(body)) {
					if(this.checker.check(rule, substitution, fb)) {
						FOFormula<Atom> application_facts = this.applier.apply(rule, substitution, fb);
						if(application_facts != null) {
							created_facts.add(application_facts);
							applied_rules.add(rule);
						}
					}
				}
			}
		}
		return new RuleApplicationStepResult(applied_rules, created_facts);
	}

}
