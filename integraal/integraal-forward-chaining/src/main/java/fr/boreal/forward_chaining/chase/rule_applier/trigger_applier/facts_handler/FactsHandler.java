package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;

/**
 * Handles newly created facts
 */
public interface FactsHandler {

	public FOFormula<Atom> add(FOFormula<Atom> new_facts, FactBase fb);

}
