package fr.boreal.forward_chaining.chase.rule_scheduler;

import java.util.Collection;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.rule.api.FORule;

/**
 * Schedules all the rules for every step
 */
public class NaiveScheduler implements RuleScheduler {
	
	private RuleBase rb;
	
	public NaiveScheduler(RuleBase rb) {
		this.init(rb);
	}
	
	@Override
	public void init(RuleBase rb) {
		this.rb = rb;
	}

	@Override
	public Collection<FORule> getRulesToApply(Collection<FORule> last_applied_rules) {
		return this.rb.getRules();
	}


}
