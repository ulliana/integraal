package fr.boreal.forward_chaining.api;

/**
 * A ForwardChaining algorithm saturates a {@link FactBase} according to the given {@link FORule}.
 */
public interface ForwardChainingAlgorithm {

	/**
	 * Execute the algorithm
	 */
	public void execute();
	
}
