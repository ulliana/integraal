package fr.boreal.forward_chaining.chase.rule_applier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.TriggerChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;

public abstract class AbstractRuleApplier implements RuleApplier {
	
	protected TriggerComputer computer;
	protected TriggerChecker checker;
	protected TriggerApplier applier;
	
	public AbstractRuleApplier(TriggerComputer computer, TriggerChecker checker, TriggerApplier applier) {
		this.computer = computer;
		this.checker = checker;
		this.applier = applier;
	}
	
	@Override
	public void init(Chase c) {
		this.computer.init(c);		
	}
	
	/**
	 * Re-groups the rules with the same body
	 */
	protected Map<FOQuery, Collection<FORule>> groupRulesByBody(Collection<FORule> rules) {
		Map<FOQuery, Collection<FORule>> map = new HashMap<FOQuery, Collection<FORule>>();
		
		for(FORule rule : rules) {
			FOQuery body = rule.getBody();
			if(!map.containsKey(body)) {
				map.put(body, new ArrayList<FORule>());
			}
			map.get(body).add(rule);
		}
		return map;
	}

}
