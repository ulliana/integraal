package fr.boreal.forward_chaining.chase.rule_applier.trigger_computer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;

/**
 * Computes only triggers that has not been computed at a previous step
 *
 * See https://wiki.epfl.ch/provenance2011/documents/foundations%20of%20databases-abiteboul-1995.pdf
 * part 13.1 for more explanations
 */
public class SemiNaiveComputer implements TriggerComputer {

	private FOQueryEvaluator<FOQuery> evaluator;
	private Chase chase;

	private TriggerComputer fallback;

	public SemiNaiveComputer() {
		this(GenericFOQueryEvaluator.defaultInstance());
	}

	public SemiNaiveComputer(FOQueryEvaluator<FOQuery> evaluator) {
		this.evaluator = evaluator;
		this.fallback = new NaiveTriggerComputer(this.evaluator);
	}

	@Override
	public void init(Chase c) {
		this.chase = c;
		this.fallback.init(c);
	}

	@Override
	public Iterator<Substitution> compute(FOQuery body, FactBase fb) {

		// First step
		if(chase.getLastStepResults().created_facts() == null) {
			return this.fallback.compute(body, fb);
		}

		FactBase last_step_facts = chase.getLastStepResults().created_facts_as_factbase();

		Set<Predicate> idb_predicates = this.chase.getRuleBase().getRules().stream()
				.map(rule -> rule.getHead())
				.map(head -> head.flatten())
				.flatMap(Collection::stream)
				.map(atom -> atom.getPredicate())
				.collect(Collectors.toSet());

		Set<Atom> atoms_idb_predicates = new HashSet<>();
		Set<Atom> atoms_edb_predicates = new HashSet<>();

		for(Atom a : body.getFormula().flatten()) {
			if(idb_predicates.contains(a.getPredicate())) {
				atoms_idb_predicates.add(a);
			} else {
				atoms_edb_predicates.add(a);
			}
		}

		Collection<Stream<Substitution>> all_matches = new ArrayList<Stream<Substitution>>();

		for(int i = 0; i < atoms_idb_predicates.size(); ++i) {

			Set<Atom> atoms_on_last_step_facts = new HashSet<Atom>();
			Set<Atom> atoms_on_factbase = new HashSet<Atom>(atoms_edb_predicates);
			Set<Atom> atoms_on_prec_factbase = new HashSet<Atom>();

			int j = 0;
			for(Atom a : atoms_idb_predicates) {
				if(j < i) {
					atoms_on_factbase.add(a);
				} else if(j == i) {
					atoms_on_last_step_facts.add(a);
				} else {
					atoms_on_prec_factbase.add(a);
				}
				++j;
			}

			FOFormulaConjunction<Atom> atoms_on_last_step_facts_formula = FOFormulaFactory.instance().createOrGetConjunction(
					atoms_on_last_step_facts.toArray(new Atom[atoms_on_last_step_facts.size()])).get();
			FOQuery query_on_last_step_facts = FOQueryFactory.instance().createOrGetConjunctionQuery(atoms_on_last_step_facts_formula, null, null);

			Stream<Substitution> possible_triggers = StreamSupport.stream(Spliterators.spliteratorUnknownSize(
					this.evaluator.evaluate(query_on_last_step_facts, last_step_facts), Spliterator.ORDERED), false);

			if(!atoms_on_factbase.isEmpty()) {
				FOFormulaConjunction<Atom> atoms_on_factbase_formula = FOFormulaFactory.instance().createOrGetConjunction(
						atoms_on_factbase.toArray(new Atom[atoms_on_factbase.size()])).get();
				possible_triggers = possible_triggers.flatMap(substitution -> {
					FOQuery query_on_factbase = FOQueryFactory.instance().createOrGetConjunctionQuery(atoms_on_factbase_formula, null, substitution);
					return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
							this.evaluator.evaluate(query_on_factbase, fb), Spliterator.ORDERED), false)
							.map(subs -> substitution.merged(subs).get());
				});
			}

			for(Atom a : atoms_on_prec_factbase) {
				possible_triggers = possible_triggers.flatMap(substitution -> {
					return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
							chase.getFactBase().match(a, substitution), Spliterator.ORDERED), false)
							.filter(atom -> !last_step_facts.contains(atom))
							.map(match -> this.computeSubstitution(a, match))
							.map(subs -> substitution.merged(subs).get());
				});
			}
			all_matches.add(possible_triggers);
		}

		return all_matches.stream()
				.flatMap(x -> x)
				.iterator();
	}

	private Substitution computeSubstitution (Atom from, Atom to) {
		Substitution s = new SubstitutionImpl();
		for(int i = 0; i < from.getPredicate().getArity(); ++i) {
			Term from_term = from.getTerm(i);
			if(from_term.isVariable()) {
				Term to_term = to.getTerm(i);
				s.add((Variable)from_term, to_term);
			}
		}		
		return s;
	}

}
