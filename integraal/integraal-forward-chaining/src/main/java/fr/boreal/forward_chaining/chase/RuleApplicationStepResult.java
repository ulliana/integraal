package fr.boreal.forward_chaining.chase;

import java.util.Collection;
import java.util.stream.Collectors;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.inmemory.SimpleInMemoryGraphStore;

/**
 * This object represents the result of a step of the chase
 * It contains the appleid rules and the created facts
 */
public class RuleApplicationStepResult {
	
	private final Collection<FORule> applied_rules;
	private final Collection<FOFormula<Atom>> created_facts;
	
	private FactBase created_facts_factbase = null;
	
	public RuleApplicationStepResult(Collection<FORule> applied_rules, Collection<FOFormula<Atom>> created_facts)  {
		this.applied_rules = applied_rules;
		this.created_facts = created_facts;
	}
	
	public Collection<FORule> applied_rules() {
		return this.applied_rules;
	}
	
	public Collection<FOFormula<Atom>> created_facts() {
		return this.created_facts;
	}	
	
	public FactBase created_facts_as_factbase() {
		if(this.created_facts_factbase == null) {
			this.created_facts_factbase = new SimpleInMemoryGraphStore(
					this.created_facts.stream()
					.flatMap(facts -> facts.flatten().stream())
					.collect(Collectors.toSet()));
		}
		return this.created_facts_factbase;
	}
}
