package fr.boreal.forward_chaining.chase.rule_scheduler;

import java.util.Collection;
import java.util.stream.Collectors;

import fr.boreal.grd.api.GraphOfFORuleDependencies;
import fr.boreal.grd.impl.GRDImpl;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.rule.api.FORule;

/**
 * Schedules all the rules that can be triggered by rules applied at the previous step
 * according to the graph of rule dependencies
 */
public class GRDScheduler implements RuleScheduler {

	private RuleBase rb;
	private GraphOfFORuleDependencies grd;

	public GRDScheduler(RuleBase rb) {
		this.init(rb);
	}

	@Override
	public void init(RuleBase rb) {
		this.rb = rb;
		this.grd = new GRDImpl(rb);
	}

	@Override
	public Collection<FORule> getRulesToApply(Collection<FORule> last_applied_rules) {
		// first call -> should use an optional instead of null
		if(last_applied_rules == null) {
			return this.rb.getRules();
		}
		return last_applied_rules.stream()
				.flatMap(rule -> this.grd.getTriggeredRules(rule).stream())
				.collect(Collectors.toSet());
	}

}
