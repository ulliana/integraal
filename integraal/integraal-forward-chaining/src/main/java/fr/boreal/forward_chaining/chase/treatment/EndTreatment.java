package fr.boreal.forward_chaining.chase.treatment;

/**
 * These treatments can be applied at the end of a step of the chase
 */
public interface EndTreatment extends Treatment {

}
