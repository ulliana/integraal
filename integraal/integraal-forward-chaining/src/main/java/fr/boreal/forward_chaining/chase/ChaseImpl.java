package fr.boreal.forward_chaining.chase;

import java.util.Collection;

import fr.boreal.forward_chaining.chase.halting_condition.HaltingCondition;
import fr.boreal.forward_chaining.chase.rule_applier.RuleApplier;
import fr.boreal.forward_chaining.chase.rule_scheduler.RuleScheduler;
import fr.boreal.forward_chaining.chase.treatment.EndTreatment;
import fr.boreal.forward_chaining.chase.treatment.Pretreatment;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.rule.api.FORule;

/**
 * Default implementation of the chase algorithm
 */
public class ChaseImpl implements Chase {
	
	private FactBase fb;
	private RuleBase rb;
	
	private RuleScheduler rule_scheduler;
	private RuleApplier rule_applier;
	private RuleApplicationStepResult last_step_result;
	
	private Collection<HaltingCondition> halting_conditions;
	
	private Collection<Pretreatment> global_pretreatments;
	private Collection<Pretreatment> step_pretreatments;
	
	private Collection<EndTreatment> global_end_treatments;
	private Collection<EndTreatment> end_of_step_treatments;

	private int step_number = 0;
	
	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Use {@link ChaseBuilder} to create a chase
	 */
	protected ChaseImpl(FactBase fb, RuleBase rb,
			RuleScheduler rule_scheduler, RuleApplier rule_applier,
			Collection<HaltingCondition> halting_conditions,
			Collection<Pretreatment> global_pretreatments,
			Collection<Pretreatment> step_pretreatments,
			Collection<EndTreatment> global_end_treatments,
			Collection<EndTreatment> end_of_step_treatments) {
		this.fb = fb;
		this.rb = rb;
		this.rule_scheduler = rule_scheduler;
		this.rule_applier = rule_applier;
		this.halting_conditions = halting_conditions;
		this.global_pretreatments = global_pretreatments;
		this.step_pretreatments = step_pretreatments;
		this.global_end_treatments = global_end_treatments;
		this.end_of_step_treatments = end_of_step_treatments;
		
		this.last_step_result = new RuleApplicationStepResult(null, null);
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////
	
	@Override
	public boolean hasNextStep() {
		return this.halting_conditions.stream().allMatch(condition -> condition.check());
	}

	@Override
	public void nextStep() {
		++step_number;
		Collection<FORule> rules_to_apply = this.rule_scheduler.getRulesToApply(this.last_step_result.applied_rules());
		RuleApplicationStepResult step_result = this.rule_applier.apply(rules_to_apply, this.fb);
		this.last_step_result = step_result;
	}

	@Override
	public void applyGlobalPretreatments() {
		this.initAll();
		this.global_pretreatments.forEach(treatment -> treatment.apply());
	}

	@Override
	public void applyPretreatments() {
		this.step_pretreatments.forEach(treatment -> treatment.apply());
	}
	
	@Override
	public void applyEndOfStepTreatments() {
		this.end_of_step_treatments.forEach(treatment -> treatment.apply());
	}

	@Override
	public void applyGlobalEndTreatments() {
		this.global_end_treatments.forEach(treatment -> treatment.apply());
	}
	
	/////////////////////////////////////////////////
	// Getter
	/////////////////////////////////////////////////
	
	@Override
	public FactBase getFactBase() {
		return this.fb;
	}
	
	@Override
	public RuleBase getRuleBase() {
		return this.rb;
	}
	
	@Override
	public void setRuleBase(RuleBase rb) {
		this.rb = rb;
	}
	
	@Override
	public RuleApplicationStepResult getLastStepResults() {
		return this.last_step_result;
	}

	@Override
	public RuleScheduler getRuleScheduler() {
		return this.rule_scheduler;
	}
	
	@Override
	public int getStepCount() {
		return this.step_number;
	}
	
	@Override
	public String toString() {
		return "Steps : " + this.step_number +
				"\nStep result :\n" + this.last_step_result +
				"\nFacts :\n" +this.fb.toString();
	}
	
	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////
	
	private void initAll() {
		this.initGlobalPretreatments();
		this.initPretreatments();
		this.initRuleApplier();
		this.initEndOfStepTreatments();
		this.initGlobalEndTreatments();
		this.initHaltingConditions();
	}

	private void initGlobalPretreatments() {
		this.global_pretreatments.forEach(treatment -> treatment.init(this));		
	}
	
	private void initRuleApplier() {
		this.rule_applier.init(this);	
	}
	
	private void initPretreatments() {
		this.step_pretreatments.forEach(treatment -> treatment.init(this));		
	}
	
	private void initEndOfStepTreatments() {
		this.end_of_step_treatments.forEach(treatment -> treatment.init(this));		
	}
	
	private void initGlobalEndTreatments() {
		this.global_end_treatments.forEach(treatment -> treatment.init(this));		
	}

	private void initHaltingConditions() {
		this.halting_conditions.forEach(condition -> condition.init(this));
	}

}
