package fr.boreal.forward_chaining.chase.treatment;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * Apply a given treatment
 */
public interface Treatment {
	
	/**
	 * Initialize the treatment for the given chase
	 */
	public void init(Chase c);
	
	/**
	 * Applies the treatment
	 */
	public void apply();
	
	

}
