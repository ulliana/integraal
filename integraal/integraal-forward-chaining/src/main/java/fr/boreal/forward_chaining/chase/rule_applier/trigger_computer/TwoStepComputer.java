package fr.boreal.forward_chaining.chase.rule_applier.trigger_computer;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.inmemory.SimpleFOFormulaStore;
import fr.boreal.storage.inmemory.SimpleInMemoryGraphStore;
import fr.lirmm.boreal.util.validator.Validator;
import fr.lirmm.boreal.util.validator.rule.ConjunctionFormulaValidator;
import fr.lirmm.boreal.util.validator.rule.PositiveFormulaValidator;

/**
 * Computes only triggers that has not been computed at a previous step
 * 
 * Send an atom on the new facts and extends the homomorphism to the factbase
 */
public class TwoStepComputer implements TriggerComputer {

	private FOQueryEvaluator<FOQuery> evaluator;
	private Chase chase;

	public TwoStepComputer() {
		this(GenericFOQueryEvaluator.defaultInstance());
	}

	public TwoStepComputer(FOQueryEvaluator<FOQuery> evaluator) {
		this.evaluator = evaluator;
	}

	@Override
	public void init(Chase c) {
		this.chase = c;
	}

	@Override
	public Iterator<Substitution> compute(FOQuery body, FactBase fb) {		
		Collection<FOFormula<Atom>> last_step_facts = this.chase.getLastStepResults().created_facts();

		// First step
		if(last_step_facts == null) {
			return this.evaluator.evaluate(body, fb);
		}

		// if the new facts are compatible with the AtomSet notion we store them in a SimpleInMemoryGraphStore to get it's optimizations
		// otherwise we store them in a SimpleFOFormulaStore
		FactBase last_step_factbase;
		Validator<FOFormula<Atom>> positive_validator = new PositiveFormulaValidator();
		Validator<FOFormula<Atom>> conjunctive_validator = new ConjunctionFormulaValidator();
		if(positive_validator.check(last_step_facts) && conjunctive_validator.check(last_step_facts)) {
			last_step_factbase = new SimpleInMemoryGraphStore(
					last_step_facts.stream()
					.map(facts -> facts.flatten())
					.flatMap(Collection::stream)
					.collect(Collectors.toSet()));
		} else {
			last_step_factbase = new SimpleFOFormulaStore(last_step_facts);
		}

		Collection<Atom> body_atoms = body.getFormula().flatten();

		// atomic body
		if(body_atoms.size() == 1) {
			return this.evaluator.evaluate(body, last_step_factbase);
		}

		Map<Atom,Set<Substitution>> partialS = new HashMap<>();

		for(Atom a : body_atoms) {
			partialS.put(a, new HashSet<>());
			AtomicFOFormula<Atom> a_formula = FOFormulaFactory.instance().createOrGetAtomic(a).get();
			FOQuery a_query = FOQueryFactory.instance().createOrGetAtomicQuery(a_formula, a.getVariables(), null);
			Iterator<Substitution> sub = this.evaluator.evaluate(a_query, last_step_factbase);
			sub.forEachRemaining(partialS.get(a)::add);
		}

		LinkedList<Iterator<Substitution>> finalResult = new LinkedList<Iterator<Substitution>>();
		
		for (Atom a : partialS.keySet()) {
			for (Substitution s : partialS.get(a)) {
				FOQuery body_copy = FOQueryFactory.instance().createOrGetQuery(body.getFormula(), body.getAnswerVariables(), body.getInitialSubstitution().merged(s).get()).get();
				finalResult.add(StreamSupport.stream(Spliterators.spliteratorUnknownSize(
					this.evaluator.evaluate(body_copy, fb), Spliterator.ORDERED), false)
					.map(subs -> s.merged(subs).get())
					.iterator());
			}
		}
		
		return finalResult.stream()
			.flatMap(i -> StreamSupport.stream(Spliterators.spliteratorUnknownSize(i, Spliterator.ORDERED), false))
			.distinct()
			.iterator();
	}
}