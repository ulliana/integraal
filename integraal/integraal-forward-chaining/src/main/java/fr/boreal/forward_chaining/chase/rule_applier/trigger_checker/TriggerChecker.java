package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * Checks if the given trigger respect a criteria
 */
public interface TriggerChecker {

	public boolean check(FORule rule, Substitution substitution, FactBase fb);

}
