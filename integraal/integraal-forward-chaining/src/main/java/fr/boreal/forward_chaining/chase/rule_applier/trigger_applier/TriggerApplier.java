package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;

/**
 * A trigger applier applies an homomorphism to the rule's head and add the resulting atoms to the factbase
 */
public interface TriggerApplier {

	/**
	 * Applies the gven substitution to the rule's head and add the result to the factbase
	 */
	public FOFormula<Atom> apply(FORule rule, Substitution substitution, FactBase fb);

}
