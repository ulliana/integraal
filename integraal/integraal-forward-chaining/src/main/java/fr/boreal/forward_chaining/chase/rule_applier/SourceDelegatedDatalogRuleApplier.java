package fr.boreal.forward_chaining.chase.rule_applier;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplierImpl;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.DirectApplication;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.FreshRenamer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.ObliviousChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.NaiveTriggerComputer;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.queryEvaluation.generic.SmartFOQueryEvaluator;
import fr.boreal.storage.wrapper.DatalogRuleDelegatable;

/**
 * The source delegated rule application applies the rules by
 * * Delegating the application to the factbase if it can handle it
 * * Using a fallback method is it can't
 */
public class SourceDelegatedDatalogRuleApplier implements RuleApplier {

	private RuleApplier fallback;

	public SourceDelegatedDatalogRuleApplier() {
		this(new BreadthFirstTriggerRuleApplier(
				new NaiveTriggerComputer(SmartFOQueryEvaluator.defaultInstance()),
				new ObliviousChecker(),
				new TriggerApplierImpl(
						new FreshRenamer(SameObjectTermFactory.instance()),
						new DirectApplication())
				));		
	}

	public SourceDelegatedDatalogRuleApplier(RuleApplier fallback) {
		this.fallback = fallback;
	}


	@Override
	public void init(Chase c) {
		// nothing to do
	}

	@Override
	public RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb) {
		if(fb instanceof DatalogRuleDelegatable) {
			DatalogRuleDelegatable storage = (DatalogRuleDelegatable)fb;
			Collection<FORule> existential_rules = new HashSet<FORule>();
			Collection<FORule> range_restricted_rules = new HashSet<FORule>();
			
			rules.forEach(rule -> {
				if(rule.getExistentials().isEmpty()) {
					range_restricted_rules.add(rule);
				} else {
					existential_rules.add(rule);
				}
			});
			
			boolean changed = false;
			try {
				changed = storage.apply(range_restricted_rules);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			RuleApplicationStepResult existential_result = this.fallback.apply(existential_rules, fb);
			
			range_restricted_rules.addAll(existential_result.applied_rules());
			Collection<FOFormula<Atom>> facts = existential_result.created_facts();
			
			if(facts.isEmpty()) {
				if(changed) {
					return new RuleApplicationStepResult(range_restricted_rules, null);
				} else {
					return new RuleApplicationStepResult(range_restricted_rules, Collections.emptyList());
				}
			} else {
				return new RuleApplicationStepResult(range_restricted_rules, facts);
			}
		}
		return this.fallback.apply(rules, fb);
	}


}
