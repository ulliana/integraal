package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import java.util.Collection;
import java.util.Optional;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;

/**
 * Accept the trigger if it cannot be extended with the head of the rule
 * This is the criteria for the restricted chase
 */
public class RestrictedChecker implements TriggerChecker {
	
	FOQueryEvaluator<FOQuery> evaluator;
	
	public RestrictedChecker() {
		this(GenericFOQueryEvaluator.defaultInstance());
	}
	
	public RestrictedChecker(FOQueryEvaluator<FOQuery> evaluator) {
		this.evaluator = evaluator;
	}

	@Override
	public boolean check(FORule rule, Substitution substitution, FactBase fb) {
		Optional<? extends FOQuery> query_opt = this.getQuery(rule, substitution);
		if(query_opt.isPresent()) {
			return !evaluator.exist(query_opt.get(), fb);
		} else {
			return false;
		}
	}
	
	private Optional<? extends FOQuery> getQuery(FORule r, Substitution s) {
		Collection<Variable> head_vars = FOFormulaUtil.getVariables(r.getHead());
		Substitution head_subst = s.limitedTo(head_vars);
		return FOQueryFactory.instance().createOrGetQuery(r.getHead(), head_vars, head_subst);
	}

}
