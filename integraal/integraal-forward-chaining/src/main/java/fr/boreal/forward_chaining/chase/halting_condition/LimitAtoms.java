package fr.boreal.forward_chaining.chase.halting_condition;

import fr.boreal.forward_chaining.chase.Chase;

/**
 * This halting condition stops the chase if there is more than a given number of atoms on the factbase
 */
public class LimitAtoms implements HaltingCondition {
	
	private Chase c;
	private long nbLimitAtoms;
	
	public LimitAtoms(long max) {
		this.nbLimitAtoms = max;
	}

	@Override
	public boolean check() {
		return c.getFactBase().size() < this.nbLimitAtoms;
	}

	@Override
	public void init(Chase c) {
		this.c = c;
	}
}
