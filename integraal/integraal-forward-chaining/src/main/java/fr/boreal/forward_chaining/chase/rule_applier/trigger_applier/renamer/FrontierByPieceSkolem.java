package fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.Rules;

/**
 * Skolem created with all the variables from the piece of the rule's frontier
 */
public class FrontierByPieceSkolem implements TriggerRenamer {
	
	private Map<FORule,Map<Substitution,Map<Variable,Term>>> existentials_names = new HashMap<>();
	Map<FORule,Collection<FORule>> splitted_rules = new HashMap<>();
	
	private TermFactory tf;
	
	public FrontierByPieceSkolem(TermFactory tf) {
		this.tf = tf;
	}

	@Override
	public Substitution renameExitentials(FORule rule, final Substitution substitution) {
		
		if (!existentials_names.containsKey(rule)) {
			existentials_names.put(rule, new HashMap<>());
		}
		
		Collection<FORule> c = splitted_rules.get(rule);
		if (c == null) {
			c = Rules.computeSinglePiece(rule);
			splitted_rules.put(rule, c);
		}
		
		Substitution renamed = new SubstitutionImpl();
		for(FORule splitted : c) {
			Substitution frontier = substitution.limitedTo(splitted.getFrontier());
			
			if (!existentials_names.get(rule).containsKey(frontier)) {
				existentials_names.get(rule).put(frontier, new HashMap<>());
			}
			
			for (Variable v : splitted.getExistentials()) {
				Map<Variable, Term> skolem_names = existentials_names.get(rule).get(frontier);
				if (!skolem_names.containsKey(v)) {
					skolem_names.put(v, this.tf.createOrGetFreshVariable());
				}
				renamed.add(v, skolem_names.get(v));
			}
		}
		
		for(Variable v : substitution.keys()) {
			renamed.remove(v);
		}
		
		return renamed.merged(substitution).get();
	}
}
