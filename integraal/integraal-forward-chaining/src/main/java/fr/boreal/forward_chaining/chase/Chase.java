package fr.boreal.forward_chaining.chase;

import fr.boreal.forward_chaining.api.ForwardChainingAlgorithm;
import fr.boreal.forward_chaining.chase.rule_scheduler.RuleScheduler;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;

/**
 * The Chase is a way to saturate a {@link FactBase} according to rules
 *
 * The work done for the modeling and implementation of the Chase is mostly part of Guillaume Pérution-Kihli internship's work (2020)
 */
public interface Chase extends ForwardChainingAlgorithm {

	/**
	 * @return true iff this chase has a next step
	 */
	public boolean hasNextStep();
	
	/**
	 * Apply the next step of this chase
	 */
	public void nextStep();
	
	/**
	 * Apply global pretreatments
	 * These are treatments to do once at the beginning of the chase
	 */
	public void applyGlobalPretreatments();
	
	/**
	 * Apply pretreatments
	 * These are treatments to do at the beginning of each step of the chase
	 */
	public void applyPretreatments();
	
	/**
	 * Apply end of step treatments
	 * These are treatments to do at the end of each step of the chase
	 */
	public void applyEndOfStepTreatments();
	
	/**
	 * Apply global end treatments
	 * These are treatments to do once at the end of the chase
	 */
	public void applyGlobalEndTreatments();
	
	@Override
	default public void execute() {
		this.applyGlobalPretreatments();
		while(this.hasNextStep()) {
			this.applyPretreatments();
			this.nextStep();
			this.applyEndOfStepTreatments();
		}
		this.applyGlobalEndTreatments();		
	}

	/////////////////////////////////
	// Used for halting conditions //
	/////////////////////////////////
	
	public FactBase getFactBase();

	public RuleBase getRuleBase();
	
	public void setRuleBase(RuleBase rb);

	public RuleApplicationStepResult getLastStepResults();

	public RuleScheduler getRuleScheduler();
	
	public int getStepCount();
	
}
