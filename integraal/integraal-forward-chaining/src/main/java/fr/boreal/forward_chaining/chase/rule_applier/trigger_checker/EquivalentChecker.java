package fr.boreal.forward_chaining.chase.rule_applier.trigger_checker;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;

/**
 * Checks if the factbase created with the given trigger would be equivalent to the current factbase
 */
public class EquivalentChecker implements TriggerChecker {
	
	private FOQueryEvaluator<FOQuery> evaluator;
	
	public EquivalentChecker() {
		this(GenericFOQueryEvaluator.defaultInstance());
	}
	
	public EquivalentChecker(FOQueryEvaluator<FOQuery> evaluator) {
		this.evaluator = evaluator;
	}

	@Override
	public boolean check(FORule rule, Substitution substitution, FactBase fb) {
		FOFormulaConjunction<Atom> piece = getPiecesInFactBase(rule, substitution, fb);
		Optional<FOFormulaConjunction<Atom>> complete_formula = FOFormulaFactory.instance()
				.createOrGetConjunction(
						piece,
						FOFormulaUtil.createImageWith(rule.getHead(), substitution));
		if(complete_formula.isPresent()) {
			FOFormulaConjunction<Atom> formula = complete_formula.get();
			FOQuery query = FOQueryFactory.instance().createOrGetConjunctionQuery(formula, null, null);
			boolean res = !this.evaluator.exist(query, fb);
			return res;
		} else {
			return false;
		}
	}

	private FOFormulaConjunction<Atom> getPiecesInFactBase(FORule r, Substitution h, FactBase factBase) {
		Collection<Variable> vars = FOFormulaUtil.getVariables(FOFormulaUtil.createImageWith(r.getBody().getFormula(), h));
		Set<Variable> addedVars = new HashSet<Variable>();
		
		Collection<FOFormula<Atom>> result = new HashSet<FOFormula<Atom>>();
		
		while (!vars.isEmpty()) {
			Variable v = vars.iterator().next();
			vars.remove(v);
			addedVars.add(v);
			
			Iterator<Atom> it = factBase.getAtoms(v).iterator();
			while (it.hasNext()) {
				Atom a = it.next();
				result.add(FOFormulaFactory.instance().createOrGetAtomic(a).get());
				
				for (Variable av : a.getVariables()) {
					if (!addedVars.contains(av)) {
						vars.add(av);
					}
				}
			}
		}
		
		return FOFormulaFactory.instance().createOrGetConjunction(result).get();
	}
}