package fr.boreal.forward_chaining.chase.rule_applier;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.TriggerChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;

/**
 * The parallel rule application applies the rules by
 * * Computing the homomorphisms from the body of the rules to the factbase
 * * Checking if the homomorphism is applicable
 * * Applying the homomorphism to the head of the rule and
 * * Adding the resulting atoms to a temporary factbase that will need to be merged into the factbase at the end of the step
 */
public class ParallelTriggerRuleApplier extends AbstractRuleApplier {
	
	public ParallelTriggerRuleApplier(TriggerComputer computer, TriggerChecker checker, TriggerApplier applier) {
		super(computer, checker, applier);
	}

	@Override
	public RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb) {
		Map<FOQuery, Collection<FORule>> rules_by_body = this.groupRulesByBody(rules);
		Collection<FORule> applied_rules = new HashSet<FORule>();
		Collection<FOFormula<Atom>> created_facts = new HashSet<FOFormula<Atom>>();
		
		// Evaluate all rule bodies and store the substitution
		for(FOQuery body : rules_by_body.keySet()) {
			Iterator<Substitution> res = this.computer.compute(body, fb);
			while(res.hasNext()) {
				Substitution h = res.next();
				
				for(FORule rule : rules_by_body.get(body)) {
					if(this.checker.check(rule, h, fb)) {
						FOFormula<Atom> application_facts = this.applier.apply(rule, h, fb);
						if(application_facts != null) {
							created_facts.add(application_facts);
							applied_rules.add(rule);
						}
					}
				}
			}
		}
		return new RuleApplicationStepResult(applied_rules, created_facts);
	}

}
