package fr.boreal.forward_chaining.chase.rule_applier;

import java.util.Collection;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.RuleApplicationStepResult;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.rule.api.FORule;

/**
 * A rule applier applies the given rules of the given factbase
 */
public interface RuleApplier {
	
	/**
	 * Initialize the rule applier for the given chase
	 */
	public void init(Chase c);
	
	/**
	 * Applies the given rules on the given factbase an returns the result (see {@link RuleApplicationStepResult})
	 */
	public RuleApplicationStepResult apply(Collection<FORule> rules, FactBase fb);

}
