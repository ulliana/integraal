package fr.boreal.forward_chaining.chase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import fr.boreal.forward_chaining.chase.halting_condition.CreatedFactsAtPreviousStep;
import fr.boreal.forward_chaining.chase.halting_condition.HaltingCondition;
import fr.boreal.forward_chaining.chase.halting_condition.HasRulesToApply;
import fr.boreal.forward_chaining.chase.rule_applier.BreadthFirstTriggerRuleApplier;
import fr.boreal.forward_chaining.chase.rule_applier.SourceDelegatedDatalogRuleApplier;
import fr.boreal.forward_chaining.chase.rule_applier.ParallelTriggerRuleApplier;
import fr.boreal.forward_chaining.chase.rule_applier.RuleApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplier;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.TriggerApplierImpl;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.DelegatedApplication;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.DirectApplication;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.facts_handler.FactsHandler;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.BodySkolem;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.FreshRenamer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.FrontierByPieceSkolem;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.FrontierSkolem;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.TriggerRenamer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.AlwaysTrueChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.EquivalentChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.ObliviousChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.RestrictedChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.SemiObliviousChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_checker.TriggerChecker;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.NaiveTriggerComputer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.SemiNaiveComputer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TriggerComputer;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_computer.TwoStepComputer;
import fr.boreal.forward_chaining.chase.rule_scheduler.GRDScheduler;
import fr.boreal.forward_chaining.chase.rule_scheduler.NaiveScheduler;
import fr.boreal.forward_chaining.chase.rule_scheduler.RuleScheduler;
import fr.boreal.forward_chaining.chase.treatment.AddCreatedFacts;
import fr.boreal.forward_chaining.chase.treatment.Debug;
import fr.boreal.forward_chaining.chase.treatment.EndTreatment;
import fr.boreal.forward_chaining.chase.treatment.Pretreatment;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.queryEvaluation.generic.SmartFOQueryEvaluator;

public class ChaseBuilder {

	private FactBase fb;
	private RuleBase rb;
	private TermFactory tf;

	private RuleScheduler rsc;
	private Scheduler scheduler = Scheduler.GRD;
	private enum Scheduler {NAIVE, GRD};

	private FOQueryEvaluator<FOQuery> eval;
	private Evaluator evaluator = Evaluator.GENERIC;
	private enum Evaluator {GENERIC, SMART};

	private RuleApplier ra;
	private Applier applier = Applier.BREADTH_FIRST_TRIGGER;
	private enum Applier {BREADTH_FIRST_TRIGGER, PARALLEL_TRIGGER, SOURCE_DELEGATED_DATALOG};

	private TriggerComputer tc;
	private Computer computer = Computer.SEMI_NAIVE;
	private enum Computer {NAIVE, SEMI_NAIVE, TWO_STEP};
	private TriggerChecker tch;
	private Checker checker = Checker.SEMI_OBLIVIOUS;
	private enum Checker {TRUE, OBLIVIOUS, SEMI_OBLIVIOUS, RESTRICTED, EQUIVALENT};
	private TriggerApplier ta;
	private FactsHandler fh;
	private Application application = Application.DIRECT;
	private enum Application {DIRECT, PARALLEL};
	private TriggerRenamer renamer;
	private Skolem skolem = Skolem.FRESH;
	private enum Skolem {FRESH, BODY, FRONTIER, FRONTIER_PIECE};


	private Collection<HaltingCondition> hcs = new ArrayList<HaltingCondition>();
	private Collection<Pretreatment> global_pts = new ArrayList<Pretreatment>();
	private Collection<Pretreatment> step_pts = new ArrayList<Pretreatment>();
	private Collection<EndTreatment> global_end_ts = new ArrayList<EndTreatment>();
	private Collection<EndTreatment> end_step_ts = new ArrayList<EndTreatment>();

	private boolean debug = false;

	///////////
	// Build //
	///////////

	/**
	 * The minimal configuration requires : <br>
	 * * a FactBase to saturate <br>
	 * * a RuleBase to get Rules from <br>
	 * * a TermFactory to create new terms <br>
	 * <br>
	 * Default behaviour is : <br>
	 * * a GRDScheduler for rules <br>
	 * * a TriggerRuleApplier with : <br>
	 * * * a SemiNaiveTriggerComputer with a default GenericFOQueryEvaluator <br>
	 * * * a SemiObliviousChecker <br>
	 * * * a Applier with : <br>
	 * * * * a FreshRenamer <br>
	 * * * * a DirectApplication <br>
	 * * HaltingConditions : <br>
	 * * * CreatedFactsAtPreviousStep <br>
	 * * * HasRulesToApply <br>
	 */
	public Optional<Chase> build() {
		if(this.getMinimalConfig().isPresent()) {
			return Optional.of(new ChaseImpl(fb, rb, rsc, ra, hcs, global_pts, step_pts, global_end_ts, end_step_ts));
		} else {
			return Optional.empty();
		}
	}

	////////////////////
	// Default chases //
	////////////////////

	public static ChaseBuilder defaultBuilder(FactBase fb, RuleBase rb, TermFactory tf) {
		return new ChaseBuilder()
				.setFactBase(fb)
				.setRuleBase(rb)
				.setTermFactory(tf);
	}

	public static Chase defaultChase(FactBase fb, RuleBase rb, TermFactory tf) {
		return new ChaseBuilder()
				.setFactBase(fb)
				.setRuleBase(rb)
				.setTermFactory(tf)
				.build()
				.get();
	}

	//////////////////////
	// Default settings //
	//////////////////////

	private Optional<ChaseBuilder> getMinimalConfig() {
		if(this.fb == null) {
			return Optional.empty();
		}
		if(this.rb == null) {
			return Optional.empty();
		}
		if(this.tf == null) {
			return Optional.empty();
		}
		if(this.rsc == null) {
			switch (this.scheduler) {
			case NAIVE : this.setRuleScheduler(new NaiveScheduler(this.rb)); break;
			case GRD : this.setRuleScheduler(new GRDScheduler(this.rb)); break;
			};
		}
		if(this.eval == null) {
			switch (this.evaluator) {
			case GENERIC : this.setFOQueryEvaluator(GenericFOQueryEvaluator.defaultInstance()); break;
			case SMART : this.setFOQueryEvaluator(SmartFOQueryEvaluator.defaultInstance()); break;
			};
		}
		if(this.ra == null) {
			switch(this.applier) {
			case BREADTH_FIRST_TRIGGER : case PARALLEL_TRIGGER : {
				if(this.tc == null) {
					switch (this.computer) {
					case NAIVE : this.setTriggerComputer(new NaiveTriggerComputer(this.eval)); break;
					case SEMI_NAIVE : this.setTriggerComputer(new SemiNaiveComputer(this.eval)); break;
					case TWO_STEP : this.setTriggerComputer(new TwoStepComputer(this.eval)); break;
					};
				}
				if(this.tch == null) {
					switch (this.checker) {
					case TRUE : this.setTriggerChecker(new AlwaysTrueChecker()); break;
					case OBLIVIOUS : this.setTriggerChecker(new ObliviousChecker()); break;
					case SEMI_OBLIVIOUS : this.setTriggerChecker(new SemiObliviousChecker()); break;
					case RESTRICTED : this.setTriggerChecker(new RestrictedChecker(this.eval)); break;
					case EQUIVALENT : this.setTriggerChecker(new EquivalentChecker(this.eval)); break;
					};
				}
				if(this.ta == null) {
					if(this.renamer == null) {
						switch (this.skolem) {
						case FRESH : this.setExistentialsRenamer(new FreshRenamer(this.tf)); break;
						case BODY : this.setExistentialsRenamer(new BodySkolem(this.tf)); break;
						case FRONTIER : this.setExistentialsRenamer(new FrontierSkolem(this.tf)); break;
						case FRONTIER_PIECE : this.setExistentialsRenamer(new FrontierByPieceSkolem(this.tf)); break;
						};
					}
					if(this.fh == null) {
						switch(this.application) {
						case DIRECT : this.setNewFactsHandler(new DirectApplication()); break;
						case PARALLEL : this.setNewFactsHandler(new DelegatedApplication()); break;
						};
					}
					this.setTriggerApplier(new TriggerApplierImpl(this.renamer, this.fh));
				}
				break;
			}
			case SOURCE_DELEGATED_DATALOG: // do nothing
				break;
			}
			switch(this.applier) {
			case BREADTH_FIRST_TRIGGER : this.setRuleApplier(new BreadthFirstTriggerRuleApplier(this.tc, this.tch, this.ta)); break;
			case PARALLEL_TRIGGER : this.setRuleApplier(new ParallelTriggerRuleApplier(this.tc, this.tch, this.ta)); break;
			case SOURCE_DELEGATED_DATALOG : this.setRuleApplier(new SourceDelegatedDatalogRuleApplier()); break;
			};
		}
		if(this.hcs.isEmpty()) {
			this.addHaltingConditions(new CreatedFactsAtPreviousStep(), new HasRulesToApply());
		}
		if(this.application == Application.PARALLEL) {
			this.addStepEndTreatments(new AddCreatedFacts());
		}
		if(this.debug) {
			this.addStepEndTreatments(new Debug());
		}
		return Optional.of(this);
	}

	///////////////////////////
	// Higher level settings //
	///////////////////////////

	// Rule Applier //

	public ChaseBuilder useTriggerRuleApplier() {
		this.applier = Applier.BREADTH_FIRST_TRIGGER;
		return this;
	}

	// FOQuery Evaluator //

	public ChaseBuilder useGenericFOQueryEvaluator() {
		this.evaluator = Evaluator.GENERIC;
		return this;
	}

	public ChaseBuilder useSmartFOQueryEvaluator() {
		this.evaluator = Evaluator.SMART;
		return this;
	}

	// Scheduler //

	public ChaseBuilder useNaiveRuleScheduler() {
		this.scheduler = Scheduler.NAIVE;
		return this;
	}

	public ChaseBuilder useGRDRuleScheduler() {
		this.scheduler = Scheduler.GRD;
		return this;
	}

	// Computer //

	public ChaseBuilder useNaiveComputer() {
		this.computer = Computer.NAIVE;
		return this;
	}

	public ChaseBuilder useSemiNaiveComputer() {
		this.computer = Computer.SEMI_NAIVE;
		return this;
	}

	public ChaseBuilder useTwoStepComputer() {
		this.computer = Computer.TWO_STEP;
		return this;
	}

	// Application //

	public ChaseBuilder useDirectApplication() {
		this.application = Application.DIRECT;
		this.applier = Applier.BREADTH_FIRST_TRIGGER;
		return this;
	}

	public ChaseBuilder useParallelApplication() {
		this.application = Application.PARALLEL;
		this.applier = Applier.PARALLEL_TRIGGER;
		return this;
	}


	public ChaseBuilder useSourceDelegatedDatalogApplication() {
		this.applier = Applier.SOURCE_DELEGATED_DATALOG;
		return this;
	}

	// Criteria //

	public ChaseBuilder useAlwaysTrueChecker() {
		this.checker = Checker.TRUE;
		return this;
	}

	public ChaseBuilder useObliviousChecker() {
		this.checker = Checker.OBLIVIOUS;
		return this;
	}

	public ChaseBuilder useSemiObliviousChecker() {
		this.checker = Checker.SEMI_OBLIVIOUS;
		return this;
	}

	public ChaseBuilder useRestrictedChecker() {
		this.checker = Checker.RESTRICTED;
		return this;
	}

	public ChaseBuilder useEquivalentChecker() {
		this.checker = Checker.EQUIVALENT;
		return this;
	}

	// Existentials naming //

	public ChaseBuilder useFreshNaming() {
		this.skolem = Skolem.FRESH;
		return this;
	}

	public ChaseBuilder useBodySkolem() {
		this.skolem = Skolem.BODY;
		return this;
	}

	public ChaseBuilder useFrontierSkolem() {
		this.skolem = Skolem.FRONTIER;
		return this;
	}

	public ChaseBuilder useFrontierByPieceSkolem() {
		this.skolem = Skolem.FRONTIER_PIECE;
		return this;
	}

	/////////////
	// Setters //
	/////////////

	public ChaseBuilder setFactBase(FactBase fb) {
		this.fb = fb;
		return this;
	}

	public ChaseBuilder setRuleBase(RuleBase rb) {
		this.rb = rb;
		return this;
	}

	public ChaseBuilder setTermFactory(TermFactory tf) {
		this.tf = tf;
		return this;
	}

	public ChaseBuilder setRuleScheduler(RuleScheduler rsc) {
		this.rsc = rsc;
		return this;
	}

	public ChaseBuilder setFOQueryEvaluator(FOQueryEvaluator<FOQuery> eval) {
		this.eval = eval;
		return this;
	}

	public ChaseBuilder setRuleApplier(RuleApplier ra) {
		this.ra = ra;
		return this;
	}

	// Triggers 

	public ChaseBuilder setTriggerComputer(TriggerComputer tc) {
		this.tc = tc;
		return this;
	}

	public ChaseBuilder setTriggerChecker(TriggerChecker tch) {
		this.tch = tch;
		return this;
	}

	public ChaseBuilder setTriggerApplier(TriggerApplier ta) {
		this.ta = ta;
		return this;
	}

	public ChaseBuilder setExistentialsRenamer(TriggerRenamer tr) {
		this.renamer = tr;
		return this;
	}

	public ChaseBuilder setNewFactsHandler(FactsHandler fh) {
		this.fh = fh;
		return this;
	}

	// halting conditions

	public ChaseBuilder addHaltingConditions(HaltingCondition... hcs) {
		for(HaltingCondition hc : hcs) {
			this.hcs.add(hc);
		}
		return this;
	}

	public ChaseBuilder addHaltingConditions(Collection<HaltingCondition> hcs) {
		this.hcs.addAll(hcs);
		return this;
	}

	// Global pretreatment

	public ChaseBuilder addGlobalPretreatments(Pretreatment... pts) {
		for(Pretreatment pt : pts) {
			this.global_pts.add(pt);
		}
		return this;
	}

	public ChaseBuilder addGlobalPretreatments(Collection<Pretreatment> pts) {
		this.global_pts.addAll(pts);
		return this;
	}

	// Step pretreament

	public ChaseBuilder addStepPretreatments(Pretreatment... pts) {
		for(Pretreatment pt : pts) {
			this.step_pts.add(pt);
		}
		return this;
	}

	public ChaseBuilder addStepPretreatments(Collection<Pretreatment> pts) {
		this.step_pts.addAll(pts);
		return this;
	}

	// Global end treatement

	public ChaseBuilder addGlobalEndTreatments(EndTreatment... ets) {
		for(EndTreatment et : ets) {
			this.global_end_ts.add(et);
		}
		return this;
	}

	public ChaseBuilder addGlobalEndTreatments(Collection<EndTreatment> ets) {
		this.global_end_ts.addAll(ets);
		return this;
	}

	// Step end treatement

	public ChaseBuilder addStepEndTreatments(EndTreatment... ets) {
		for(EndTreatment et : ets) {
			this.end_step_ts.add(et);
		}
		return this;
	}

	public ChaseBuilder addStepEndTreatments(Collection<EndTreatment> ets) {
		this.end_step_ts.addAll(ets);
		return this;
	}

	public ChaseBuilder debug() {
		this.debug = true;
		return this;
	}

}
