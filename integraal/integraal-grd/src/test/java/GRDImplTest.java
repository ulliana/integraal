import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.grd.impl.GRDImpl;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;

class GRDImplTest {

	static Predicate p = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 1);
	static Predicate q = SameObjectPredicateFactory.instance().createOrGetPredicate("q", 1);
	static Predicate r = SameObjectPredicateFactory.instance().createOrGetPredicate("r", 2);
	static Predicate s = SameObjectPredicateFactory.instance().createOrGetPredicate("s", 2);
	static Predicate t = SameObjectPredicateFactory.instance().createOrGetPredicate("t", 2);

	static Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");
	static Constant b = SameObjectTermFactory.instance().createOrGetConstant("b");
	static Constant c = SameObjectTermFactory.instance().createOrGetConstant("c");
	static Constant d = SameObjectTermFactory.instance().createOrGetConstant("d");
	static Constant e = SameObjectTermFactory.instance().createOrGetConstant("e");

	static Variable u = SameObjectTermFactory.instance().createOrGetVariable("u");
	static Variable v = SameObjectTermFactory.instance().createOrGetVariable("v");
	static Variable w = SameObjectTermFactory.instance().createOrGetVariable("w");
	static Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	static Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	static Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");

	static Atom px = new AtomImpl(p, x);
	static Atom py = new AtomImpl(p, y);
	static Atom qx = new AtomImpl(q, x);

	static Atom rxy = new AtomImpl(r, x, y);
	static Atom ruv = new AtomImpl(r, u, v);

	static Atom qy = new AtomImpl(q, y);
	static Atom ruu = new AtomImpl(r, u, u);

	// p(x) -> q(x)
	static FORule Rpx_qx = new FORuleImpl(
			FOQueryFactory.instance().createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(px).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(qx).get());

	// p(x) -> p(y)
	static FORule Rpx_py = new FORuleImpl(
			FOQueryFactory.instance().createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(px).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(py).get());

	// q(x) -> r(x, y)
	static FORule Rqx_rxy = new FORuleImpl(
			FOQueryFactory.instance().createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(qx).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(rxy).get());

	@Parameters
	static Stream<Arguments> grdData() {
		return Stream.of(
				Arguments.of(Set.of(), Map.of()), // empty Ruleset -> empty GRD
				Arguments.of(Set.of(Rpx_qx), Map.of()), // single rule Ruleset -> empty GRD
				Arguments.of(Set.of(Rpx_py), Map.of(Rpx_py, Set.of(Rpx_py))), // single recursive rule Ruleset -> GRD with circular dependency
				Arguments.of(Set.of(Rpx_qx, Rqx_rxy), Map.of(Rpx_qx, Set.of(Rqx_rxy))), // 2 rules with a dependency (path shaped) -> GRD with 1 dependencies
				Arguments.of(Set.of(Rpx_py, Rpx_qx, Rqx_rxy), Map.of(Rpx_qx, Set.of(Rqx_rxy), Rpx_py, Set.of(Rpx_py, Rpx_qx))) // 2 previous cases together + 1 linear dependency
				);
	}

	@DisplayName("Test GRD construction")
	@ParameterizedTest(name = "{index}: ({0})")
	@MethodSource("grdData")
	void grdTest(Collection<FORule> rules, Map<FORule, Set<FORule>> expected) {
		GRDImpl grd = new GRDImpl(new RuleBaseImpl(rules));

		for(FORule r : expected.keySet()) {
			Set<FORule> computed_dependencies = grd.getTriggeredRules(r);
			Set<FORule> expected_dependencies = expected.get(r);

			Assert.assertTrue("All computed dependencies are expected", expected_dependencies.containsAll(computed_dependencies));

			Assert.assertTrue("All expected dependencies are computed", computed_dependencies.containsAll(expected_dependencies));
		}
	}

}
