package fr.boreal.grd.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.grd.api.GraphOfFORuleDependencies;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.graal.unifier.QueryUnifier;
import fr.lirmm.boreal.graal.unifier.QueryUnifierAlgorithm;
import fr.lirmm.boreal.util.Rules;

/**
 * GRD implementation using unification
 */
public class GRDImpl implements GraphOfFORuleDependencies {

	private Map<FORule, Set<FORule>> graph;

	/**
	 * Creates a new GRD
	 * Adds all the given rules to the grd and computes the dependencies
	 * @param rules
	 */
	public GRDImpl(RuleBase rb) {
		this.graph = new HashMap<FORule, Set<FORule>>();

		for(FORule r : rb.getRules()) {
			graph.put(r, new HashSet<FORule>());
		}

		this.computeDependencies(rb);

	}

	@Override
	public Set<FORule> getTriggeredRules(FORule src) {
		return this.graph.get(src);
	}

	/**
	 * Computes and stores dependencies
	 * A dependency between r1 and r2 means that the rule r1 can trigger the rule r2
	 */
	private void computeDependencies(RuleBase rb) {
		for(FORule r1 : rb.getRules()) {
			Set<FORule> marked = new HashSet<FORule>();
			for(Atom a1 : r1.getHead().flatten()) {
				Set<FORule> candidates = rb.getRulesByBodyPredicate(a1.getPredicate()).stream()
				.filter(r2 -> !marked.contains(r2))
				.collect(Collectors.toSet());
				
				marked.addAll(candidates);
				
				candidates.forEach(r2 -> this.computeDependency(r1, r2));
			}
		}
	}
	
	/**
	 * Computes and stores dependencies between r1 and r2
	 * A dependency means that the rule r1 can trigger the rule r2
	 */
	private void computeDependency(FORule r1, FORule r2) {
		FORule r1safe = Rules.freshRenaming(r1);
		FORule r2safe = Rules.freshRenaming(r2);
		
		// exist unifier r2.body dans r1.head en ignorant les variables réponses
		FOQuery r2q = FOQueryFactory.instance().createOrGetQuery(r2safe.getBody().getFormula(), Set.of(), r2safe.getBody().getInitialSubstitution()).get();
		
		QueryUnifierAlgorithm unifierAlgo = new QueryUnifierAlgorithm();
		Collection<QueryUnifier> unifiers = unifierAlgo.getMostGeneralSinglePieceUnifiers(r2q, r1safe);
		
		if(!unifiers.isEmpty()) {
			this.graph.get(r1).add(r2);
		}
	}

}
