package fr.boreal.grd.impl;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.boreal.grd.api.GraphOfFORuleDependencies;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.converter.RuleConverter;
import fr.lirmm.graphik.graal.api.core.GraphOfRuleDependencies;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.core.grd.DefaultGraphOfRuleDependencies;

public class OldGraphOfFORuleDependenciesImpl implements GraphOfFORuleDependencies {

	private GraphOfRuleDependencies grd;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS
	// /////////////////////////////////////////////////////////////////////////

	public OldGraphOfFORuleDependenciesImpl(Collection<FORule> rules) {
		Stream<Rule> converted_rules = rules.stream().map(RuleConverter::convert);
		this.grd = new DefaultGraphOfRuleDependencies(converted_rules.iterator());
	}

	@Override
	public Set<FORule> getTriggeredRules(FORule src) {
		return this.grd.getTriggeredRules(RuleConverter.convert(src)).stream()
				.map(RuleConverter::reverse)
				.collect(Collectors.toSet());
	}
}
