/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KÖNIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLÈRE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.boreal.io.dlgp.impl.builtin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.functions.Invoker;
import fr.boreal.model.functions.JavaMethodInvoker;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.ComputedAtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.Rule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.lirmm.graphik.dlgp3.parser.ADlgpItemFactory.OBJECT_TYPE;
import fr.lirmm.graphik.dlgp3.parser.InvokeManager;
import fr.lirmm.graphik.dlgp3.parser.ParserListener;
import fr.lirmm.graphik.util.Prefix;
import fr.lirmm.graphik.util.stream.InMemoryStream;

class DlgpListener implements ParserListener {

	private TermFactory termFactory;
	private PredicateFactory predicateFactory;
	private static final FOQueryFactory foQueryFactory = FOQueryFactory.instance();
	private static final FOFormulaFactory foFormulaFactory = FOFormulaFactory.instance();

	private List<Variable> answerVars;
	private Set<Atom> atomSet = null;
	private Set<Atom> atomSet2 = null;

	private boolean isNegativeMode = false;
	private Set<Set<Atom>> negative_parts = null;
	private Set<Atom> current_negative_part = null;

	private Atom atom;
	private String label;

	private InMemoryStream<Object> set;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	DlgpListener(InMemoryStream<Object> buffer, TermFactory termfactory, PredicateFactory predicatefactory) {
		this.set = buffer;
		this.termFactory = termfactory;
		this.predicateFactory = predicatefactory;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public void declarePrefix(String prefix, String ns) {
		this.set.write(new Prefix(prefix.substring(0, prefix.length() - 1), ns));
	}

	@Override
	public void declareBase(String base) {
		this.set.write(new Directive(Directive.Type.BASE, base));
	}

	@Override
	public void declareTop(String top) {
		this.set.write(new Directive(Directive.Type.TOP, top));
	}

	@Override
	public void declareUNA() {
		this.set.write(new Directive(Directive.Type.UNA, null));
	}

	@Override
	public void directive(String text) {
		this.set.write(new Directive(Directive.Type.COMMENT, text));
	}

	@Override
	public void startsObject(OBJECT_TYPE objectType, String name) {
		this.label = (name == null) ? "" : name;

		this.atomSet = new LinkedHashSet<Atom>();
		this.atomSet2 = null;
		this.negative_parts = new LinkedHashSet<Set<Atom>>();
		this.current_negative_part = null;

		if (OBJECT_TYPE.QUERY.equals(objectType)) {
			this.answerVars = new LinkedList<Variable>();
		}
	}

	private void addAtomToCorrectSet(Atom a) {
		if(this.isNegativeMode) {
			this.current_negative_part.add(a);
		} else {
			this.atomSet.add(atom);
		}
	}

	@Override
	public void createsAtom(Object predicate, Object[] terms) {
		List<Term> list = new LinkedList<Term>();
		for (Object t : terms) {
			list.add(this.createTerm(t));
		}

		Predicate pred = this.createPredicate(predicate.toString(), terms.length);
		this.atom = new AtomImpl(pred, list);

		this.addAtomToCorrectSet(atom);
	}

	@Override
	public void createsEquality(Object term1, Object term2) {
		// TODO
		throw new UnsupportedOperationException("Equality is not implemented yet");
	}

	@Override
	public void answerTermList(Object[] terms) {
		for (Object t : terms) {
			Term term = createTerm(t);
			if (term.isVariable()) {
				this.answerVars.add((Variable) term);
			} else {
				throw new RuntimeException("[Adding in answer variables : The term " + term + " is not a variable");
				// TODO : initial substitution ?
			}
		}
	}

	@Override
	public void endsConjunction(OBJECT_TYPE objectType) {
		switch (objectType) {
		case QUERY:
			this.createQueryConjunction(this.label, this.atomSet, this.negative_parts, this.answerVars);
			break;
		case NEG_CONSTRAINT:
			// TODO
			break;
		case RULE:
			if (this.atomSet2 == null) {
				this.atomSet2 = this.atomSet;
				this.atomSet = new LinkedHashSet<Atom>();
			} else {
				this.createRule(this.label, this.atomSet, this.negative_parts, this.atomSet2);
			}
			break;
		case FACT:
			this.createAtomSet(this.atomSet);
			break;
		default:
			break;
		}
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	private void createAtomSet(Set<Atom> atomset) {
		for (Atom atom : atomset) {
			this.set.write(atom);
		}
	}

	private Term createTerm(Object t) {
		if (t instanceof Term) {
			return (Term) t;
		} else {
			return this.createConstant(t.toString());
		}
	}

	private Predicate createPredicate(String label, int arity) {
		return this.predicateFactory.createOrGetPredicate(label, arity);
	}

	private Constant createConstant(String uri) {
		return this.termFactory.createOrGetConstant(uri);
	}

	private void createQueryConjunction(String label, Set<Atom> atomSet, Set<Set<Atom>> negParts, List<Variable> answerVars) {
		List<FOFormula<Atom>> subqueries = new ArrayList<FOFormula<Atom>>();
		if(atomSet.size() == 1 && negParts.isEmpty()) {
			Optional<AtomicFOFormula<Atom>> formula = foFormulaFactory.createOrGetAtomic(atomSet.iterator().next());
			if(formula.isPresent()) {
				this.set.write(foQueryFactory.createOrGetAtomicQuery(formula.get(), answerVars, new SubstitutionImpl()));
			}
		} else {
			for (Atom a : atomSet) {
				Optional<AtomicFOFormula<Atom>> formula = foFormulaFactory.createOrGetAtomic(a);
				if(formula.isPresent()) {
					subqueries.add(formula.get());
				}
			}
			for (Set<Atom> negPart : negParts) {
				List<FOFormula<Atom>> negsubqueries = new ArrayList<FOFormula<Atom>>();
				for (Atom a : negPart) {
					Optional<AtomicFOFormula<Atom>> formula = foFormulaFactory.createOrGetAtomic(a);
					if(formula.isPresent()) {
						negsubqueries.add(formula.get());
					}
				}
				Optional<FOFormulaConjunction<Atom>> formula = foFormulaFactory.createOrGetConjunction(negsubqueries);
				if(formula.isPresent()) {
					Optional<FOFormulaNegation<Atom>> formulaNeg = foFormulaFactory.createOrGetNegation(formula.get());
					if(formulaNeg.isPresent()) {
						subqueries.add(formulaNeg.get());
					}
				}
			}
			Optional<FOFormulaConjunction<Atom>> formula = foFormulaFactory.createOrGetConjunction(subqueries);
			if(formula.isPresent()) {
				FOQuery query = foQueryFactory.createOrGetConjunctionQuery(formula.get(), answerVars, new SubstitutionImpl());
				this.set.write(query);
			}
		}
	}

	private void createRule(String label, Set<Atom> body, Set<Set<Atom>> negParts, Set<Atom> head) {

		Collection<FOFormula<Atom>> body_formulas = new ArrayList<FOFormula<Atom>>();
		FOQuery body_query = null;
		for (Atom a : body) {
			Optional<AtomicFOFormula<Atom>> formula = foFormulaFactory.createOrGetAtomic(a);
			if(formula.isPresent()) {
				body_formulas.add(formula.get());
			}
		}
		for (Set<Atom> negPart : negParts) {
			List<FOFormula<Atom>> negsubqueries = new ArrayList<FOFormula<Atom>>();
			for (Atom a : negPart) {
				Optional<AtomicFOFormula<Atom>> formula = foFormulaFactory.createOrGetAtomic(a);
				if(formula.isPresent()) {
					negsubqueries.add(formula.get());
				}
			}
			Optional<FOFormulaConjunction<Atom>> formula = foFormulaFactory.createOrGetConjunction(negsubqueries);
			if(formula.isPresent()) {
				Optional<FOFormulaNegation<Atom>> formulaNeg = foFormulaFactory.createOrGetNegation(formula.get());
				if(formulaNeg.isPresent()) {
					body_formulas.add(formulaNeg.get());
				}
			}
		}
		Optional<FOFormulaConjunction<Atom>> body_formula = foFormulaFactory.createOrGetConjunction(body_formulas);
		if(body_formula.isPresent()) {
			FOQuery query = foQueryFactory.createOrGetConjunctionQuery(body_formula.get(), answerVars, new SubstitutionImpl());
			body_query = query;
		}

		Collection<FOFormula<Atom>> head_subformulas = new ArrayList<FOFormula<Atom>>();
		FOFormula<Atom> head_formula = null;
		for (Atom a : head) {
			Optional<AtomicFOFormula<Atom>> formula = foFormulaFactory.createOrGetAtomic(a);
			if(formula.isPresent()) {
				head_subformulas.add(formula.get());
			}
		}
		Optional<FOFormulaConjunction<Atom>> head_formula_opt = foFormulaFactory.createOrGetConjunction(head_subformulas);
		if(head_formula_opt.isPresent()) {
			head_formula = head_formula_opt.get();
		}

		Rule rule = new FORuleImpl(body_query, head_formula);
		this.set.write(rule);
	}

	@Override
	public void declareComputed(String prefix, String ns) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createsComputedAtom(Object predicate, Object[] terms, Object invoker) {
		List<Term> list = new LinkedList<Term>();
		for (Object t : terms) {
			list.add(this.createTerm(t));
		}

		String fname = predicate.toString().split("#")[1];
		Predicate pred = this.createPredicate(fname, terms.length);

		String class_name = ((Class<?>)((InvokeManager)invoker).getInvokerObject(predicate.toString().split("#")[0] + "#")).getName();

		try {
			Invoker graal_invoker = new JavaMethodInvoker(
					this.termFactory,
					InvokeManager.classToPath.get(class_name),
					class_name,
					fname);

			this.atom = new ComputedAtomImpl(graal_invoker, pred, list);
			this.addAtomToCorrectSet(atom);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void createsSpecialAtom(Object leftTerm, String predicate, Object rightTerm, Object invokerObject) {
		// 2 + 3

	}

	@Override
	public void beginNegation() {
		this.current_negative_part = new LinkedHashSet<Atom>();
		this.isNegativeMode = true;
	}

	@Override
	public void endNegation() {
		this.negative_parts.add(this.current_negative_part);
		this.current_negative_part = null;
		this.isNegativeMode = false;
	}

}