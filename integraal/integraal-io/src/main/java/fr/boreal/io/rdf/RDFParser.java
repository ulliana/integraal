package fr.boreal.io.rdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.eclipse.rdf4j.rio.ParserConfig;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;

import fr.boreal.io.api.Parser;
import fr.lirmm.graphik.graal.api.io.ParseError;
import fr.lirmm.graphik.util.stream.ArrayBlockingStream;

/**
 * This is an import from Graal 1.3
 */
public class RDFParser implements Parser<Object> {

	private ArrayBlockingStream<Object> buffer = new ArrayBlockingStream<Object>(
			512);

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTOR
	// /////////////////////////////////////////////////////////////////////////

	public RDFParser(File file, RDFFormat format) throws FileNotFoundException {
		this(new FileReader(file), format);
	}

	public RDFParser(File file, RDFFormat format, RDFTranslationMode mode) throws FileNotFoundException {
		this(new FileReader(file), format, null, mode);
	}

	public RDFParser(Reader reader, RDFFormat format) {
		this(reader, format, null, RDFTranslationMode.NaturalFull);
	}

	public RDFParser(Reader reader, RDFFormat format, ParserConfig parserConfig, RDFTranslationMode mode) {
		new Thread(new Producer(reader, buffer, format, parserConfig, mode)).start();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public boolean hasNext() {
		return buffer.hasNext();
	}

	@Override
	public Object next() {
		return buffer.next();
	}

	@Override
	public void close() {
		this.buffer.close();
	}

	//
	// Private class Producer
	//

	class Producer implements Runnable {

		private Reader reader;
		private ArrayBlockingStream<Object> buffer;
		private RDFFormat format;
		private ParserConfig                config;
		private RDFTranslationMode mode;

		Producer(Reader reader, ArrayBlockingStream<Object> buffer, RDFFormat format, ParserConfig config, RDFTranslationMode mode) {
			this.reader = reader;
			this.buffer = buffer;
			this.format = format;
			this.config = config;
			this.mode = mode;
		}

		@Override
		public void run() {

			org.eclipse.rdf4j.rio.RDFParser rdfParser = Rio.createParser(format);
			if (this.config != null) {
				rdfParser.setParserConfig(config);
			}
			RDFTranslator translator = null;
			switch (this.mode) {
			case NaturalFull : translator = new NaturalFullRDFTranslator(); break;
			case Natural : translator = new NaturalRDFTranslator(); break;
			case Raw : translator = new RawRDFTranslator(); break;
			};
			
			rdfParser.setRDFHandler(new RDFListener(buffer, translator));
			try {
				rdfParser.parse(this.reader, "");
			} catch (RDFParseException e) {
				throw new ParseError("An error occured while parsing", e);
			} catch (RDFHandlerException e) {
				throw new ParseError("An error occured while parsing", e);
			} catch (IOException e) {
				throw new ParseError("An error occured while parsing", e);
			}
			buffer.close();

			try {
				this.reader.close();
			} catch (IOException e) {
			}

		}
	}

}
