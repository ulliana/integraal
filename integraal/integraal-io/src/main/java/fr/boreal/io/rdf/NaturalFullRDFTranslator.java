package fr.boreal.io.rdf;

import org.eclipse.rdf4j.model.Statement;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;

/**
 * Convert the given RDF statement into an atom
 * This method takes into account the rdf:type predicate to instead create a unary predicate for class membership.
 * @see {@link RDFTranslationMode#NaturalFull}
 * @param st the RDF statement to convert
 * @return the atom corresponding to the given statement
 */
public class NaturalFullRDFTranslator implements RDFTranslator {
	
	private final static String rdfTypePredicate = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

	@Override
	public Atom statementToAtom(Statement st) {
		Atom a;
		if(st.getPredicate().stringValue().equals(rdfTypePredicate)) {
			Predicate className = SameObjectPredicateFactory.instance().createOrGetPredicate(st.getObject().stringValue(), 1);
			Term subject = SameObjectTermFactory.instance().createOrGetConstant(st.getSubject().stringValue());
			a = new AtomImpl(className, subject);
		} else {
			Predicate predicate = SameObjectPredicateFactory.instance().createOrGetPredicate(st.getPredicate().stringValue(), 2);
			Term subject = SameObjectTermFactory.instance().createOrGetConstant(st.getSubject().stringValue());
			Term object = SameObjectTermFactory.instance().createOrGetLiteral(st.getObject().stringValue());
			a = new AtomImpl(predicate, subject, object);
		}
		return a;
	}

}
