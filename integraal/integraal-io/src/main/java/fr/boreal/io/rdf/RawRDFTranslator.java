package fr.boreal.io.rdf;

import org.eclipse.rdf4j.model.Statement;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;

/**
 * Convert the given RDF statement into an atom
 * @see {@link RDFTranslationMode#Raw}
 * @param st the RDF statement to convert
 * @return the atom corresponding to the given statement
 */
public class RawRDFTranslator implements RDFTranslator {

	@Override
	public Atom statementToAtom(Statement st) {
		Predicate triple = SameObjectPredicateFactory.instance().createOrGetPredicate("triple", 3);
		Term subject = SameObjectTermFactory.instance().createOrGetConstant(st.getSubject().stringValue());
		Term predicate = SameObjectTermFactory.instance().createOrGetConstant(st.getPredicate().stringValue());
		Term object = SameObjectTermFactory.instance().createOrGetLiteral(st.getObject().stringValue());
		Atom a = new AtomImpl(triple, subject, predicate, object);
		return a;
	}

}
