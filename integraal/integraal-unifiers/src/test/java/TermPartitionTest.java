import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.FOQueryConjunction;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.api.Rule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.lirmm.boreal.graal.unifier.partition.TermPartition;

@RunWith(Parameterized.class)
class TermPartitionTest {

	static Predicate p = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 1);
	static Predicate q = SameObjectPredicateFactory.instance().createOrGetPredicate("q", 1);
	static Predicate r = SameObjectPredicateFactory.instance().createOrGetPredicate("r", 2);

	static Constant a = SameObjectTermFactory.instance().createOrGetConstant("a");
	static Constant b = SameObjectTermFactory.instance().createOrGetConstant("b");
	static Constant c = SameObjectTermFactory.instance().createOrGetConstant("c");
	static Constant d = SameObjectTermFactory.instance().createOrGetConstant("d");
	static Constant e = SameObjectTermFactory.instance().createOrGetConstant("e");

	static Variable u = SameObjectTermFactory.instance().createOrGetVariable("u");
	static Variable v = SameObjectTermFactory.instance().createOrGetVariable("v");
	static Variable w = SameObjectTermFactory.instance().createOrGetVariable("w");
	static Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	static Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	static Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");

	static Atom rxy = new AtomImpl(r, x, y);
	static Atom ruv = new AtomImpl(r, u, v);
	static Atom px = new AtomImpl(p, x);
	static Atom qy = new AtomImpl(q, y);
	static Atom ruu = new AtomImpl(r, u, u);

	// p(x) -> r(x,y), q(y), r(u,u)
	static FORule R = new FORuleImpl(
			FOQueryFactory.instance().createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(px).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(rxy, ruu, qy).get());


	static FOQuery Q = FOQueryFactory.instance().createOrGetConjunctionQuery(
			FOFormulaFactory.instance().createOrGetConjunction(px).get(), 
			List.of(x), 
			new SubstitutionImpl());

	static FOQuery Q2 = FOQueryFactory.instance().createOrGetConjunctionQuery(
			FOFormulaFactory.instance().createOrGetConjunction(px).get(), 
			List.of(), 
			new SubstitutionImpl());
	
	static FOQuery Q3 = FOQueryFactory.instance().createOrGetConjunctionQuery(
			FOFormulaFactory.instance().createOrGetConjunction(ruu).get(), 
			List.of(u), 
			new SubstitutionImpl());

	@Parameters
	static Stream<Arguments> isValidData() {
		return Stream.of(
				Arguments.of(List.of(Set.of(a, b)), R, false),
				Arguments.of(List.of(Set.of(x, y), Set.of(u)), R, false),
				Arguments.of(List.of(Set.of(y, u)), R, false),
				Arguments.of(List.of(Set.of(a, u), Set.of(x), Set.of(y)), R, false),
				Arguments.of(List.of(Set.of(x, y, z), Set.of(u, v, a), Set.of(w)), R, false),
				Arguments.of(List.of(Set.of(x), Set.of(y), Set.of(u)), R, true),
				Arguments.of(List.of(Set.of(x, c), Set.of(y), Set.of(u)), R, true),
				Arguments.of(List.of(Set.of(u, x)), R, false),
				Arguments.of(List.of(), R, true),
				Arguments.of(List.of(Set.of(w)), R, true),
				Arguments.of(List.of(Set.of(x, c), Set.of(y), Set.of(x, a)), R, false)
				);
	}

	@Parameters
	static Stream<Arguments> separatingStickyVariablesData() {
		return Stream.of(
				Arguments.of(List.of(), Set.of(x, y, z), R, Set.of()),
				Arguments.of(List.of(Set.of(x, z, a), Set.of(u), Set.of(y, w)),
						Set.of(), R,
						Set.of()),
				Arguments.of(List.of(Set.of(x, z, a), Set.of(u), Set.of(y, w)),
						Set.of(z, y), R,
						Set.of(y)),
				Arguments.of(List.of(Set.of(x, z, a), Set.of(u), Set.of(y, w)),
						Set.of(z, y, w), R,
						Set.of(y, w))				
				);
	}

	@Parameters
	static Stream<Arguments> associatedSubstitutionData() {
		Substitution s1 = new SubstitutionImpl();
		s1.add(u, a);
		s1.add(v, a);
		s1.add(w, w);

		Substitution s2 = new SubstitutionImpl(s1);
		s2.add(x, x);
		s2.add(y, x);
		s2.add(z, x);
		return Stream.of(
				Arguments.of(List.of(), null, Optional.of(new SubstitutionImpl())),
				Arguments.of(List.of(Set.of(a, b)), null, Optional.empty()),
				Arguments.of(List.of(Set.of(x, y, z, a), Set.of(u, v, b, c), Set.of(w)), null, Optional.empty()),
				Arguments.of(List.of(Set.of(x, y, z), Set.of(u, v, a), Set.of(w)), null, Optional.of(s1)),
				Arguments.of(List.of(Set.of(x, y, z), Set.of(u, v, a), Set.of(w)), Q, Optional.of(s2)),
				Arguments.of(List.of(Set.of(x, y, z), Set.of(u, v, a), Set.of(w)), Q2, Optional.of(s1)),
				Arguments.of(List.of(Set.of(x, y, z), Set.of(u, v, a), Set.of(w)), Q3, Optional.of(s1))
				);
	}

	@DisplayName("Test isValid method")
	@ParameterizedTest(name = "{index}: ({0}, {1}, {2})")
	@MethodSource("isValidData")
	public void isValidTest(List<Set<Term>> partition, Rule rule, boolean expected_value) {
		TermPartition tp = new TermPartition(partition);
		Assert.assertEquals(expected_value, tp.isValid(rule));
		Assert.assertEquals(expected_value, tp.isValid(rule)); // use cache
	}

	@DisplayName("Test getSeparatingStickyVariables method")
	@ParameterizedTest(name = "{index}: ({0}, {1}, {2}, {3})")
	@MethodSource("separatingStickyVariablesData")
	public void getSeparatingStickyVariablesTest(List<Set<Term>> partition, Set<Variable> sep, Rule rule, Set<Variable> expected_result) {
		TermPartition tp = new TermPartition(partition);
		Assert.assertEquals(expected_result, tp.getSeparatingStickyVariables(sep, rule));
	}

	@DisplayName("Test getAssociatedSubstitution method")
	@ParameterizedTest(name = "{index}: ({0}, {1}, {2})")
	@MethodSource("associatedSubstitutionData")
	public void getAssociatedSubstitutionTest(List<Set<Term>> partition, FOQueryConjunction context, Optional<Substitution> expected_result_opt) {
		TermPartition tp = new TermPartition(partition);

		Optional<Substitution> result_opt = tp.getAssociatedSubstitution(context);

		// First possibility : No substitution found
		if(expected_result_opt.isEmpty()) {
			Assert.assertTrue(result_opt.isEmpty());
		} else {
			Substitution result = result_opt.get();
			Substitution expected_result = expected_result_opt.get();

			// For each Term in each class
			// We check if the image is correct
			for(Set<Term> classs: tp.getClasses()) {
				Term expected_image = null;
				for(Term t : classs) {
					Term gotten_image = result.createImageOf(t);
					// Second possibility : The expected image is known
					// This is when the representative is fixed : either a constant or an answer variable from the context
					if(expected_result.keys().contains(t)) {
						expected_image = expected_result.createImageOf(t);
						Assert.assertEquals(expected_image, gotten_image);
					}
					// Last possibility : The representative is a random variable from the class
					// We check that all the variables from the class have the same image associated
					else if(expected_image == null) {
						expected_image = gotten_image;
					} else {
						Assert.assertEquals(expected_image, gotten_image);
					}
				}
			}
		}




	}
}

