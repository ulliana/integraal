import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.lirmm.boreal.graal.unifier.QueryUnifier;
import fr.lirmm.boreal.graal.unifier.QueryUnifierAlgorithm;
import fr.lirmm.boreal.graal.unifier.partition.TermPartition;

class UnifierTest {

	static QueryUnifierAlgorithm algo = new QueryUnifierAlgorithm();

	static Predicate p = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 2);
	static Predicate q = SameObjectPredicateFactory.instance().createOrGetPredicate("q", 2);
	static Predicate r = SameObjectPredicateFactory.instance().createOrGetPredicate("r", 2);
	static Predicate s = SameObjectPredicateFactory.instance().createOrGetPredicate("s", 1);
	static Predicate t = SameObjectPredicateFactory.instance().createOrGetPredicate("t", 2);

	static Variable u = SameObjectTermFactory.instance().createOrGetVariable("u");
	static Variable v = SameObjectTermFactory.instance().createOrGetVariable("v");
	static Variable w = SameObjectTermFactory.instance().createOrGetVariable("w");
	static Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	static Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	static Variable z = SameObjectTermFactory.instance().createOrGetVariable("z");

	static Atom qxy = new AtomImpl(q, x, y);

	static Atom pxy = new AtomImpl(p, x, y);
	static Atom pxz = new AtomImpl(p, x, z);
	static Atom puv = new AtomImpl(p, u, v);
	static Atom pwv = new AtomImpl(p, w, v);

	static Atom rwu = new AtomImpl(r, w, u);
	
	static Atom sx = new AtomImpl(s, x);
	static Atom qxz = new AtomImpl(q, x, z);
	static Atom qvu = new AtomImpl(q, v, u);

	// q(x, y) -> p(x, y)
	static FORule Rqxy_pxy = new FORuleImpl(
			FOQueryFactory.instance()
			.createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(qxy).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(pxy).get());

	// q(x, y) -> p(x, z)
	static FORule Rqxy_pxz = new FORuleImpl(
			FOQueryFactory.instance()
			.createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(qxy).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(pxz).get());

	// ?() :- p(u, v), p(w, v), r(w, u)
	static FOQuery Qpuv_pwv_rwv = FOQueryFactory.instance().createOrGetConjunctionQuery(
			FOFormulaFactory.instance().createOrGetConjunction(puv, pwv, rwu).get(),
			List.of(), 
			new SubstitutionImpl());
	
	// s(X) -> q(X, Y)
	static FORule Rsx_pxz = new FORuleImpl(
			FOQueryFactory.instance()
			.createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(sx).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(qxz).get());
	
	// ?(u) :- q(v, u)
	static FOQuery Qu_qvu = FOQueryFactory.instance().createOrGetConjunctionQuery(
			FOFormulaFactory.instance().createOrGetConjunction(qvu).get(),
			List.of(u), 
			new SubstitutionImpl());

	@Parameters
	static Stream<Arguments> getMostGeneralSinglePieceUnifiersData() {
		return Stream.of(
				Arguments.of(Qpuv_pwv_rwv, Rqxy_pxy, Set.of(
						Set.of(Set.of(u, x), Set.of(v, y)),
						Set.of(Set.of(w, x), Set.of(v, y)))),
				Arguments.of(Qpuv_pwv_rwv, Rqxy_pxz, Set.of(
						Set.of(Set.of(u, w, x), Set.of(v, z)))),
				Arguments.of(Qu_qvu, Rsx_pxz, Set.of())
				);
	}

//	@DisplayName("Test MostGeneralSinglePieceUnifiers computing")
//	@ParameterizedTest(name = "{index}: unif({0}, {1}) = {2}")
//	@MethodSource("getMostGeneralSinglePieceUnifiersData")
//	void getMostGeneralSinglePieceUnifiersTest(FOQuery query, FORule rule, Collection<Collection<Set<Term>>> expected_partitions) {
//		Collection<QueryUnifier> unifiers = algo.getMostGeneralSinglePieceUnifiers(query, rule);
//
//		Collection<TermPartition> computeds = unifiers.stream().map(u -> u.getPartition()).collect(Collectors.toSet());
//		Collection<TermPartition> expecteds = expected_partitions.stream().map(p -> new TermPartition(p)).collect(Collectors.toSet());
//		
//		Assert.assertTrue(expecteds.containsAll(computeds));
//		Assert.assertTrue(computeds.containsAll(expecteds));
//
//
//	}

}
