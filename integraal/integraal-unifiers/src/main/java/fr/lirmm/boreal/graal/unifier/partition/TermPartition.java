package fr.lirmm.boreal.graal.unifier.partition;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.FOQueryConjunction;
import fr.boreal.model.rule.api.Rule;
import fr.lirmm.boreal.util.Partition;

/**
 * @author Guillaume Pérution-Kihli
 * @author Florent Tornil
 * 
 * Partition of terms
 * Adds method specific for terms in addition to partition ones
 */
public class TermPartition extends Partition<Term> {

	/**
	 * Cache for valid property
	 */
	Map<Rule, Boolean> valid = new HashMap<>();
	
	/**
	 * Cache for associated substitution property
	 */
	Map<FOQueryConjunction, Substitution> associatedSubstitution = new HashMap<>();

	/**
	 * Comparator used when choosing a representative of a class
	 * Chooses constants first
	 */
	private static Comparator<Term> getComparator() {
		return((t1, t2) -> {
			if(t1.isConstant() && t2.isConstant()) {
				return 0;
			} else if(t1.isConstant()) {
				return -1;
			} else if(t2.isConstant()) {
				return 1;
			}
			return 0;
		});
	}

	/**
	 * Creates a new empty TermPartition
	 */
	public TermPartition () {
		super(getComparator());
	}

	/**
	 * Creates a new TermPartition with the given elements
	 */
	public TermPartition (Set<Term> initialElements) {
		super(initialElements, getComparator());
	}

	/**
	 * Creates a new TermPartition with the given classes
	 */
	public TermPartition (Collection<Set<Term>> partition) {
		super(partition, getComparator());
	}

	/**
	 * Creates a new TermPartition, copying the given one
	 */
	public TermPartition (Partition<Term> toCopy) {
		super(toCopy, getComparator());
	}

	////////////////////
	// PUBLIC METHODS //
	////////////////////

	/**
	 * @return false iff a class of the receiving partition
	 * contains two constants,
	 * or contains two existential variable of R,
	 * or contains a constant and an existential variable of R,
	 * or contains an existential variable of R and a frontier variable of R
	 */
	public synchronized boolean isValid(Rule rule) {
		if (!valid.containsKey(rule)) {
			boolean result = this.isValid(rule, null);
			valid.put(rule, result);
			return result;
		} else {
			return valid.get(rule);
		}
	}

	/**
	 * Takes into account answer variables of a context query, preventing them to be associated with an existential variable of the rule
	 * @param context the query in which to test the validity
	 * @return false iff a class of the receiving partition
	 * contains two constants,
	 * or contains two existential variable of R,
	 * or contains a constant and an existential variable of R,
	 * or contains an existential variable of R and a frontier variable of R,
	 * or an existential and an answer variable of the context
	 */
	public boolean isValid(Rule rule, FOQuery context) {
		for (Collection<Term> cl : this.getClasses()) {
			boolean hasCst = false, hasExistFromRule= false, hasFr = false, hasAnsVar = false;
			for (Term term : cl) {
				if (term.isConstant()) {
					if (hasCst || hasExistFromRule) {
						valid.put(rule, false);
						return false;
					}
					hasCst = true;
				}
				else if (rule.getExistentials().contains(term)) {
					if (hasExistFromRule || hasCst || hasFr || hasAnsVar) {
						return false;
					}
					hasExistFromRule = true;
				}
				else if (rule.getFrontier().contains(term)) {
					if (hasExistFromRule) {
						return false;
					}
					hasFr = true;
				} else if(context != null && context.getAnswerVariables().contains(term)) {
					if (hasExistFromRule) {
						return false;
					}
					hasAnsVar = true;
				}
			}
		}
		return true;
	}

	/** 
	 * @return the subset of sep containing variables that are in the same class than
	 * an existential variable of rule.
	 */
	public Set<Variable> getSeparatingStickyVariables(Set<Variable> sep, Rule rule) {
		Set<Variable> separatingStickyVariables = new HashSet<Variable>();
		for (Collection<Term> classs : this.getClasses()) {
			if (classs.stream().anyMatch(rule.getExistentials()::contains)) {
				classs.stream().filter(sep::contains).map(t -> (Variable)t).forEach(separatingStickyVariables::add);
			}
		}
		return separatingStickyVariables;
	}

	/**
	 * Compute the substitution associated with this partition.
	 * This method computes a substitution by choosing one representative term by
	 * class.
	 * Choosing first constant then answer variable (in context)
	 * Return an empty optional if the partition contain two constants in the same class
	 */
	public synchronized Optional<Substitution> getAssociatedSubstitution(FOQueryConjunction context) {
		if (!associatedSubstitution.containsKey(context)) {
			Substitution subs = new SubstitutionImpl();

			// Choose variables from the answer variables of the query first
			Map<Term, Term> alternativeRepresentative = new HashMap<>();
			if(context != null) {
				for(Variable v : context.getAnswerVariables()) {
					Term rep = this.getRepresentative(v);
					if(!rep.isConstant()) {
						alternativeRepresentative.put(rep, v);
					}
				}
			}

			// Choose constant representative next
			for (Collection<Term> classs : getClasses()) {
				for(Term term : classs) {
					Term representative = this.getRepresentative(term);
					representative = alternativeRepresentative.getOrDefault(representative, representative);
					if(term.isVariable()) {
						subs.add((Variable)term, representative);
					} else if(!term.equals(representative)) {
						return Optional.empty();
					}
				}
			}
			associatedSubstitution.put(context, subs);
			return Optional.of(subs);
		} else {
			return Optional.ofNullable(associatedSubstitution.get(context));
		}
	}

	@Override
	protected void eraseMemoizedValues () {
		super.eraseMemoizedValues();
		valid = new HashMap<>();
		associatedSubstitution = new HashMap<>();
	}
}
