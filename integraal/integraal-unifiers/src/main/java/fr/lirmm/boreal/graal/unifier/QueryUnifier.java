/**
 * 
 */
package fr.lirmm.boreal.graal.unifier;

import java.util.Set;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQueryConjunction;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.graal.unifier.partition.TermPartition;

/**
 * @author Guillaume Pérution-Kihli
 * @author Florent Tornil
 * 
 * An unifier of a query Q with a rule R is a triple u(Q', H', P)
 * where Q' is a subset of Q
 * H' is a subset of  head(R)
 * P is a partition of the terms of Q' and C'
 * 
 * It describes how to unify a piece of a query with a part of an head
 * FORule in order to rewrite the fact according to the FORule
 */
public interface QueryUnifier {

	/**
	 * @return the FORules where the unifier apply
	 */
	public Set<FORule> getInitialFORules();
	
	/**
	 * @return the aggregated FORule where the unifier apply
	 */
	public FORule getAggregatedFORule();

	/**
	 * @return the piece of the query that is unified by this unifier
	 */
	public FOFormula<Atom> getPiece();

	/**
	 * @return the query unified by this unifier
	 */
	public FOQueryConjunction getQuery();

	/**
	 * @return the partition that unify the piece and a part of the head FORule
	 */
	public TermPartition getPartition();

	/**
	 * @return the image of a given fact by the substituion associated to this unifier
	 */
	public FOFormula<Atom> getImageOf(FOFormula<Atom> f);
	
	/**
	 * @return the substitution associated to this unifier
	 */
	public Substitution getAssociatedSubstitution();

	/**
	 * Creates a new unifier corresponding to the aggregation of this unifier and the given one
	 * @param u unifier to aggregate
	 * @return the aggregation of the given unifier and the receiving unifier
	 */
	public QueryUnifier aggregate(QueryUnifier u);

	/**
	 * If the pieces of the two unifiers have atom in common the unifiers are not compatible
	 * Otherwise check if the two partition of the unifiers are possible to join
	 * @param u unifier to check compatibility with
	 * @return true iff this unifier is compatible with the given unifier
	 */
	public boolean isCompatible(QueryUnifier u);


}
