package fr.lirmm.boreal.graal.unifier.partition;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;

/**
 * Creates TermPartition using atoms with the same predicate
 */
public class TermPartitionFactory {


	private static final TermPartitionFactory INSTANCE = new TermPartitionFactory();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * @return the default instance of this factory
	 */
	public static TermPartitionFactory instance() {
		return INSTANCE;
	}
	
	/**
	 * By position partition of A, a set of atoms sharing the
	 * predicate p, that is the finest partition on the terms 
	 * of A such that two terms of A appearing at the same 
	 * position i (0 < i < arity(p)) are in the same class
	 * of the partition
	 * @param atoms : all the atoms share the same predicate
	 */
	public TermPartition getByPositionPartition (Collection<Atom> A) {
		if (A.isEmpty()) {
			return new TermPartition();
		}
		
		TermPartition tp = new TermPartition(A.stream()
				.flatMap(a -> Stream.of(a.getTerms()))
				.collect(Collectors.toSet()));
		Predicate p = A.iterator().next().getPredicate();
		
		Map<Integer,Term> posToTerm = new HashMap<>();
		for (Atom a : A) {
			for (int i = 0; i < p.getArity(); ++i) {
				if (posToTerm.containsKey(i)) {
					tp.union(a.getTerm(i), posToTerm.get(i));
				} else {
					posToTerm.put(i, a.getTerm(i));
				}
			}
		}
		return tp;
	}

}
