package fr.lirmm.boreal.graal.unifier;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.graal.unifier.partition.TermPartition;
import fr.lirmm.boreal.graal.unifier.partition.TermPartitionFactory;
import fr.lirmm.boreal.util.Rules;

/**
 * Computes the most general single piece unifiers
 * This uses Mélanie König's thesis algorithms
 */
public class QueryUnifierAlgorithm {

	/**
	 * Algorithm 2 in Mélanie König's thesis
	 * Computes the single piece most general unifiers of the given query with the given rule
	 * The rule is renamed with fresh variables before computing the unifiers such that each unifier is on a different set of variables
	 * @param q the query
	 * @param r the rule
	 * @return all the most general single piece unifiers of the query with the rule
	 */
	public Collection<QueryUnifier> getMostGeneralSinglePieceUnifiers (FOQuery q, FORule r) {
		Set<QueryUnifier> u = new HashSet<>();
		Set<QueryUnifier> apu = computeAtomicPreUnifiers(q, r);
		while (!apu.isEmpty()) {
			QueryUnifier qu = apu.iterator().next();
			apu.remove(qu);
			u.addAll(extend(q, qu, apu));
		}

		return u;
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * Algorithm 3 in Mélanie König's thesis
	 */
	private Set<QueryUnifier> computeAtomicPreUnifiers (FOQuery q, FORule r) {
		Set<QueryUnifier> atomicQueryUnifiers = new HashSet<>();

		Collection<Atom> queryAtoms = q.getFormula().flatten();
		for (Atom b : queryAtoms) {
			FORule safe_rule = Rules.freshRenaming(r);
			for (Atom a : safe_rule.getHead().flatten()) {
				if (a.getPredicate().equals(b.getPredicate())) {
					TermPartition tp = TermPartitionFactory.instance().getByPositionPartition(List.of(a, b));
					if (tp.isValid(safe_rule, q)) {
						atomicQueryUnifiers.add(
								new QueryUnifierImpl(
										FOQueryFactory.instance().createOrGetConjunctionQuery(
												FOFormulaFactory.instance().createOrGetConjunction(b).get(), q.getAnswerVariables(), null), 
										FOFormulaFactory.instance().createOrGetAtomic(b).get(), 
										tp, 
										safe_rule, 
										Set.of(safe_rule)));
					}
				}
			}
		}

		return atomicQueryUnifiers;
	}

	/**
	 * Algorithm 4 in Mélanie König's thesis
	 */
	private Set<QueryUnifier> extend (FOQuery rewritedQuery, QueryUnifier queryUnifier, Set<QueryUnifier> atomicQueryUnifiers) {
		Set<Atom> difference = rewritedQuery.getFormula().flatten().stream().collect(Collectors.toSet());
		difference.removeAll(queryUnifier.getQuery().getFormula().flatten());
		
		Set<Variable> differenceVariables = difference.stream()
				.flatMap(a -> a.getVariables().stream())
				.collect(Collectors.toSet());
		
		Set<Variable> separatingVariables = rewritedQuery.getFormula().flatten().stream()
				.flatMap(a -> a.getVariables().stream())
				.filter(v -> differenceVariables.contains(v))
				.collect(Collectors.toSet());

		Set<Variable> separatingStickyVariables = queryUnifier.getPartition().getSeparatingStickyVariables(
				separatingVariables, 
				queryUnifier.getAggregatedFORule());

		if (separatingStickyVariables.isEmpty()) {
			return Set.of(queryUnifier);
		} else {
			Set<QueryUnifier> res = new HashSet<>();
			Set<Atom> Qext = difference.stream()
					.filter(a -> !queryUnifier.getPartition().getSeparatingStickyVariables(a.getVariables(), queryUnifier.getAggregatedFORule()).isEmpty())
					.collect(Collectors.toSet());
			for (QueryUnifier uExt: extend_aux(queryUnifier, Qext, atomicQueryUnifiers)) {
				TermPartition tp = new TermPartition(queryUnifier.getPartition());
				tp.join(uExt.getPartition());
				if (tp.isValid(queryUnifier.getAggregatedFORule())) {
					res.addAll(extend(rewritedQuery, queryUnifierUnion(queryUnifier, uExt), atomicQueryUnifiers));
				}
			}

			return res;
		}
	}

	/**
	 * Algorithm 5 in Mélanie König's thesis
	 */
	private Set<QueryUnifier> extend_aux (
			QueryUnifier queryUnifier, 
			Set<Atom> Qext, 
			Set<QueryUnifier> atomicQueryUnifiers) {
		if (Qext.isEmpty()) {
			return Set.of(queryUnifier);
		} else {
			Set<QueryUnifier> ext = new HashSet<>();
			Atom a = Qext.iterator().next();

			for (QueryUnifier pu : atomicQueryUnifiers) {
				if (pu.getQuery().getFormula().flatten().iterator().next().equals(a)) {
					TermPartition tp = new TermPartition(queryUnifier.getPartition());
					tp.join(pu.getPartition());
					if (tp.isValid(pu.getAggregatedFORule())) {
						var newQext = new HashSet<>(Qext);
						newQext.remove(a);
						ext.addAll(extend_aux(queryUnifierUnion(queryUnifier, pu), newQext, atomicQueryUnifiers));
					}
				}
			}

			return ext;
		}
	}

	/**
	 * 
	 * @param qu1
	 * @param qu2
	 * @return
	 */
	private QueryUnifier queryUnifierUnion(QueryUnifier qu1, QueryUnifier qu2) {
		Set<Atom> q = new HashSet<>(qu1.getQuery().getFormula().flatten());
		q.addAll(qu2.getQuery().getFormula().flatten());
		Set<Atom> c = new HashSet<>(qu1.getPiece().flatten());
		c.addAll(qu2.getPiece().flatten());
		TermPartition partition = new TermPartition(qu1.getPartition());
		partition.join(qu2.getPartition());

		return new QueryUnifierImpl(
				FOQueryFactory.instance().createOrGetConjunctionQuery(
						FOFormulaFactory.instance().createOrGetConjunction(q.toArray(new Atom[q.size()])).get(), qu1.getQuery().getAnswerVariables(), null), 
				FOFormulaFactory.instance().createOrGetConjunction(c.toArray(new Atom[c.size()])).get(), 
				partition, 
				qu1.getAggregatedFORule(), 
				qu1.getInitialFORules());

	}
}
