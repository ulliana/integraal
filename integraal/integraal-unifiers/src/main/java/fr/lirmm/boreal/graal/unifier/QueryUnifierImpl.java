/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KÖNIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLÈRE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the FORules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.lirmm.boreal.graal.unifier;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQueryConjunction;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.lirmm.boreal.graal.unifier.partition.TermPartition;

/**
 * Basic implementation for the query unifier
 */
public class QueryUnifierImpl implements QueryUnifier {

	/**
	 * Aggregation of all the FORule that are unified with the query
	 */
	private FORule aggregatedFORule;

	/**
	 * Set of all rules unified with the query
	 */
	private Set<FORule> initialFORules;
	
	/**
	 * Query that is unified with the FORule head
	 */
	private FOQueryConjunction query;

	/**
	 * Part of the query that is unified
	 */
	private FOFormula<Atom> piece;

	/**
	 * Partition that unify the piece of the query and a part of the head FORule
	 */
	private TermPartition partition;

	/**
	 * Substitution associated to this unifier
	 * Uses the substitution associated to the partition using variable names from the query when possible
	 * This variable is used as a cache
	 */
	private Substitution associatedSubstitution;

	/**
	 * Creates a new QueryUnifier using the given parameters
	 * @param query
	 * @param piece
	 * @param partition
	 * @param aggregatedFORule
	 * @param initialFORules
	 */
	public QueryUnifierImpl(
			FOQueryConjunction query, 
			FOFormula<Atom> piece, 
			TermPartition partition, 
			FORule aggregatedFORule,
			Set<FORule> initialFORules) {
		this.aggregatedFORule = aggregatedFORule;
		this.query = query;
		this.piece = piece;
		this.partition = partition;
		this.initialFORules = initialFORules;
	}

	@Override
	public FORule getAggregatedFORule() {
		return aggregatedFORule;
	}

	@Override
	public FOFormula<Atom> getPiece() {
		return piece;
	}

	@Override
	public FOQueryConjunction getQuery() {
		return query;
	}

	@Override
	public TermPartition getPartition() {
		return partition;
	}
	
	@Override
	public Set<FORule> getInitialFORules() {
		return initialFORules;
	}
	
	@Override
	public Substitution getAssociatedSubstitution() {
		if (associatedSubstitution == null) {
			associatedSubstitution = partition.getAssociatedSubstitution(query).orElseGet(null);
		}
		return new SubstitutionImpl(associatedSubstitution);
	}

	@Override
	public FOFormula<Atom> getImageOf(FOFormula<Atom> f) {
		FOFormula<Atom> atomset = null;
		if (associatedSubstitution == null) {
			associatedSubstitution = partition.getAssociatedSubstitution(query).orElse(null);
		}
		if (associatedSubstitution != null) {
			atomset = FOFormulaUtil.createImageWith(f, associatedSubstitution);
		}
		return atomset;
	}

	@Override
	public QueryUnifier aggregate(QueryUnifier other) {
		// we create a piece that is the conjunction of the two pieces
		FOFormula<Atom> pieces = FOFormulaFactory.instance()
				.createOrGetConjunction(
						this.getPiece(),
						other.getPiece()).orElseGet(null);

		// we create a FORule that is the aggregation of the two FORules
		FOFormula<Atom> b = FOFormulaFactory.instance()
				.createOrGetConjunction(
						this.getAggregatedFORule().getBody().getFormula(),
						other.getAggregatedFORule().getBody().getFormula()).orElseGet(null);
		FOFormula<Atom> h = FOFormulaFactory.instance()
				.createOrGetConjunction(
						this.getAggregatedFORule().getHead(),
						other.getAggregatedFORule().getHead()).orElseGet(null);
		FORule aggregated_rule = new FORuleImpl(
				FOQueryFactory.instance().createOrGetQuery(b, null, null).orElseGet(null),
				h);

		// we create the partition which is the join of the two partitions
		TermPartition part = new TermPartition(this.getPartition());
		part.join(other.getPartition());

		Set<FORule> rules = new HashSet<>(this.getInitialFORules());
		rules.addAll(other.getInitialFORules());

		return new QueryUnifierImpl(this.getQuery(), pieces, part, aggregated_rule, rules);
	}

	@Override
	public boolean isCompatible(QueryUnifier other) {
		var thisPiece = this.getPiece().flatten();
		for (Atom a : other.getPiece().flatten()) {
			if (thisPiece.contains(a)) {
				return false;
			}
		}
		var part = new TermPartition(this.getPartition());
		part.join(other.getPartition()); 
		return part.getAssociatedSubstitution(null).isPresent();
	}
	
	@Override
	public String toString() {
		try {
			return "(QueryUnifier |  " + piece + " <=> " + aggregatedFORule.getHead() + " | = " + partition + ")";
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(query, piece, partition, aggregatedFORule, initialFORules);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (!(o instanceof QueryUnifier)) {
			return false;
		}

		QueryUnifier other = (QueryUnifier)o;
		return this.getQuery().equals(other.getQuery()) &&
				this.getPiece().equals(other.getPiece()) &&
				this.getPartition().equals(other.getPartition()) &&
				this.getAggregatedFORule().equals(other.getAggregatedFORule()) &&
				this.getInitialFORules().equals(other.getInitialFORules());
	}

}
