import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.boreal.backward_chaining.old.RewritingBridge;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.inmemory.SimpleInMemoryGraphStore;

@RunWith(Parameterized.class)
class RewritingBridgeTest {

	static Predicate p = SameObjectPredicateFactory.instance().createOrGetPredicate("p", 1);
	static Predicate pp = SameObjectPredicateFactory.instance().createOrGetPredicate("pp", 1);
	static Predicate ppp = SameObjectPredicateFactory.instance().createOrGetPredicate("ppp", 1);
	static Predicate q = SameObjectPredicateFactory.instance().createOrGetPredicate("q", 2);
	static Predicate r = SameObjectPredicateFactory.instance().createOrGetPredicate("r", 1);
	static Predicate s = SameObjectPredicateFactory.instance().createOrGetPredicate("s", 2);
	static Predicate t = SameObjectPredicateFactory.instance().createOrGetPredicate("t", 2);

	static Variable x = SameObjectTermFactory.instance().createOrGetVariable("x");
	static Variable x1 = SameObjectTermFactory.instance().createOrGetVariable("x1");
	static Variable x2 = SameObjectTermFactory.instance().createOrGetVariable("x2");
	static Variable y = SameObjectTermFactory.instance().createOrGetVariable("y");
	static Variable u = SameObjectTermFactory.instance().createOrGetVariable("u");
	static Variable v = SameObjectTermFactory.instance().createOrGetVariable("v");

	static Atom qx1x2 = new AtomImpl(q, x1, x2);
	static Atom pppx2 = new AtomImpl(ppp, x2);
	static Atom rx1 = new AtomImpl(r, x1);
	static Atom px = new AtomImpl(p, x);
	static Atom ppx = new AtomImpl(pp, x);
	static Atom pppx = new AtomImpl(ppp, x);

	static Atom qxy = new AtomImpl(q, x, y);
	static Atom py = new AtomImpl(p, y);
	
	static Atom ppy = new AtomImpl(pp, y);
	static Atom pppy = new AtomImpl(ppp, y);
	static Atom rx = new AtomImpl(r, x);
	
	static Atom sxy = new AtomImpl(s, x, y);
	static Atom txy = new AtomImpl(t, x, y);
	static Atom tuv = new AtomImpl(t, u, v);
	static Atom tvu = new AtomImpl(t, v, u);
	static Atom quv = new AtomImpl(q, u, v);
	static Atom suv = new AtomImpl(s, u, v);
	static Atom svu = new AtomImpl(s, v, u);

	// r(X1) -> q(X1, X2), ppp(X2)
	static FORule R1 = new FORuleImpl(
			FOQueryFactory.instance().createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(rx1).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(qx1x2, pppx2).get());

	// ppp(X) -> pp(X)
	static FORule R2 = new FORuleImpl(
			FOQueryFactory.instance().createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(pppx).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(ppx).get());

	// pp(X) -> p(X)
	static FORule R3 = new FORuleImpl(
			FOQueryFactory.instance().createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(ppx).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(px).get());

	// ?(X) :- q(X,Y), p(Y)
	static FOQuery Q = FOQueryFactory.instance().createOrGetConjunctionQuery(
			FOFormulaFactory.instance().createOrGetConjunction(qxy, py).get(), 
			List.of(x), 
			new SubstitutionImpl());
	
	// s(X, Y) -> t(X, Y)
	static FORule R4 = new FORuleImpl(
			FOQueryFactory.instance().createOrGetAtomicQuery(
					FOFormulaFactory.instance().createOrGetAtomic(sxy).orElse(null), 
					List.of(), 
					new SubstitutionImpl()),
			FOFormulaFactory.instance().createOrGetConjunction(txy).get());
	
	// ?(U, V) :- t(U, V), t(V, U), q(U, V)
	static FOQuery Q2 = FOQueryFactory.instance().createOrGetConjunctionQuery(
			FOFormulaFactory.instance().createOrGetConjunction(tuv, tvu, quv).get(), 
			List.of(u, v), 
			new SubstitutionImpl());

	@Parameters
	static Stream<Arguments> bridgeData() {
		// ?(X) :- q(X,Y), p(Y)
		FOQuery Qres1 = Q;
		// ?(X) :- q(X,Y), pp(Y)
		FOQuery Qres2 = FOQueryFactory.instance().createOrGetConjunctionQuery(
				FOFormulaFactory.instance().createOrGetConjunction(qxy, ppy).get(), 
				List.of(x), 
				new SubstitutionImpl());
		// ?(X) :- q(X,Y), ppp(Y)
		FOQuery Qres3 = FOQueryFactory.instance().createOrGetConjunctionQuery(
				FOFormulaFactory.instance().createOrGetConjunction(qxy, pppy).get(), 
				List.of(x), 
				new SubstitutionImpl());
		// ?(X) :- r(X)
		FOQuery Qres4 = FOQueryFactory.instance().createOrGetConjunctionQuery(
				FOFormulaFactory.instance().createOrGetConjunction(rx).get(), 
				List.of(x), 
				new SubstitutionImpl());
		
		
		// ?(U, V) :- t(U, V), t(V, U), q(U, V)
		FOQuery Q2res1 = Q2;
		// ?(U, V) :- s(U, V), t(V, U), q(U, V)
		FOQuery Q2res2 = FOQueryFactory.instance().createOrGetConjunctionQuery(
				FOFormulaFactory.instance().createOrGetConjunction(suv, tvu, quv).get(), 
				List.of(u, v), 
				new SubstitutionImpl());
		// ?(U, V) :- t(U, V), s(V, U), q(U, V)
		FOQuery Q2res3 = FOQueryFactory.instance().createOrGetConjunctionQuery(
				FOFormulaFactory.instance().createOrGetConjunction(tuv, svu, quv).get(), 
				List.of(u, v), 
				new SubstitutionImpl());
		// ?(U, V) :- s(U, V), s(V, U), q(U, V)
		FOQuery Q2res4 = FOQueryFactory.instance().createOrGetConjunctionQuery(
				FOFormulaFactory.instance().createOrGetConjunction(suv, svu, quv).get(), 
				List.of(u, v), 
				new SubstitutionImpl());

		return Stream.of(
				Arguments.of(Q, Set.of(R1, R2, R3), Set.of(Qres1, Qres2, Qres3, Qres4)),
				Arguments.of(Q2, Set.of(R4), Set.of(Q2res1, Q2res2, Q2res3, Q2res4))
				);
	}

	@DisplayName("Test rewriting bridge")
	@ParameterizedTest(name = "{index}: rewrite {0} with {1} = {2})")
	@MethodSource("bridgeData")
	public void rewritingBridgeTest(FOQuery query, Collection<FORule> rules,  Collection<FOQuery> expected) {
		RewritingBridge algo = new RewritingBridge();
		Set<FOQuery> result = algo.rewrite(query, new RuleBaseImpl(rules));
		Assert.assertTrue(expected.size() == result.size());
		
		for(FOQuery q : expected) {
			int count = 0;
			FactBase fb = new SimpleInMemoryGraphStore(q.getFormula().flatten());
			for(FOQuery q2 : result) {
				boolean exist = GenericFOQueryEvaluator.defaultInstance().exist(q2, fb);
				if(exist) {
					Assert.assertEquals(q.getAnswerVariables(), q2.getAnswerVariables());
					FactBase fb2 = new SimpleInMemoryGraphStore(q2.getFormula().flatten());
					Assert.assertTrue(GenericFOQueryEvaluator.defaultInstance().exist(q, fb2));
					count++;
				}
			}
			Assert.assertEquals(1, count);
		}
	}
}

