package fr.boreal.backward_chaining.pure.rewriting_operator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Sets;

import fr.boreal.backward_chaining.pure.rewriting_operator.dlx.DLX;
import fr.boreal.backward_chaining.pure.rewriting_operator.dlx.DLXResult;
import fr.boreal.backward_chaining.pure.rewriting_operator.dlx.DLXResultProcessor;
import fr.boreal.backward_chaining.pure.rewriting_operator.dlx.data.ColumnObject;
import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.graal.unifier.QueryUnifier;
import fr.lirmm.boreal.graal.unifier.QueryUnifierAlgorithm;
import fr.lirmm.boreal.util.FOQueryUtil;

/**
 * This operator rewrites a query with the given rules assuming the rules are source to target rules
 * meaning that the vocabulary of the initial query and the rewritings will be totally disjoint.
 * This let us rewrite with mapping rules in a single step
 * This implementation uses DLX algorithm (Dancing links) to compute a full cover of the query using single piece unifiers.
 */
public class SourceTargetOperator implements RewritingOperator {

	QueryUnifierAlgorithm unifier_algo;

	/**
	 * Creates a new SourceTargetOperator using default parameters
	 * query unifier algorithm : QueryUnifierAlgorithm
	 */
	public SourceTargetOperator() {
		this(new QueryUnifierAlgorithm());
	}

	/**
	 * Creates a new SourceTargetOperator using the given parameters
	 * @param unifier_algo the query unifier algorithm to use
	 */
	public SourceTargetOperator(QueryUnifierAlgorithm queryUnifierAlgorithm) {
		this.unifier_algo = queryUnifierAlgorithm;
	}

	@Override
	public Set<FOQuery> rewrite(FOQuery query, RuleBase rules) {

		// Select the subset of the rules that are possible candidates
		// This is the rules which have at least one atom with a corresponding predicate to the query
		Set<FORule> candidates = new HashSet<FORule>();
		for(Atom a1 : query.getFormula().flatten()) {
			candidates.addAll(rules.getRulesByHeadPredicate(a1.getPredicate()));
		}

		// Compute all the unifiers of the query with all the candidates
		Set<QueryUnifier> unifiers = new HashSet<QueryUnifier>();
		for(FORule r : candidates) {
			unifiers.addAll(unifier_algo.getMostGeneralSinglePieceUnifiers(query, r));
		}

		unifiers = this.getTotalAggregatedUnifiers(unifiers, query);

		// Rewrite the query with the unifiers
		Set<FOQuery> rewritings = new HashSet<FOQuery>();
		for(QueryUnifier unifier : unifiers) {
			FORule rule = unifier.getAggregatedFORule();
			FOFormula<Atom> piece = unifier.getPiece();

			// Part of the new query coming from the body of the rule, renamed by the unifier
			FOQuery from_unified_rule = FOQueryUtil.createImageWith(rule.getBody(), unifier.getAssociatedSubstitution());

			// Part of the new query left from the initial one
			// Computed as the query minus the unified piece
			FOQuery renamed_query = FOQueryUtil.createImageWith(query, unifier.getAssociatedSubstitution());
			FOFormula<Atom> renamed_piece = FOFormulaUtil.createImageWith(piece, unifier.getAssociatedSubstitution());
			Set<Atom> left_from_initial_query = Sets.difference((Set<Atom>)(renamed_query.getFormula().flatten()),(Set<Atom>)(renamed_piece.flatten()));

			// The new query conditions is the union of the rule part and the initial query that is not unified
			Set<Atom> new_query_condtions = Sets.union(left_from_initial_query, (Set<Atom>)(from_unified_rule.getFormula().flatten()));
			FOFormula<Atom> new_query_formula = FOFormulaFactory.instance().createOrGetConjunction(new_query_condtions.toArray(new Atom[new_query_condtions.size()])).get();

			FOQuery rewriting = FOQueryFactory.instance().createOrGetQuery(
					new_query_formula,
					renamed_query.getAnswerVariables(),
					renamed_query.getInitialSubstitution())
					.orElse(null);
			rewritings.add(rewriting);
		}

		return rewritings;
	}

	private Set<QueryUnifier> getTotalAggregatedUnifiers(Set<QueryUnifier> unifToAggregate, FOQuery query) {

		if(unifToAggregate.size() == 0) {
			return new HashSet<QueryUnifier>();
		}

		// ---- building of matrix of covering ----

		//rows
		List<List<Byte>> rows = new LinkedList<List<Byte>>();
		//map from row ids to set of unifiers covering that row
		Map<String, Set<QueryUnifier>> unifiersCoveringOfRow = new HashMap<String,Set<QueryUnifier>>();

		for(QueryUnifier u : unifToAggregate) {
			Iterator<Atom> it = query.getFormula().flatten().iterator();

			List<Byte> row = new LinkedList<Byte>();
			String rowId = "";

			//column index
			int j = 0;
			while(it.hasNext()) {
				Atom queryAtom = it.next();

				if (u.getPiece().flatten().contains(queryAtom)) {
					row.add((byte)1);
					rowId += "-" + j;

				} else {
					row.add((byte)0);
				}
				j++;
			}

			//if it is not a redundant line
			if(!unifiersCoveringOfRow.containsKey(rowId)) {
				rows.add(row);

				Set<QueryUnifier> rowCoveringUnifiers = new HashSet<QueryUnifier>();
				rowCoveringUnifiers.add(u);
				unifiersCoveringOfRow.put(rowId, rowCoveringUnifiers);
			} else {
				unifiersCoveringOfRow.get(rowId).add(u);
			}
		}

		ColumnObject h = translateToColumnObject(rows);

		AggregatedUnifDLXResultProcessor resultProcessor = new AggregatedUnifDLXResultProcessor(unifiersCoveringOfRow);

		DLX.solve(h, false, resultProcessor);

		return resultProcessor.getAggregatedUnifiers();
	}

	private ColumnObject translateToColumnObject(List<List<Byte>> rows) {
		int queryAtomNumber = rows.get(0).size();

		// --conversion to arrays--
		// covering matrix of query atoms by piece unifiers
		byte[][] coveringMatrix = new byte[rows.size()][queryAtomNumber];
		// label of the colunms
		Object[] columnLabels = new Object[queryAtomNumber];

		//filling of coveringMatrix
		for(int i = 0; i < rows.size(); i++) {
			for(int j = 0; j < queryAtomNumber; j++) {
				coveringMatrix[i][j] = (byte) rows.get(i).get(j);
			}
		}

		// filling of column labels
		for(int j = 0; j < queryAtomNumber; j++) {
			columnLabels[j] = "" + j;
		}

		return DLX.buildSparseMatrix(coveringMatrix, columnLabels);
	}

	public class AggregatedUnifDLXResultProcessor implements DLXResultProcessor {

		private	Set<QueryUnifier> totalAggregatedUnifiers = new HashSet<QueryUnifier>();
		private Map<String, Set<QueryUnifier>> unifiersCoveringOfRow;

		public AggregatedUnifDLXResultProcessor(Map<String, Set<QueryUnifier>> unifiersCoveringOfRow) {
			super();
			this.unifiersCoveringOfRow = unifiersCoveringOfRow;
		}

		/**
		 * result is exact covering set of rows
		 * rows are described by column labels where a 1 appears in the row
		 * 
		 * We realize a breadth-first walk to build aggregated unifiers 
		 * defined by the result
		 *
		 * @see DLXResultProcessor#processResult(DLXResult)
		 */
		public boolean processResult(DLXResult result) {

			final Iterator<List<Object>> rows = result.rows();

			//for each possible branchment
			if(rows.hasNext()) {
				//row is defined by a list of column labels
				List<Object> columnLabelsOfRow = rows.next();
				//build the row identifier
				String rowId = "";
				for(Object columnLabel : columnLabelsOfRow) rowId += "-" + columnLabel;
				//piece unifiers corresponding to the row
				Set<QueryUnifier> rowUnifiers = unifiersCoveringOfRow.get(rowId);

				fromResultToAggregatedUnifiers(rowUnifiers, rows);             
			}

			return true;
		}

		public Set<QueryUnifier> getAggregatedUnifiers() {
			return this.totalAggregatedUnifiers;
		}

		private void fromResultToAggregatedUnifiers(Set<QueryUnifier> previousAggregatedUnifiers,
				Iterator<List<Object>> rows) {

			if(rows.hasNext()) {
				// row is defined by a list of column labels
				List<Object> columnLabelsOfRow = rows.next();
				//build the row identifier
				String rowId = "";
				for(Object columnLabel : columnLabelsOfRow) rowId += "-" + columnLabel;
				//piece unifiers corresponding to the row
				Set<QueryUnifier> rowUnifiers = unifiersCoveringOfRow.get(rowId);

				Set<QueryUnifier> partiallyAggregatedUnifiers = new HashSet<QueryUnifier>();

				//we aggregate each previous aggregated unifiers with the new row unifiers
				for(QueryUnifier previousUnifier : previousAggregatedUnifiers) {
					for(QueryUnifier rowUnifier : rowUnifiers) {
						if(previousUnifier.isCompatible(rowUnifier)) {

							partiallyAggregatedUnifiers.add(previousUnifier.aggregate(rowUnifier));
						}
					}
				}

				// we continue with the next row
				fromResultToAggregatedUnifiers(partiallyAggregatedUnifiers, rows);

			} else {
				// when there are no more row, unifiers are totals
				this.totalAggregatedUnifiers.addAll(previousAggregatedUnifiers);
			}
		}
	}

}
