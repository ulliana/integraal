package fr.boreal.backward_chaining.pure.cover;

import java.util.Set;

import fr.boreal.model.query.api.FOQuery;

/**
 * From Melanie Konïg's thesis
 * 
 * Let Q1s and Q2s two set of queries
 * We say that Q1s covers Q2s (noted Q1s >= Q2s)
 * if for every query Q2 of Q2
 * there exist a query Q1 from Q1s such that 
 * Q1 >= Q2 (Q1 subsumes Q2)
 * 
 * For termination reasons, the cover should keep in priority the already explored queries
 * 
 */
public interface CoverFunction {
	
	/**
	 * @return the cover of the given queries
	 */
	public Set<FOQuery> cover(Set<FOQuery> queries);

}
