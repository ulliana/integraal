package fr.boreal.backward_chaining.pure.rewriting_operator.dlx;

public class DLXException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DLXException() {
		// Do nothing
	}

	public DLXException(String message) {
		super(message);
	}

}
