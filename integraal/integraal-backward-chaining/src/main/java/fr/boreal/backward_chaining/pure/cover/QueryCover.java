package fr.boreal.backward_chaining.pure.cover;

import java.util.HashSet;
import java.util.Set;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.inmemory.SimpleInMemoryGraphStore;

/**
 * Computes the cover of a set of queries using homomorphism to detect if a query is more general than another
 * We assume that queries have the exact same answer variables, otherwise this cover function may not apply
 */
public class QueryCover implements CoverFunction {

	@Override
	public Set<FOQuery> cover(Set<FOQuery> queries) {
		Set<FOQuery> cover = new HashSet<FOQuery>();
		for(FOQuery q1 : queries) {
			boolean toAdd = true;
			for(FOQuery q2 : cover) {
				if(this.isMoreGeneralThan(q2, q1)) {
					toAdd = false;
					break;
				}
				else if(this.isMoreGeneralThan(q1, q2)) {
					cover.remove(q2);
				}
			}
			if(toAdd) {
				cover.add(q1);
			}
		}
		return cover;
	}

	/**
	 * @return true iff q1 subsumes q2
	 */
	private boolean isMoreGeneralThan(FOQuery q1, FOQuery q2) {
		FactBase fb = new SimpleInMemoryGraphStore(q2.getFormula().flatten());

		Substitution freeze_answer_variables = new SubstitutionImpl();

		for(Variable v : q1.getAnswerVariables()) {
			freeze_answer_variables.add(v, v);
		}

		Substitution s = freeze_answer_variables.merged(q1.getInitialSubstitution()).orElse(null);
		if(s == null) {
			return false;
		}

		FOQuery q = FOQueryFactory.instance().createOrGetQuery(q1.getFormula(), q1.getAnswerVariables(), s).get();
		return GenericFOQueryEvaluator.defaultInstance().exist(q, fb);
	}

}
