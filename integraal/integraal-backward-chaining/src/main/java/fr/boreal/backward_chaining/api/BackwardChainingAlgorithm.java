package fr.boreal.backward_chaining.api;

import java.util.Set;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.FOQuery;

/**
 * A Backward Chaining algorithm rewrites a {@link FOQuery} according to the given {@link RuleBase}.
 */
public interface BackwardChainingAlgorithm {
	
	/**
	 * Execute the algorithm
	 * @param query the query to rewrite
	 * @param rules the rules with which to rewrite the query
	 * @return the set of all rewritings of the query with the rules
	 */
	public Set<FOQuery> rewrite(FOQuery query, RuleBase rules);

}
