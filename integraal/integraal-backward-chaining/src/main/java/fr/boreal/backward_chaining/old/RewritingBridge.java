package fr.boreal.backward_chaining.old;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.backward_chaining.api.BackwardChainingAlgorithm;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.FOQuery;
import fr.lirmm.boreal.util.converter.QueryConverter;
import fr.lirmm.boreal.util.converter.RuleConverter;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.backward_chaining.pure.PureRewriter;

/**
 * Bridge with Graal 1.3 rewriting
 */
public class RewritingBridge implements BackwardChainingAlgorithm {
	
	private PureRewriter rewriter = new PureRewriter();

	@Override
	public Set<FOQuery> rewrite(FOQuery query, RuleBase rules) {
		
		ConjunctiveQuery cq = QueryConverter.convert(query);
		List<Rule> old_rules = rules.getRules().stream().map(RuleConverter::convert).collect(Collectors.toList());
		
		var old_rewritings = rewriter.execute(cq, old_rules);
		
		Set<FOQuery> rewritings = new HashSet<FOQuery>();
		old_rewritings.forEachRemaining(it -> rewritings.add(QueryConverter.reverse(it)));
		return rewritings;

	}

}
