package fr.boreal.backward_chaining.pure.rewriting_operator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.graal.unifier.QueryUnifier;
import fr.lirmm.boreal.graal.unifier.QueryUnifierAlgorithm;
import fr.lirmm.boreal.util.FOQueryUtil;

/**
 * From Melanie Konïg's thesis
 * Rewriting operator SRA
 * 
 * Query rewriting engine that rewrite query using
 * aggregation by rule of most general single piece-unifiers
 */
public class SingleRuleAggregator implements RewritingOperator {

	QueryUnifierAlgorithm unifier_algo;
	
	/**
	 * Creates a new SingleRuleAggregator using default parameters
	 * query unifier algorithm : QueryUnifierAlgorithm
	 */
	public SingleRuleAggregator() {
		this(new QueryUnifierAlgorithm());
	}
	
	/**
	 * Creates a new SingleRuleAggregator using the given parameters
	 * @param unifier_algo the query unifier algorithm to use
	 */
	public SingleRuleAggregator(QueryUnifierAlgorithm unifier_algo) {
		this.unifier_algo = unifier_algo;
	}

	@Override
	public Set<FOQuery> rewrite(FOQuery query, RuleBase rb) {

		// Select the subset of the rules that are possible candidates
		// This is the rules which have at least one atom with a corresponding predicate to the query
		Set<FORule> candidates = new HashSet<FORule>();
		for(Atom a1 : query.getFormula().flatten()) {
			candidates.addAll(rb.getRulesByHeadPredicate(a1.getPredicate()));
		}

		// Compute all the unifiers of the query with all the candidates
		Set<QueryUnifier> unifiers = new HashSet<QueryUnifier>();
		for(FORule r : candidates) {
			unifiers.addAll(this.getSingleRuleUnifiers(query, r));
		}

		// Rewrite the query with the unifiers
		Set<FOQuery> rewritings = new HashSet<FOQuery>();
		for(QueryUnifier unifier : unifiers) {
			FORule rule = unifier.getAggregatedFORule();
			FOFormula<Atom> piece = unifier.getPiece();
			
			// Part of the new query coming from the body of the rule, renamed by the unifier
			FOQuery from_unified_rule = FOQueryUtil.createImageWith(rule.getBody(), unifier.getAssociatedSubstitution());
			
			// Part of the new query left from the initial one
			// Computed as the query minus the unified piece
			FOQuery renamed_query = FOQueryUtil.createImageWith(query, unifier.getAssociatedSubstitution());
			FOFormula<Atom> renamed_piece = FOFormulaUtil.createImageWith(piece, unifier.getAssociatedSubstitution());
			Set<Atom> left_from_initial_query = Sets.difference((Set<Atom>)(renamed_query.getFormula().flatten()),(Set<Atom>)(renamed_piece.flatten()));

			// The new query conditions is the union of the rule part and the initial query that is not unified
			Set<Atom> new_query_condtions = Sets.union(left_from_initial_query, (Set<Atom>)(from_unified_rule.getFormula().flatten()));
			FOFormula<Atom> new_query_formula = FOFormulaFactory.instance().createOrGetConjunction(new_query_condtions.toArray(new Atom[new_query_condtions.size()])).get();
			
			FOQuery rewriting = FOQueryFactory.instance().createOrGetQuery(
					new_query_formula,
					renamed_query.getAnswerVariables(),
					renamed_query.getInitialSubstitution())
					.orElse(null);
			rewritings.add(rewriting);
		}

		return rewritings;
	}

	/**
	 * Computes all the possible unifiers between the given query and rule
	 * The possible unifiers are the most general single piece unifiers as well as all the possible aggregations between them
	 * @return all the possible unifiers between the given query and rule
	 */
	private Collection<QueryUnifier> getSingleRuleUnifiers(FOQuery query, FORule r) {

		// Compute single piece unifiers
		// We assume that all unifers are renamed by the getMostGeneralMonoPieceUnifiers method
		Collection<QueryUnifier> all_unifiers = unifier_algo.getMostGeneralSinglePieceUnifiers(query, r);

		// We use a list to handle the unifiers that haven't been handled yet
		// this let us aggregate together aggregated unifiers or single piece unifiers and aggregated ones
		// We use this list as a queue by only checking further than the current index for aggregation because of it's associativity
		List<QueryUnifier> to_handle = new ArrayList<QueryUnifier>(all_unifiers);
		int index = 0;

		while(index < to_handle.size()) {
			// We store the new unifiers here
			// and add them back in the end of the loop
			Collection<QueryUnifier> new_unifiers = new HashSet<QueryUnifier>();

			// We select an unifier
			QueryUnifier u = to_handle.get(index);

			// Aggregate the selected unifier with all the other unifiers if they are compatible
			// We only need to check the unifiers that are further in the list because of the associativity of the aggregation
			for(int i = index + 1; i < to_handle.size(); ++i) {
				QueryUnifier u2 = to_handle.get(i);
				if(!u.equals(u2) && u.isCompatible(u2)) {
					QueryUnifier aggregated_unifier = u.aggregate(u2);
					new_unifiers.add(aggregated_unifier);
				}
			}

			// Add the new unifiers to the set of all unifiers
			// and also to the queue of unifiers to handle
			for(QueryUnifier u2 : new_unifiers) {
				if(!all_unifiers.contains(u2)) {
					all_unifiers.add(u2);
					to_handle.add(u2);
				}
			}
			++index;
		}
		return all_unifiers;
	}

}
