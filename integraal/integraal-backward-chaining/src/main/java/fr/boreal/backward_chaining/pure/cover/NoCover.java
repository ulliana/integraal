package fr.boreal.backward_chaining.pure.cover;

import java.util.Set;

import fr.boreal.model.query.api.FOQuery;

/**
 * Does nothing
 */
public class NoCover implements CoverFunction {

	@Override
	public Set<FOQuery> cover(Set<FOQuery> queries) {
		return queries;
	}

}
