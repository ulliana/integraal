package fr.boreal.backward_chaining.pure.rewriting_operator;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.FOQuery;

/**
 * From Melanie Konïg's thesis
 * A rewriting operator rew is a function which takes a conjunctive query Q and a set of rules R
 * and outputs a set of conjunctive queries noted rew(Q, R).
 * 
 * This computes direct rewritings of the query Q with regards to R
 * 
 * We can extend this definition to a set of queries as the union of all independent rewritings for each query.
 */
public interface RewritingOperator {
	
	/**
	 * A rewriting operator rew is a function which takes a conjunctive query Q and a set of rules R
	 * and outputs a set of conjunctive queries noted rew(Q, R)
	 * 
	 * @param query Q
	 * @param rules R
	 * @return a set of conjunctive queries, rewritings of Q with R
	 */
	public Set<FOQuery> rewrite(FOQuery query, RuleBase rules);
	
	/**
	 * Generalization to a set of queries
	 * 
	 * @param queries Q
	 * @param rules R
	 * @return a set of conjunctive queries, rewritings of Q with R
	 */
	default public Set<FOQuery> rewrite(Collection<FOQuery> queries, RuleBase rules) {
		return queries.stream().flatMap(q -> this.rewrite(q, rules).stream()).collect(Collectors.toSet());
	}

}
