package fr.boreal.backward_chaining.pure;

import java.util.Set;

import com.google.common.collect.Sets;

import fr.boreal.backward_chaining.api.BackwardChainingAlgorithm;
import fr.boreal.backward_chaining.pure.cover.CoverFunction;
import fr.boreal.backward_chaining.pure.cover.QueryCover;
import fr.boreal.backward_chaining.pure.rewriting_operator.RewritingOperator;
import fr.boreal.backward_chaining.pure.rewriting_operator.SingleRuleAggregator;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.query.api.FOQuery;

/**
 * From Melanie Konïg's thesis, algorithm 1
 * Produces a cover of the set of all rewritings of a query Q with a set of rules R using piece unifiers
 */
public class PureRewriter implements BackwardChainingAlgorithm {
	
	private RewritingOperator rew;
	private CoverFunction coverFct;
	
	/**
	 * Creates a new PureRewriter using default parameters
	 * Rewriting operator SRA
	 * Cover function query cover
	 */
	public PureRewriter() {
		this(new SingleRuleAggregator(), new QueryCover());
	}
	
	/**
	 * Creates a new PureRewriter using the given parameters
	 * @param rew the rewriting operator to use
	 * @param coverFct the cover function to use
	 */
	public PureRewriter(RewritingOperator rew, CoverFunction coverFct) {
		this.rew = rew;
		this.coverFct = coverFct;
	}

	@Override
	public Set<FOQuery> rewrite(FOQuery query, RuleBase rules) {
		// Qf = {Q}
		Set<FOQuery> end_rewritings = Set.of(query);
		
		// Qe = {Q}
		Set<FOQuery> to_explore_rewritings = Set.of(query);
		
		while(!to_explore_rewritings.isEmpty()) {
			// Qc = cover(Qf U rew(Qe, R))
			Set<FOQuery> rewritings_cover = Sets.union(end_rewritings, this.coverFct.cover(this.rew.rewrite(to_explore_rewritings, rules)));
			
			// Qe = Qc\Qf
			to_explore_rewritings = Sets.difference(rewritings_cover, end_rewritings);
			
			// Qf = Qc
			end_rewritings = rewritings_cover;
		}
		return this.coverFct.cover(end_rewritings);
	}

}
