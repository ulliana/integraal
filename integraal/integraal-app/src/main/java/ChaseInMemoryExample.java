import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.impl.builtin.DlgpParser;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;

public class ChaseInMemoryExample {

	public static void main(String[] args) throws Exception {

		///////////////
		// Factories //
		///////////////

		TermFactory termfactory = SameObjectTermFactory.instance();
		PredicateFactory predicatefactory = SameObjectPredicateFactory.instance();

		//////////////////
		// DLGP parsing //
		//////////////////

		long time = System.currentTimeMillis();

		Collection<Atom> atoms = new ArrayList<Atom>();
		Collection<FORule> rules = new ArrayList<FORule>();
		Collection<FOQuery> queries = new ArrayList<FOQuery>();

		for(String filepath : new String[] {
				"src/main/resources/deep/deep300.dlgp"
		}) {

			File file = new File(filepath);
			DlgpParser dlgp_parseur = new DlgpParser(file, termfactory, predicatefactory);
			while (dlgp_parseur.hasNext()) {
				try {
					Object result = dlgp_parseur.next();
					if (result instanceof Atom) {
						atoms.add((Atom)result);
					} else if (result instanceof FORule) {
						rules.add((FORule)result);
					} else if (result instanceof FOQuery) {
						queries.add((FOQuery)result);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			dlgp_parseur.close();
		}

		System.out.println("Import time : " + (System.currentTimeMillis() - time));

		FactBase fb = StorageBuilder.getSimpleInMemoryGraphStore();
		fb.addAll(atoms);

		RuleBase rb = new RuleBaseImpl(rules);

		System.out.println("Initial factbase size : " + fb.size());
		time = System.currentTimeMillis();

		ChaseBuilder.defaultBuilder(fb, rb, termfactory)
		.debug()
		.build()
		.get()
		.execute();

		System.out.println("---");
		System.out.println("Time : " + (System.currentTimeMillis() - time));
		System.out.println("Final factbase size : " + fb.size());

	}

}