import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import fr.boreal.backward_chaining.pure.PureRewriter;
import fr.boreal.backward_chaining.pure.cover.QueryCover;
import fr.boreal.backward_chaining.pure.rewriting_operator.SourceTargetOperator;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.impl.builtin.DlgpParser;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;

public class RewritingExample {

	public static void main(String[] args) throws Exception {

		///////////////
		// Factories //
		///////////////

		TermFactory termfactory = SameObjectTermFactory.instance();
		PredicateFactory predicatefactory = SameObjectPredicateFactory.instance();

		//////////////////
		// DLGP parsing //
		//////////////////

		long time = System.currentTimeMillis();

		Collection<Atom> atoms = new ArrayList<Atom>();
		Collection<FORule> rules = new ArrayList<FORule>();
		Collection<FOQuery> queries = new ArrayList<FOQuery>();

		for(String filepath : new String[] {
				"src/main/resources/tatooine_example.dlgp",
		}) {

			File file = new File(filepath);
			DlgpParser dlgp_parseur = new DlgpParser(file, termfactory, predicatefactory);
			while (dlgp_parseur.hasNext()) {
				try {
					Object result = dlgp_parseur.next();
					if (result instanceof Atom) {
						atoms.add((Atom)result);
					} else if (result instanceof FORule) {
						rules.add((FORule)result);
					} else if (result instanceof FOQuery) {
						queries.add((FOQuery)result);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			dlgp_parseur.close();
		}

		System.out.println("Import time : " + (System.currentTimeMillis() - time));

		FactBase fb = StorageBuilder.getSimpleInMemoryGraphStore();
		fb.addAll(atoms);

		RuleBase rb = new RuleBaseImpl(rules);

		PureRewriter rewriter = new PureRewriter();
		PureRewriter mappingRewriter = new PureRewriter(new SourceTargetOperator(), new QueryCover());
		for(FOQuery query : queries) {
			Set<FOQuery> rewritings = rewriter.rewrite(query, rb);
			System.out.println("Classical");
			for(var q : rewritings) {
				System.out.println(q);
			}
			System.out.println("---");

			Set<FOQuery> mappingRewritings = mappingRewriter.rewrite(query, rb);
			System.out.println("Mappings");
			for(var q : mappingRewritings) {
				System.out.println(q);
			}
		}

	}


}
