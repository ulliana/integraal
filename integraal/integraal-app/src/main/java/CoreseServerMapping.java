import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.rdf4j.query.TupleQueryResult;

import fr.boreal.io.dlgp.impl.builtin.DlgpParser;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.wrapper.mappings.MappingDatasourceWrapper;
import fr.boreal.storage.wrapper.mappings.SPARQLMappingDatasourceWrapper;

public class CoreseServerMapping {

	public static void main(String[] args) throws Exception {

		///////////////
		// Factories //
		///////////////

		TermFactory tf = SameObjectTermFactory.instance();
		PredicateFactory pf = SameObjectPredicateFactory.instance();

		////////////////////////////////////////
		// Triple store access using mappings //
		////////////////////////////////////////

		// native queries

		String coreseQuery = "PREFIX : <http://example.com/music/>\r\n"
				+ "\r\n"
				+ "				SELECT ?name (count(?song) AS ?nb_song)\r\n"
				+ "				WHERE {\r\n"
				+ "					?artist :name ?name .\r\n"
				+ "					?song :artist ?artist .\r\n"
				+ "				}\r\n"
				+ "				GROUP BY ?artist\r\n"
				+ "				ORDER BY DESC(?nb_song)";

		// endpoint url

		String coreseEndpoint = "http://localhost:8080/sparql";

		// relational view predicates

		Predicate vPerson = pf.createOrGetPredicate("viewSoloArtist", 2);

		// datasource creation

		MappingDatasourceWrapper<String, TupleQueryResult> coreseDatasource = 
				new SPARQLMappingDatasourceWrapper(coreseEndpoint);

		coreseDatasource.addMapping(vPerson, coreseQuery);

		///////////////
		// Reasoning //
		///////////////

		// DLGP rule parsing
		// + iterate over all the strates
		// and execute a chase for each one
		// over the federation
		Collection<FOQuery> queries = new ArrayList<FOQuery>();

		DlgpParser dlgp_parseur = new DlgpParser(new File("src/main/resources/coreseQuery.dlgp"), tf, pf);
		while (dlgp_parseur.hasNext()) {
			Object result = dlgp_parseur.next();
			if (result instanceof FOQuery) {
				queries.add((FOQuery)result);
			}
		}
		dlgp_parseur.close();


		for(FOQuery q : queries) {
			System.out.println(q);
			var it = GenericFOQueryEvaluator.defaultInstance().evaluate(q, coreseDatasource);
			Substitution s = it.next();
			
			it.forEachRemaining(System.out::println);
			System.out.println("---");
		}
	}

}
