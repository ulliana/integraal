import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCursor;

import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.impl.builtin.DlgpParser;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.FederatedFactBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.inmemory.SimpleInMemoryGraphStore;
import fr.boreal.storage.wrapper.mappings.MappingDatasourceWrapper;
import fr.boreal.storage.wrapper.mappings.MongoDBMappingDatasourceWrapper;


public class MongoDBGeospatialTest {

	public static void main(String[] args) throws Exception {

		///////////////
		// Factories //
		///////////////

		TermFactory tf = SameObjectTermFactory.instance();
		PredicateFactory pf = SameObjectPredicateFactory.instance();

		//////////////////////////////////////
		// WEB API querying using mappings //
		//////////////////////////////////////

		// Mongo connection

		String mongoServer = "mongodb+srv://ftornil:florenttornil@cluster0.cxwlvfk.mongodb.net/?retryWrites=true&w=majority";
		String database = "test_overpass";
		String collection = "belmerbach";

		Predicate vMongo = pf.createOrGetPredicate("vRiverNearby", 4);
		String query = "{ $geoNear: {\r\n"
				+ "					near: {\r\n"
				+ "						type: \"LineString\" ,\r\n"
				+ "						coordinates: [ %% ,  %% ]\r\n"
				+ "					},\r\n"
				+ "					distanceField: \"calcDistance\",\r\n"
				+ "					maxDistance: %%\r\n"
				+ "				}}";

		MappingDatasourceWrapper<String, MongoCursor<Document>> datasource = new MongoDBMappingDatasourceWrapper(mongoServer, database, collection, List.of("rivername"));
		datasource.addMapping(vMongo, query);

		////////////////////////////////////////////////
		// Local storage used to store inferred facts //
		///////////////////////////////////////////////

		SimpleInMemoryGraphStore local_storage = StorageBuilder.getSimpleInMemoryGraphStore();

		// we use this local storage to simulate the existing data of the tractor position sensor.

		String tractor_sensor_file = "src/main/resources/mongo/tractor_sensor.dlgp";
		File file = new File(tractor_sensor_file);

		DlgpParser tractor_dlgp_parseur = new DlgpParser(file, tf, pf);

		while (tractor_dlgp_parseur.hasNext()) {
			try {
				Object result = tractor_dlgp_parseur.next();
				if (result instanceof Atom) {
					local_storage.add((Atom)result);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		tractor_dlgp_parseur.close();

		/////////////////////////////////////
		// Federation : "virtual" factbase //
		/////////////////////////////////////

		FederatedFactBase federation = new FederatedFactBase(local_storage);
		federation.addStorage(vMongo, datasource);

		///////////////
		// Reasoning //
		///////////////

		// DLGP rule parsing
		// and execute a chase
		// over the federation

		for(String filepath : new String[] {
				"src/main/resources/mongo/mongo_geo.dlgp"
		}) {
			Collection<FORule> rules = new ArrayList<FORule>();
			DlgpParser dlgp_parseur = new DlgpParser(new File(filepath), tf, pf);
			while (dlgp_parseur.hasNext()) {
				Object result = dlgp_parseur.next();
				if (result instanceof FORule) {
					rules.add((FORule)result);
				}
			}
			dlgp_parseur.close();

			RuleBase rb = new RuleBaseImpl(rules);

			ChaseBuilder.defaultBuilder(federation, rb, tf)
			.useDirectApplication()
			.useObliviousChecker()
			.useNaiveComputer()
			.useNaiveRuleScheduler()
			.build()
			.get()
			.execute();
		}


		federation.getAtoms().forEach(System.out::println);

		///////////////////////////////////////
		// File export of the local factbase //
		///////////////////////////////////////

		File export_file = new File("src/main/resources/mongo/tractor_problems_" + + System.currentTimeMillis() + "_export.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(export_file));
		federation.getAtoms().forEach(a -> {
			try {
				bw.write(a.toString()+"\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		bw.flush();
		bw.close();
	}

}
