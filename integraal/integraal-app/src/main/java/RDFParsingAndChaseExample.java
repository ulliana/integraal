import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.rdf4j.rio.RDFFormat;

import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.impl.builtin.DlgpParser;
import fr.boreal.io.rdf.RDFParser;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.queryEvaluation.generic.GenericFOQueryEvaluator;
import fr.boreal.storage.builder.StorageBuilder;

public class RDFParsingAndChaseExample {

	public static void main(String[] args) throws Exception {

		TermFactory termfactory = SameObjectTermFactory.instance();
		PredicateFactory predicatefactory = SameObjectPredicateFactory.instance();

		long time;

		///

		Collection<Atom> atoms = new ArrayList<Atom>();

		System.out.println("RDF import...");
		time = System.currentTimeMillis();

		RDFParser parser = new RDFParser(new File("src/main/resources/3DAA/3Dassembly_knowledgeBase.ttl"), RDFFormat.TURTLE);

		FactBase fb = StorageBuilder.getSimpleInMemoryGraphStore();

		while(parser.hasNext()) {
			Object o = parser.next();
			if(o instanceof Atom) {
				atoms.add((Atom)o);
			}
		}
		parser.close();

		System.out.println("RDF data imported");
		System.out.println("Import time : " + (System.currentTimeMillis() - time) + "ms");

		Collection<FORule> rules = new ArrayList<FORule>();
		Collection<FOQuery> queries = new ArrayList<FOQuery>();

		System.out.println("Rules and queries import...");
		time = System.currentTimeMillis();

		File file = new File("src/main/resources/3DAA/queriesAndRules3dAA.dlgp");
		DlgpParser dlgp_parseur = new DlgpParser(file, termfactory, predicatefactory);
		while (dlgp_parseur.hasNext()) {
			try {
				Object result = dlgp_parseur.next();
				if (result instanceof Atom) {
					atoms.add((Atom)result);
				} else if (result instanceof FORule) {
					rules.add((FORule)result);
				} else if (result instanceof FOQuery) {
					queries.add((FOQuery)result);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		dlgp_parseur.close();

		System.out.println("Rules and queries imported");
		System.out.println("Import time : " + (System.currentTimeMillis() - time) + "ms");

		System.out.println("Setting up the factbase...");
		fb.addAll(atoms);
		RuleBase rb = new RuleBaseImpl(rules);



		System.out.println("Setting up the chase...");
		time = System.currentTimeMillis();

		ChaseBuilder.defaultBuilder(fb, rb, termfactory)
		.debug()
		.build()
		.get()
		.execute();

		System.out.println("---");
		System.out.println("Time : " + (System.currentTimeMillis() - time) + "ms");
		System.out.println("Final factbase size : " + fb.size());
		System.out.println("---");

		System.out.println("Query evaluation");
		System.out.println("---");	
		for(FOQuery q : queries) {
			System.out.println(q);
			var results = GenericFOQueryEvaluator.defaultInstance().evaluate(q, fb);
			results.forEachRemaining(System.out::println);
			System.out.println("---");
		}
	}

}
