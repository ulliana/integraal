package use_cases;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.query.TupleQueryResult;

import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.io.dlgp.impl.builtin.DlgpParser;
import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.FederatedFactBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.wrapper.mappings.MappingDatasourceWrapper;
import fr.boreal.storage.wrapper.mappings.SPARQLMappingDatasourceWrapper;
import fr.boreal.storage.wrapper.mappings.WebAPIMappingDatasourceWrapper;
import fr.boreal.storage.wrapper.mappings.transformer.json.Checker;

public class O2SolubilityINRAEUseCase {

	public static void main(String[] args) throws Exception {

		///////////////
		// Factories //
		///////////////

		TermFactory tf = SameObjectTermFactory.instance();
		PredicateFactory pf = SameObjectPredicateFactory.instance();

		////////////////////////////////////////
		// Triple store access using mappings //
		////////////////////////////////////////

		// native queries

		String sparqlQueryMesures = Files.readString(Path.of("src/main/resources/O2SolubilityINRAEUseCase/mesures_query.sparql"));
		String sparqlQueryAlignment = Files.readString(Path.of("src/main/resources/O2SolubilityINRAEUseCase/alignment_query.sparql"));
		String sparqlQueryNutriWeb = Files.readString(Path.of("src/main/resources/O2SolubilityINRAEUseCase/nutriments_query.sparql"));

		// endpoint url

		String endpoint_atWeb = "https://ico.iate.inra.fr/fuseki/annotation";

		// relational view predicates

		Predicate vMesureAtWeb = pf.createOrGetPredicate("vWebMesure", 7);
		Predicate vAlignmentAtWeb = pf.createOrGetPredicate("vWebAlign", 3);
		Predicate vNutriAtWeb = pf.createOrGetPredicate("vWebNutri", 4);

		// datasource creation

		MappingDatasourceWrapper<String, TupleQueryResult> datasource_atWeb = new SPARQLMappingDatasourceWrapper(endpoint_atWeb);

		datasource_atWeb.addMapping(vMesureAtWeb, sparqlQueryMesures);
		datasource_atWeb.addMapping(vAlignmentAtWeb, sparqlQueryAlignment);
		datasource_atWeb.addMapping(vNutriAtWeb, sparqlQueryNutriWeb);

		//////////////////////////////////////
		// WEB API querying using mappings //
		//////////////////////////////////////

		// native http query

		String httpQueryNutri = "https://ico.iate.inra.fr/meatylab/origin_databases/%%/foods/%%.json";

		// relational view predicate

		Predicate vNutriMeaty = pf.createOrGetPredicate("vMeatyNutri", 7);

		// datasource creation

		String username = "federico.ulliana@inria.fr";
		String password = Files.readString(Path.of("src/main/resources/O2SolubilityINRAEUseCase/password.txt"));

		// JSON handle

		String position_query = "/compos";

		List<Set<Checker>> termCheckers = new ArrayList<Set<Checker>>();
		termCheckers.add(Set.of(new Checker("/component/ecompid", o -> o.equals("FAT"))));
		termCheckers.add(Set.of(new Checker("/component/ecompid", o -> o.equals("NACL"))));
		termCheckers.add(Set.of(new Checker("/component/ecompid", o -> o.equals("WATER"))));
		termCheckers.add(Set.of(new Checker("/component/ecompid", o -> o.equals("SUGAR"))));
		termCheckers.add(Set.of(new Checker("/component/ecompid", o -> o.equals("PROT"))));

		List<String> selectionQueries = List.of("/value", "/value", "/value", "/value", "/value");

		MappingDatasourceWrapper<String, String> datasource_meaty = new WebAPIMappingDatasourceWrapper(username, password, null, position_query, termCheckers, selectionQueries);
		datasource_meaty.addMapping(vNutriMeaty, httpQueryNutri);

		////////////////////////////////////////////////
		// Local storage used to store inferred facts //
		///////////////////////////////////////////////

		FactStorage local_storage = StorageBuilder.getSimpleInMemoryGraphStore();

		/////////////////////////////////////
		// Federation : "virtual" factbase //
		/////////////////////////////////////

		FederatedFactBase federation = new FederatedFactBase(local_storage);
		federation.addStorage(vMesureAtWeb, datasource_atWeb);
		federation.addStorage(vAlignmentAtWeb, datasource_atWeb);
		federation.addStorage(vNutriAtWeb, datasource_atWeb);
		federation.addStorage(vNutriMeaty, datasource_meaty);

		///////////////
		// Reasoning //
		///////////////

		// DLGP rule parsing
		// + iterate over all the strates
		// and execute a chase for each one
		// over the federation

		for(String filepath : new String[] {
				"src/main/resources/O2SolubilityINRAEUseCase/strate1_optim.dlgp",
				"src/main/resources/O2SolubilityINRAEUseCase/strate2_optim.dlgp",
		}) {
			Collection<FORule> rules = new ArrayList<FORule>();
			DlgpParser dlgp_parseur = new DlgpParser(new File(filepath), tf, pf);
			while (dlgp_parseur.hasNext()) {
				Object result = dlgp_parseur.next();
				if (result instanceof FORule) {
					rules.add((FORule)result);
				}
			}
			dlgp_parseur.close();

			RuleBase rb = new RuleBaseImpl(rules);

			ChaseBuilder.defaultBuilder(federation, rb, tf)
			.useDirectApplication()
			.useObliviousChecker()
			.useNaiveComputer()
			.useNaiveRuleScheduler()
			.build()
			.get()
			.execute();
		}

		///////////////////////////////////////
		// File export of the local factbase //
		///////////////////////////////////////

		File export_file = new File("src/main/resources/O2SolubilityINRAEUseCase/" + System.currentTimeMillis() + "_export.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(export_file));
		federation.getAtoms().forEach(a -> {
			try {
				bw.write(a.toString()+"\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		bw.flush();
		bw.close();
	}

}
