package use_cases;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.rdf4j.query.TupleQueryResult;

import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.io.dlgp.impl.builtin.DlgpParser;
import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.FederatedFactBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.wrapper.mappings.MappingDatasourceWrapper;
import fr.boreal.storage.wrapper.mappings.SPARQLMappingDatasourceWrapper;

public class BiocompositesINRAEUseCase {

	public static void main(String[] args) throws Exception {

		///////////////
		// Factories //
		///////////////

		TermFactory tf = SameObjectTermFactory.instance();
		PredicateFactory pf = SameObjectPredicateFactory.instance();

		////////////////////////////////////////
		// Triple store access using mappings //
		////////////////////////////////////////

		// native queries

		String sparqlQueryBiomass = Files.readString(Path.of("src/main/resources/BiocompositesINRAEUseCase/biomass_query.sparql"));
		String sparqlQueryComposite = Files.readString(Path.of("src/main/resources/BiocompositesINRAEUseCase/composite_query.sparql"));
		String sparqlQueryPHBV = Files.readString(Path.of("src/main/resources/BiocompositesINRAEUseCase/PHBV_query.sparql"));

		// endpoint url

		String endpoint_spoq = "https://ico.iate.inra.fr/graphdb/repositories/Composite_making_process";

		// relational view predicates

		Predicate vBiomass = pf.createOrGetPredicate("vBiomass", 3);
		Predicate vComposite = pf.createOrGetPredicate("vComposite", 5);
		Predicate vFiber = pf.createOrGetPredicate("vFiber", 3);

		// datasource creation

		MappingDatasourceWrapper<String, TupleQueryResult> datasource_spoq = new SPARQLMappingDatasourceWrapper(endpoint_spoq);

		datasource_spoq.addMapping(vBiomass, sparqlQueryBiomass);
		datasource_spoq.addMapping(vComposite, sparqlQueryComposite);
		datasource_spoq.addMapping(vFiber, sparqlQueryPHBV);

		////////////////////////////////////////////////
		// Local storage used to store inferred facts //
		///////////////////////////////////////////////

		FactStorage local_storage = StorageBuilder.getSimpleInMemoryGraphStore();

		/////////////////////////////////////
		// Federation : "virtual" factbase //
		/////////////////////////////////////

		FederatedFactBase federation = new FederatedFactBase(local_storage);
		federation.addStorage(vBiomass, datasource_spoq);
		federation.addStorage(vComposite, datasource_spoq);
		federation.addStorage(vFiber, datasource_spoq);

		///////////////
		// Reasoning //
		///////////////

		// DLGP rule parsing
		// + iterate over all the strates
		// and execute a chase for each one
		// over the federation

		for(String filepath : new String[] {
				"src/main/resources/BiocompositesINRAEUseCase/rules.dlgp"
		}) {
			Collection<FORule> rules = new ArrayList<FORule>();
			DlgpParser dlgp_parseur = new DlgpParser(new File(filepath), tf, pf);
			while (dlgp_parseur.hasNext()) {
				Object result = dlgp_parseur.next();
				if (result instanceof FORule) {
					rules.add((FORule)result);
				}
			}
			dlgp_parseur.close();

			RuleBase rb = new RuleBaseImpl(rules);

			ChaseBuilder.defaultBuilder(federation, rb, tf)
			.useDirectApplication()
			.useObliviousChecker()
			.useNaiveComputer()
			.useNaiveRuleScheduler()
			.build()
			.get()
			.execute();
		}

		///////////////////////////////////////
		// File export of the local factbase //
		///////////////////////////////////////

		File export_file = new File("src/main/resources/BiocompositesINRAEUseCase/" + System.currentTimeMillis() + "_export.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(export_file));
		federation.getAtoms().forEach(a -> {
			try {
				bw.write(a.toString()+"\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		bw.flush();
		bw.close();
	}

}
