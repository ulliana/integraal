import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCursor;

import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.io.api.ParseException;
import fr.boreal.io.dlgp.impl.builtin.DlgpParser;
import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.FederatedFactBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.storage.wrapper.mappings.MappingDatasourceWrapper;
import fr.boreal.storage.wrapper.mappings.MongoDBMappingDatasourceWrapper;

public class MongoDBTest {

	public static void main(String[] args) throws Exception {

		///////////////
		// Factories //
		///////////////

		TermFactory tf = SameObjectTermFactory.instance();
		PredicateFactory pf = SameObjectPredicateFactory.instance();

		/////////////////////////////////////
		// MongoDB querying using mappings //
		/////////////////////////////////////

		String mongoServer = "mongodb+srv://ftornil:florenttornil@cluster0.cxwlvfk.mongodb.net/?retryWrites=true&w=majority";
		String database = "sample_mflix";
		String collection = "comments";

		Predicate vMongo = pf.createOrGetPredicate("vMongo", 2);
		String query = "{$match: {name: {$exists: true} } }";

		MappingDatasourceWrapper<String, MongoCursor<Document>> datasource = new MongoDBMappingDatasourceWrapper(mongoServer, database, collection, List.of("name", "movie_id"));
		datasource.addMapping(vMongo, query);

		////////////////////////////////////////////////
		// Local storage used to store inferred facts //
		///////////////////////////////////////////////

		FactStorage local_storage = StorageBuilder.getSimpleInMemoryGraphStore();

		/////////////////////////////////////
		// Federation : "virtual" factbase //
		/////////////////////////////////////

		FederatedFactBase federation = new FederatedFactBase(local_storage);
		federation.addStorage(vMongo, datasource);

		///////////////
		// Reasoning //
		///////////////

		// DLGP rule parsing
		// and execute a chase
		// over the federation

		for(String filepath : new String[] {
				"src/main/resources/mongo/mongo.dlgp"
		}) {
			Collection<FORule> rules = new ArrayList<FORule>();
			DlgpParser dlgp_parseur = new DlgpParser(new File(filepath), tf, pf);
			while (dlgp_parseur.hasNext()) {
				try {
					Object result = dlgp_parseur.next();
					if (result instanceof FORule) {
						rules.add((FORule)result);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			dlgp_parseur.close();

			RuleBase rb = new RuleBaseImpl(rules);

			ChaseBuilder.defaultBuilder(federation, rb, tf)
			.useDirectApplication()
			.useObliviousChecker()
			.useNaiveComputer()
			.useNaiveRuleScheduler()
			.build()
			.get()
			.execute();
		}

		///////////////////////////////////////
		// File export of the local factbase //
		///////////////////////////////////////

		File export_file = new File("src/main/resources/mongo/mongo_" + + System.currentTimeMillis() + "_export.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(export_file));
		federation.getAtoms().forEach(a -> {
			try {
				bw.write(a.toString()+"\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		bw.flush();
		bw.close();
	}

}
