package fr.lirmm.graphik.util.stream;
import java.util.function.Function;

/**
 * This functional interface allows to not catch an Exception in a
 * lambda expression : use it at your own risk, the exception
 * becomes unchecked.
 * It is recommended to use it only in a method that can throw the 
 * Exception type that is unchecked. So the exception can be
 * checked when using the method.
 * 
 * Source : http://4comprehension.com/sneakily-throwing-exceptions-in-lambda-expressions-in-java/
 * 
 * @author Guillaume Pérution-Kihli
 */

@FunctionalInterface
public interface ThrowingFunction<T, R> {
    R apply(T t) throws Exception;

    @SuppressWarnings("unchecked")
    static <T extends Exception, R> R sneakyThrow(Exception t) throws T {
        throw (T) t;
    }

    static <T, R> Function<T, R> unchecked(ThrowingFunction<T, R> f) {
        return t -> {
            try {
                return f.apply(t);
            } catch (Exception ex) {
                return ThrowingFunction.sneakyThrow(ex);
            }
        };
    }
}
