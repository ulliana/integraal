package fr.lirmm.boreal.util.validator;

import java.util.Collection;

public interface Validator<T> {

	/**
	 * @return true iff all of the elements respect a implementation defined property
	 */
	default public boolean check(Collection<? extends T> elements) {
		return elements.stream().map(e -> this.check(e)).allMatch(b -> b);
	}
	
	/**
	 * @return true iff the element respect a implementation defined property
	 */
	public boolean check(T element);
	
}
