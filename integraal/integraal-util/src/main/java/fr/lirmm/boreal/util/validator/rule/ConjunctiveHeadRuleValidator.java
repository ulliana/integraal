package fr.lirmm.boreal.util.validator.rule;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.validator.Validator;

public class ConjunctiveHeadRuleValidator implements Validator<FORule> {

	@Override
	public boolean check(FORule rule) {
		Validator<FOFormula<Atom>> conjunction_checker = new ConjunctionFormulaValidator();
		return conjunction_checker.check(rule.getHead());
	}

}
