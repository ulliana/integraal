package fr.lirmm.boreal.util.converter;

import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.lirmm.graphik.graal.core.factory.DefaultPredicateFactory;

public class PredicateConverter {
	
	public static fr.lirmm.graphik.graal.api.core.Predicate convert(Predicate p) {
		return DefaultPredicateFactory.instance().create(p.getLabel(), p.getArity());
	}
	
	public static Predicate reverse(fr.lirmm.graphik.graal.api.core.Predicate p) {
		return SameObjectPredicateFactory.instance().createOrGetPredicate(p.getIdentifier().toString(), p.getArity());
	}

}
