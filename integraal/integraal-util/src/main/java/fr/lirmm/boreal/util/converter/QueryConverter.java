package fr.lirmm.boreal.util.converter;

import java.util.stream.Collectors;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;

public class QueryConverter {

	private static BiMap<FOQuery, ConjunctiveQuery> cache = HashBiMap.create();

	public static ConjunctiveQuery convert(FOQuery query) {
		ConjunctiveQuery q = cache.get(query);
		if(q == null) {
			q = DefaultConjunctiveQueryFactory.instance().create(
					AtomSetConverter.convert(query.getFormula()),
					query.getAnswerVariables().stream().map(TermConverter::convert).collect(Collectors.toList()));
			cache.put(query, q);
		}
		return q;

	}

	public static FOQuery reverse(ConjunctiveQuery query) {
		FOQuery q = cache.inverse().get(query);
		if(q == null) {
			q = FOQueryFactory.instance().createOrGetQuery(
					AtomSetConverter.reverse(query.getAtomSet()),
					query.getAnswerVariables().stream().map(TermConverter::reverse).map(t -> (Variable)t).collect(Collectors.toList())
					, null).orElse(null);
			//cache.inverse().put(query, q);
			// cannot use cache because hashCode on ConjunctiveQuery is not defined
		}
		return q;
	}

}
