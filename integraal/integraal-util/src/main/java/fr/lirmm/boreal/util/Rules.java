package fr.lirmm.boreal.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;

/**
 * Utility class for rules
 */
public final class Rules {
	
	/**
	 * Generate a set of mono-piece rules equivalent of the specified rule.
	 * 
	 * @param rule
	 * @return a Collection of Rule which is a decomposition of the specified rule to single piece rules.
	 */
	public static Collection<FORule> computeSinglePiece(FORule rule) {
		Collection<FORule> monoPiece = new LinkedList<FORule>();
		PiecesSplitter splitter = new PiecesSplitter(true, rule.getExistentials());
		
		for (Collection<Atom> piece : splitter.split(rule.getHead().flatten())) {
			Collection<FOFormula<Atom>> piece_as_formula = piece.stream()
					.map(a -> FOFormulaFactory.instance().createOrGetAtomic(a).get())
					.collect(Collectors.toSet());
			monoPiece.add(new FORuleImpl(
					rule.getBody(),
					FOFormulaFactory.instance().createOrGetConjunction(piece_as_formula).get()));
		}
		return monoPiece;
	}
	
	/**
	 * Create a new rule corresponding to the given rule by renaming all the variables with fresh variables
	 * 
	 * @param rule to rename
	 * @return a new rule corresponding to the given rule by renaming all the variables with fresh variables
	 */
	public static FORule freshRenaming(FORule rule) {
		TermFactory tf = SameObjectTermFactory.instance();
		Substitution s = new SubstitutionImpl();
		
		for(Variable v : FOFormulaUtil.getVariables(rule.getBody().getFormula())) {
			if(!s.keys().contains(v)) {
				s.add(v, tf.createOrGetFreshVariable());
			}
		}
		for(Variable v : FOFormulaUtil.getVariables(rule.getHead())) {
			if(!s.keys().contains(v)) {
				s.add(v, tf.createOrGetFreshVariable());
			}
		}
		
		return Rules.createImageWith(rule, s);
	}
	
	/**
	 * @return a new FORule corresponding to the given FORule in which the
	 *         given substitution have been applied
	 */
	public static FORule createImageWith(FORule r, Substitution s) {
		FOQuery body = FOQueryUtil.createImageWith(r.getBody(), s);
		FOFormula<Atom> head = FOFormulaUtil.createImageWith(r.getHead(), s);
		return new FORuleImpl(body, head);
	}

}
