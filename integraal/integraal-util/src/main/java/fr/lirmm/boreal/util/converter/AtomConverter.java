package fr.lirmm.boreal.util.converter;

import java.util.List;
import java.util.stream.Collectors;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.lirmm.graphik.graal.core.factory.DefaultAtomFactory;

public class AtomConverter {
	
	public static fr.lirmm.graphik.graal.api.core.Atom convert(Atom a) {
		fr.lirmm.graphik.graal.api.core.Predicate p = PredicateConverter.convert(a.getPredicate());
		List<fr.lirmm.graphik.graal.api.core.Term> terms = List.of(a.getTerms()).stream().map(t -> TermConverter.convert(t)).collect(Collectors.toList());
		return DefaultAtomFactory.instance().create(p, terms);
	}
	
	public static Atom reverse(fr.lirmm.graphik.graal.api.core.Atom a) {
		Predicate p = PredicateConverter.reverse(a.getPredicate());
		List<Term> terms = a.getTerms().stream().map(t -> TermConverter.reverse(t)).collect(Collectors.<Term>toList());
		return new AtomImpl(p, terms);
	}

}
