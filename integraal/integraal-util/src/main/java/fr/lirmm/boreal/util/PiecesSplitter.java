/**
 * 
 */
package fr.lirmm.boreal.util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * Split a set of atoms in pieces
 * A piece of an atom set is a set of atoms such that the atoms containing a variable v
 * are all in the same piece and the size of the pieces is minimal for this property
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
public class PiecesSplitter {
	private boolean includeGroundedAtoms;
	private Collection<Variable> existentialVariables;
	
	public PiecesSplitter() {
		this(true);
	}
	
	public PiecesSplitter(boolean includeGroundedAtoms) {
		this(true, null);
	}
	
	public PiecesSplitter(boolean includeGroundedAtoms, Collection<Variable> existentialVariables) {
		this.includeGroundedAtoms = includeGroundedAtoms;
		this.existentialVariables = existentialVariables;
	}

	public Collection<Collection<Atom>> split(Collection<Atom> toSplit) {
		List<Collection<Atom>> pieces = new ArrayList<Collection<Atom>>();
		
		if(this.existentialVariables == null) {
			this.existentialVariables = new HashSet<>();
			toSplit.forEach(atom -> this.existentialVariables.addAll(atom.getVariables()));
		}

		Collection<Variable> variables = this.existentialVariables;
		Set<Variable> varToTreat = new HashSet<Variable>();
		varToTreat.addAll(variables);
		
		while (!varToTreat.isEmpty()) {
			Variable root = varToTreat.iterator().next();
			Deque<Variable> queue = new ArrayDeque<>();
			queue.add(root);
			
			Collection<Atom> piece = new HashSet<Atom>();
			
			while (!queue.isEmpty()) {
				Variable v = queue.pollFirst();
				varToTreat.remove(v);
				
				Iterator<Atom> it = toSplit.iterator();
				while (it.hasNext()) {
					Atom a = it.next();
					if(a.contains(v)) {
						a.getVariables().stream()
								.filter(vv -> variables.contains(vv) && varToTreat.contains(vv))
								.distinct()
								.forEach(vv -> { queue.add(vv); varToTreat.remove(vv); });
						
						piece.add(a);
					}
				}
			}
			pieces.add(piece);
		}
		
		if (this.includeGroundedAtoms) {
			Iterator<Atom> it = toSplit.iterator();
			while (it.hasNext()) {
				Atom a = it.next();
				long nbVar = a.getVariables().stream()
						.filter(v -> variables.contains(v))
						.distinct()
						.count();
				
				// If the atom is not totally grounded
				if (nbVar == 0) {
					Collection<Atom> atomSet = new HashSet<Atom>();
					atomSet.add(a);
					pieces.add(atomSet);
				}
			}
		}
		
		return pieces;
	}
	
}
