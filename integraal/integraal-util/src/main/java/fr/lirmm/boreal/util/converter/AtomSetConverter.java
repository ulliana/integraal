package fr.lirmm.boreal.util.converter;

import java.util.HashSet;
import java.util.Set;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.util.stream.IteratorException;

public class AtomSetConverter {
	
	public static fr.lirmm.graphik.graal.api.core.InMemoryAtomSet convert(FOFormula<Atom> f) {
		fr.lirmm.graphik.graal.api.core.InMemoryAtomSet as = new fr.lirmm.graphik.graal.core.atomset.LinkedListAtomSet();
		f.flatten().forEach(a -> {
			as.add(AtomConverter.convert(a));
		});
		return as;
	}
	
	public static FOFormula<Atom> reverse(fr.lirmm.graphik.graal.api.core.InMemoryAtomSet as) {
		Set<Atom> atoms = new HashSet<Atom>();
		fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException<fr.lirmm.graphik.graal.api.core.Atom> it = as.iterator();
		while(it.hasNext()) {
			atoms.add(AtomConverter.reverse(it.next()));
		}
		return FOFormulaFactory.instance().createOrGetConjunction(atoms.toArray(new Atom[atoms.size()])).orElse(null);
	}

}
