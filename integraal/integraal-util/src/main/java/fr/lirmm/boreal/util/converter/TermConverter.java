package fr.lirmm.boreal.util.converter;


import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.lirmm.graphik.graal.core.term.DefaultTermFactory;

public class TermConverter {

	public static fr.lirmm.graphik.graal.api.core.Term convert(Term t) {
		if(t instanceof Constant) {
			Constant c = (Constant)t;
			return DefaultTermFactory.instance().createConstant(c.getLabel());
		} else if(t instanceof Variable) {
			Variable v = (Variable)t;
			return DefaultTermFactory.instance().createVariable(v.getLabel());
		} else {
			return null;
		}
	}

	public static Term reverse(fr.lirmm.graphik.graal.api.core.Term t) {
		if(t instanceof fr.lirmm.graphik.graal.api.core.Constant) {
			fr.lirmm.graphik.graal.api.core.Constant c = (fr.lirmm.graphik.graal.api.core.Constant)t;
			return SameObjectTermFactory.instance().createOrGetConstant(c.getLabel());
		} else if(t instanceof fr.lirmm.graphik.graal.api.core.Variable) {
			fr.lirmm.graphik.graal.api.core.Variable v = (fr.lirmm.graphik.graal.api.core.Variable)t;
			return SameObjectTermFactory.instance().createOrGetVariable(v.getLabel());
		} else {
			return null;
		}
	}

}
