package fr.lirmm.boreal.util;

import java.util.ArrayList;
import java.util.Collection;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;

/**
 * Utility class for First Order Queries
 */
public class FOQueryUtil {
	
	/**
	 * constants or literals affected to answer variables by the given substitution are put in the initial substitution
	 * variable renaming is directly applied
	 * @return a new FOFormula corresponding to the given FOFormula in which the
	 *         given substitution have been applied
	 */
	public static FOQuery createImageWith(FOQuery query, Substitution s) {
		FOFormula<Atom> formula = FOFormulaUtil.createImageWith(query.getFormula(), s);
		Substitution initial_substitution = new SubstitutionImpl();
		Collection<Variable> answer_variables = new ArrayList<Variable>();
		for(Variable v : query.getAnswerVariables()) {
			if(s.keys().contains(v)) {
				Term substitut = s.createImageOf(v);
				if(!substitut.isVariable()) {
					initial_substitution.add(v, substitut);
				} else {
					answer_variables.add((Variable)substitut);
				}
			} else {
				answer_variables.add(v);
			}
		}
		return FOQueryFactory.instance().createOrGetQuery(formula, answer_variables, initial_substitution).orElse(null);
	}

}
