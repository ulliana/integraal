package fr.lirmm.boreal.util.converter;


import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.model.rule.impl.FORuleImpl;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.core.factory.DefaultRuleFactory;

public class RuleConverter {
	
	private static BiMap<FORule, Rule> cache = HashBiMap.create();

	public static Rule convert(FORule rule) {
		Rule r = cache.get(rule);
		if(r == null) {
			r = DefaultRuleFactory.instance().create(
					AtomSetConverter.convert(rule.getBody().getFormula()),
					AtomSetConverter.convert(rule.getHead()));
			cache.put(rule, r);
		}
		return r;
		
	}
	
	public static FORule reverse(Rule rule) {
		FORule r = cache.inverse().get(rule);
		if(r == null) {
			r = new FORuleImpl(
					FOQueryFactory.instance().createOrGetQuery(AtomSetConverter.reverse(rule.getBody()), null, null).orElse(null),
					AtomSetConverter.reverse(rule.getHead()));
			cache.inverse().put(rule, r);
		}
		return r;
	}

}
