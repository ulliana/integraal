package fr.lirmm.boreal.util.validator.rule;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.logicalElements.api.Atom;
import fr.lirmm.boreal.util.validator.Validator;

public class ConjunctionFormulaValidator implements Validator<FOFormula<Atom>> {

	@Override
	public boolean check(FOFormula<Atom> formula) {
		if(formula.isConjunction()) {
			Collection<? extends FOFormula<Atom>> subqueries = ((FOFormulaConjunction<Atom>) formula).getSubElements();
			return this.check(subqueries);
		} else {
			return formula.isAtomic();
		}
	}

}
