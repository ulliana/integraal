package fr.lirmm.boreal.util.validator.rule;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.logicalElements.api.Atom;
import fr.lirmm.boreal.util.validator.Validator;

public class PositiveFormulaValidator implements Validator<FOFormula<Atom>> {

	@Override
	public boolean check(FOFormula<Atom> element) {
		if(element.isConjunction()) {
			return this.check(((FOFormulaConjunction<Atom>) element).getSubElements());
		} else if(element.isDisjunction()) {
			return this.check(((FOFormulaDisjunction<Atom>) element).getSubElements());
		} else if(element.isNegation()) {
			return false;
		} else if(element.isAtomic()) {
			return true;
		}
		
		// fallback for unknown types
		return false;
	}

}
