package fr.lirmm.boreal.util.validator.data;

import fr.boreal.model.logicalElements.api.Atom;
import fr.lirmm.boreal.util.validator.Validator;

public class ConstantOnlyAtomValidator implements Validator<Atom> {

	@Override
	public boolean check(Atom element) {
		return element.getVariables().isEmpty();
	}

}
