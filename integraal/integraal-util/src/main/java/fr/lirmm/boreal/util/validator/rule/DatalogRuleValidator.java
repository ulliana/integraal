package fr.lirmm.boreal.util.validator.rule;

import fr.boreal.model.rule.api.FORule;
import fr.lirmm.boreal.util.validator.Validator;

public class DatalogRuleValidator implements Validator<FORule> {

	@Override
	public boolean check(FORule element) {
		return element.getExistentials().isEmpty();
	}

}
