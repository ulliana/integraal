package fr.boreal.model.functions;

import java.util.Optional;

import fr.boreal.model.logicalElements.api.Term;

public interface Invoker {

	public Optional<Term> invoke(Term... args);
}
