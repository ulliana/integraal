package fr.boreal.model.logicalElements.impl.identityObjects;

public class IdentityFreshVariableImpl extends IdentityVariableImpl {

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public IdentityFreshVariableImpl(String label) {
		super(label);
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public String toString() {
		return super.toString();
	}
}
