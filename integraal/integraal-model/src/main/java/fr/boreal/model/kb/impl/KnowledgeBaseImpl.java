package fr.boreal.model.kb.impl;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.KnowledgeBase;
import fr.boreal.model.kb.api.RuleBase;

public class KnowledgeBaseImpl implements KnowledgeBase {

	FactBase factbase;
	RuleBase rulebase;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public KnowledgeBaseImpl(FactBase factbase, RuleBase rulebase) {
		this.factbase = factbase;
		this.rulebase = rulebase;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public FactBase getFactBase() {
		return this.factbase;
	}

	@Override
	public RuleBase getRuleBase() {
		return this.rulebase;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getFactBase().hashCode();
		result = prime * result + this.getRuleBase().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.factbase.toString());
		sb.append("\n");
		sb.append(this.rulebase.toString());
		return sb.toString();
	}

}
