package fr.boreal.model.logicalElements.api;

public interface Literal<T> extends Term {
	
	@Override
	default boolean isLiteral() {
		return true;
	}
	
	T getValue();

}
