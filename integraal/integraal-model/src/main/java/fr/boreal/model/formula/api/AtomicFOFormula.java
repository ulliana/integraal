package fr.boreal.model.formula.api;

import java.util.Collection;
import java.util.Set;

public interface AtomicFOFormula<T> extends FOFormula<T> {

	@Override
	default public boolean isAtomic() {
		return true;
	}

	public T getElement();

	@Override
	default Collection<T> flatten() throws UnsupportedOperationException {
		return Set.of(this.getElement());
	}
}
