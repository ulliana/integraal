package fr.boreal.model.logicalElements.factory.impl;

import java.util.HashMap;
import java.util.Map;

import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityConstantImpl;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityFreshVariableImpl;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityLiteralImpl;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityVariableImpl;

/**
 * This factory creates Terms For each call at a same method with the same
 * parameters, the same java object will be returned. Therefore, the equality
 * test between those objects can be restricted to java object reference
 * comparison (==). Please make sure the terms implementations used are
 * immutable otherwise there could be unwanted side effects.
 */
public class SameObjectTermFactory implements TermFactory {

	private static final TermFactory INSTANCE = new SameObjectTermFactory();
	private static final String FRESH_PREFIX = "Graal:EE";

	private Map<String, Constant> constants = new HashMap<String, Constant>();
	private Map<Object, Literal<?>> literals = new HashMap<Object, Literal<?>>();
	private Map<String, Variable> variables = new HashMap<String, Variable>();

	private int fresh_counter = 0;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public static TermFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Constant createOrGetConstant(String label) {
		Constant result = this.constants.get(label);
		if (result == null) {
			result = new IdentityConstantImpl(label);
			this.constants.put(label, result);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Literal<T> createOrGetLiteral(T value) {
		Literal<T> result = (Literal<T>) this.literals.get(value);
		if (result == null) {
			result = new IdentityLiteralImpl<T>(value);
			this.literals.put(value, result);
		}
		return result;
	}

	@Override
	public Variable createOrGetVariable(String label) {
		Variable result = this.variables.get(label);
		if (result == null) {
			result = new IdentityVariableImpl(label);
			this.variables.put(label, result);
		}
		return result;
	}

	@Override
	public Variable createOrGetFreshVariable() {
		return new IdentityFreshVariableImpl(FRESH_PREFIX + ++fresh_counter);
	}

}
