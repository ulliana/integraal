package fr.boreal.model.queryEvaluation.api;

import java.util.Iterator;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;

/**
 * An FOQuery evaluator is one possible algorithm to evaluate a {@link FOQuery}.
 * 
 * Evaluating a FOQuery means retrieving all the {@link Substitution} of the answer variables from the query
 * with respect to the atoms from the {@link FactBase}.
 * 
 * @param <Q> is the (sub)type of {@link FOQuery} that the algorithm implementation can handle.
*/
public interface FOQueryEvaluator<Q extends FOQuery> {

	/**
	 * @param query {@link FOQuery} to evaluate
	 * @param factbase {@link FactBase} in which atoms are stored
	 * @return an {@link Iterator} of {@link Substitution}
	 * over all the answers of the given query in the given factbase
	 * with respect to the query answer variables.
	 */
	public Iterator<Substitution> evaluate(Q query, FactBase factbase);
	
	/**
	 * @param query {@link FOQuery} to evaluate
	 * @param factbase {@link FactBase} in which atoms are stored
	 * @return true iff there exist a substitution s that is an answer to the query on the factbase
	 */
	default boolean exist(Q query, FactBase factbase) {
		return this.evaluate(query, factbase).hasNext();
	}

}
