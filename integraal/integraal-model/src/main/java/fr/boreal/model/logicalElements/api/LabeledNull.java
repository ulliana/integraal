package fr.boreal.model.logicalElements.api;

public interface LabeledNull extends Term {
	
	@Override
	default boolean isLabeledNull() {
		return true;
	}

}
