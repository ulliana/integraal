package fr.boreal.model.logicalElements.api;

import java.util.Collection;
import java.util.Optional;

public interface Substitution {

	Term createImageOf(Term term);

	Collection<Variable> keys();

	void add(Variable v, Term t);

	Atom createImageOf(Atom atom);

	Optional<Substitution> merged(Substitution other);

	Substitution limitedTo(Collection<Variable> vars);

	boolean isExtentionOf(Substitution other);

	void remove(Variable v);

	boolean isEmpty();
}
