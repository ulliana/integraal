package fr.boreal.model.query.impl;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQueryNegation;

public class FOQueryNegationImpl extends AbstractFOQuery implements FOQueryNegation {

	FOFormulaNegation<Atom> formula;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FOQueryNegationImpl(FOFormulaNegation<Atom> formula, Collection<Variable> answer_variables,
			Substitution initial_substitution) {
		super(formula, answer_variables, initial_substitution);
		this.formula = formula;
	}

	@Override
	public FOFormulaNegation<Atom> getFormula() {
		return this.formula;
	}
}
