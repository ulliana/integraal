package fr.boreal.model.logicalElements.impl;

import fr.boreal.model.logicalElements.api.Constant;

public class ConstantImpl implements Constant {

	private String label;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public ConstantImpl(String label) {
		this.label = label;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public String getLabel() {
		return this.label;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return this.label.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof Constant) {
			Constant other = (Constant) o;
			return this.getLabel().equals(other.getLabel());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.label;
	}
}
