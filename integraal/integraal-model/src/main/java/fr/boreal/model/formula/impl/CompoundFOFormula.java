package fr.boreal.model.formula.impl;

import java.util.Collection;
import java.util.stream.Collectors;

import fr.boreal.model.formula.api.FOFormula;

public abstract class CompoundFOFormula<T> implements FOFormula<T> {

	private Collection<FOFormula<T>> subformulas;

	private Collection<T> flat;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public CompoundFOFormula(Collection<FOFormula<T>> subformulas) {
		this.subformulas = subformulas;
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	public abstract String getConnectorRepresentation();

	public Collection<FOFormula<T>> getSubElements() {
		return this.subformulas;
	}

	@Override
	public Collection<T> flatten() {
		if (this.flat == null) {
			this.flat = this.getSubElements().stream().flatMap(f -> f.flatten().stream())
					.collect(Collectors.<T>toSet());
		}
		return this.flat;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
//		sb.append(" (");
		boolean first = true;
		for (FOFormula<T> q : this.getSubElements()) {
			if (!first) {
				sb.append(" ");
				sb.append(this.getConnectorRepresentation());
				sb.append(" ");
			}
			sb.append(q.toString());
			first = false;
		}
//		sb.append(")");
		return sb.toString();
	}
}