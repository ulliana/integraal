package fr.boreal.model.logicalElements.factory.impl;

import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.impl.PredicateImpl;

public class PredicateFactoryImpl implements PredicateFactory {

	private static final PredicateFactory INSTANCE = new PredicateFactoryImpl();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public static PredicateFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Predicate createOrGetPredicate(String label, int arity) {
		return new PredicateImpl(label, arity);
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

}
