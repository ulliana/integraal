package fr.boreal.model.kb.api;

import java.util.Collection;

import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.rule.api.FORule;

public interface RuleBase {

	Collection<FORule> getRules();

	Collection<FORule> getRulesByHeadPredicate(Predicate p);

	Collection<FORule> getRulesByBodyPredicate(Predicate p);

}
