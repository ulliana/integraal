package fr.boreal.model.logicalElements.impl;

import fr.boreal.model.logicalElements.api.Literal;

public class LiteralImpl<T> implements Literal<T> {

	private T value;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public LiteralImpl(T value) {
		this.value = value;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public T getValue() {
		return this.value;
	}

	@Override
	public String getLabel() {
		return this.value.toString();
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return this.value.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof Literal) {
			Literal<?> other = (Literal<?>) o;
			return this.getValue().equals(other.getValue());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.value.toString();
	}
}
