package fr.boreal.model.logicalElements.factory.impl;

import java.util.HashMap;
import java.util.Map;

import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.impl.identityObjects.IdentityPredicateImpl;

public class SameObjectPredicateFactory implements PredicateFactory {

	private static final PredicateFactory INSTANCE = new SameObjectPredicateFactory();

	private Map<String, Predicate> predicates = new HashMap<String, Predicate>();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public static PredicateFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Predicate createOrGetPredicate(String label, int arity) {
		Predicate result = this.predicates.get(label);
		if (result == null) {
			result = new IdentityPredicateImpl(label, arity);
			this.predicates.put(label, result);
		} else if (arity != result.getArity()) {
			throw new IllegalArgumentException("Cannot create a predicate " + label + " of arity " + arity
					+ " because a predicate with the same label of arity " + result.getArity() + " already exists.");
		}
		return result;
	}

}
