package fr.boreal.model.functions;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Optional;
import java.util.stream.Stream;

import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.ConstantImpl;

public class JavaMethodInvoker implements Invoker {

	private Method method;
	private int method_arg_count;

	private Class<?> last_arg_type;

	private TermFactory termFactory;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public JavaMethodInvoker(TermFactory termFactory, String class_file_path, String class_full_name,
			String method_name) throws ClassNotFoundException, IOException, NoSuchMethodException {

		this.termFactory = termFactory;

		File class_file = new File(class_file_path);
		URL url = class_file.toURI().toURL();
		URLClassLoader loader = new URLClassLoader(new URL[] { url });

		Class<?> invoked_class = loader.loadClass(class_full_name);
		loader.close();

		Optional<Method> opt_m = Stream.of(invoked_class.getMethods()).filter(m -> m.getName().equals(method_name))
				.findAny();
		if (opt_m.isPresent()) {
			this.method = opt_m.get();
			this.method_arg_count = method.getParameterCount();
			this.last_arg_type = this.method.getParameterTypes()[this.method_arg_count - 1];
		} else {
			throw new NoSuchMethodException("Method " + method_name + " does not exist for class " + class_full_name);
		}
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Optional<Term> invoke(Term... terms) {

		Object[] args = new Object[terms.length];

		for (int i = 0; i < terms.length; ++i) {
			if (terms[i] instanceof Literal<?>) {
				args[i] = ((Literal<?>)terms[i]).getValue();
			} else {
				args[i] = terms[i].getLabel();
			}
		}

		// If the last argument of the method is an array
		// We need to group all the leftovers in an array
		// The resulting array will be typed with Object[]
		// Therefore the method can only use this same type to define the array
		if (last_arg_type.isArray()) {

			Object[] final_args = new Object[this.method_arg_count];

			for (int i = 0; i < this.method_arg_count - 1; i++) {
				final_args[i] = args[i];
			}

			Object[] last_arg = new Object[(args.length - this.method_arg_count) + 1];
			for (int i = 0; i < last_arg.length; i++) {
				last_arg[i] = args[this.method_arg_count - 1 + i];
			}
			final_args[final_args.length - 1] = last_arg;
			args = final_args;
		}

		try {
			Object result = this.method.invoke(null, args);

			if (result instanceof Boolean) {
				return Optional.ofNullable(termFactory.<Boolean>createOrGetLiteral((Boolean)result));
			} else if (result instanceof Integer) {
				return Optional.ofNullable(termFactory.<Integer>createOrGetLiteral((Integer)result));
			} else if (result instanceof String) {
				return Optional.ofNullable(termFactory.<String>createOrGetLiteral((String)result));
			} else if (result instanceof Double) {
				return Optional.ofNullable(termFactory.<Double>createOrGetLiteral((Double)result));
			} else {
				return Optional.ofNullable(new ConstantImpl(result.toString()));
			}

		} catch (IllegalAccessException e) {
			System.err.println("Tried to call method " + method.getName() + " which is not accessible.");
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			System.err.println("Tried to call method " + method.getName() + " with arguments " + args
					+ " which are of an incorrect type.");
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			System.err.println("Tried to call method " + method.getName() + " which thrown an exception.");
			e.printStackTrace();
		}
		return Optional.empty();
	}

}
