package fr.boreal.model.query.api;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * This interface represents a first order Query It is designed as a tree using
 * the Composite design pattern The node elements of the tree are
 * {@link FOQuery} The leaf elements of the tree are {@link AtomicFOQueryImpl} The
 * non-leaf elements of the tree are either a {@link FOQueryConjunctionImpl} or a
 * {@link FOQueryDisjunctionImpl} or a {@link FOQueryNegationImpl}
 */
public interface FOQuery extends Query {

	/**
	 * @return a collection of variable representing the answer variables of this
	 *         step in the first order query The answer variables must be coherent
	 *         with respect to the coherent definition of answer variable from each
	 *         sub-interface Answer variables from a {@link AtomicFOQueryImpl} must be
	 *         part of the variables from the atom or in the initial substitution.
	 *         Answer variables from a {@link FOQueryConjunctionImpl} must be part of
	 *         the variables from at least one of the sub-queries or in the initial
	 *         substitution. Answer variables from a {@link FOQueryDisjunctionImpl} must
	 *         be part of the common variables of all the sub-queries or in the
	 *         initial substitution. Answer variables from a {@link FOQueryNegationImpl}
	 *         are not defined.
	 */
	@Override
	Collection<Variable> getAnswerVariables();

	/**
	 * @return the initial substitution associated to this step of the first order
	 *         query This is used to associate a default value to a variable (ie: X
	 *         = a) with the same meaning as SELECT 'a' as X WHERE ... in SparQL
	 */
	@Override
	Substitution getInitialSubstitution();

	/**
	 * @return the first order formula that represents this first order query
	 */
	FOFormula<Atom> getFormula();

}
