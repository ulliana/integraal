package fr.boreal.model.kb.api;

public enum FactBaseType {
	GRAAL, RDBMS, ENDPOINT, MONGO, WEBAPI
}
