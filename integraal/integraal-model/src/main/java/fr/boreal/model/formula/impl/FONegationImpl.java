package fr.boreal.model.formula.impl;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaNegation;

public class FONegationImpl<T> implements FOFormulaNegation<T> {

	private FOFormula<T> element;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FONegationImpl(FOFormula<T> element) {
		this.element = element;
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public FOFormula<T> getElement() {
		return this.element;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FOFormulaNegation<?>) {
			FOFormulaNegation<?> other = (FOFormulaNegation<?>)o;
			return this.getElement().equals(other.getElement());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "not( " + this.getElement().toString() + ")";
	}

	@Override
	public int hashCode() {
		return this.element.hashCode() * 11;
	}
}
