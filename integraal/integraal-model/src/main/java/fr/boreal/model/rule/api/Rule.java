package fr.boreal.model.rule.api;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.Query;

public interface Rule {

	Query getBody();

	FOFormula<Atom> getHead();

	Collection<Variable> getFrontier();

	Collection<Variable> getExistentials();

}
