package fr.boreal.model.logicalElements.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.ComputedAtom;
import fr.boreal.model.logicalElements.api.FunctionalTerm;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;

public class SubstitutionImpl implements Substitution {

	private Map<Variable, Term> map;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public SubstitutionImpl() {
		this.map = new HashMap<Variable, Term>();
	}

	public SubstitutionImpl(Map<Variable, Term> associations) {
		this.map = associations;
	}

	public SubstitutionImpl(Substitution s) {
		this(new HashMap<Variable, Term>(s.keys().size()));
		for (Variable v : s.keys()) {
			this.add(v, s.createImageOf(v));
		}
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Term createImageOf(Term term) {
		Term substitut = null;
		if (term instanceof FunctionalTerm) {
			FunctionalTerm f = (FunctionalTerm)term;
			substitut = f.eval(this);
		}

		if (substitut == null) {
			substitut = this.map.get(term);
		}

		return (substitut == null) ? term : substitut;
	}

	@Override
	public Atom createImageOf(Atom atom) {
		Atom substitut = null;

		if (atom instanceof ComputedAtom) {
			substitut = ((ComputedAtom)atom).eval(this);
		}

		if (substitut == null) {
			List<Term> res = new ArrayList<Term>();
			for (int i = 0; i < atom.getPredicate().getArity(); i++) {
				Term t = atom.getTerm(i);
				res.add(this.createImageOf(t));
			}
			substitut = new AtomImpl(atom.getPredicate(), res);
		}

		return (substitut == null) ? atom : substitut;
	}

	@Override
	public Collection<Variable> keys() {
		return this.map.keySet();
	}

	@Override
	public void add(Variable v, Term t) {
		this.map.put(v, t);
	}

	@Override
	public void remove(Variable v) {
		this.map.remove(v);
	}

	@Override
	public boolean isEmpty() {
		return this.map.isEmpty();
	}

	@Override
	public Optional<Substitution> merged(Substitution other) {
		if (this.isEmpty()) {
			return Optional.of(other);
		}
		if (other.isEmpty()) {
			return Optional.of(this);
		}

		Substitution result = new SubstitutionImpl(this);
		for (Variable current_variable : other.keys()) {
			Term b_image = other.createImageOf(current_variable);
			if (result.keys().contains(current_variable)) {
				Term current_image = result.createImageOf(current_variable);
				if (!current_image.equals(b_image)) {
					return Optional.empty();
				}
			} else {
				result.add(current_variable, b_image);
			}
		}
		return Optional.of(result);
	}

	@Override
	public Substitution limitedTo(Collection<Variable> vars) {
		Substitution result = new SubstitutionImpl();
		for (Variable v : this.keys()) {
			if (vars.contains(v)) {
				result.add(v, this.createImageOf(v));
			}
		}
		return result;
	}

	@Override
	public boolean isExtentionOf(Substitution other) {
		Substitution limited_copy = this.limitedTo(other.keys());
		return limited_copy.equals(other);
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return this.map.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof SubstitutionImpl) {
			return this.map.equals(((SubstitutionImpl)o).map);
		} else {
			return false;
		}
	}

	/**
	 * @return a string representation of this substitution of the form {v1:t1, ...,
	 *         vn:tn}
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		boolean first = true;
		for (Variable v : this.keys()) {
			if (!first) {
				sb.append(", ");
			}
			sb.append(v.toString());
			sb.append(":");
			sb.append(this.map.get(v));
			first = false;
		}
		sb.append("}");
		return sb.toString();
	}
}
