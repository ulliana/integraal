package fr.boreal.model.logicalElements.factory.impl;

import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.ConstantImpl;
import fr.boreal.model.logicalElements.impl.FreshVariableImpl;
import fr.boreal.model.logicalElements.impl.LiteralImpl;
import fr.boreal.model.logicalElements.impl.VariableImpl;

public class TermFactoryImpl implements TermFactory {

	private static final TermFactory INSTANCE = new TermFactoryImpl();
	private static final String FRESH_PREFIX = "Graal:EE";

	private int fresh_counter = 0;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public static TermFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Constant createOrGetConstant(String label) {
		return new ConstantImpl(label);
	}

	@Override
	public <T> Literal<T> createOrGetLiteral(T value) {
		return new LiteralImpl<T>(value);
	}

	@Override
	public Variable createOrGetVariable(String label) {
		return new VariableImpl(label);
	}

	@Override
	public Variable createOrGetFreshVariable() {
		return new FreshVariableImpl(FRESH_PREFIX + ++fresh_counter);
	}

}
