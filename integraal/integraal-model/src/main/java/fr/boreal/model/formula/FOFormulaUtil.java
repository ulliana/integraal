package fr.boreal.model.formula;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;

public class FOFormulaUtil {

	private static FOFormulaFactory formula_factory = FOFormulaFactory.instance();

	/**
	 * @return a new FOFormula corresponding to the given FOFormula in which the
	 *         given substitution have been applied
	 */
	public static FOFormula<Atom> createImageWith(FOFormula<Atom> formula, Substitution s) {
		if (formula.isAtomic()) {
			Atom substituted_atom = s.createImageOf(((AtomicFOFormula<Atom>) formula).getElement());
			return formula_factory.createOrGetAtomic(substituted_atom).orElseGet(null);
		} else if (formula.isNegation()) {
			FOFormula<Atom> new_element = FOFormulaUtil
					.createImageWith(((FOFormulaNegation<Atom>) formula).getElement(), s);
			return formula_factory.createOrGetNegation(new_element).orElseGet(null);
		} else if (formula.isConjunction()) {
			Collection<FOFormula<Atom>> new_elements = ((FOFormulaConjunction<Atom>) formula).getSubElements().stream()
					.map(f -> FOFormulaUtil.createImageWith(f, s)).collect(Collectors.toList());
			return formula_factory.createOrGetConjunction(new_elements).orElseGet(null);
		} else if (formula.isDisjunction()) {
			Collection<FOFormula<Atom>> new_elements = ((FOFormulaDisjunction<Atom>) formula).getSubElements().stream()
					.map(f -> FOFormulaUtil.createImageWith(f, s)).collect(Collectors.toList());
			return formula_factory.createOrGetDisjunction(new_elements).orElseGet(null);
		}
		return null;
	}

	/**
	 * @return all the variables of the given formula
	 */
	public static Collection<Variable> getVariables(FOFormula<Atom> formula) {
		if (formula.isAtomic()) {
			return ((AtomicFOFormula<Atom>) formula).getElement().getVariables();
		} else if (formula.isNegation()) {
			return FOFormulaUtil.getVariables(((FOFormulaNegation<Atom>) formula).getElement());
		} else if (formula.isConjunction()) {
			return ((FOFormulaConjunction<Atom>) formula).getSubElements().stream().map(FOFormulaUtil::getVariables)
					.flatMap(Collection::stream).collect(Collectors.toSet());
		} else if (formula.isDisjunction()) {
			return ((FOFormulaDisjunction<Atom>) formula).getSubElements().stream().map(FOFormulaUtil::getVariables)
					.flatMap(Collection::stream).collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}

	/**
	 * @return all the terms of the given formula
	 */
	public static Collection<Term> getTerms(FOFormula<Atom> formula) {
		if (formula.isAtomic()) {
			return Arrays.asList(((AtomicFOFormula<Atom>) formula).getElement().getTerms());
		} else if (formula.isNegation()) {
			return FOFormulaUtil.getTerms(((FOFormulaNegation<Atom>) formula).getElement());
		} else if (formula.isConjunction()) {
			return ((FOFormulaConjunction<Atom>) formula).getSubElements().stream().map(FOFormulaUtil::getTerms)
					.flatMap(Collection::stream).collect(Collectors.toSet());
		} else if (formula.isDisjunction()) {
			return ((FOFormulaDisjunction<Atom>) formula).getSubElements().stream().map(FOFormulaUtil::getTerms)
					.flatMap(Collection::stream).collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}

	/**
	 * @param formula the atoms to order which may be compatible with flatten
	 * @return all the atoms in the given formula in an order avoiding cartesian products when possible
	 */
	public static LinkedHashSet<Atom> getOrderedAtoms(FOFormula<Atom> formula) {
		Collection<Atom> atoms = formula.flatten();
		LinkedHashSet<Atom> result = new LinkedHashSet<Atom>();
		Queue<Atom> toCheck = new ArrayDeque<Atom>();
		while (!atoms.isEmpty()) {
			Iterator<Atom> atomsIt = atoms.iterator();
			Atom atom1;
			if(toCheck.isEmpty()) {
				atom1 = atomsIt.next();
				atomsIt.remove();
			} else {
				atom1 = toCheck.poll();
			}
			result.add(atom1);
			while (atomsIt.hasNext()) {
				Atom atom2 = atomsIt.next();
				for (Variable v : atom2.getVariables()) {
					if (atom1.getVariables().contains(v)) {
						boolean alreadySeen = result.add(atom2);
						if(!alreadySeen) {
							toCheck.add(atom2);
						}
						atomsIt.remove();
						toCheck.add(atom2);
						break;
					}
				}
			}
		}
		return result;
	}
}
