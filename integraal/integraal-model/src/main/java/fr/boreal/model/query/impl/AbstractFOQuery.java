package fr.boreal.model.query.impl;

import java.util.Arrays;
import java.util.Collection;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;

public abstract class AbstractFOQuery implements FOQuery {

	private FOFormula<Atom> formula;
	private Collection<Variable> answer_variables;
	private Substitution initial_substitution;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public AbstractFOQuery(FOFormula<Atom> formula, Collection<Variable> answer_variables,
			Substitution initial_substitution) {
		this.formula = formula;

		if (answer_variables == null) {
			answer_variables = FOFormulaUtil.getVariables(formula);
		}
		this.answer_variables = answer_variables;

		if (initial_substitution == null) {
			initial_substitution = new SubstitutionImpl();
		}
		this.initial_substitution = initial_substitution;
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public Collection<Variable> getAnswerVariables() {
		return this.answer_variables;
	}

	@Override
	public Substitution getInitialSubstitution() {
		return this.initial_substitution;
	}

	@Override
	public FOFormula<Atom> getFormula() {
		return this.formula;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FOQuery) {
			FOQuery other = (FOQuery)o;
			return Arrays.equals(this.getAnswerVariables().toArray(), other.getAnswerVariables().toArray())
					&& this.getInitialSubstitution().equals(other.getInitialSubstitution())
					&& this.getFormula().equals(other.getFormula());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(this.getAnswerVariables().toArray()) * this.getInitialSubstitution().hashCode()
				* this.getFormula().hashCode();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("?(");
		boolean first = true;
		for (Variable v : this.answer_variables) {
			if (!first) {
				sb.append(", ");
			}
			sb.append(v.toString());
			first = false;
		}
		sb.append(")");

		sb.append(this.initial_substitution.toString());
		sb.append(" ");
		sb.append(this.formula.toString());

		return sb.toString();
	}

}
