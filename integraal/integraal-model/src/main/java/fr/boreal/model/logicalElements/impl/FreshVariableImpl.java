package fr.boreal.model.logicalElements.impl;

public class FreshVariableImpl extends VariableImpl {

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FreshVariableImpl(String label) {
		super(label);
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return super.hashCode() * 31;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FreshVariableImpl) {
			FreshVariableImpl other = (FreshVariableImpl)o;
			return this.getLabel().equals(other.getLabel());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
