package fr.boreal.model.formula.api;

import java.util.Collection;

public interface FOFormulaNegation<T> extends FOFormula<T> {

	@Override
	default public boolean isNegation() {
		return true;
	}

	public FOFormula<T> getElement();

	@Override
	default Collection<T> flatten() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Flattening a negation would result in a incorrect set");
	}
}
