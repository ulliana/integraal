package fr.boreal.model.logicalElements.api;

public interface Variable extends Term {
	
	@Override
	default boolean isVariable() {
		return true;
	}

}
