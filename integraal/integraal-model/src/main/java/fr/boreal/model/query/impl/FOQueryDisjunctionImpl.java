package fr.boreal.model.query.impl;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQueryDisjunction;

public class FOQueryDisjunctionImpl extends AbstractFOQuery implements FOQueryDisjunction {

	FOFormulaDisjunction<Atom> formula;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FOQueryDisjunctionImpl(FOFormulaDisjunction<Atom> formula, Collection<Variable> answer_variables,
			Substitution initial_substitution) {
		super(formula, answer_variables, initial_substitution);
		this.formula = formula;
	}

	@Override
	public FOFormulaDisjunction<Atom> getFormula() {
		return this.formula;
	}
}
