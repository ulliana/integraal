package fr.boreal.model.mediation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.query.api.FOQuery;

/**
 * Converts a FOQuery to a query Plan using the given PlanFactory (system specific)
 */
public class FOQueryToPlanFactory {

	private PlanFactory factory;
	private FactBase fb;

	public FOQueryToPlanFactory(PlanFactory factory, FactBase fb) {
		this.factory = factory;
		this.fb = fb;
	}

	public Plan convert(FOQuery query) {
		Plan plan = this.convert(query.getFormula());
		plan = this.handleSelection(plan, query.getInitialSubstitution());
		plan = this.handleProjection(plan, query.getAnswerVariables());
		plan = this.factory.distinct(plan);
		return plan;
	}

	//
	//
	//

	private Plan convert(FOFormula<Atom> formula) {
		if(formula.isDisjunction()) {
			return this.convert((FOFormulaDisjunction<Atom>)formula);
		} else if(formula.isConjunction()) {
			return this.convert((FOFormulaConjunction<Atom>)formula);
		} else if(formula.isNegation()) {
			return this.convert((FOFormulaNegation<Atom>)formula);
		} else if(formula.isAtomic()) {
			return this.convert((AtomicFOFormula<Atom>)formula);
		} else {
			throw new RuntimeException("Unknow type of formula to convert : " + formula);
		}
	}

	private Plan convert(FOFormulaDisjunction<Atom> formula) {
		List<Plan> subplans = new ArrayList<Plan>();
		for(FOFormula<Atom> subformula : formula.getSubElements()) {
			subplans.add(this.convert(subformula));
		}
		return this.factory.union(subplans);
	}

	private Plan convert(FOFormulaConjunction<Atom> formula) {
		Plan join = null;
		Collection<? extends FOFormula<Atom>> subElements;
		try {
			LinkedHashSet<Atom> atoms = FOFormulaUtil.getOrderedAtoms(formula);
			subElements = atoms.stream().map(a -> FOFormulaFactory.instance().createOrGetAtomic(a).get()).collect(Collectors.toList());
		} catch (Exception e) {
			// formula is not a simple CQ
			subElements = formula.getSubElements();
		}
		for(FOFormula<Atom> subformula : subElements) {
			Plan current = this.convert(subformula);
			if(join == null) {
				join = current;
			} else {
				join = this.factory.join(join, current);
			}
		}
		return join;
	}

	private Plan convert(FOFormulaNegation<Atom> formula) {
		throw new RuntimeException("Unknow type of formula to convert : " + formula);
	}

	private Plan convert(AtomicFOFormula<Atom> formula) {
		Atom a = formula.getElement();
		Predicate p = a.getPredicate();
		Plan plan = this.factory.create(fb, p);

		// Rename the column using the term labels
		Set<Term> allTerms = new HashSet<Term>();
		List<String> newColumnNames = new ArrayList<String>();

		// Filter with the renamings
		Map<String, String> constantSelectionMap = new HashMap<String, String>();
		Map<String, Set<String> > equalitySelectionMap = new HashMap<String, Set<String>>();

		for (Term t : a.getTerms()) {
			// If  the term is a constant
			// we rename the current term with a fresh variable
			// and we add the corresponding constant equality condition
			if (t.isConstant() || t.isLiteral()) {
				Variable newTerm = SameObjectTermFactory.instance().createOrGetFreshVariable();
				newColumnNames.add(newTerm.getLabel());
				constantSelectionMap.put(newTerm.getLabel(), t.getLabel());
			}
			// If the term has already been seen in the atom
			// we rename the current term with a fresh variable
			// and we add the corresponding equality condition
			else if(allTerms.contains(t)) {
				Variable newTerm = SameObjectTermFactory.instance().createOrGetFreshVariable();
				newColumnNames.add(newTerm.getLabel());
				Set<String> classs = equalitySelectionMap.getOrDefault(t.getLabel(), new HashSet<String>());
				classs.add(t.getLabel());
				classs.add(newTerm.getLabel());
				equalitySelectionMap.put(t.getLabel(), classs);
			}
			else {
				newColumnNames.add(t.getLabel());
				allTerms.add(t);
			}
		}

		// Rename the atom column names
		plan = this.factory.rename(plan, newColumnNames);

		// Apply constant selection
		if (!constantSelectionMap.isEmpty()) {
			plan = this.factory.select(plan, constantSelectionMap);
		}

		for(Set<String> classs : equalitySelectionMap.values()) {
			plan = this.factory.select(plan, classs);
		}
		
		return plan;
	}

	private Plan handleSelection(Plan plan, Substitution affectation) {
		Map<String, String> constantSelectionMap = new HashMap<String, String>();
		Map<String, Set<String> > equalitySelectionMap = new HashMap<String, Set<String>>();
		
		for(Variable v : affectation.keys()) {
			Term image = affectation.createImageOf(v);
			if(image != v) {
				if(image.isConstant() || image.isLiteral()) {
					constantSelectionMap.put(v.getLabel(), image.getLabel());
				} else if(image.isVariable()) {
					Set<String> classs = equalitySelectionMap.getOrDefault(v.getLabel(), new HashSet<String>());
					classs.add(v.getLabel());
					classs.add(image.getLabel());
					equalitySelectionMap.put(v.getLabel(), classs);
				}
			}
		}
		
		
		if (!constantSelectionMap.isEmpty()) {
			plan = this.factory.select(plan, constantSelectionMap);
		}

		for(Set<String> classs : equalitySelectionMap.values()) {
			plan = this.factory.select(plan, classs);
		}
		
		return plan;
	}

	private Plan handleProjection(Plan subplan, Collection<Variable> affectation) {
		List<String> columnToKeep = affectation.stream().map(v -> v.getLabel()).collect(Collectors.toList());
		return this.factory.project(subplan, columnToKeep);
	}

}
