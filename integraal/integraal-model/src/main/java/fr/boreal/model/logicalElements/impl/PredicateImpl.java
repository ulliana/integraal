package fr.boreal.model.logicalElements.impl;

import fr.boreal.model.logicalElements.api.Predicate;

public class PredicateImpl implements Predicate {

	private String label;
	private int arity;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public PredicateImpl(String label, int arity) {
		this.label = label;
		this.arity = arity;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public int getArity() {
		return this.arity;
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getArity();
		result = prime * result * this.label.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof Predicate) {
			Predicate other = (Predicate) o;
			return this.getArity() == other.getArity() && this.getLabel().equals(other.getLabel());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.label;
	}

}
