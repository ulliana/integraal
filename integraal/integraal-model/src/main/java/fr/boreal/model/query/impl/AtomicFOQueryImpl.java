package fr.boreal.model.query.impl;

import java.util.Collection;

import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.AtomicFOQuery;

public class AtomicFOQueryImpl extends AbstractFOQuery implements AtomicFOQuery {

	AtomicFOFormula<Atom> formula;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public AtomicFOQueryImpl(AtomicFOFormula<Atom> formula, Collection<Variable> answer_variables,
			Substitution initial_substitution) {
		super(formula, answer_variables, initial_substitution);
		this.formula = formula;
	}

	@Override
	public AtomicFOFormula<Atom> getFormula() {
		return this.formula;
	}

}
