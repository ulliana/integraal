package fr.boreal.model.query.impl;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQueryConjunction;

public class FOQueryConjunctionImpl extends AbstractFOQuery implements FOQueryConjunction {

	FOFormulaConjunction<Atom> formula;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FOQueryConjunctionImpl(FOFormulaConjunction<Atom> formula, Collection<Variable> answer_variables,
			Substitution initial_substitution) {
		super(formula, answer_variables, initial_substitution);
		this.formula = formula;
	}

	@Override
	public FOFormulaConjunction<Atom> getFormula() {
		return this.formula;
	}
}
