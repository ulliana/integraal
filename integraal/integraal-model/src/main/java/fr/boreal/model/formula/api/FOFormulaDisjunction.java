package fr.boreal.model.formula.api;

import java.util.Collection;

public interface FOFormulaDisjunction<T> extends FOFormula<T> {

	@Override
	default public boolean isDisjunction() {
		return true;
	}

	public Collection<? extends FOFormula<T>> getSubElements();

	@Override
	default Collection<T> flatten() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Flattening a disjunction would result in a set with no meaning");
	}
}
