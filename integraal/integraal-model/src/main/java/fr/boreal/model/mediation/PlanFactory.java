package fr.boreal.model.mediation;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Predicate;

public interface PlanFactory {

	/**
	 * Create a view of the answers of the query on the datasource The view column
	 * names are the query column names
	 *
	 * @param datasource
	 * @param query
	 */
	public Plan create(FactBase datasource, Predicate viewPredicate);

	public Plan createEmpty(List<String> columnNames);

	public Plan createConstant(List<String> tuple, List<String> columnNames);

	/**
	 * Create an new view by renaming the columns
	 * 
	 * @param view        - a view
	 * @param columnNames - a list of column names, all differents
	 */
	public Plan rename(Plan view, List<String> columnNames);

	/**
	 * Create an view by applying a filter (selection operator) on the input view, selecting rows where all selected columns take equal values
	 * @param view
	 * @param columnNames - selected column names
	 */
	public Plan select(Plan view, Collection<String> columnNames);

	public Plan select(Plan view, Map<String, String> selectionMap);

	public Plan project(Plan view, List<String> columnNames);

	/** 
	 * Create an new view by inserting constant columns
	 * @param view 
	 * @param insertionMap - specifying the constant tuples to insert before certain column position
	 * @return the input view where constant column have been inserted following the insertionMap. Fresh column names are generated for the constant columns.
	 */
	public Plan insertConstantColumn(Plan view, Map<Integer, List<String>> insertionMap);

	public Plan join(Plan view1, Plan view2);

	/**
	 * create a union of the views, removing duplicated answers    
	 */
	public Plan union(Collection<Plan> views);

	/**
	 * Create a distinct operation
	 */
	public Plan distinct(Plan plan);

}
