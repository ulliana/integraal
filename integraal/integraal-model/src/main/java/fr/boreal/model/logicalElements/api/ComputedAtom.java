package fr.boreal.model.logicalElements.api;

public interface ComputedAtom extends Atom {

	public Atom eval(Substitution s);

}
