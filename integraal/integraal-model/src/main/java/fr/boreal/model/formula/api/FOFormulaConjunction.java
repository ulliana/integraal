package fr.boreal.model.formula.api;

import java.util.Collection;
import java.util.stream.Collectors;

public interface FOFormulaConjunction<T> extends FOFormula<T> {

	@Override
	default public boolean isConjunction() {
		return true;
	}

	public Collection<? extends FOFormula<T>> getSubElements();

	@Override
	default Collection<T> flatten() throws UnsupportedOperationException {
		return this.getSubElements().stream().flatMap(f -> f.flatten().stream()).collect(Collectors.<T>toSet());
	}
}
