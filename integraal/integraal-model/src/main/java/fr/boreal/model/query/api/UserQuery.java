package fr.boreal.model.query.api;

/**
 * This interface represents a query given by a user.
 * It is made from a first order query and a set of additional conditions such as counts, limits, orders, ...
 */
public interface UserQuery {

	// TODO add user part

	public Query getQuery();

}
