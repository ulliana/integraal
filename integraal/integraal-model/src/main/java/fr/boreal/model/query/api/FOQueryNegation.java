package fr.boreal.model.query.api;

import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.logicalElements.api.Atom;

public interface FOQueryNegation extends FOQuery {

	/**
	 * @return the first order formula that represents this first order query
	 */
	@Override
	FOFormulaNegation<Atom> getFormula();

}
