package fr.boreal.model.logicalElements.impl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

import fr.boreal.model.functions.Invoker;
import fr.boreal.model.logicalElements.api.FunctionalTerm;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;

public class FunctionalTermImpl implements FunctionalTerm {

	private String function_name;
	private ArrayList<Term> sub_terms;

	private Invoker invoker;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FunctionalTermImpl(String function_name, Invoker invoker, ArrayList<Term> sub_terms) {
		this.function_name = function_name;
		this.sub_terms = sub_terms;
		this.invoker = invoker;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Term eval(Substitution s) {
		ArrayList<Term> sub_terms_image = new ArrayList<Term>();
		for (Term t : this.sub_terms) {
			sub_terms_image.add(s.createImageOf(t));
		}
		Optional<Term> result = this.invoker.invoke(sub_terms_image.toArray(new Term[sub_terms_image.size()]));
		return result.orElse(null);
	}

	@Override
	public String getLabel() {
		throw new IllegalArgumentException(
				"Cannot get the label for a functional term. To get a String representation, please use toString");
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return Objects.hash(this.function_name, sub_terms, invoker);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FunctionalTermImpl) {
			FunctionalTermImpl other = (FunctionalTermImpl) o;
			// TODO: auto-generated method
			return false;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.function_name + "(" + sub_terms.toString() + ")";
	}

}
