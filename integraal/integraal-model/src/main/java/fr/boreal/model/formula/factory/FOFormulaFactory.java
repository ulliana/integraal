package fr.boreal.model.formula.factory;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.formula.impl.AtomicFOImpl;
import fr.boreal.model.formula.impl.FOConjunctionImpl;
import fr.boreal.model.formula.impl.FODisjunctionImpl;
import fr.boreal.model.formula.impl.FONegationImpl;

public class FOFormulaFactory {

	private static final FOFormulaFactory INSTANCE = new FOFormulaFactory();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public static FOFormulaFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	public <T> Optional<FOFormulaConjunction<T>> createOrGetConjunction(Collection<FOFormula<T>> sub_formulas) {
		return Optional.ofNullable(new FOConjunctionImpl<T>(sub_formulas));
	}

	@SafeVarargs
	public final <T> Optional<FOFormulaConjunction<T>> createOrGetConjunction(FOFormula<T>... sub_formulas) {
		Collection<FOFormula<T>> set = new LinkedHashSet<FOFormula<T>>();
		for (FOFormula<T> f : sub_formulas) {
			set.add(f);
		}
		return this.createOrGetConjunction(set);
	}

	@SafeVarargs
	public final <T> Optional<FOFormulaConjunction<T>> createOrGetConjunction(T... sub_elements) {
		Collection<FOFormula<T>> set = new LinkedHashSet<FOFormula<T>>();
		for (T e : sub_elements) {
			set.add(this.createOrGetAtomic(e).orElse(null));
		}
		return this.createOrGetConjunction(set);
	}

	public <T> Optional<FOFormulaDisjunction<T>> createOrGetDisjunction(Collection<FOFormula<T>> sub_formulas) {
		return Optional.ofNullable(new FODisjunctionImpl<T>(
				sub_formulas.stream().map(f -> this.copy(f).orElse(null)).collect(Collectors.toSet())));
	}

	@SafeVarargs
	public final <T> Optional<FOFormulaDisjunction<T>> createOrGetDisjunction(FOFormula<T>... sub_formulas) {
		return Optional.ofNullable(new FODisjunctionImpl<T>(
				Set.of(sub_formulas).stream().map(f -> this.copy(f).orElse(null)).collect(Collectors.toSet())));
	}

	@SafeVarargs
	public final <T> Optional<FOFormulaDisjunction<T>> createOrGetDisjunction(T... sub_elements) {
		return Optional.ofNullable(new FODisjunctionImpl<T>(Set.of(sub_elements).stream()
				.map(e -> this.createOrGetAtomic(e).orElse(null)).collect(Collectors.toSet())));
	}

	public <T> Optional<FOFormulaNegation<T>> createOrGetNegation(FOFormula<T> sub_formula) {
		return Optional.ofNullable(new FONegationImpl<T>(this.copy(sub_formula).orElse(null)));
	}

	public <T> Optional<AtomicFOFormula<T>> createOrGetAtomic(T element) {
		return Optional.of(new AtomicFOImpl<T>(element));
	}

	public <T> Optional<? extends FOFormula<T>> copy(FOFormula<T> formula) {
		if (formula.isAtomic()) {
			return this.copyAtomic((AtomicFOFormula<T>) formula);
		}
		if (formula.isConjunction()) {
			return this.copyConjunction((FOFormulaConjunction<T>) formula);
		}
		if (formula.isDisjunction()) {
			return this.copyDisjunction((FOFormulaDisjunction<T>) formula);
		}
		if (formula.isNegation()) {
			return this.copyNegation((FOFormulaNegation<T>) formula);
		}
		return null;
	}

	public <T> Optional<FOFormulaConjunction<T>> copyConjunction(FOFormulaConjunction<T> formula) {
		Collection<FOFormula<T>> new_sub_formulas = new HashSet<>();
		for (FOFormula<T> old_sub_formula : formula.getSubElements()) {
			Optional<? extends FOFormula<T>> new_sub_formula = this.copy(old_sub_formula);
			if (new_sub_formula.isPresent()) {
				new_sub_formulas.add(new_sub_formula.get());
			} else {
				return Optional.empty();
			}
		}
		FOFormulaConjunction<T> new_formula = new FOConjunctionImpl<T>(new_sub_formulas);
		return Optional.of(new_formula);
	}

	public <T> Optional<FOFormulaDisjunction<T>> copyDisjunction(FOFormulaDisjunction<T> formula) {
		Collection<FOFormula<T>> new_sub_formulas = new HashSet<>();
		for (FOFormula<T> old_sub_formula : formula.getSubElements()) {
			Optional<? extends FOFormula<T>> new_sub_formula = this.copy(old_sub_formula);
			if (new_sub_formula.isPresent()) {
				new_sub_formulas.add(new_sub_formula.get());
			} else {
				return Optional.empty();
			}
		}
		FOFormulaDisjunction<T> new_formula = new FODisjunctionImpl<T>(new_sub_formulas);
		return Optional.of(new_formula);
	}

	public <T> Optional<FOFormulaNegation<T>> copyNegation(FOFormulaNegation<T> formula) {
		Optional<? extends FOFormula<T>> sub_formula = this.copy(formula.getElement());
		if (sub_formula.isPresent()) {
			return Optional.of(new FONegationImpl<T>(sub_formula.get()));
		} else {
			return Optional.empty();
		}
	}

	public <T> Optional<AtomicFOFormula<T>> copyAtomic(AtomicFOFormula<T> formula) {
		return Optional.of(new AtomicFOImpl<T>(formula.getElement()));
	}
}
