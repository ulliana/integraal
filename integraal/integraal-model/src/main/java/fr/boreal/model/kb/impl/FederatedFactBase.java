package fr.boreal.model.kb.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.google.common.collect.Iterators;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.ColumnType;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;

public class FederatedFactBase implements FactBase {

	private Map<Predicate, FactStorage> facts_by_storage;
	private FactStorage default_storage;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FederatedFactBase(FactStorage default_base) {
		this(default_base, new HashMap<Predicate, FactStorage>());
	}

	public FederatedFactBase(FactStorage default_base, Predicate p, FactStorage storage) {
		this(default_base);
		this.addStorage(p, storage);
	}

	public FederatedFactBase(FactStorage default_base, Map<Predicate, FactStorage> facts_by_storage) {
		this.default_storage = default_base;
		this.facts_by_storage = facts_by_storage;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> match(Atom a) {
		return this.chooseStorage(a.getPredicate()).match(a);
	}

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {
		return this.chooseStorage(a.getPredicate()).match(a, s);
	}

	public void addStorage(Predicate p, FactStorage s) {
		this.facts_by_storage.put(p, s);
	}

	public void setDefaultStorage(FactStorage s) {
		this.default_storage = s;
	}

	@Override
	public boolean add(FOFormula<Atom> atoms) {
		return this.default_storage.add(atoms);
	}

	@Override
	public Stream<Atom> getAtoms() {
		Stream<Atom> sources_atoms = this.facts_by_storage.values().stream().flatMap(source -> {
			try {
				return source.getAtoms();
			} catch (UnsupportedOperationException e) {
				return Stream.empty();
			}
		});
		return Stream.concat(this.default_storage.getAtoms(), sources_atoms);
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate predicate) {
		return this.chooseStorage(predicate).getAtomsByPredicate(predicate);
	}
	
	@Override
	public Iterator<Predicate> getPredicates() {
		return Iterators.concat(this.facts_by_storage.keySet().iterator(), this.default_storage.getPredicates());
	}
	
	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return this.chooseStorage(p).getTermsByPredicatePosition(p, position);
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		return this.default_storage.addAll(atoms);
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return this.chooseStorage(viewPredicate).getType(viewPredicate);
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return this.chooseStorage(viewPredicate).getDescription(viewPredicate);
	}

	@Override
	public List<ColumnType> getColumnsType(Predicate viewPredicate) {
		return this.chooseStorage(viewPredicate).getColumnsType(viewPredicate);
	}
	
	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////
	
	private FactStorage chooseStorage(Predicate p) {
		return this.facts_by_storage.getOrDefault(p, this.default_storage);
	}

}