package fr.boreal.model.query.api;

import java.util.Collection;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;

/**
 * This interface represents a general Query
 */
public interface Query {

	/**
	 * @return a collection of variable representing the answer variables of this query
	 */
	Collection<Variable> getAnswerVariables();

	/**
	 * @return the initial substitution associated to this query
	 * This is used to associate a default value to a variable (ie: X = a) with the same meaning as
	 * SELECT 'a' as X WHERE ... in SparQL
	 */
	Substitution getInitialSubstitution();

}