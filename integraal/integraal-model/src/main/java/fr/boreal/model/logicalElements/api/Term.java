package fr.boreal.model.logicalElements.api;

/**
 * In analogy to natural language, where a noun phrase refers to an object,
 * a term denotes a mathematical object referring to someone or something.
 * A term is either a {@link Constant}, a {@link Variable}, a {@link Literal} a {@link LabeledNull} or a {@link FunctionalTerm}
 */
public interface Term {

	/**
	 * @return a the string representation of this object
	 */
	String getLabel();

	/**
	 * @return true iff this object is a {@link Constant}
	 */
	default boolean isConstant() {
		return false;
	}

	/**
	 * @return true iff this object is a {@link Variable}
	 */
	default boolean isVariable() {
		return false;
	}

	/**
	 * @return true iff this object is a {@link Literal}
	 */
	default boolean isLiteral() {
		return false;
	}

	/**
	 * @return true iff this object is a {@link LabeledNull}
	 */
	default boolean isLabeledNull() {
		return false;
	}

	/**
	 * @return true iff this object is a {@link FunctionalTerm}
	 */
	default boolean isFunctionalTerm() {
		return false;
	}

}
