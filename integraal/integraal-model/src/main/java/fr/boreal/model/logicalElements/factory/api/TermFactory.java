package fr.boreal.model.logicalElements.factory.api;

import fr.boreal.model.logicalElements.api.Constant;
import fr.boreal.model.logicalElements.api.Literal;
import fr.boreal.model.logicalElements.api.Variable;

public interface TermFactory {

	/**
	 * Two object created by a call to this function with the same label argument must be equals according to the definition given by the {@link Constant} implementation used
	 */
	Constant createOrGetConstant(String label);

	/**
	 * Two object created by a call to this function with the same typed value argument must be equals according to the definition given by the {@link Literal} implementation used
	 */
	<T> Literal<T> createOrGetLiteral(T value);

	/**
	 * Two object created by a call to this function with the same label argument must be equals according to the definition given by the {@link Variable} implementation used
	 */
	Variable createOrGetVariable(String label);

	/**
	 * Two object created by a call to this function must never be equals according to the definition given by the {@link Variable} implementation used
	 * A Fresh Variable must not be equals to an existing Variable created by {@link createOrGetVariable}.
	 */
	Variable createOrGetFreshVariable();

}
