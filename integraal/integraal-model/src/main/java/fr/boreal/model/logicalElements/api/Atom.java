package fr.boreal.model.logicalElements.api;

import java.util.HashSet;
import java.util.Set;

/**
 * Interface for Atoms. An atom is an atomic query of the form P(t1, ..., tn)
 * where P is a {@link Predicate} of arity n and t1, ..., tn are {@link Term}s.
 */
public interface Atom {

	/**
	 * @return the {@link Predicate} associated to this atom.
	 */
	Predicate getPredicate();

	/**
	 * @return the {@link Term} at the index position in the atom.
	 */
	Term getTerm(int index);

	/**
	 * @return the {@link Term} at the index position in the atom.
	 */
	Term[] getTerms();

	/**
	 * @return true iff the {@link Term} t is present in this atom.
	 */
	boolean contains(Term term);

	/**
	 * @return the index of the first occurrence of {@link Term} t in this atom. Any
	 *         negative non-zero value is returned if this atom does not contains t.
	 */
	int indexOf(Term term);

	/**
	 * @return the indexes of all the occurrence of {@link Term} t in this atom. An
	 *         empty array is returned if this atom does not contains t.
	 */
	int[] indexesOf(Term term);

	/**
	 * This method can be rewritten in the Atom implementation if there is some
	 * internal structures or indexes on terms.
	 *
	 * @return a Collection which contains all the {@link Variable} of this atom
	 */
	default Set<Variable> getVariables() {
		final int arity = this.getPredicate().getArity();
		Set<Variable> variables = new HashSet<Variable>();
		for (int i = 0; i < arity; i++) {
			Term term_i = this.getTerm(i);
			if (term_i.isVariable()) {
				variables.add((Variable) term_i);
			}
		}
		return variables;
	}

}
