package fr.boreal.model.mediation;

import java.util.List;

import fr.boreal.model.kb.api.ColumnType;

public interface Plan {
	
	 /**
     * get the arity of the plan
     * 
     */
    public int getArity();

    /**
     * get column names of the plan
     * 
     */
    public List<String> getColumnNames();

    /**
     * get column types of the plan
     * 
     */
    public List<ColumnType> getColumnTypes();

    /**
     * get the name of the plan
     * 
     */
    public String getName();


}
