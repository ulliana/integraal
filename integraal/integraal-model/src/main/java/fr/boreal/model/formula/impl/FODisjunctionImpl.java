package fr.boreal.model.formula.impl;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaDisjunction;

public class FODisjunctionImpl<T> extends CompoundFOFormula<T> implements FOFormulaDisjunction<T> {

	private static final String connector_representation = "v";

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FODisjunctionImpl(Collection<FOFormula<T>> subformulas) {
		super(subformulas);
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public String getConnectorRepresentation() {
		return connector_representation;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FOFormulaDisjunction<?>) {
			FOFormulaDisjunction<?> other = (FOFormulaDisjunction<?>)o;
			return this.getSubElements().equals(other.getSubElements());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return this.getSubElements().stream().map(f -> f.hashCode() * this.getSubElements().size() * 31).reduce(0,
				Integer::sum);
	}
}
