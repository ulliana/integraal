package fr.boreal.model.query.factory;

import java.util.Collection;
import java.util.Optional;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.formula.api.FOFormulaNegation;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.AtomicFOQuery;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.FOQueryConjunction;
import fr.boreal.model.query.api.FOQueryDisjunction;
import fr.boreal.model.query.api.FOQueryNegation;
import fr.boreal.model.query.impl.AtomicFOQueryImpl;
import fr.boreal.model.query.impl.FOQueryConjunctionImpl;
import fr.boreal.model.query.impl.FOQueryDisjunctionImpl;
import fr.boreal.model.query.impl.FOQueryNegationImpl;

public class FOQueryFactory {

	private static final FOQueryFactory INSTANCE = new FOQueryFactory();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public static FOQueryFactory instance() {
		return INSTANCE;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	public Optional<? extends FOQuery> createOrGetQuery(FOFormula<Atom> formula, Collection<Variable> answer_variables,
			Substitution initial_substitution) {
		if (formula.isAtomic()) {
			return Optional.of(this.createOrGetAtomicQuery((AtomicFOFormula<Atom>) formula, answer_variables,
					initial_substitution));
		} else if (formula.isConjunction()) {
			return Optional.of(this.createOrGetConjunctionQuery((FOFormulaConjunction<Atom>) formula, answer_variables,
					initial_substitution));
		} else if (formula.isDisjunction()) {
			return Optional.of(this.createOrGetDisjunctionQuery((FOFormulaDisjunction<Atom>) formula, answer_variables,
					initial_substitution));
		} else if (formula.isNegation()) {
			return Optional.of(this.createOrGetNegationQuery((FOFormulaNegation<Atom>) formula, answer_variables,
					initial_substitution));
		} else {
			return Optional.empty();
		}
	}

	public AtomicFOQuery createOrGetAtomicQuery(AtomicFOFormula<Atom> formula, Collection<Variable> answer_variables,
			Substitution initial_substitution) {
		if (answer_variables == null) {
			answer_variables = FOFormulaUtil.getVariables(formula);
		}
		return new AtomicFOQueryImpl(formula, answer_variables, initial_substitution);
	}

	public FOQueryConjunction createOrGetConjunctionQuery(FOFormulaConjunction<Atom> formula,
			Collection<Variable> answer_variables, Substitution initial_substitution) {
		return new FOQueryConjunctionImpl(formula, answer_variables, initial_substitution);
	}

	public FOQueryDisjunction createOrGetDisjunctionQuery(FOFormulaDisjunction<Atom> formula,
			Collection<Variable> answer_variables, Substitution initial_substitution) {
		return new FOQueryDisjunctionImpl(formula, answer_variables, initial_substitution);
	}

	public FOQueryNegation createOrGetNegationQuery(FOFormulaNegation<Atom> formula,
			Collection<Variable> answer_variables, Substitution initial_substitution) {
		return new FOQueryNegationImpl(formula, answer_variables, initial_substitution);
	}

}
