package fr.boreal.model.logicalElements.api;

public interface FunctionalTerm extends Term {

	@Override
	default boolean isFunctionalTerm() {
		return true;
	}

	public Term eval(Substitution s);

}
