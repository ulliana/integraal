package fr.boreal.model.kb.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;

public interface FactBase {

	Iterator<Atom> match(Atom a);

	Iterator<Atom> match(Atom a, Substitution s);

	boolean add(FOFormula<Atom> atoms);

	boolean addAll(Collection<Atom> atoms);

	/**
	 * @return a stream over all the atoms of the factbase
	 */
	Stream<Atom> getAtoms();
	
	/**
	 * Returns an iterator over all atoms with the specified predicate.
	 * 
	 * @param predicate to search
	 * @return an iterator over all atoms with the specified predicate
	 */
	Iterator<Atom> getAtomsByPredicate(Predicate predicate);

	/**
	 * Returns an Iterator of all predicates in this factbase.
	 * 
	 * @return an Iterator of all predicates.
	 */
	Iterator<Predicate> getPredicates();
	
	/**
	 * Returns an iterator over terms which are in a specific position in at
	 * least one atom with the given predicate.
	 * 
	 * @param p predicate
	 * @param position the position of the term in atoms, positions starts from 0.
	 * @return an iterator over terms which appear in the specified position of the specified predicate.
	 */
	Iterator<Term> getTermsByPredicatePosition(Predicate p, int position);

	/**
	 * @return a stream over all the atoms of the factbase that contains the given
	 *         term t
	 */
	default Stream<Atom> getAtoms(Term t) {
		return this.getAtoms().filter(a -> a.contains(t));
	}

	/**
	 * @return true iff this storage contains the given atom
	 */
	default boolean contains(Atom a) {
		return this.getAtoms().anyMatch(atom -> atom.equals(a));
	}

	/**
	 * @return the number of atoms represented by this factbase
	 */
	default long size() {
		return this.getAtoms().count();
	}

	// Tatooine temporary methods

	FactBaseDescription getDescription(Predicate viewPredicate);
	
	FactBaseType getType(Predicate viewPredicate);

	default List<ColumnType> getColumnsType(Predicate viewPredicate) {
		int arity = viewPredicate.getArity();
		List<ColumnType> types = new ArrayList<ColumnType>(arity);
		for (int i = 0; i < arity; i++) {
			types.add(ColumnType.STRING);
		}
		return types;
	}

}
