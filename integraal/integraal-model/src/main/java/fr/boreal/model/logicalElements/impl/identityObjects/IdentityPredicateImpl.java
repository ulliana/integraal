package fr.boreal.model.logicalElements.impl.identityObjects;

import fr.boreal.model.logicalElements.api.Predicate;

public class IdentityPredicateImpl implements Predicate {

	private String label;
	private int arity;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public IdentityPredicateImpl(String label, int arity) {
		this.label = label;
		this.arity = arity;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public int getArity() {
		return this.arity;
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public String toString() {
		return this.label;
	}

}
