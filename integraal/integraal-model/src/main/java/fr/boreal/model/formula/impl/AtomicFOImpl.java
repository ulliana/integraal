package fr.boreal.model.formula.impl;

import fr.boreal.model.formula.api.AtomicFOFormula;

public class AtomicFOImpl<T> implements AtomicFOFormula<T> {

	private T element;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public AtomicFOImpl(T element) {
		this.element = element;
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public T getElement() {
		return this.element;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof AtomicFOFormula<?>) {
			return this.getElement().equals(((AtomicFOFormula<?>)o).getElement());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.element.toString();
	}

	@Override
	public int hashCode() {
		return this.element.hashCode() * 7;
	}
}
