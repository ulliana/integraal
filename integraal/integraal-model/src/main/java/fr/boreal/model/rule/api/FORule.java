package fr.boreal.model.rule.api;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.FOQuery;

public interface FORule extends Rule {

	@Override
	FOQuery getBody();

	@Override
	FOFormula<Atom> getHead();
}
