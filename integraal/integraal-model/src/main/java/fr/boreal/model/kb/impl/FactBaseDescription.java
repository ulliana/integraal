package fr.boreal.model.kb.impl;

public class FactBaseDescription {
	
	private String url;
	private String query;

	public FactBaseDescription(String url, String query) {
		this.url = url;
		this.query = query;
	}

	public String getQuery() {
		return this.query;
	}

	public String getUrl() {
		return this.url;
	}

}
