package fr.boreal.model.functions;

import java.util.Optional;
import java.util.stream.Stream;

import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;

public class GraalInvokers {

	private final TermFactory termFactory;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public GraalInvokers(TermFactory tf) {
		this.termFactory = tf;
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	public Invoker getSumInvoker() {
		return this.sum;
	}

	public Invoker getMaxInvoker() {
		return this.max;
	}

	public Invoker getMinInvoker() {
		return this.min;
	}

	public Invoker getAverageInvoker() {
		return this.average;
	}

	public Invoker getEvenInvoker() {
		return this.isEven;
	}

	public Invoker getOddInvoker() {
		return this.isOdd;
	}

	public Invoker getGreaterThanInvoker() {
		return this.isGreaterThan;
	}

	public Invoker getGreaterOrEqualsToInvoker() {
		return this.isGreaterOrEqualsTo;
	}

	public Invoker getSmallerThanInvoker() {
		return this.isSmallerThan;
	}

	public Invoker getSmallerOrEqualsToInvoker() {
		return this.isSmallerOrEqualsTo;
	}

	public Invoker getPrimeInvoker() {
		return this.isPrime;
	}

	/////////////////////////////////////////////////
	// Predefined Invokers
	/////////////////////////////////////////////////

	// Functions //

	private Invoker sum = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory
					.<Integer>createOrGetLiteral(Stream.of(args).mapToInt(t -> Integer.valueOf(t.getLabel())).sum()));
		}
	};

	private Invoker max = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory.<Integer>createOrGetLiteral(
					Stream.of(args).mapToInt(t -> Integer.valueOf(t.getLabel())).max().orElse(Integer.MIN_VALUE)));
		}
	};

	private Invoker min = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory.<Integer>createOrGetLiteral(
					Stream.of(args).mapToInt(t -> Integer.valueOf(t.getLabel())).min().orElse(Integer.MIN_VALUE)));
		}
	};

	private Invoker average = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory.<Double>createOrGetLiteral(
					Stream.of(args).mapToDouble(t -> Double.valueOf(t.getLabel())).average().orElseGet(null)));
		}
	};

	// Predicates //

	private Invoker isEven = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory
					.<Boolean>createOrGetLiteral(args.length == 1 && (Integer.valueOf(args[0].getLabel()) % 2 == 0)));
		}
	};

	private Invoker isOdd = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory
					.<Boolean>createOrGetLiteral(args.length == 1 && (Integer.valueOf(args[0].getLabel()) % 2 == 1)));
		}
	};

	private Invoker isGreaterThan = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory.<Boolean>createOrGetLiteral(
					args.length == 2 && (Integer.valueOf(args[0].getLabel()) > Integer.valueOf(args[1].getLabel()))));
		}
	};

	private Invoker isGreaterOrEqualsTo = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory.<Boolean>createOrGetLiteral(
					args.length == 2 && (Integer.valueOf(args[0].getLabel()) >= Integer.valueOf(args[1].getLabel()))));
		}
	};

	private Invoker isSmallerThan = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory.<Boolean>createOrGetLiteral(
					args.length == 2 && (Integer.valueOf(args[0].getLabel()) < Integer.valueOf(args[1].getLabel()))));
		}
	};

	private Invoker isSmallerOrEqualsTo = new Invoker() {
		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory.<Boolean>createOrGetLiteral(
					args.length == 2 && (Integer.valueOf(args[0].getLabel()) <= Integer.valueOf(args[1].getLabel()))));
		}
	};

	private Invoker isPrime = new Invoker() {
		private boolean isPrime(int n) {
			if (n <= 1)
				return false;
			if (n <= 3)
				return true;
			if (n % 2 == 0 || n % 3 == 0)
				return false;
			for (int i = 5; i * i <= n; i += 6)
				if (n % i == 0 || n % (i + 2) == 0)
					return false;
			return true;
		}

		@Override
		public Optional<Term> invoke(Term... args) {
			return Optional.ofNullable(termFactory
					.<Boolean>createOrGetLiteral(args.length == 1 && (isPrime(Integer.valueOf(args[0].getLabel())))));
		}
	};

}
