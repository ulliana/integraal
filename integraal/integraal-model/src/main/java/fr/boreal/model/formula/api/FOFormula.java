package fr.boreal.model.formula.api;

import java.util.Collection;

public interface FOFormula<T> {

	default boolean isConjunction() {
		return false;
	}

	default boolean isDisjunction() {
		return false;
	}

	default boolean isNegation() {
		return false;
	}

	default boolean isAtomic() {
		return false;
	}

	/**
	 * The given collection does not represent any meaning on the formula as
	 * conjunctions and disjunctions will be merged together
	 *
	 * @return all the roots elements of this formula
	 */
	public Collection<T> flatten() throws UnsupportedOperationException;

}
