package fr.boreal.model.query.api;

import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.logicalElements.api.Atom;

public interface AtomicFOQuery extends FOQuery {

	/**
	 * @return the first order formula that represents this first order query
	 */
	@Override
	AtomicFOFormula<Atom> getFormula();

}
