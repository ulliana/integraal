package fr.boreal.model.kb.api;

public enum ColumnType {
    STRING, INTEGER
}
