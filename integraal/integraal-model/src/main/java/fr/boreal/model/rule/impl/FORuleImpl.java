package fr.boreal.model.rule.impl;

import java.util.Collection;

import fr.boreal.model.formula.FOFormulaUtil;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.rule.api.FORule;

public class FORuleImpl implements FORule {

	FOQuery body;
	FOFormula<Atom> head;

	Collection<Variable> frontier = null;
	Collection<Variable> existentials = null;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FORuleImpl(FOQuery body, FOFormula<Atom> head) {
		this.head = head;
		this.body = body;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public FOQuery getBody() {
		return this.body;
	}

	@Override
	public FOFormula<Atom> getHead() {
		return this.head;
	}

	@Override
	public Collection<Variable> getFrontier() {
		if (this.frontier == null) {
			this.frontier = FOFormulaUtil.getVariables(this.getHead());
			this.frontier.retainAll(FOFormulaUtil.getVariables(this.getBody().getFormula()));
		}
		return this.frontier;
	}

	@Override
	public Collection<Variable> getExistentials() {
		if (this.existentials == null) {
			this.existentials = FOFormulaUtil.getVariables(this.getHead());
			this.existentials.removeAll(FOFormulaUtil.getVariables(this.getBody().getFormula()));
		}
		return this.existentials;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getBody().hashCode();
		result = prime * result + this.getHead().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FORule) {
			FORule other = (FORule) o;
			return this.getBody().equals(other.getBody()) && this.getHead().equals(other.getHead());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getBody().toString());
		sb.append(" -> ");
		sb.append(this.getHead().toString());
		return sb.toString();
	}
}
