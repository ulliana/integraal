package fr.boreal.model.kb.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.rule.api.FORule;

public class RuleBaseImpl implements RuleBase {

	Collection<FORule> rules;
	Map<Predicate, Collection<FORule>> rules_by_head_predicate;
	Map<Predicate, Collection<FORule>> rules_by_body_predicate;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public RuleBaseImpl(Collection<FORule> rules) {
		this.rules = rules;
		this.rules_by_head_predicate = new HashMap<Predicate, Collection<FORule>>();
		this.rules_by_body_predicate = new HashMap<Predicate, Collection<FORule>>();
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public Collection<FORule> getRules() {
		return this.rules;
	}

	@Override
	public Collection<FORule> getRulesByHeadPredicate(Predicate p) {
		if (this.rules_by_head_predicate.containsKey(p)) {
			return this.rules_by_head_predicate.get(p);
		} else {
			Set<FORule> matches = this.getRules().stream().filter(rule -> rule.getHead().flatten().stream()
					.map(a -> a.getPredicate()).anyMatch(pred -> pred.equals(p))).collect(Collectors.toSet());
			this.rules_by_head_predicate.put(p, matches);
			return matches;
		}
	}

	@Override
	public Collection<FORule> getRulesByBodyPredicate(Predicate p) {
		if (this.rules_by_body_predicate.containsKey(p)) {
			return this.rules_by_body_predicate.get(p);
		} else {
			Set<FORule> matches = this.getRules().stream().filter(rule -> rule.getBody().getFormula().flatten().stream()
					.map(a -> a.getPredicate()).anyMatch(pred -> pred.equals(p))).collect(Collectors.toSet());
			this.rules_by_body_predicate.put(p, matches);
			return matches;
		}
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return this.rules.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.rules.toString();
	}

}
