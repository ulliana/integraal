package fr.boreal.model.logicalElements.impl;

import fr.boreal.model.logicalElements.api.Variable;

public class VariableImpl implements Variable {

	private String label;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public VariableImpl(String label) {
		this.label = label;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public String getLabel() {
		return this.label;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public int hashCode() {
		return this.label.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FreshVariableImpl) {
			return false;
		} else if (o instanceof Variable) {
			Variable other = (Variable)o;
			return this.getLabel().equals(other.getLabel());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return this.label;
	}
}
