package fr.boreal.model.logicalElements.api;

public interface Constant extends Term {
	
	@Override
	default boolean isConstant() {
		return true;
	}

}
