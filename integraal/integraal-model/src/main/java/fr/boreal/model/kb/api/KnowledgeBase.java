package fr.boreal.model.kb.api;

public interface KnowledgeBase {
	
	FactBase getFactBase();
	
	RuleBase getRuleBase();

}
