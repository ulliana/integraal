package fr.boreal.model.query.api;

import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.logicalElements.api.Atom;

public interface FOQueryConjunction extends FOQuery {

	/**
	 * @return the first order formula that represents this first order query
	 */
	@Override
	FOFormulaConjunction<Atom> getFormula();

}
