package fr.boreal.model.query.api;

import fr.boreal.model.formula.api.FOFormulaDisjunction;
import fr.boreal.model.logicalElements.api.Atom;

public interface FOQueryDisjunction extends FOQuery {

	/**
	 * @return the first order formula that represents this first order query
	 */
	@Override
	FOFormulaDisjunction<Atom> getFormula();

}
