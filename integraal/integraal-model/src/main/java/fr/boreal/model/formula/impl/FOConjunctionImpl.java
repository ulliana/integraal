package fr.boreal.model.formula.impl;

import java.util.Collection;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;

public class FOConjunctionImpl<T> extends CompoundFOFormula<T> implements FOFormulaConjunction<T> {

	private static final String connector_representation = "^";

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	public FOConjunctionImpl(Collection<FOFormula<T>> subformulas) {
		super(subformulas);
	}

	/////////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////////

	@Override
	public String getConnectorRepresentation() {
		return connector_representation;
	}

	/////////////////////////////////////////////////
	// Object methods
	/////////////////////////////////////////////////

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null) {
			return false;
		} else if (o instanceof FOFormulaConjunction<?>) {
			FOFormulaConjunction<?> other = (FOFormulaConjunction<?>)o;
			return this.getSubElements().containsAll(other.getSubElements())
					&& other.getSubElements().containsAll(this.getSubElements());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return this.getSubElements().stream().map(f -> f.hashCode() * this.getSubElements().size() * 17).reduce(0,
				Integer::sum);
	}
}
