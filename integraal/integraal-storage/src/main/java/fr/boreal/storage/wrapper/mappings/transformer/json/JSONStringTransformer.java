package fr.boreal.storage.wrapper.mappings.transformer.json;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.github.jsonldjava.shaded.com.google.common.collect.Sets;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.storage.wrapper.mappings.transformer.Transformer;

/**
 * Transform a String representing a JSON object into atoms.
 * 
 * The first step is a positioning query on the JSON object
 * * If all the positioningCheckers are valid
 * * We go down on the object using the positioningQuery
 * 
 * The second step is to retrieve an ordered list of elements, which will represent our terms.
 * * For each term checking condition in termsCheckers
 * * If all of the conditions are valid
 * * We retrieve the results of the corresponding query in termsQuery
 * * And store it as possible values for the corresponding term
 * 
 * The last step is to combine all the possible values for each term using a Cartesian product
 * * This is especially useful if the JSON object contains arrays
 */
public class JSONStringTransformer implements Transformer<String> {

	private TermFactory tf;

	// all conditions must be met
	private Set<Checker> positioningCheckers;
	private String positioningQuery;

	private List<Set<Checker>> termsCheckers;
	private List<String> termsQuery;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new transformer using user-given parameters
	 */
	public JSONStringTransformer(TermFactory tf, Set<Checker> positioningCheckers, String positioningQuery, List<Set<Checker>> termsCheckers, List<String> termsQuery) {
		this.tf = tf;
		if(positioningCheckers == null) {
			positioningCheckers = new HashSet<Checker>();
		}
		this.positioningCheckers = positioningCheckers;
		this.positioningQuery = positioningQuery;

		if(termsCheckers == null) {
			termsCheckers = new ArrayList<Set<Checker>>();
		}
		this.termsCheckers = termsCheckers;
		this.termsQuery = termsQuery;
	}

	/////////////////////////////////////////////////
	// Public method
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> transform(String nativeResults, Atom a) {
		try {
			JSONObject json_result = new JSONObject(nativeResults);

			boolean isValidContext =  this.positioningCheckers.stream().map(c -> c.check(json_result)).allMatch(b -> b);
			if(! isValidContext) {
				return Collections.<Atom>emptyIterator();
			}

			Object json_positioning_result = json_result.query(this.positioningQuery);
			List<Set<Term>> terms_by_position = new ArrayList<Set<Term>>();
			
			Predicate p = a.getPredicate();
			int arity = p.getArity();
			int diff = arity - this.termsCheckers.size();
			for(int i = 0; i < diff; i++) {
				terms_by_position.add(Set.of(a.getTerm(i)));
			}

			for(int i = 0; i < this.termsCheckers.size(); i++) {

				Set<Term> matched_terms = new HashSet<Term>();

				Set<Checker> checkers_i = this.termsCheckers.get(i);
				String term_value_query = termsQuery.get(i);

				if(json_positioning_result instanceof JSONObject) {
					boolean isValidObject = checkers_i.stream().map(c -> c.check(json_result)).allMatch(b -> b);
					if(isValidObject) {
						Object value = ((JSONObject) json_positioning_result).query(term_value_query);
						Term t = this.tf.createOrGetLiteral(value);
						if(! a.getTerm(diff+i).isVariable() && ! a.getTerm(diff+i).equals(t)) {
							return Collections.<Atom>emptyIterator();
						} else {
							matched_terms.add(t);
						}
					}
				} else if(json_positioning_result instanceof JSONArray) {
					for(Object array_element : (JSONArray)json_positioning_result) {
						JSONObject json_element = (JSONObject) array_element;
						boolean isValidObject = checkers_i.stream().map(c -> c.check(json_element)).allMatch(b -> b);
						if(isValidObject) {
							Object value = json_element.query(term_value_query);
							Term t = this.tf.createOrGetLiteral(value);
							if(! a.getTerm(diff+i).isVariable() && ! a.getTerm(diff+i).equals(t)) {
								// continue;
							} else {
								matched_terms.add(t);
							}
						}
					}
				} else {
					// TODO: See what other types can be recovered if any
					// Maybe a String directly
				}

				if(matched_terms.isEmpty()) {
					return Collections.<Atom>emptyIterator();
				} else  {
					terms_by_position.add(matched_terms);
				}
			}
			
			Set<List<Term>> all_matches = Sets.cartesianProduct(terms_by_position);

			return all_matches.stream().map(terms -> (Atom)new AtomImpl(p, terms)).iterator();
		}
		catch(JSONException e) {
			System.err.println(e);
			System.err.println(nativeResults);
			return Collections.<Atom>emptyIterator();
		}


		
	}
}

