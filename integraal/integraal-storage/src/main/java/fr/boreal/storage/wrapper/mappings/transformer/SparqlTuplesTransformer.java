package fr.boreal.storage.wrapper.mappings.transformer;

import java.util.Iterator;
import java.util.List;

import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.TupleQueryResult;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;

/**
 * Transform Sparql tuples (rdf4j {@link TupleQueryResult}) to Atoms
 * A tuple (t0, t1, ..., tn) applied to the atom of predicate p generates a new atom of arity n+1
 * p(f(t0), f(t1), ..., f(tn))
 * where f transform a {@link Value} into the correct type of {@link Term}
 */
public class SparqlTuplesTransformer implements Transformer<TupleQueryResult> {

	private PredicateFactory pf;
	private TermFactory tf;

	private static final String XSD = "http://www.w3.org/2001/XMLSchema#";
	private static final String XSD_INTEGER = XSD+"integer";

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new transformer using default factories
	 */
	public SparqlTuplesTransformer() {
		this(SameObjectPredicateFactory.instance(), SameObjectTermFactory.instance());
	}

	/**
	 * Creates a new transformer using user-given factories
	 */
	public SparqlTuplesTransformer(PredicateFactory pf, TermFactory tf) {
		this.pf = pf;
		this.tf = tf;
	}

	/////////////////////////////////////////////////
	// Public method
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> transform(TupleQueryResult nativeResults, Atom a) {
		Predicate p = pf.createOrGetPredicate(a.getPredicate().getLabel(), a.getPredicate().getArity());
		int arity = p.getArity();
		List<String> ordered_binding_names = nativeResults.getBindingNames();

		return nativeResults.stream()
				.map(binding_set -> {
					int diff = arity - binding_set.size();
					Term[] terms_array = new Term[arity];
					for(int i = 0; i < diff; i++) {
						terms_array[i] = a.getTerm(i);
					}

					int i = diff;
					for(String binding_name : ordered_binding_names) {
						Value rdf4j_value = binding_set.getBinding(binding_name).getValue();
						// We get the correct term type according to the type of the value.
						// If the value is a java null (safety check, probably never used) we create a fresh existential
						// Blank nodes are represented as existential
						// IRIs are represented as constants
						// RDF-literals are converted to the correct literal type
						// TODO: For now we only handle String and Integer values
						// TODO: Other values will be handled as String
						// We can also compute a first filter here according to constants of the atom
						if(rdf4j_value == null) {
							terms_array[i] = tf.createOrGetFreshVariable();
							if(! a.getTerm(i).isVariable()) {
								return null;
							}
						} else if(rdf4j_value.isBNode()) {
							terms_array[i] = tf.createOrGetVariable(rdf4j_value.stringValue());
							if(! a.getTerm(i).isVariable()) {
								return null;
							}
						} else if(rdf4j_value.isIRI()) {
							terms_array[i] = tf.createOrGetConstant(rdf4j_value.stringValue());
							if(! a.getTerm(i).isVariable() && ! a.getTerm(i).equals(terms_array[i])) {
								return null;
							}
						} else if(rdf4j_value.isLiteral()) {
							Literal rdf4j_literal = (Literal) rdf4j_value;
							String rdf4j_datatype = rdf4j_literal.getDatatype().stringValue();
							if(rdf4j_datatype.equals(XSD_INTEGER)) {
								terms_array[i] = tf.createOrGetLiteral(rdf4j_literal.intValue());
							} else {
								terms_array[i] = tf.createOrGetLiteral(rdf4j_literal.stringValue());
							}
							if(! a.getTerm(i).isVariable() && ! a.getTerm(i).equals(terms_array[i])) {
								return null;
							}
						}
						++i;
					}
					return (Atom)new AtomImpl(p, terms_array);
				}).filter(x -> x != null)
				.iterator();

	}
}
