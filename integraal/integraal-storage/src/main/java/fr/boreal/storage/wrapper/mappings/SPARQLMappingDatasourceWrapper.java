package fr.boreal.storage.wrapper.mappings;

import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.storage.wrapper.evaluator.SparqlQueryEvaluator;
import fr.boreal.storage.wrapper.mappings.specializer.NoSpecializer;
import fr.boreal.storage.wrapper.mappings.transformer.SparqlTuplesTransformer;

public class SPARQLMappingDatasourceWrapper extends MappingDatasourceWrapper<String, TupleQueryResult> {
	
	private String url;
	
	public SPARQLMappingDatasourceWrapper(String endpointUrl) {		
		super(new NoSpecializer<String>(), new SparqlQueryEvaluator(new SPARQLRepository(endpointUrl)), new SparqlTuplesTransformer());
		this.url = endpointUrl;
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return new FactBaseDescription(this.url, this.getQuery(viewPredicate));
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.ENDPOINT;
	}

}
