package fr.boreal.storage.wrapper.rdbms.strategy;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.storage.wrapper.rdbms.SQLParameterizedQuery;
import fr.boreal.storage.wrapper.rdbms.driver.RDBMSDriver;

/**
 * The AdHoc strategy stores atoms as follow
 * 
 * A predicate table stores all the predicates associated with their arity and the corresponding table
 * Each predicate is associated to a SQL table
 * A term table stores all the terms and their corresponding type (variable, literal or constant)
 * 
 * For each predicate, the associated table contains it's arity number of columns
 * Each column represents a position in the atom
 * Each row stores a tuple representing a list of term corresponding to an atom.
 * 
 */
public class AdHocSQLStrategy implements RDBMSStorageStrategy {

	protected RDBMSDriver driver;
	protected QueryRunner runner;

	private Map<Predicate, String> table_name_by_predicate = new HashMap<Predicate, String>();
	private long predicate_count = -1;

	protected static String getType(Term t) {
		if (t.isVariable()) {
			return "V";
		} else if (t.isLiteral()) {
			return "L";
		} else {
			return "C";
		}
	}

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new Strategy over the given driver
	 */
	public AdHocSQLStrategy(RDBMSDriver driver) {
		this.driver = driver;
		DataSource ds = driver.getDatasource();
		this.runner = new QueryRunner(ds);
	}

	/////////////////////////////////////////////////
	// SQLStorageStrategy methods
	/////////////////////////////////////////////////

	@Override
	public boolean canHandleFiltering() {
		return true;
	}

	@Override
	public String getTableName(Atom atom) throws SQLException {
		Predicate p = atom.getPredicate();
		if(table_name_by_predicate.containsKey(p)) {
			return table_name_by_predicate.get(p);
		}
		try {
			List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_predicate_table_query(), new ArrayListHandler(), p.getLabel(), p.getArity());
			if(result.isEmpty()) {
				String fresh_table_name = this.getFreshPredicateTableName();
				this.runner.insert(this.driver.getConnection(), this.get_insert_predicate_query(), new ArrayListHandler(), p.getLabel(), p.getArity(), fresh_table_name);
				String query = this.get_create_atom_table().replace("%s", fresh_table_name);
				for(int i = 0 ; i < p.getArity() ; ++i) {
					if(i == p.getArity() - 1) {
						query = query.replace("%f", this.getColumnName(fresh_table_name, i) + " " + this.driver.getTextFieldName());
					} else {
						query = query.replace("%f", this.getColumnName(fresh_table_name, i) + " " + this.driver.getTextFieldName() + ", %f");
					}
				}
				this.runner.update(this.driver.getConnection(), query);
				table_name_by_predicate.put(p, fresh_table_name);
				return fresh_table_name;
			} else {
				String table_name = (String)result.get(0)[0];
				table_name_by_predicate.put(p, table_name);
				return table_name;
			}
		} catch (SQLException e) {
			throw new SQLException("[AdHocSQLStrategy] An SQL error occured while trying to get the name of the table for the atom " + atom, e);
		}
	}

	@Override
	public Collection<String> getAllTableNames() throws SQLException {
		Collection<String> table_names = new ArrayList<String>();
		try {
			List<Object[]> results = this.runner.query(this.driver.getConnection(), "SELECT predicate_table FROM " + this.get_predicate_table_name() + ";", new ArrayListHandler());
			for(Object[] res : results) {
				table_names.add(res[0].toString());
			}
		} catch (SQLException e) {
			throw new SQLException("[AdHocSQLStrategy] An SQL error occured while trying to get the name of all the tables in the database", e);
		}
		return table_names;
	}
	
	@Override
	public Collection<Predicate> getAllPredicates(PredicateFactory factory) throws SQLException {
		Collection<Predicate> predicates_names = new ArrayList<Predicate>();
		try {
			List<Object[]> results = this.runner.query(this.driver.getConnection(), this.get_all_predicates_query(), new ArrayListHandler());
			for(Object[] res : results) {
				predicates_names.add(this.createPredicate(res[0].toString(), Integer.parseInt(res[1].toString()), factory));
			}
		} catch (SQLException e) {
			throw new SQLException("[AdHocSQLStrategy] An SQL error occured while trying to get the name of all the predicates in the database", e);
		}
		return predicates_names;
	}

	@Override
	public void handleTerms(Set<Term> terms) throws SQLException {
		String query = this.driver.getBaseSafeInsertQuery();
		query = query.replace("%t", this.get_terms_table_name() + "(term, term_type)");
		query = query.replace("%f", "? ,?");
		List<String[]> arguments = terms.parallelStream().map(term -> new String[] {term.getLabel(), getType(term)}).collect(Collectors.toList());
		try {
			this.runner.insertBatch(this.driver.getConnection(), query, new ArrayListHandler(), arguments.toArray(new String[arguments.size()][]));
		} catch(SQLException e) {
			throw new SQLException("[AdHocSQLStrategy] An SQL error occured while trying to insert terms", e);
		}
	}

	@Override
	public String getRepresentation(Term t) throws SQLException {
		return t.getLabel();
	}

	@Override
	public Term createTerm(String term, TermFactory factory) throws SQLException {
		List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_term_table_query(), new ArrayListHandler(), term);
		if(result.size() > 0) {
			String type = result.get(0)[1].toString();
			switch(type) {
			case "V" : return factory.createOrGetVariable(term);
			case "C" : return factory.createOrGetConstant(term);
			case "L" : return factory.createOrGetLiteral(term);
			default : throw new IllegalArgumentException("Unexpected value: " + type);
			}
		}
		return null;
	}
	
	@Override
	public Predicate createPredicate(String predicate, int arity, PredicateFactory factory) {
		return factory.createOrGetPredicate(predicate, arity);
	}

	@Override
	public String getColumnName(String table, int term_index) {
		return "TERM"+term_index;
	}

	@Override
	public SQLParameterizedQuery addSpecificConditions(String sql_query, List<String> arguments, FOQuery q) {
		return new SQLParameterizedQuery(sql_query, arguments);
	}

	@Override
	public boolean hasCorrectDatabaseSchema(RDBMSDriver driver) throws SQLException {
		return driver.hasTable(this.get_predicate_table_name()) && driver.hasTable(this.get_terms_table_name());
	}

	@Override
	public void createDatabaseSchema(RDBMSDriver driver) throws SQLException {		
		String predicate_query = this.get_create_predicate_table_query().replaceAll("%s", this.driver.getTextFieldName());
		predicate_query = predicate_query.replaceAll("%i", this.driver.getNumberFieldName());
		this.runner.update(this.driver.getConnection(), predicate_query);
		String term_query = this.get_create_terms_table_query().replaceAll("%s", this.driver.getTextFieldName());
		term_query = term_query.replaceAll("%i", this.driver.getTextFieldName());
		this.runner.update(this.driver.getConnection(), term_query);
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	private String getFreshPredicateTableName() throws SQLException {
		if(this.predicate_count == -1) {
			List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_count_predicate_query(), new ArrayListHandler());
			if(result.isEmpty()) {
				this.predicate_count = 0;
			} else {
				this.predicate_count = Long.parseLong(result.get(0)[0].toString());
			}
		}
		return this.get_atom_table_prefix() + this.predicate_count++;
	}

	/////////////////////////////////////////////////
	// Constant getters
	/////////////////////////////////////////////////

	// methods are protected because they can be redefined by sub-classes

	protected String get_predicate_table_name() { return "predicates"; }

	protected String get_create_predicate_table_query() { return "CREATE TABLE " +
			this.get_predicate_table_name() +
			"(predicate_label %s PRIMARY KEY, predicate_arity %i, predicate_table %s);"; }

	protected String get_predicate_table_query() { return "SELECT predicate_table" + 
			" FROM " + this.get_predicate_table_name() +
			" WHERE predicate_label = ? AND predicate_arity = ?;"; }
	
	protected String get_all_predicates_query() { return "SELECT predicate_label, predicate_arity" + 
			" FROM " + this.get_predicate_table_name() + ";"; }

	protected String get_insert_predicate_query() { return "INSERT INTO " + this.get_predicate_table_name() + " VALUES (?, ?, ?);"; }

	protected String get_count_predicate_query() { return "SELECT count(*) FROM " + this.get_predicate_table_name() + ";"; }

	protected String get_terms_table_name() { return "terms"; }

	protected String get_create_terms_table_query() { return "CREATE TABLE " + this.get_terms_table_name() + "(term %s PRIMARY KEY, term_type %s);"; }

	protected String get_term_table_query() { return "SELECT term, term_type" + 
			" FROM " + this.get_terms_table_name() +
			" WHERE term = ?;"; }

	protected String get_create_atom_table() { return "CREATE TABLE %s(%f);"; }

	protected String get_atom_table_prefix() { return "pred"; }

}