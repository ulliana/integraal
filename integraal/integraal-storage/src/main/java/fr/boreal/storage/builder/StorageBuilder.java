package fr.boreal.storage.builder;

import java.util.Optional;

import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.storage.inmemory.DefaultInMemoryAtomSet;
import fr.boreal.storage.inmemory.SimpleFOFormulaStore;
import fr.boreal.storage.inmemory.SimpleInMemoryGraphStore;
import fr.boreal.storage.wrapper.AbstractStorageWrapper;
import fr.boreal.storage.wrapper.rdbms.RDBMSWrapper;
import fr.boreal.storage.wrapper.rdbms.driver.HSQLDBDriver;
import fr.boreal.storage.wrapper.rdbms.driver.MySQLDriver;
import fr.boreal.storage.wrapper.rdbms.driver.PostgreSQLDriver;
import fr.boreal.storage.wrapper.rdbms.driver.RDBMSDriver;
import fr.boreal.storage.wrapper.rdbms.driver.SQLiteDriver;
import fr.boreal.storage.wrapper.rdbms.strategy.AdHocSQLStrategy;
import fr.boreal.storage.wrapper.rdbms.strategy.EncodingAdHocSQLStrategy;
import fr.boreal.storage.wrapper.rdbms.strategy.RDBMSStorageStrategy;
import fr.boreal.storage.wrapper.triplestore.TripleStoreWrapper;

/**
 * This Builder creates storages for atoms according to the configuration
 */
public class StorageBuilder {

	private TermFactory tf = SameObjectTermFactory.instance();
	private PredicateFactory pf = SameObjectPredicateFactory.instance();

	private FactStorage fs;
	private AbstractStorageWrapper wrapper;

	private DBType dbType = DBType.SQL;
	private enum DBType {SQL, SPARQL};

	private RDBMSDriver driver;
	private DriverType driverType = DriverType.HSQLDB;
	private enum DriverType {HSQLDB, MySQL, PostgreSQL, SQLite};

	private RDBMSStorageStrategy storageStrategy;
	private Strategies strategy = Strategies.EncodingAdHocSQL;
	private enum Strategies {AdHocSQL, EncodingAdHocSQL};
	
	private String dbParams;

	///////////
	// Build //
	///////////

	/**
	 * Creates and return the FactStorage associated to the current configuration
	 * @return an optional containing the created storage if the configuration was correct or an empty optional otherwise.
	 */
	public Optional<FactStorage> build() {
		if(this.getMinimalConfig().isPresent()) {
			return Optional.of(this.fs);
		} else {
			return Optional.empty();
		}
	}

	//////////////////////
	// Default settings //
	//////////////////////

	private Optional<StorageBuilder> getMinimalConfig() {
		if(this.fs == null) {
			if(this.wrapper == null) {
				try {
					switch(this.dbType) {
					case SPARQL :
						if(this.dbParams == null) {
							this.setWrapper(new TripleStoreWrapper(this.tf, this.pf));
						} else {
							this.setWrapper(new TripleStoreWrapper(this.dbParams, this.tf, this.pf));
						}
						break;
					case SQL :
						if(this.driver == null) {
							switch(this.driverType) {
							case HSQLDB : this.setDriver(new HSQLDBDriver(this.dbParams)); break;
							case MySQL : this.setDriver(new MySQLDriver(this.dbParams)); break;
							case PostgreSQL : this.setDriver(new PostgreSQLDriver(this.dbParams)); break;
							case SQLite : this.setDriver(new SQLiteDriver(this.dbParams)); break;
							};
						}
						if(this.storageStrategy == null) {
							switch(this.strategy) {
							case AdHocSQL : this.setStorageStrategy(new AdHocSQLStrategy((RDBMSDriver)this.driver)); break;
							case EncodingAdHocSQL : this.setStorageStrategy(new EncodingAdHocSQLStrategy((RDBMSDriver)this.driver)); break;
							};
						}
						this.setWrapper(new RDBMSWrapper(
								(RDBMSDriver)this.driver,
								(RDBMSStorageStrategy) this.storageStrategy,
								this.tf,
								this.pf));
						break;
					}
				} catch(Exception e) {
					System.err.println(e);
					return Optional.empty();
				}
			}
			this.setFactStorage(this.wrapper);
		}
		return Optional.of(this);
	}

	/////////////////////
	// Default builder //
	/////////////////////

	/**
	 * @return a default configuration of the builder
	 */
	public static StorageBuilder defaultBuilder() {
		return new StorageBuilder();
	}

	///////////////////////
	// In-memory storage //
	///////////////////////

	/**
	 * The default storage is a {@link SimpleInMemoryGraphStore}
	 * @return the default storage used by Graal
	 */
	public static FactStorage defaultStorage() {
		return StorageBuilder.getSimpleInMemoryGraphStore();
	}

	/**
	 * @return a new {@link SimpleInMemoryGraphStore}
	 */
	public static SimpleInMemoryGraphStore getSimpleInMemoryGraphStore() {
		return new SimpleInMemoryGraphStore();
	}

	/**
	 * @return a new {@link DefaultInMemoryAtomSet}
	 */
	public static DefaultInMemoryAtomSet getDefaultInMemoryAtomSet() {
		return new DefaultInMemoryAtomSet();
	}

	/**
	 * @return a new {@link SimpleFOFormulaStore}
	 */
	public static SimpleFOFormulaStore getSimpleFOFormulaStore() {
		return new SimpleFOFormulaStore();
	}

	///////////////////////////
	// Higher level settings //
	///////////////////////////


	/**
	 * Configure this builder to use a HSQLDB driver with the given parameters
	 */
	public StorageBuilder useHSQLDB(String params) {
		this.dbType = DBType.SQL;
		this.driverType = DriverType.HSQLDB;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a MySQL driver with the given parameters
	 */
	public StorageBuilder useMySQLDB(String params) {
		this.dbType = DBType.SQL;
		this.driverType = DriverType.MySQL;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a Postgres driver with the given parameters
	 */
	public StorageBuilder usePostgreSQLDB(String params) {
		this.dbType = DBType.SQL;
		this.driverType = DriverType.PostgreSQL;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a SQLite driver with the given parameters
	 */
	public StorageBuilder useSQLiteDB(String params) {
		this.dbType = DBType.SQL;
		this.driverType = DriverType.SQLite;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a SPARQL endpoint with the given parameters
	 */
	public StorageBuilder useSPARQLEndpoint(String params) {
		this.dbType = DBType.SPARQL;
		this.dbParams = params;
		return this;
	}

	/**
	 * Configure this builder to use a AdHoc storage strategy
	 */
	public StorageBuilder useAdHocSQLStrategy() {
		this.strategy = Strategies.AdHocSQL;
		return this;
	}

	/**
	 * Configure this builder to use a AdHoc storage strategy with encoding
	 */
	public StorageBuilder useEncodingAdHocSQLStrategy() {
		this.strategy = Strategies.EncodingAdHocSQL;
		return this;
	}

	///////////////////////////
	// Mapping configuration //
	///////////////////////////

	/**
	 * Configure this builder with the given mapping configuration file
	 * This is not implemented yet !
	 */
	public StorageBuilder fromMappingFile(String filepath) {
		//TODO
		return this;
	}

	/////////////
	// Setters //
	/////////////

	/**
	 * Directly sets the factory for terms for this builder as given by the user
	 */
	public StorageBuilder setTermFactory(TermFactory f) {
		this.tf = f;
		return this;
	}

	/**
	 * Directly sets the factory for predicates for this builder as given by the user
	 */
	public StorageBuilder setPredicateFactory(PredicateFactory f) {
		this.pf = f;
		return this;
	}

	/**
	 * Directly sets the RDBMS driver for this builder as given by the user
	 */
	public StorageBuilder setDriver(RDBMSDriver d) {
		this.driver = d;
		return this;
	}

	/**
	 * Directly sets the storage strategy for this builder as given by the user
	 */
	public StorageBuilder setStorageStrategy(RDBMSStorageStrategy s) {
		this.storageStrategy = s;
		return this;
	}

	/**
	 * Directly sets the wrapper for this builder as given by the user
	 */
	public StorageBuilder setWrapper(AbstractStorageWrapper w) {
		this.wrapper = w;
		return this;
	}

	/**
	 * Directly sets the fact storage for this builder as given by the user
	 */
	public StorageBuilder setFactStorage(FactStorage f) {
		this.fs = f;
		return this;
	}

}
