package fr.boreal.storage.wrapper.evaluator;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Sends a HTTP query.
 * This evaluator uses java.net's {@link HttpRequest} to send the query.
 * The result is a String which represents the results according to the queried service
 * * This will often represents raw HTML or JSON data.
 */
public class HttpQueryEvaluator implements NativeQueryEvaluator<String, String> {

	private String username;
	private String password;

	/**
	 * Construct a new evaluator which will used the given credentials
	 * @param username
	 * @param password
	 */
	public HttpQueryEvaluator(String username, String password) {
		this.username = username;
		this.password = password;
	}

	@Override
	public Optional<String> evaluate(String query) {
		try {
			HttpRequest request = HttpRequest.newBuilder().uri(new URI(query)).build();
			CompletableFuture<HttpResponse<String>> response = HttpClient.newBuilder()
					.authenticator(new Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(
									HttpQueryEvaluator.this.username, 
									HttpQueryEvaluator.this.password.toCharArray());
						}
					}).build()
					.sendAsync(request, BodyHandlers.ofString());
			return Optional.of(response.get().body());
		} catch (Exception e) {
			System.err.println(e);
			return Optional.empty();
		}
	}

}
