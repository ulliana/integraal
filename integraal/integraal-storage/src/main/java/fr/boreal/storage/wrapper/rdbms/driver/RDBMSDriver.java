package fr.boreal.storage.wrapper.rdbms.driver;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * RDBMSDrivers represents a way to connect and physically execute queries over a database.
 * This is a wrapper over java.sql which also give access to some useful and driver-specific SQL queries.
 * 
 * A driver may keep a connection open in order to execute queries faster
 */
public interface RDBMSDriver {
	
	/**
	 * @return the {@link DataSource} represented by this driver
	 */
	public DataSource getDatasource();
	
	/**
	 * A driver may keep a connection open in order to execute queries faster or create a new one each time
	 * @return a {@link Connection} for the database represented by this driver
	 */
	public Connection getConnection();
	
	public String getJDBCString();
	
	/**
	 * @return true iff the database represented by this driver have a table with the given name
	 * @throws SQLException iff something bad happen
	 */
	default public boolean hasTable(String table_name) throws SQLException {
		DatabaseMetaData dbMetaData = this.getDatasource().getConnection().getMetaData();
		ResultSet results = dbMetaData.getTables(null, null, table_name, new String[] {"TABLE"});
		if(results.next() ) {
			return results.getString(3).equals(table_name);
		}
		return false;
	}
	
	/**
	 * @return the base SELECT query corresponding to this driver
	 */
	default public String getBaseSelectQuery() {
		return "SELECT %s FROM %t";
	}
	
	/**
	 * @return the base SELECT query with a WHERE clause corresponding to this driver
	 */
	default public String getBaseSelectFilteredQuery() {
		return "SELECT %s FROM %t WHERE %c";
	}
	
	/**
	 * @return the base alias used to name tables for this driver
	 */
	default public String getBaseTableAlias() {
		return "atom";
	}
	
	/**
	 * @return the base INSERT query corresponding to this driver
	 */
	default public String getBaseInsertQuery() {
		return "INSERT INTO %t VALUES(%d);";
	}
	
	/**
	 * @return the base INSERT query which does not creates exceptions in case of duplicates corresponding to this driver
	 */
	public String getBaseSafeInsertQuery();

	/**
	 * @return the base INSERT query using a SELECT clause which does not creates exceptions in case of duplicates corresponding to this driver
	 */
	public String getBaseSafeInsertSelectQuery();

	/**
	 * @return the name of the Text field corresponding to this driver
	 */
	public String getTextFieldName();

	/**
	 * @return the name of the Number field corresponding to this driver
	 */
	public String getNumberFieldName();

}
