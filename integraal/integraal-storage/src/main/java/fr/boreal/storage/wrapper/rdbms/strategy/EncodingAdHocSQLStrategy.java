package fr.boreal.storage.wrapper.rdbms.strategy;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.dbutils.handlers.ArrayListHandler;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.storage.wrapper.rdbms.driver.RDBMSDriver;

/**
 * This strategy works in the same way as the AdHoc but additionally
 * Each term is also stored with it's encoding (a number)
 * In the tables representing atoms, only the encoding is stored
 * This lets us work only with numbers for comparison and also potentially reduces the storage cost of string representations for each occurrence of the term.
 */
public class EncodingAdHocSQLStrategy extends AdHocSQLStrategy {

	private BiMap<Term, Long> dictionary= HashBiMap.create();

	private long encoding_counter = -1;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new Strategy over the given driver
	 */
	public EncodingAdHocSQLStrategy(RDBMSDriver driver) {
		super(driver);
	}

	/////////////////////////////////////////////////
	// SQLStorageStrategy methods
	/////////////////////////////////////////////////

	@Override
	public void handleTerms(Set<Term> terms) throws SQLException {	
		if(this.encoding_counter == -1) {
			List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_max_term_encoding_query(), new ArrayListHandler());
			if(result.isEmpty() || result.get(0)[0] == null) {
				this.encoding_counter = 0;
			} else {
				this.encoding_counter = Long.parseLong(result.get(0)[0].toString());
			}
		}

		String query = this.driver.getBaseSafeInsertQuery();
		query = query.replace("%t", this.get_terms_table_name() + "(term, term_type, encoding)");
		query = query.replace("%f", "? ,?, ?");
		List<String[]> arguments = terms.stream()
				.filter(term -> ! this.dictionary.containsKey(term))
				.map(term -> {
					long term_encoding = ++this.encoding_counter;
					this.dictionary.put(term, term_encoding);
					return new String[] {term.getLabel(), getType(term), String.valueOf(term_encoding)};
				}).collect(Collectors.toList());
		if(!arguments.isEmpty()) {
			try {
				this.runner.insertBatch(this.driver.getConnection(), query, r -> null, arguments.toArray(new String[arguments.size()][]));
			} catch(SQLException e) {
				System.err.println(e);
				throw new SQLException("[AdHocSQLStrategy] An SQL error occured while trying to insert terms", e);
			}
		}
	}

	@Override
	public String getRepresentation(Term t) throws SQLException {
		if(this.dictionary.containsKey(t)) {
			return String.valueOf(this.dictionary.get(t));
		} else {
			List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_term_table_query_from_label(), new ArrayListHandler(), t.getLabel());
			if(result.size() > 0) {
				this.dictionary.put(t, Long.valueOf(result.get(0)[0].toString()));
				return result.get(0)[0].toString();
			}
			return null;
		}
	}

	@Override
	public Term createTerm(String term_encoding, TermFactory factory) throws SQLException {
		if(this.dictionary.containsValue(Long.valueOf(term_encoding))) {
			return this.dictionary.inverse().get(Long.valueOf(term_encoding));
		}

		List<Object[]> result = this.runner.query(this.driver.getConnection(), this.get_term_table_query_from_encoding(), new ArrayListHandler(), term_encoding);
		if(result.size() > 0) {
			String term_label = result.get(0)[0].toString();
			String type = result.get(0)[1].toString();
			switch(type) {
			case "V" : return factory.createOrGetVariable(term_label);
			case "C" : return factory.createOrGetConstant(term_label);
			case "L" : return factory.createOrGetLiteral(term_label);
			default : throw new IllegalArgumentException("Unexpected value: " + type);
			}
		}
		return null;
	}

	/////////////////////////////////////////////////
	// Constant getters
	/////////////////////////////////////////////////

	@Override
	protected String get_create_terms_table_query() { return "CREATE TABLE " + this.get_terms_table_name() + "(term %s PRIMARY KEY, term_type %s, encoding %i);"; }

	protected String get_term_table_query_from_encoding() { return "SELECT term, term_type" + 
			" FROM " + this.get_terms_table_name() +
			" WHERE encoding = ?;";
	}

	protected String get_term_table_query_from_label() { return "SELECT encoding" + 
			" FROM " + this.get_terms_table_name() +
			" WHERE term = ?;";		
	}

	protected String get_max_term_encoding_query() { return "SELECT MAX(encoding) FROM " + this.get_terms_table_name() + ";";}
}
