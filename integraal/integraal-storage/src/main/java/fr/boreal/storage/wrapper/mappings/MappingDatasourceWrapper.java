package fr.boreal.storage.wrapper.mappings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.impl.TermFactoryImpl;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.storage.wrapper.AbstractStorageWrapper;
import fr.boreal.storage.wrapper.evaluator.NativeQueryEvaluator;
import fr.boreal.storage.wrapper.mappings.specializer.Specializer;
import fr.boreal.storage.wrapper.mappings.transformer.Transformer;

/**
 * This wrapper represents data-sources accessed using mappings.
 * Theses are sources queried by user-given queries with read-only rights
 * 
 * A data-source can define several relational views, which associate a Predicate (of the relational view) to a specification of this view 
 * @see {@link RelationalViewSpecification} for more information.
 *
 * @param <NativeQueryType> the type used to represent the native query
 * @param <NativeResultType> the type used to represent the results as the native format
 */
public abstract class MappingDatasourceWrapper<NativeQueryType, NativeResultType> extends AbstractStorageWrapper {

	private Specializer<NativeQueryType> specializer;
	private NativeQueryEvaluator<NativeQueryType, NativeResultType> evaluator;
	private Transformer<NativeResultType> transformer;
	private Map<Predicate, NativeQueryType> queryByPredicate;

	public MappingDatasourceWrapper(Specializer<NativeQueryType> specializer, NativeQueryEvaluator<NativeQueryType, NativeResultType> evaluator, Transformer<NativeResultType> transformer) {
		this.specializer = specializer;
		this.evaluator = evaluator;
		this.transformer = transformer;
		this.queryByPredicate = new HashMap<Predicate, NativeQueryType>();
	}

	/**
	 * Adds the given mapping association
	 * Replaces existing mapping associated to the given predicate if any exists
	 * @param p view predicate associated to this mapping
	 * @param query associated to this view predicate
	 */
	public void addMapping(Predicate p, NativeQueryType query) {
		this.queryByPredicate.put(p, query);
	}

	/**
	 * @return the native query associated to the view predicate p
	 */
	public NativeQueryType getQuery(Predicate p) {
		return this.queryByPredicate.get(p);
	}

	/**
	 * @return the native query specializer associated to the view predicate p
	 */
	public Specializer<NativeQueryType> getSpecializer(Predicate p) {
		return this.specializer;
	}

	/**
	 * @return the native query evaluator associated to the view predicate p
	 */
	public NativeQueryEvaluator<NativeQueryType, NativeResultType> getEvaluator(Predicate p) {
		return this.evaluator;
	}

	/**
	 * @return the native result transformer associated to the view predicate p
	 */
	public Transformer<NativeResultType> getTransformer(Predicate p) {
		return this.transformer;
	}

	/////////////////////////////////////////////////
	// FactStorage methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {		
		List<String> parameters = new ArrayList<String>();
		Atom substitut = s.createImageOf(a);
		for(Term t : substitut.getTerms()) {
			if(!t.isVariable()) {
				parameters.add(t.getLabel());
			} else {
				// once we get the first variable we stop the parameter creation
				break;
			}
		}

		Predicate p = a.getPredicate();
		NativeQueryType nativeQuery = this.getQuery(p);
		nativeQuery = this.getSpecializer(p).specialize(nativeQuery, parameters);

		Optional<NativeResultType> nativeResult = this.getEvaluator(p).evaluate(nativeQuery);
		if(nativeResult.isEmpty()) {
			return Collections.<Atom>emptyIterator();
		} else {
			Iterator<Atom> matched_atoms = this.getTransformer(p).transform(nativeResult.get(), substitut);
			return post_filter(matched_atoms, a, s);
		}

	}

	@Override
	public Iterator<Atom> match(Atom a) {
		return this.match(a, new SubstitutionImpl());
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate predicate) {
		List<Term> variables = new ArrayList<Term>();
		for (int i = 0; i < predicate.getArity(); i++) {
			variables.add(TermFactoryImpl.instance().createOrGetFreshVariable());
		}
		Atom query = new AtomImpl(predicate, variables);
		return this.match(query);
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		return this.queryByPredicate.keySet().iterator();
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return StreamSupport.stream(((Iterable<Atom>) () -> this.getAtomsByPredicate(p)).spliterator(), false)
				.map(a -> a.getTerm(position))
				.distinct()
				.iterator();
	}

	@Override
	public Stream<Atom> getAtoms() {
		return StreamSupport.stream(((Iterable<Predicate>) () -> this.getPredicates()).spliterator(), false)
				.flatMap(p -> StreamSupport.stream(((Iterable<Atom>) () -> this.getAtomsByPredicate(p)).spliterator(), false))
				.distinct();
	}

	////////////////////////////////////////////
	// FactBase methods 
	// These can't be defined with mappings
	// So throw an exception	

	@Override
	public boolean add(FOFormula<Atom> atoms) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		throw new UnsupportedOperationException();
	}

}
