package fr.boreal.storage.wrapper.mappings.transformer;

import java.util.Iterator;
import java.util.List;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;

/**
 * Transform SQL tuples (java.net List<Object[]>) to Atoms
 * A tuple (t0, t1, ..., tn) applied to the atom of predicate p generates a new atom of arity n+1
 * p(f(t0), f(t1), ..., f(tn))
 * where f transform a Object into the correct type of {@link Term}
 */
public class SQLTuplesTransformer implements Transformer<List<Object[]>> {

	PredicateFactory pf;
	TermFactory tf;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new transformer using default factories
	 */
	public SQLTuplesTransformer() {
		this(SameObjectPredicateFactory.instance(), SameObjectTermFactory.instance());
	}

	/**
	 * Creates a new transformer using user-given factories
	 */
	public SQLTuplesTransformer(PredicateFactory pf, TermFactory tf) {
		this.pf = pf;
		this.tf = tf;
	}

	/////////////////////////////////////////////////
	// Public method
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> transform(List<Object[]> nativeResults, Atom a) {
		Predicate p = pf.createOrGetPredicate(a.getPredicate().getLabel(), a.getPredicate().getArity());
		int arity = p.getArity();
		return nativeResults.stream()
				.map(object_terms_array -> {
					int diff = arity - object_terms_array.length;
					Term[] terms_array = new Term[arity];
					for(int i = 0; i < diff; i++) {
						terms_array[i] = a.getTerm(i);
					}
					for (int i = 0; i < object_terms_array.length; i++) {
						Object object_term_i = object_terms_array[i];
						// We assume that the JDBC converted the database column type to a Java type
						// According to it's documentation, a SQL NULL is returned as a Java null
						// In this case we create a fresh variable representing an existential in the database : a (not-labeled) null
						// Otherwise we create a Literal with the corresponding type
						// We can also compute a first filter here according to constants of the atom
						if(object_term_i == null) {
							terms_array[diff+i] = tf.createOrGetFreshVariable();
							if(! a.getTerm(diff+i).isVariable()) {
								return null;
							}
						} else {
							terms_array[diff+i] = tf.createOrGetLiteral(object_term_i);
							if(! a.getTerm(diff+i).isVariable() && ! a.getTerm(diff+i).equals(terms_array[diff+i])) {
								return null;
							}
						}
					}
					return (Atom)new AtomImpl(p, terms_array);
				})
				.filter(x -> x != null)
				.iterator();
	}
}
