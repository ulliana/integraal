package fr.boreal.storage.wrapper.mappings.transformer;

import java.util.Iterator;

import fr.boreal.model.logicalElements.api.Atom;

/**
 * Transform a native result into Atoms
 * The transformation and correctness regarding data-types is implementation-specific
 */
public interface Transformer<NativeResultType>{
	
	/**
	 * Computes and returns all the atoms represented by the native results
	 * Some implementations may compute a filter using the given atom when converting the results
	 * @param nativeResults the native results representing atoms
	 * @param a the atoms to instantiate
	 * @return all the atoms represented by the native results
	 */
	public Iterator<Atom> transform(NativeResultType nativeResults, Atom a);

}
