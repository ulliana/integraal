package fr.boreal.storage.wrapper.rdbms.driver;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;
import org.sqlite.SQLiteErrorCode;
import org.sqlite.SQLiteException;

/**
 * Driver for SQLite databases
 */
public class SQLiteDriver implements RDBMSDriver {
	
	private SQLiteDataSource ds;
	private Connection test_connection;
	private String JDBCString;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new driver over the given database file
	 * @param file which contains the database
	 * @throws SQLiteException iff some error occur
	 */
	public SQLiteDriver(File file) throws SQLiteException {
		this.ds = new SQLiteDataSource(new SQLiteConfig());
		this.ds.setUrl("jdbc:sqlite:" + file);
		this.JDBCString = "jdbc:sqlite:" + file;
		try {
			Connection c = this.ds.getConnection();
			this.test_connection = c;
		} catch (SQLException e) {
			throw new SQLiteException("[SQLiteDriver] An error occured while connecting to the SQLite database at " + file.getAbsolutePath() + "\n"
					+ "Please make sure this path is correct and the database is accessible.", SQLiteErrorCode.getErrorCode(e.getErrorCode()));

		}
	}
	
	/**
	 * Creates a new driver over the given database file
	 * @param filepath the path of the file which contains the database
	 * @throws SQLiteException iff some error occur
	 */
	public SQLiteDriver(String filepath) throws SQLiteException {
		this(new File(filepath));
	}

	@Override
	public DataSource getDatasource() {
		return this.ds;
	}
	
	@Override
	public String getBaseSafeInsertQuery() {
		return "INSERT OR IGNORE INTO %t VALUES(%f);";
	}
	
	@Override
	public String getBaseSafeInsertSelectQuery() {
		return "INSERT OR IGNORE INTO %t %s;";
	}
	
	@Override
	public String getTextFieldName() {
		return "TEXT";
	}

	@Override
	public String getNumberFieldName() {
		return "INTEGER";
	}

	@Override
	public Connection getConnection() {
		return this.test_connection;
	}

	@Override
	public String getJDBCString() {
		return this.JDBCString;
	}
}
