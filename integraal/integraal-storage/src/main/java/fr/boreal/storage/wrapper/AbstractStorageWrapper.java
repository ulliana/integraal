package fr.boreal.storage.wrapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;

/**
 * A StorageWrapper is a {@link FactStorage} for which atoms are not stored directly in memory.
 * This is the case for databases, triple store, api's, files, ...
 */
public abstract class AbstractStorageWrapper implements FactStorage {

	/////////////////////////////////////////////////
	// Utility methods
	/////////////////////////////////////////////////
	
	/**
	 * Filters the given atom iterator, removing atoms that do not match the given atom
	 * This can be used by storages that do not include filtering or generation of queries with the correct filters.
	 * An atom match another one if constants are the same and the same variables are instantiated by the same constant.
	 */
	protected Iterator<Atom> post_filter(Iterator<Atom> atoms, Atom a, Substitution s) {
		
		Collection<Atom> filtered_atoms = new ArrayList<Atom>();
		
		while(atoms.hasNext()) {
			boolean valid = true;
			Atom current_match = atoms.next();
			for(int i = 0; valid && i<a.getPredicate().getArity(); i++) {
				Term t_a = a.getTerm(i);
				if(t_a.isConstant() || s.keys().contains(t_a) || t_a.isLiteral()) {
					t_a = s.createImageOf(t_a);
					// check constants
					if(! t_a.equals(current_match.getTerm(i))) {
						valid = false;
					}
				} else if(t_a.isVariable()) {
					// check variables equality
					int[] indexes = a.indexesOf(t_a);
					if(indexes.length > 1) {
						for(int j = 1; valid && j<indexes.length; j++) {
							int index_j = indexes[j];
							int index_j_1 = indexes[j-1];
							if(! current_match.getTerm(index_j).equals(current_match.getTerm(index_j_1))) {
								valid = false;
							}
						}
					}
				}
			}
			if(valid) {
				filtered_atoms.add(current_match);
			}
		}
		return filtered_atoms.iterator();
	}

}
