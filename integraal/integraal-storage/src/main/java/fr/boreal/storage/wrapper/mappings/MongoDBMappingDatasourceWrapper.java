package fr.boreal.storage.wrapper.mappings;

import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCursor;

import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.storage.wrapper.evaluator.MongoDBQueryEvaluator;
import fr.boreal.storage.wrapper.mappings.specializer.NoSpecializer;
import fr.boreal.storage.wrapper.mappings.transformer.MongoDocumentTransformer;

public class MongoDBMappingDatasourceWrapper extends MappingDatasourceWrapper<String, MongoCursor<Document>> {
	
	private String url;
	private String database;
	private String collection;
	private List<String> projectionPaths;
	
	public MongoDBMappingDatasourceWrapper(String url, String database, String collection, List<String> projectionPaths) {
		super(new NoSpecializer<String>(), new MongoDBQueryEvaluator(url, database, collection), new MongoDocumentTransformer(projectionPaths));
		this.url = url;
		this.database = database;
		this.collection = collection;
		this.projectionPaths = projectionPaths;
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return new MongoDBDescription(this.url, this.database, this.collection, this.getQuery(viewPredicate), this.projectionPaths);
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.MONGO;
	}

}
