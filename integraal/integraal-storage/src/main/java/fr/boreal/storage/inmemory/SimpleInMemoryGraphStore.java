package fr.boreal.storage.inmemory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import com.github.jsonldjava.shaded.com.google.common.collect.Sets;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.lirmm.boreal.util.validator.Validator;
import fr.lirmm.boreal.util.validator.rule.ConjunctionFormulaValidator;
import fr.lirmm.boreal.util.validator.rule.PositiveFormulaValidator;
import fr.lirmm.graphik.util.AtomType;
import fr.lirmm.graphik.util.stream.CloseableIteratorAdapter;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;
import fr.lirmm.graphik.util.stream.Iterators;
import fr.lirmm.graphik.util.stream.filter.FilterIteratorWithoutException;
import fr.lirmm.graphik.util.stream.filter.TypeFilter;


/**
 * This storage represents the atoms as a Graph in memory.
 * Nodes are Atoms
 * Vertices are Predicates and Terms used by the corresponding atoms.
 * There is also some indexes used to retrieve all the vertices corresponding to a given Predicate or Term.
 */
public class SimpleInMemoryGraphStore implements FactStorage {

	private Map<Predicate, PredicateVertex> predicateVertices;
	private Map<Term, TermVertex> termVertices;
	private long size;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new SimpleInMemoryGraphStore
	 */
	public SimpleInMemoryGraphStore() {
		predicateVertices = new HashMap<>();
		termVertices = new HashMap<>();
		size = 0;
	}


	/**
	 * Creates a new SimpleInMemoryGraphStore and adds all of the given atoms to it
	 */
	public SimpleInMemoryGraphStore(Collection<Atom> atoms) {
		this();
		this.addAll(atoms);
	}

	/////////////////////////////////////////////////
	// Inner Classes
	/////////////////////////////////////////////////

	private class PredicateVertex {
		Predicate predicate;
		Set<Atom> adjacentEdges;
		List<Set<Term>> termPositions;

		public PredicateVertex(Predicate p) {
			predicate = p;

			termPositions =  new ArrayList<>(p.getArity());
			for (int i = 0; i < p.getArity(); ++i) {
				termPositions.add(new HashSet<>());
			}

			adjacentEdges = new HashSet<>();
		}

		public boolean addEdge(Atom a) {
			if (!a.getPredicate().equals(predicate)) {
				return false;
			}

			List<Term> terms = List.of(a.getTerms());
			for (int i = 0; i < terms.size(); ++i) {
				termPositions.get(i).add(terms.get(i));
			}

			adjacentEdges.add(a);
			return true;
		}

		public Set<Atom> getAdjacentEdges() {
			return adjacentEdges;
		}

		public Set<Term> getTermsAtPosition (int position) {
			return termPositions.get(position);
		}

	}

	private class TermVertex {
		Term term;
		Map<Predicate, List<Set<Atom>>> edgesByPosition;
		Set<Atom> adjacentEdges;

		public TermVertex(Term t) {
			term = t;
			edgesByPosition = new HashMap<>();
			adjacentEdges = new HashSet<>();
		}

		public boolean addEdge(Atom a) {
			boolean hasBeenModified = false;

			List<Set<Atom>> positions = edgesByPosition.get(a.getPredicate());
			for (int i = 0; i < a.getPredicate().getArity(); ++i) {
				Term t = a.getTerm(i);
				if (t.equals(term)) {
					hasBeenModified = true;

					if (positions == null) {
						positions = new ArrayList<Set<Atom>>();
						for (int j = 0; j < a.getPredicate().getArity(); ++j) {
							positions.add(new HashSet<>());
						}
						edgesByPosition.put(a.getPredicate(), positions);
					}

					positions.get(i).add(a);
				}
			}

			if (hasBeenModified) {
				adjacentEdges.add(a);
			}

			return hasBeenModified;
		}

		public Set<Atom> getAdjacentEdgesAtPosition(Predicate p, int position) {
			List<Set<Atom>> edgesOfPredicate = edgesByPosition.get(p);

			if (edgesOfPredicate == null) {
				return Collections.emptySet();
			}

			Set<Atom> edges = edgesOfPredicate.get(position);

			if (edges == null) {
				return Collections.emptySet();
			}

			return edges;
		}
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public long size() {
		return this.size;
	}

	@Override
	public CloseableIteratorWithoutException<Atom> match(Atom atom) {
		return this.match(atom, new SubstitutionImpl());
	}

	@Override
	public CloseableIteratorWithoutException<Atom> match(Atom atom, Substitution s) {
		CloseableIteratorWithoutException<Atom> it = null;
		final AtomType atomType = new AtomType(atom, s);
		if (atomType.isThereConstant()) {
			// find smallest iterator
			int size = Integer.MAX_VALUE;
			for (int i = 0; i < atom.getPredicate().getArity(); i++) {
				if (atomType.getType(i) == AtomType.CONSTANT_OR_FROZEN_VAR) {
					Term t = atom.getTerm(i);
					Optional<TermVertex> otv = this.getTermVertex(s.createImageOf(t));
					if (otv.isPresent()) {
						TermVertex tv = otv.get();
						int tmpSize = tv.getAdjacentEdgesAtPosition(atom.getPredicate(), i).size();
						if (tmpSize < size) {
							size = tmpSize;
							it = new CloseableIteratorAdapter<Atom>(
									tv.getAdjacentEdgesAtPosition(atom.getPredicate(), i).iterator());
						}
					} else {
						size = 0;
						it = Iterators.<Atom>emptyIterator();
					}
				}
			}
		} else {
			it = new CloseableIteratorAdapter<Atom>(this.getAtomsByPredicate(atom.getPredicate()));
		}

		if (atomType.isThereConstraint()) {
			return new FilterIteratorWithoutException<Atom, Atom>(it, new TypeFilter(atomType, s.createImageOf(atom)));
		} else {
			return it;
		}
	}

	@Override
	public boolean add(FOFormula<Atom> atoms) {
		Validator<FOFormula<Atom>> positive_checker = new PositiveFormulaValidator();
		Validator<FOFormula<Atom>> conjunction_checker = new ConjunctionFormulaValidator();

		if(positive_checker.check(atoms) && conjunction_checker.check(atoms)) {
			return this.addAll(atoms.flatten());
		} else {
			throw new IllegalArgumentException("[SimpleInMemoryGraphStore] Cannot add non-positive-conjunctions formulas");
		}
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		boolean changed = false;
		for(Atom a : atoms) {
			changed = this.add(a) || changed;
		}
		return changed;
	}

	/**
	 * Adds the given atom to the graph
	 * Also updates the indexes with the newly-added atom
	 * @return true iff this atom was not already in the graph
	 */
	public boolean add(Atom atom) {
		if (this.contains(atom)) {
			return false;
		}

		Optional<PredicateVertex> opv = this.getPredicateVertex(atom.getPredicate());
		PredicateVertex pv;

		if (opv.isPresent()) {
			pv = opv.get();
		} else {
			pv = this.createPredicateVertex(atom.getPredicate());
		}
		pv.addEdge(atom);

		for (int i = 0; i < atom.getPredicate().getArity(); i++) {
			Term t = atom.getTerm(i);
			Optional<TermVertex> otv = this.getTermVertex(t);
			TermVertex tv;

			if (otv.isPresent()) {
				tv = otv.get();
			} else {
				tv = this.createTermVertex(t);
			}

			tv.addEdge(atom);
		}

		++size;

		return true;
	}

	@Override
	public Stream<Atom> getAtoms() {
		return this.predicateVertices.keySet().stream()
				.map(p -> this.getAtomsByPredicate(p))
				.map(it -> Sets.newHashSet(it))
				.flatMap(set -> set.stream());
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate p) {
		Optional<PredicateVertex> pv = this.getPredicateVertex(p);
		if (!pv.isPresent()) {
			return Iterators.<Atom>emptyIterator();
		}
		return pv.get().getAdjacentEdges().iterator();
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		return this.predicateVertices.keySet().iterator();
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		if (this.getPredicateVertex(p).isPresent()) {
			return this.predicateVertices.get(p).getTermsAtPosition(position).iterator();
		}
		return Iterators.<Term>emptyIterator();

	}

	@Override
	public boolean contains(Atom atom) {
		PredicateVertex predicateVertex = this.predicateVertices.get(atom.getPredicate());
		if (predicateVertex == null) {
			return false;
		}
		return predicateVertex.getAdjacentEdges().contains(atom);
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	private Optional<TermVertex> getTermVertex(Term t) {
		return Optional.ofNullable(this.termVertices.get(t));
	}

	private Optional<PredicateVertex> getPredicateVertex(Predicate p) {
		return Optional.ofNullable(this.predicateVertices.get(p));
	}

	private TermVertex createTermVertex(Term t) {
		TermVertex tv = new TermVertex(t);
		this.termVertices.put(t, tv);
		return tv;
	}

	private PredicateVertex createPredicateVertex(Predicate p) {
		PredicateVertex pv = new PredicateVertex(p);
		this.predicateVertices.put(p, pv);
		return pv;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(var p : this.predicateVertices.keySet()) {
			var atom_it = this.getAtomsByPredicate(p);
			while(atom_it.hasNext()) {
				sb.append(atom_it.next().toString());
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return null;
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.GRAAL;
	}}
