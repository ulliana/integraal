/**
 * 
 */
package fr.boreal.storage.inmemory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.lirmm.graphik.util.AtomType;
import fr.lirmm.graphik.util.stream.filter.FilterIteratorWithoutException;
import fr.lirmm.graphik.util.stream.filter.TypeFilter;

/**
 * The DefaultInMemoryAtomSet is a simple in memory atom set based on a java set.
 * It is a wrapper for any Collection that implements the Set interface.
 * Recommended sets :
 * - By default, use a HashSet<Atom>
 * - If you want an ordered set, you can use a TreeSet<Atom>
 * - If you want to access elements in the order there are added into the set,
 * 	use LinkedHashSet<Atom> that combine a HashSet and a Linked List
 * 
 * @author Guillaume Pérution-Kihli
 *
 */
public class DefaultInMemoryAtomSet implements FactStorage {

	private Set<Atom> atomSet;

	/**
	 * Default constructor
	 * By default, we use a HashSet<Atom>
	 */
	public DefaultInMemoryAtomSet () {
		atomSet = new HashSet<Atom>();
	}

	/**
	 * This constructor allows you to use any Collection that implements
	 * the Set<Atom> interface
	 * @param javaAtomSet the Set that will contain all the atoms
	 */
	public DefaultInMemoryAtomSet (Set<Atom> javaAtomSet) {
		atomSet = javaAtomSet;
	}

	/**
	 * A copy constructor that can copy any Collection<Atom>
	 * @param collection
	 */
	public DefaultInMemoryAtomSet (Collection<Atom> collection) {
		this();
		this.atomSet.addAll(collection);
	}

	/**
	 * A copy constructor using a variable number of Atoms
	 * @param atoms
	 */
	public DefaultInMemoryAtomSet(Atom... atoms) {
		this();
		for (Atom a : atoms)
			this.atomSet.add(a);
	}

	@Override
	public Iterator<Atom> match(Atom a) {
		return this.match(a, new SubstitutionImpl());
	}

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {
		final AtomType atomType = new AtomType(a, s);
		return new FilterIteratorWithoutException<Atom, Atom>(this.getAtomsByPredicate(a.getPredicate()), 
				new TypeFilter(atomType, s.createImageOf(a)));
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate p) {
		return (Iterator<Atom>) this.atomSet.stream()
				.filter(a -> a.getPredicate().equals(p))
				.iterator();
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		return this.atomSet.stream()
				.map(a -> a.getPredicate())
				.distinct()
				.iterator();
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return this.atomSet.stream()
				.filter(a -> a.getPredicate().equals(p))
				.map(a -> a.getTerm(position))
				.distinct()
				.iterator();
	}

	@Override
	public boolean add(FOFormula<Atom> atoms) {
		return this.addAll(atoms.flatten());
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		return this.atomSet.addAll(atoms);
	}

	@Override
	public Stream<Atom> getAtoms() {
		return this.atomSet.stream();
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return null;
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.GRAAL;
	}	

}
