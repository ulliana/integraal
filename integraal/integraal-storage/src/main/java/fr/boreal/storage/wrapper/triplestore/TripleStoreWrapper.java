package fr.boreal.storage.wrapper.triplestore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sparql.SPARQLConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.api.FOFormulaConjunction;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.wrapper.AbstractStorageWrapper;
import fr.boreal.storage.wrapper.DatalogRuleDelegatable;
import fr.boreal.storage.wrapper.evaluator.SparqlQueryEvaluator;
import fr.lirmm.boreal.util.validator.Validator;
import fr.lirmm.boreal.util.validator.rule.ConjunctionFormulaValidator;
import fr.lirmm.boreal.util.validator.rule.PositiveFormulaValidator;

/**
 * This wrapper represents Triple stores handled by Graal.
 * They are accessed by translating atoms, queries and even some rules into SPARQL queries or simple {@link Statement} if possible 
 */
public class TripleStoreWrapper extends AbstractStorageWrapper implements DatalogRuleDelegatable {

	private TermFactory tf = SameObjectTermFactory.instance();
	private PredicateFactory pf = SameObjectPredicateFactory.instance();

	private static final String RDF_PREFIX = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

	private ValueFactory innerFactory;
	private RepositoryConnection connection;
	private SparqlQueryEvaluator evaluator;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new Triple store kept in memory
	 */
	public TripleStoreWrapper() {
		this(new SailRepository(new MemoryStore()));
	}

	/**
	 * Creates a new wrapper over the given repository
	 * @param repo
	 */
	public TripleStoreWrapper(Repository repo) {
		try {
			this.innerFactory = repo.getValueFactory();
			this.connection = repo.getConnection();
			this.evaluator = new SparqlQueryEvaluator(repo);
		} catch (RepositoryException e) {
			System.err.println(e);
		}	
	}

	/**
	 * Creates a new Triple store kept in memory with the user-given factories
	 */
	public TripleStoreWrapper(TermFactory tf, PredicateFactory pf) {
		this();
		this.tf = tf;
		this.pf = pf;
	}

	/**
	 * Creates a new Triple store over the given endpoint url
	 */
	public TripleStoreWrapper(String endpoint_url) {
		this(new SPARQLRepository(endpoint_url));
	}

	/**
	 * Creates a new Triple store over the given endpoint url
	 * with user-given factories
	 */
	public TripleStoreWrapper(String endpoint_url, TermFactory tf, PredicateFactory pf) {
		this(endpoint_url);
		this.tf = tf;
		this.pf = pf;
	}

	/////////////////////////////////////////////////
	// FactStorage methods
	/////////////////////////////////////////////////

	@Override
	public boolean add(FOFormula<Atom> atoms) {
		Validator<FOFormula<Atom>> positive_checker = new PositiveFormulaValidator();
		Validator<FOFormula<Atom>> conjunction_checker = new ConjunctionFormulaValidator();

		if(positive_checker.check(atoms) && conjunction_checker.check(atoms)) {
			return this.addAll(atoms.flatten());
		} else {
			throw new IllegalArgumentException("[SparqlWrapper] Cannot add non-positive-conjunctions formulas");
		}
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		this.evaluator.insertBatch(atoms.parallelStream().map(this::toStatement).collect(Collectors.toList()));
		return true;
	}

	@Override
	public Iterator<Atom> match(Atom atom) {
		int arity = atom.getPredicate().getArity();
		if(arity > 2) {
			throw new IllegalArgumentException("Error on " + atom + " : arity " + arity + " is not supported by this storage."
					+ "This storage can only be used for predicates whose arity is <= 2.");
		}

		Resource subject;
		IRI predicate;
		Value object;

		Term term0 = atom.getTerm(0);
		if(term0.isVariable()) {
			subject = null;
		} else {
			subject = this.createIRI(term0);
		}

		if (arity == 1) {
			predicate = this.createIRI(RDF_PREFIX + "type");
			object = this.createIRI(atom.getPredicate());
		} else {
			predicate = this.createIRI(atom.getPredicate());

			Term term1 = atom.getTerm(1);
			if(term1.isVariable()) {
				object = null;
			} else if(term1.isLiteral()) {
				object = this.createLiteral(term1);
			} else {
				object = this.createIRI(term1);
			}
		}

		return this.connection.getStatements(subject, predicate, object, false)
				.stream()
				.map(this::toAtom)
				.iterator();
	}



	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {
		// We can't query specific BNode but if we already have a BNode in memory, we can still check the equality as a post treatment
		// However, one should not use it if there is BNode in the data as they will be seen as constants.
		//		for(Term t : s.keys()) {
		//			if(s.createImageOf(t).isVariable()) {
		//				throw new IllegalArgumentException("Error while matching " + a + " with initial substitution : " + s
		//						+ ". We can't query specified blank node on TripleStore : " + s.createImageOf(t));
		//			}
		//		}
		return this.post_filter(this.match(a), a, s);
	}

	@Override
	public Stream<Atom> getAtoms() {
		return this.connection.getStatements(null, null, null, false)
				.stream()
				.map(this::toAtom);
	}

	/**
	 * Evaluates the given FOQuery directly onto the storage system
	 */
	public Iterator<Substitution> evaluate(FOQuery query) {
		String sparql_query = this.translate(query);
		Optional<TupleQueryResult> result_opt = this.evaluator.evaluate(sparql_query);
		if(result_opt.isEmpty()) {
			return Collections.<Substitution>emptyIterator();
		} else {
			var result = result_opt.get();
			return result.stream().map(tuple -> {
				Substitution s = new SubstitutionImpl();
				for (Variable var : query.getAnswerVariables()) {
					Term value = this.tf.createOrGetConstant(tuple.getValue(var.getLabel()).stringValue());
					s.add(var, value);
				}
				return s;
			}).iterator();
		}
	}

	public boolean apply(Collection<FORule> rules) {
		long old_size = this.size();
		for(FORule r : rules) {
			StringBuilder sparql_query = new StringBuilder();
			sparql_query.append("INSERT {\n");
			for(Atom a : r.getHead().flatten()) {
				sparql_query.append(this.toQueryStatement(a));
				sparql_query.append("\n");
			}
			sparql_query.append("}\nWHERE {\n");
			this.translateFormula(sparql_query, r.getBody().getFormula(), new SubstitutionImpl());
			sparql_query.append("\n}");
			this.evaluator.update(sparql_query.toString());
		}
		return old_size != this.size();
	}

	/////////////////////////////////////////////////
	// Redefining default methods
	/////////////////////////////////////////////////

	@Override
	public Stream<Atom> getAtoms(Term t) {
		// the term is either the subject or the object of the triples
		return Stream.concat(
				this.connection.getStatements(this.createIRI(t), null, null, false).stream().map(this::toAtom),
				this.connection.getStatements(null, null, this.createIRI(t), false).stream().map(this::toAtom)
				);
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate predicate) {
		return this.connection.getStatements(null, this.createIRI(predicate), null, false).stream().map(this::toAtom).iterator();
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		return this.connection.getStatements(null, null, null, false).stream()
				.map(this::toAtom)
				.map(a -> a.getPredicate())
				.distinct()
				.iterator();
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return this.connection.getStatements(null, this.createIRI(p), null, false).stream()
				.map(this::toAtom)
				.map(a -> a.getTerm(position))
				.iterator();
	}

	@Override
	public boolean contains(Atom a) {
		return this.connection.hasStatement(this.toStatement(a), false);
	}

	@Override
	public long size() {
		return this.connection.size();
	}

	/////////////////////////////////////////////////
	// Private utility methods
	/////////////////////////////////////////////////

	private Atom toAtom(Statement st) {
		Predicate predicate = this.pf.createOrGetPredicate(st.getPredicate().stringValue(), 2);
		Term term0 = this.tf.createOrGetConstant(st.getSubject().stringValue());
		Term term1 = this.tf.createOrGetConstant(st.getObject().stringValue());
		return new AtomImpl(predicate, term0, term1);
	}

	private String toQueryStatement(Atom a) {
		StringBuilder sb = new StringBuilder();
		int arity = a.getPredicate().getArity();
		Term t0 = a.getTerm(0);
		if(t0.isVariable()) {
			sb.append("?" + t0.getLabel());
		} else if(t0.isLiteral()) {
			sb.append("\"");
			sb.append(this.createLiteral(t0));
			sb.append("\"");
		} else {
			sb.append("<");
			sb.append(this.createIRI(t0));
			sb.append(">");
		}

		sb.append(" ");

		if (arity == 1) {

			sb.append("<");
			sb.append(this.createIRI(RDF_PREFIX + "type"));
			sb.append(">");

			sb.append(" ");

			sb.append("<");
			sb.append(this.createIRI(a.getPredicate()));
			sb.append(">");
		} else {

			sb.append("<");
			sb.append(this.createIRI(a.getPredicate()));
			sb.append(">");

			sb.append(" ");
			Term t1 = a.getTerm(1);
			if(t1.isVariable()) {
				sb.append("?" + t1.getLabel());
			} else if(t1.isLiteral()) {
				sb.append("\"");
				sb.append(this.createLiteral(t1));
				sb.append("\"");
			} else {
				sb.append("<");
				sb.append(this.createIRI(t1));
				sb.append(">");
			}
		}
		sb.append(" .");
		return sb.toString();
	}

	private Statement toStatement(Atom atom) {
		int arity = atom.getPredicate().getArity();
		if(arity > 2) {
			throw new IllegalArgumentException("Error on " + atom + " : arity " + arity + " is not supported by this storage."
					+ "This storage can only be used for predicates whose arity is <= 2.");
		}

		Resource subject = this.createIRI(atom.getTerm(0));
		IRI predicate;
		Value object;

		if (arity == 1) {
			predicate = this.createIRI(RDF_PREFIX + "type");
			object = this.createIRI(atom.getPredicate());
		} else {
			predicate = this.createIRI(atom.getPredicate());
			Term t = atom.getTerm(1);
			if(t.isLiteral()) {
				object = this.createLiteral(t);
			} else {
				object = this.createIRI(t);
			}
		}

		return this.innerFactory.createStatement(subject, predicate, object);
	}

	private IRI createIRI(Term t) {
		if(t.isVariable()) {
			return this.createIRI("_:" + t.getLabel());
		} else {
			return this.createIRI(t.getLabel());
		}
	}

	private IRI createIRI(Predicate p) {
		return this.createIRI(p.getLabel());
	}

	private IRI createIRI(String s) {
		return this.innerFactory.createIRI(s);
	}

	private Literal createLiteral(Term term1) {
		return this.innerFactory.createLiteral(term1.getLabel());
	}

	private String translate(FOQuery query) {

		StringBuilder sparql_query = new StringBuilder();

		if(query.getAnswerVariables().isEmpty()) {
			sparql_query.append("ASK ");
		} else {
			sparql_query.append("SELECT");
			for(Variable v : query.getAnswerVariables()) {
				Term sub = query.getInitialSubstitution().createImageOf(v);
				if(sub.isVariable()) {
					sparql_query.append(" ?" + sub.getLabel());
				} else if(sub.isLiteral()) {
					sparql_query.append(" \"" + sub.getLabel() + "\"");
				} else {
					sparql_query.append(" <" + sub.getLabel() + ">");
				}
			}
		}
		sparql_query.append("\nWHERE {\n");
		this.translateFormula(sparql_query, query.getFormula(), query.getInitialSubstitution());
		sparql_query.append("}");
		return sparql_query.toString();
	}

	private void translateFormula(StringBuilder sparql_query, FOFormula<Atom> formula, Substitution s ) {
		if(formula.isAtomic()) {

			Atom a = s.createImageOf(((AtomicFOFormula<Atom>)formula).getElement());
			int arity = a.getPredicate().getArity();
			if(arity > 2) {
				throw new IllegalArgumentException("Error on " + a + " : arity " + arity + " is not supported by this storage."
						+ "This storage can only be used for predicates whose arity is <= 2.");
			}
			sparql_query.append(this.toQueryStatement(a));
		} else if(formula.isConjunction()) {
			for(FOFormula<Atom> subformula : ((FOFormulaConjunction<Atom>)formula).getSubElements()) {
				this.translateFormula(sparql_query, subformula, s);
				sparql_query.append("\n");
			}
		} else {
			// we do not handle disjunction (yet)
		}
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		int arity = viewPredicate.getArity();

		List<Term> terms = new ArrayList<Term>(arity);
		List<Variable> answerVariables = new ArrayList<Variable>(arity);
		for(int i = 0; i < arity; i++) {
			Variable v = this.tf.createOrGetFreshVariable();
			terms.add(v);
			answerVariables.add(v);
		}
		Atom a = new AtomImpl(viewPredicate, terms);
		FOQuery predicateAsQuery = FOQueryFactory.instance().createOrGetAtomicQuery(
				FOFormulaFactory.instance().createOrGetAtomic(a).get(),
				answerVariables,
				null);

		String query = this.translate(predicateAsQuery);
		String url = ((SPARQLConnection)this.connection).toString();

		return new FactBaseDescription(url, query);
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.ENDPOINT;
	}


}
