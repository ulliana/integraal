package fr.boreal.storage.wrapper.rdbms;

import java.util.List;

/**
 * This record represents a parametric SQL query with it's parameters.
 * This is used because we often need to return both values together.
 */
public class SQLParameterizedQuery {
	private String query;
	private List<String> arguments;

	public SQLParameterizedQuery(String query, List<String> arguments) {
		this.query = query;
		this.arguments = arguments;
	}

	public String query() {
		return query;
	}

	public List<String> arguments() {
		return arguments;
	}

}
