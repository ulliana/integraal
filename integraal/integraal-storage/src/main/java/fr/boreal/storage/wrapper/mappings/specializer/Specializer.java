package fr.boreal.storage.wrapper.mappings.specializer;

import java.util.List;

/**
 * Specialize the native query in order to take into account the given parameters.
 *
 * @param <NativeQueryType> the type of the query to specialize
 */
public interface Specializer<NativeQueryType> {

	/**
	 * There is no guaranty that the query will be specialized to take into account all of the parameters.
	 * This is specific to the implementation and the possibilities given by the native language.
	 * @param query to specialize
	 * @param parameters to take into account
	 * @return the specialized query if this is possible
	 */
	public NativeQueryType specialize(NativeQueryType query, List<String> parameters);
}
