package fr.boreal.storage.wrapper.evaluator;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;

import fr.boreal.storage.wrapper.rdbms.SQLParameterizedQuery;
import fr.boreal.storage.wrapper.rdbms.driver.RDBMSDriver;

/**
 * Evaluates a SQL query using the associated driver.
 * This evaluator uses Apache's {@link QueryRunner} to execute the query on the database
 * The result is a List<Object[]> which represents the list of all resulting tuples.
 * 
 * This evaluator also provides additional methods for inserting data on the database.
 */
public class SQLParameterizedQueryEvaluator implements NativeQueryEvaluator<SQLParameterizedQuery, List<Object[]>> {
	
	private QueryRunner runner;
	private Connection connection;
	
	/**
	 * Construct a new evaluator over the database represented by the given driver
	 * @param driver representing the database to connect to
	 * @throws SQLException if the initial connection to the database cannot be performed
	 */
	public SQLParameterizedQueryEvaluator(RDBMSDriver driver) throws SQLException {
		DataSource ds = driver.getDatasource();
		this.connection = ds.getConnection();
		this.runner = new QueryRunner(ds);
	}

	@Override
	public Optional<List<Object[]>> evaluate(SQLParameterizedQuery query) {
		try {
			return Optional.of(this.runner.query(this.connection, query.query(), new ArrayListHandler(), query.arguments().toArray()));
		} catch (Exception e) {
			System.err.println(e);
			return Optional.empty();
		}
	}
	
	/**
	 * Execute the given insert query
	 * @param query to execute, representing an insert query
	 * @return true iff the query has been executed without problem
	 */
	public boolean insert(SQLParameterizedQuery query) {
		try {
			runner.insert(this.connection, query.query(), new ArrayListHandler(), query.arguments().toArray());
			return true;
		} catch (SQLException e) {
			System.err.println(e);
			return false;
		}
	}
	
	/**
	 * Insert the given values, using a batch insert and parameters for the query
	 * This is more efficient than looping over the parameters and executing each query at a time
	 * @param query the parameterized query
	 * @param parameters The query replacement parameters
	 * @return true iff the queries have been executed without problem
	 */
	public boolean insertBatch(String query, List<String[]> parameters) {
		try {
			runner.insertBatch(this.connection, query, r -> r.next(), parameters.toArray(new String[parameters.size()][]));
			return true;
		} catch (SQLException e) {
			System.err.println(e);
			return false;
		}
	}

}
