package fr.boreal.storage.wrapper.mappings;

import java.util.List;
import java.util.Set;

import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.storage.wrapper.evaluator.HttpQueryEvaluator;
import fr.boreal.storage.wrapper.mappings.specializer.OrderedStringReplacementSpecializer;
import fr.boreal.storage.wrapper.mappings.transformer.json.Checker;
import fr.boreal.storage.wrapper.mappings.transformer.json.JSONStringTransformer;

public class WebAPIMappingDatasourceWrapper extends MappingDatasourceWrapper<String, String> {

	public WebAPIMappingDatasourceWrapper(String username, String password,
			Set<Checker> positionCheckers, String positionQuery,
			List<Set<Checker>> termCheckers, List<String> selectionQueries) {
		super(	new OrderedStringReplacementSpecializer("%%"),
				new HttpQueryEvaluator(username, password),
				new JSONStringTransformer(SameObjectTermFactory.instance(),
						positionCheckers, positionQuery, termCheckers, selectionQueries));
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return new FactBaseDescription(null, this.getQuery(viewPredicate));
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.WEBAPI;
	}

}
