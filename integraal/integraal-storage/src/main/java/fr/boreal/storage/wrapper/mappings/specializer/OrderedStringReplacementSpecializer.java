package fr.boreal.storage.wrapper.mappings.specializer;

import java.util.List;

/**
 * This specializer replaces a given placeholder from the initial String query by the given parameters
 * in the order of the parameters until either
 * * All the occurrences of the placeholder in the query have been replaced
 * * All the parameters have been taken into account
 */
public class OrderedStringReplacementSpecializer implements Specializer<String> {

	private String placeholder;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new OrderedStringReplacementSpecializer with the given placeholder
	 * @param placeholder to replace
	 */
	public OrderedStringReplacementSpecializer(String placeholder) {
		this.placeholder = placeholder;
	}

	/////////////////////////////////////////////////
	// Public method
	/////////////////////////////////////////////////

	@Override
	public String specialize(String query, List<String> parameters) {
		int i = 0;
		int arity = parameters.size();

		while(i < arity && query.contains(this.placeholder)) {
			String replacement = parameters.get(i);
			query = query.replaceFirst(this.placeholder, replacement);
			++i;
		}

		return query;
	}

}
