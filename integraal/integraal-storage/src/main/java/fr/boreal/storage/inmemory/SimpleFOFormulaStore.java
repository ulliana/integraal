package fr.boreal.storage.inmemory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.api.FactStorage;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.lirmm.graphik.util.AtomType;
import fr.lirmm.graphik.util.stream.filter.FilterIteratorWithoutException;
import fr.lirmm.graphik.util.stream.filter.TypeFilter;

/**
 * This storage stores formulas in a collection
 */
public class SimpleFOFormulaStore implements FactStorage {

	Collection<FOFormula<Atom>> facts;

	/**
	 * Default constructor
	 * By default, we use a HashSet<Atom>
	 */
	public SimpleFOFormulaStore() {
		this.facts = new HashSet<FOFormula<Atom>>();
	}

	/**
	 * A constructor initialized with a single formula
	 * @param formula the formula stored in this system
	 */
	public SimpleFOFormulaStore(FOFormula<Atom> formula) {
		this();
		this.add(formula);
	}

	/**
	 * This constructor allows you to use any Collection
	 * @param facts the Collection that will contain all the atoms
	 */
	public SimpleFOFormulaStore(Collection<FOFormula<Atom>> facts) {
		this.facts = facts;
	}

	@Override
	/**
	 * Be careful about storing negation in the formula as it is not (yet) used and atoms negated will still match
	 */
	public Iterator<Atom> match(Atom a) {
		return this.match(a, new SubstitutionImpl());
	}

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {
		Iterator<Atom> atoms_by_predicate = this.getAtoms()
				.filter(other -> other.getPredicate().equals(a.getPredicate()))
				.iterator();

		final AtomType atomType = new AtomType(a, s);
		return new FilterIteratorWithoutException<Atom, Atom>(atoms_by_predicate, new TypeFilter(atomType, a));
	}

	@Override
	public boolean add(FOFormula<Atom> atoms) {
		return this.facts.add(atoms);
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {
		return this.add(FOFormulaFactory.instance().createOrGetConjunction(
				atoms.stream()
				.map(a -> FOFormulaFactory.instance().createOrGetAtomic(a).get())
				.collect(Collectors.toList()))
				.get());
	}

	@Override
	public Stream<Atom> getAtoms() {
		return this.facts.stream().flatMap(formula -> formula.flatten().stream());
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate p) {
		return (Iterator<Atom>) this.getAtoms()
				.filter(a -> a.getPredicate().equals(p))
				.iterator();
	}
	
	@Override
	public Iterator<Predicate> getPredicates() {
		return this.getAtoms()
				.map(a -> a.getPredicate())
				.distinct()
				.iterator();
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return this.getAtoms()
				.filter(a -> a.getPredicate().equals(p))
				.map(a -> a.getTerm(position))
				.distinct()
				.iterator();
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return null;
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.GRAAL;
	}

}
