package fr.boreal.storage.wrapper.rdbms;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.formula.factory.FOFormulaFactory;
import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.wrapper.AbstractStorageWrapper;
import fr.boreal.storage.wrapper.DatalogRuleDelegatable;
import fr.boreal.storage.wrapper.evaluator.SQLParameterizedQueryEvaluator;
import fr.boreal.storage.wrapper.rdbms.driver.RDBMSDriver;
import fr.boreal.storage.wrapper.rdbms.strategy.RDBMSStorageStrategy;
import fr.lirmm.boreal.util.validator.Validator;
import fr.lirmm.boreal.util.validator.rule.ConjunctionFormulaValidator;
import fr.lirmm.boreal.util.validator.rule.PositiveFormulaValidator;
import fr.lirmm.graphik.util.stream.ThrowingFunction;

/**
 * This wrapper represents RDBMS handled by Graal.
 * They are accessed by translating atoms, queries and even some rules into SQL queries
 * following the given storage strategy.
 */
public class RDBMSWrapper extends AbstractStorageWrapper implements DatalogRuleDelegatable {

	private static final String SQL_VAR_PREFIX = "SQLVAR";

	private SQLParameterizedQueryEvaluator evaluator;

	private RDBMSDriver driver;
	private RDBMSStorageStrategy strategy;

	private TermFactory termFactory;
	private PredicateFactory predicateFactory;

	private Map<FOQuery, SQLParameterizedQuery> already_generated_queries = new HashMap<FOQuery, SQLParameterizedQuery>();
	private Map<FORule, Collection<SQLParameterizedQuery>> already_generated_rules = new HashMap<FORule, Collection<SQLParameterizedQuery>>();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new wrapper over a RDBMS using user-given parameters
	 * @throws SQLException
	 */
	public RDBMSWrapper(RDBMSDriver driver, RDBMSStorageStrategy strategy, TermFactory termFactory, PredicateFactory predicateFactory) throws SQLException {
		this.driver = driver;
		this.strategy = strategy;
		this.termFactory = termFactory;
		this.predicateFactory = predicateFactory;

		if(!this.strategy.hasCorrectDatabaseSchema(driver)) {
			this.strategy.createDatabaseSchema(driver);
		}

		this.evaluator = new SQLParameterizedQueryEvaluator(driver);
	}

	/////////////////////////////////////////////////
	// FactStorage methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Atom> match(Atom a) {
		return this.match(a, new SubstitutionImpl());
	}

	@Override
	public Iterator<Atom> match(Atom a, Substitution s) {

		Collection<Variable> answer_variables = new ArrayList<Variable>();
		for(Term t : a.getTerms()) {
			if(t.isVariable()) {
				answer_variables.add((Variable)t);
			} else {
				Variable new_var = this.termFactory.createOrGetVariable(SQL_VAR_PREFIX + t.getLabel());
				answer_variables.add(new_var);
				s.add(new_var, t);
			}
		}

		FOQuery q = FOQueryFactory.instance().createOrGetAtomicQuery(
				FOFormulaFactory.instance().createOrGetAtomic(a).get(),
				answer_variables,
				s);

		SQLParameterizedQuery sql_query;
		try {
			sql_query = this.translate(q);

			Optional<List<Object[]>> result_opt = evaluator.evaluate(sql_query);
			if(result_opt.isEmpty()) {
				return Collections.<Atom>emptyIterator();
			} else {
				var result = result_opt.get();
				return result.stream().map(ThrowingFunction.unchecked(o -> {
					Predicate p = this.predicateFactory.createOrGetPredicate(a.getPredicate().getLabel(), a.getPredicate().getArity());
					Term[] terms = new Term[o.length];
					for(int i = 0 ; i < o.length ; ++i) {
						terms[i] = this.strategy.createTerm(o[i].toString(), this.termFactory);
					}
					return (Atom)new AtomImpl(p, terms);
				})).iterator();
			}
		} catch (SQLException e) {
			return Collections.<Atom>emptyIterator();
		}
	}

	@Override
	public boolean add(FOFormula<Atom> atoms) {
		Validator<FOFormula<Atom>> positive_checker = new PositiveFormulaValidator();
		Validator<FOFormula<Atom>> conjunction_checker = new ConjunctionFormulaValidator();

		if(positive_checker.check(atoms) && conjunction_checker.check(atoms)) {
			return this.addAll(atoms.flatten());
		} else {
			throw new IllegalArgumentException("[SQLWrapper] Cannot add non-positive-conjunctions formulas");
		}
	}

	@Override
	public boolean addAll(Collection<Atom> atoms) {

		try {
			Map<String, List<String[]>> parameters_by_table = new HashMap<String, List<String[]>>();

			// We need to go over the atoms twice :
			// The first time to give all the terms to the storage to handle possible specific storage.
			// The second time is to get the specific label to use in the tables for the each term
			//	This is mostly useful in case of encoding

			// 	We do not want to do it term by term because we can use batch insertion to handle it faster
			//	In most cases, going over the atoms twice will be faster than sending multiple insert queries to the database

			// handling terms
			Set<Term> all_terms = new HashSet<Term>();
			for(Atom atom : atoms) {
				Predicate p = atom.getPredicate();
				for(int i = 0 ; i < p.getArity() ; ++i) {
					Term t = atom.getTerm(i);
					all_terms.add(t);
				}
			}
			if(!all_terms.isEmpty()) {
				this.strategy.handleTerms(all_terms);
			}

			// handling atoms by table
			for(Atom atom : atoms) {
				Predicate p = atom.getPredicate();
				String[] new_parameters = new String[p.getArity()];
				for(int i = 0 ; i < p.getArity() ; ++i) {
					Term t = atom.getTerm(i);
					new_parameters[i] = this.strategy.getRepresentation(t);
				}

				// handling predicates
				String table_name = this.strategy.getTableName(atom);
				if(!parameters_by_table.containsKey(table_name)) {
					parameters_by_table.put(table_name, new ArrayList<String[]>());
				}
				List<String[]> parameters = parameters_by_table.get(table_name);
				parameters.add(new_parameters);
			}

			// handling atoms
			for(String table_name : parameters_by_table.keySet()) {
				String query = this.driver.getBaseInsertQuery();
				query = query.replace("%t", table_name);
				List<String[]> parameters = parameters_by_table.get(table_name);
				int arg_count = parameters.get(0).length;
				for(int i = 0 ; i < arg_count ; ++i) {
					if(i == arg_count - 1) {
						query = query.replace("%d", "?");
					} else {
						query = query.replace("%d", "?, %d");
					}
				}
				this.evaluator.insertBatch(query, parameters);
			}
			return true;

		} catch (SQLException e) {
			System.err.println(e.getMessage());
			return false;
		}
	}

	@Override
	public Stream<Atom> getAtoms() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<Atom> getAtomsByPredicate(Predicate predicate) {
		List<Term> variables = new ArrayList<Term>();
		for (int i = 0; i < predicate.getArity(); i++) {
			variables.add(this.termFactory.createOrGetFreshVariable());
		}
		Atom query = new AtomImpl(predicate, variables);
		return this.match(query);
	}

	@Override
	public Iterator<Predicate> getPredicates() {
		try {
			return this.strategy.getAllPredicates(this.predicateFactory).iterator();
		} catch (SQLException e) {
			System.err.println("[RDBMSWrapper] An SQL Error occured while trying to get all the predicates \n" + e);
			return Collections.<Predicate>emptyIterator();
		}
	}

	@Override
	public Iterator<Term> getTermsByPredicatePosition(Predicate p, int position) {
		return StreamSupport.stream(((Iterable<Atom>) () -> this.getAtomsByPredicate(p)).spliterator(), false)
				.map(a -> a.getTerm(position))
				.distinct()
				.iterator();
	}

	/////////////////////////////////////////////////
	// Specific methods
	/////////////////////////////////////////////////

	/**
	 * Evaluates the given FOQuery directly onto the storage system
	 */
	public Iterator<Substitution> evaluate(FOQuery query) throws SQLException {
		SQLParameterizedQuery sql_query = this.translate(query);
		Optional<List<Object[]>> result_opt = this.evaluator.evaluate(sql_query);
		if(result_opt.isEmpty()) {
			return Collections.<Substitution>emptyIterator();
		} else {
			var result = result_opt.get();
			return result.stream().map(ThrowingFunction.unchecked(o -> {
				Iterator<Variable> answer_variables = query.getAnswerVariables().iterator();
				Substitution s = new SubstitutionImpl();
				for(int i = 0 ; i < o.length && answer_variables.hasNext(); ++i) {
					Variable ans = answer_variables.next();
					s.add(ans, this.strategy.createTerm(o[i].toString(), this.termFactory));
				}
				return s;
			})).iterator();
		}
	}

	public boolean apply(Collection<FORule> rules) throws SQLException {
		long old_size = this.size();
		for(FORule r : rules) {
			Collection<SQLParameterizedQuery> sql_queries = this.translate(r);
			for(SQLParameterizedQuery sql_query : sql_queries) {
				this.evaluator.insert(sql_query);
			}
		}
		return old_size != this.size();
	}


	/////////////////////////////////////////////////
	// Redefining default methods
	/////////////////////////////////////////////////

	@Override
	public Stream<Atom> getAtoms(Term t) {
		return this.getAtoms().filter(a -> a.contains(t));
	}

	@Override
	public boolean contains(Atom a) {
		Substitution s = new SubstitutionImpl();
		int arity = a.getPredicate().getArity();
		Term[] terms = new Term[arity];
		for(int i = 0 ; i < arity ; ++i) {
			Variable v = this.termFactory.createOrGetFreshVariable();
			terms[i] = v;
			s.add(v, a.getTerm(i));
		}
		Atom to_match = new AtomImpl(a.getPredicate(), terms);
		return this.match(to_match, s).hasNext();
	}

	@Override
	public long size() {
		long count = 0;
		Collection<String> table_names;
		try {
			table_names = this.strategy.getAllTableNames();
			for(String table : table_names) {
				String sql_query = "SELECT DISTINCT count(*) FROM " + table + ";";
				Optional<List<Object[]>> result_opt = this.evaluator.evaluate(new SQLParameterizedQuery(sql_query, List.of()));
				if(result_opt.isEmpty()) {
					return -1;
				} else {
					count += Long.parseLong(result_opt.get().get(0)[0].toString());
				}

			}
		} catch (SQLException e) {
			return -1;
		}
		return count;
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.RDBMS;
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		int arity = viewPredicate.getArity();

		List<Term> terms = new ArrayList<Term>(arity);
		List<Variable> answerVariables = new ArrayList<Variable>(arity);
		for(int i = 0; i < arity; i++) {
			Variable v = this.termFactory.createOrGetFreshVariable();
			terms.add(v);
			answerVariables.add(v);
		}
		Atom a = new AtomImpl(viewPredicate, terms);
		FOQuery predicateAsQuery = FOQueryFactory.instance().createOrGetAtomicQuery(
				FOFormulaFactory.instance().createOrGetAtomic(a).get(),
				answerVariables,
				null);

		String query, jdbc;
		try {
			query = this.translate(predicateAsQuery).query();
			jdbc = this.driver.getJDBCString();
		} catch (SQLException e) {
			return null;
		}

		return new FactBaseDescription(jdbc, query);
	}

	/////////////////////////////////////////////////
	// Translation methods
	/////////////////////////////////////////////////

	private SQLParameterizedQuery translate(FOQuery query) throws SQLException {
		if(this.already_generated_queries.containsKey(query)) {
			return this.already_generated_queries.get(query);
		}

		Collection<Atom> query_atoms = query.getFormula().flatten();

		Map<Atom, String[]> table_name_by_atom = this.init_tables_by_atom(query_atoms);

		String sql_query = this.driver.getBaseSelectFilteredQuery();

		// Fill the SELECT clause

		sql_query = this.handle_select_clause(table_name_by_atom, sql_query, query.getAnswerVariables(), query.getInitialSubstitution());

		// Fill the FROM clause

		sql_query = this.handle_from_clause(table_name_by_atom, sql_query);

		// Fill the WHERE clause

		SQLParameterizedQuery result = this.handle_where_clause(table_name_by_atom, sql_query, query.getInitialSubstitution(), query);

		sql_query = result.query();
		List<String> arguments = result.arguments();

		// cleanup 
		sql_query = this.cleanup_query(sql_query);

		SQLParameterizedQuery final_query = new SQLParameterizedQuery(sql_query, arguments);
		this.already_generated_queries.put(query, final_query);
		return final_query;
	}

	/**
	 * Preconditions :
	 * - the given rule have a body that can be translated by {@link this.translate FOQuery}
	 * - the head have no existential variables
	 * @return all the queries and there arguments that should be executed to apply the given rule on the database
	 * @throws SQLException 
	 */
	public Collection<SQLParameterizedQuery> translate(FORule rule) throws SQLException {
		if(this.already_generated_rules.containsKey(rule)) {
			return this.already_generated_rules.get(rule);
		}

		Collection<Atom> head_atoms = rule.getHead().flatten();

		// All terms that could be unknown get handled first
		Set<Term> non_variables_terms = new HashSet<Term>();
		for(Atom a : head_atoms) {
			for(Term t : a.getTerms()) {
				if(!t.isVariable()) {
					non_variables_terms.add(t);
				}
			}
		}

		if(!non_variables_terms.isEmpty()) {
			this.strategy.handleTerms(non_variables_terms);
		}

		// We create a query for each atom in the head of the rule
		Collection<SQLParameterizedQuery> queries = new ArrayList<SQLParameterizedQuery>();
		for(Atom a : head_atoms) {
			Collection<Variable> answer_variables = new ArrayList<Variable>();
			Substitution initial_substitution = new SubstitutionImpl();
			for(Term t : a.getTerms()) {
				if(t.isVariable()) {
					answer_variables.add((Variable)t);
				} else {
					Variable new_var = this.termFactory.createOrGetVariable(SQL_VAR_PREFIX + t.getLabel());
					answer_variables.add(new_var);
					initial_substitution.add(new_var, t);
				}
			}
			FOQuery sub_query = FOQueryFactory.instance().createOrGetQuery(rule.getBody().getFormula(), answer_variables, initial_substitution).get();
			SQLParameterizedQuery body_select_query = this.translate(sub_query);
			String select_query = body_select_query.query();
			select_query = select_query.substring(0, select_query.length() -1); // remove the last character : ;

			String table_name = this.strategy.getTableName(a);

			String insert_query = this.driver.getBaseSafeInsertSelectQuery();
			insert_query = insert_query.replace("%t", table_name);
			insert_query = insert_query.replace("%s", select_query);

			queries.add(new SQLParameterizedQuery(insert_query, body_select_query.arguments()));

		}

		return queries;
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////


	/**
	 * Initialize database tables to query for each atom and it's alias
	 * each atom need it's own table even if it's the same predicate
	 * except if there are redundancy in the given query at which point we accept to have redundancy in the SQL query
	 */
	private Map<Atom, String[]> init_tables_by_atom(Collection<Atom> query_atoms) throws SQLException {
		Map<Atom, String[]> table_name_by_atom = new HashMap<Atom, String[]>();
		int atom_index = 0;
		for(Atom a : query_atoms) {
			table_name_by_atom.put(a, new String[] {this.strategy.getTableName(a), this.driver.getBaseTableAlias() + atom_index++});
		}
		return table_name_by_atom;
	}

	/**
	 * Add all the given terms to the select clause of the sql_query in the iteration order
	 * @throws SQLException 
	 */
	private String handle_select_clause(Map<Atom, String[]> table_name_by_atom, String sql_query, Collection<Variable> answer_terms, Substitution initial_substitution) throws SQLException {
		if(answer_terms.isEmpty()) {
			sql_query = sql_query.replace("%s", "1");
		} else {
			int answer_terms_count = 0;
			for(Variable t : answer_terms) {
				answer_terms_count++;
				String replacement;
				if(initial_substitution.keys().contains(t)) {
					// 'a'
					replacement = "'" + this.strategy.getRepresentation(initial_substitution.createImageOf(t)) + "'";
				} else {
					Optional<Atom> opt_first_occurence = table_name_by_atom.keySet().stream()
							.filter(a -> a.contains(t))
							.findFirst();
					if(opt_first_occurence.isEmpty()) {
						// FIXME: error, an answer variable is not bound to any atom in the query ; should not happen if the query is correctly created
						// This could happen if translating rules with existential variables
						replacement = "";
					} else {
						// atomX.TERMY
						final Atom first_occurence = opt_first_occurence.get();
						replacement = table_name_by_atom.get(first_occurence)[1] +
								"." +
								this.strategy.getColumnName(table_name_by_atom.get(first_occurence)[0], first_occurence.indexOf(t));
					}
				}
				if(answer_terms_count == answer_terms.size()) {
					sql_query = sql_query.replace("%s", replacement);
				} else {
					sql_query = sql_query.replace("%s", replacement + ", %s");
				}
			}
		}
		return sql_query;
	}

	private String handle_from_clause(Map<Atom, String[]> table_name_by_atom, String sql_query) {
		int atom_index = 0;
		for(Atom a : table_name_by_atom.keySet()) {
			atom_index++;
			String table_as = table_name_by_atom.get(a)[0] + " AS " + table_name_by_atom.get(a)[1];
			if(atom_index == table_name_by_atom.size()) {
				sql_query = sql_query.replace("%t", table_as);
			} else {
				sql_query = sql_query.replace("%t", table_as + ", %t");
			}
		}
		return sql_query;
	}

	private SQLParameterizedQuery handle_where_clause(Map<Atom, String[]> table_name_by_atom, String sql_query, Substitution initial_substitution, FOQuery query) throws SQLException {
		List<String> arguments = new ArrayList<String>();
		Map<Term, String> already_seen = new HashMap<Term, String>();

		for(Atom a : table_name_by_atom.keySet()) {
			for(int term_index = 0 ; term_index < a.getPredicate().getArity() ; term_index++) {
				Term t = a.getTerm(term_index);
				String column_name = this.strategy.getColumnName(table_name_by_atom.get(a)[0], term_index);
				String t_database_alias = table_name_by_atom.get(a)[1] + "." + column_name;
				if(t.isConstant() || t.isLiteral() || initial_substitution.keys().contains(t)) {					
					sql_query = sql_query.replace("%c", t_database_alias + " = ? AND %c");
					arguments.add(this.strategy.getRepresentation(initial_substitution.createImageOf(t)));
				} else if(already_seen.containsKey(t)) {					
					sql_query = sql_query.replace("%c", t_database_alias + " = " + already_seen.get(t) + " AND %c");
				} else {
					already_seen.put(t, t_database_alias);
				}
			}
		}

		// Add the storage strategy specific conditions
		SQLParameterizedQuery result = this.strategy.addSpecificConditions(sql_query, arguments, query);
		return result;
	}

	private String cleanup_query(String sql_query) {
		if(sql_query.endsWith("WHERE %c")) {
			sql_query = sql_query.substring(0, sql_query.length() - "WHERE %c".length());
		} else if(sql_query.endsWith("AND %c")) {
			sql_query = sql_query.substring(0, sql_query.length() - "AND %c".length());
		}
		sql_query = sql_query.concat(";");
		return sql_query;
	}

}
