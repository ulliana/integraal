package fr.boreal.storage.wrapper.evaluator;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;

import fr.boreal.storage.wrapper.rdbms.driver.RDBMSDriver;

/**
 * Evaluates a SQL query using the associated driver.
 * This evaluator uses Apache's {@link QueryRunner} to execute the query on the database
 * The result is a List<Object[]> which represents the list of all resulting tuples.
 * 
 * This evaluator also provides additional methods for inserting data on the database.
 */
public class SQLQueryEvaluator implements NativeQueryEvaluator<String, List<Object[]>> {
	
	private QueryRunner runner;
	private Connection connection;
	
	/**
	 * Construct a new evaluator over the database represented by the given driver
	 * @param driver representing the database to connect to
	 * @throws SQLException if the initial connection to the database cannot be performed
	 */
	public SQLQueryEvaluator(RDBMSDriver driver) throws SQLException {
		DataSource ds = driver.getDatasource();
		this.connection = ds.getConnection();
		this.runner = new QueryRunner(ds);
	}

	@Override
	public Optional<List<Object[]>> evaluate(String query) {
		try {
			return Optional.of(this.runner.query(this.connection, query, new ArrayListHandler()));
		} catch (Exception e) {
			System.err.println(e);
			return Optional.empty();
		}
	}

}
