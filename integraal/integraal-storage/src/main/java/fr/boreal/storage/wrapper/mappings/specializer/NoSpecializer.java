package fr.boreal.storage.wrapper.mappings.specializer;

import java.util.List;

/**
 * This specializer do nothing
 */
public class NoSpecializer<T> implements Specializer<T> {

	@Override
	public T specialize(T query, List<String> parameters) {
		return query;
	}

}
