package fr.boreal.storage.wrapper.rdbms.driver;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.postgresql.ds.PGSimpleDataSource;
import org.postgresql.util.PSQLException;
import org.postgresql.util.PSQLState;

/**
 * Driver for PostgreSQL databases
 */
public class PostgreSQLDriver implements RDBMSDriver {
	
	private String JDBCString;

	private PGSimpleDataSource ds;
	
	private Connection test_connection;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////
	
	/**
	 * Creates a new driver using the given parameters
	 * @param host the url of the database
	 * @param dbName the name of the database
	 * @param user the username used to connect to the database
	 * @param password the password used to connect to the database
	 * @throws PSQLException iff some error occur
	 */
	public PostgreSQLDriver(String host, String dbName, String user, String password) throws PSQLException {
		this("jdbc:postgresql://" + host + "/" + dbName + "?user=" + user + "&password=" + password);
	}

	/**
	 * Creates a new driver over the given jdbc string
	 * @param uri jdbc string
	 * @throws PSQLException iff some error occur
	 */
	public PostgreSQLDriver(String uri) throws PSQLException {
		this.ds = new PGSimpleDataSource();
		this.ds.setUrl(uri);
		this.JDBCString = uri;
		try {
			this.test_connection = this.ds.getConnection();
		} catch (SQLException e) {
			PSQLState state = null;
			for(int i = 0; state == null && i < PSQLState.values().length ; ++i) {
				PSQLState state_i = PSQLState.values()[i];
				if(state_i.getState().equals(e.getErrorCode() + "")) {
					state = state_i;
				}
			}
			throw new PSQLException("[PostgreSQLDriver] An error occured while connecting to the PostgreSQL database at " + uri + "\n"
					+ "Please make sure this connection path is correct and the database is accessible.", state, e);
		}
	}
	
	@Override
	public String getJDBCString() {
		return this.JDBCString;
	}

	@Override
	public DataSource getDatasource() {
		return this.ds;
	}
	
	@Override
	public Connection getConnection() {
		return this.test_connection;
	}

	@Override
	public String getBaseSafeInsertQuery() {
		return "INSERT INTO %t VALUES(%f) ON CONFLICT DO NOTHING;";
	}
	
	@Override
	public String getBaseSafeInsertSelectQuery() {
		return "INSERT INTO %t %s ON CONFLICT DO NOTHING;";
	}

	@Override
	public String getTextFieldName() {
		return "TEXT";
	}

	@Override
	public String getNumberFieldName() {
		return "INT";
	}

}
