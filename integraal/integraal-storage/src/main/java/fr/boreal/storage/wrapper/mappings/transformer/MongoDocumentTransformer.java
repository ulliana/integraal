package fr.boreal.storage.wrapper.mappings.transformer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.bson.Document;

import com.mongodb.client.MongoCursor;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectPredicateFactory;
import fr.boreal.model.logicalElements.factory.impl.SameObjectTermFactory;
import fr.boreal.model.logicalElements.impl.AtomImpl;

/**
 * Ignores results with no values for a asked path
 * Does not create existentials
 */
public class MongoDocumentTransformer implements Transformer<MongoCursor<Document>>{

	PredicateFactory pf;
	TermFactory tf;

	List<String> projection_paths;


	public MongoDocumentTransformer(List<String> projection_paths) {
		this(projection_paths, SameObjectPredicateFactory.instance(), SameObjectTermFactory.instance());
	}
	
	public MongoDocumentTransformer(List<String> projection_paths, PredicateFactory pf, TermFactory tf) {
		this.projection_paths = projection_paths;
		this.pf = pf;
		this.tf = tf;
	}

	@Override
	public Iterator<Atom> transform(MongoCursor<Document> nativeResults, Atom a) {
		return new DocumentToAtomIterator(nativeResults, a);
	}

	class DocumentToAtomIterator implements Iterator<Atom> {

		MongoCursor<Document> it;
		Atom filter_atom;

		Atom next_element = null;

		DocumentToAtomIterator(MongoCursor<Document> nativeResults, Atom a) {
			this.it = nativeResults;
			this.filter_atom = a;
		}

		@Override
		public boolean hasNext() {
			if(next_element != null) {
				return true;
			} else if(! this.it.hasNext()) {
				return false;
			} else {
				this.computeNext();
				return next_element != null;
			}
		}

		@Override
		public Atom next() {
			if(hasNext()) {
				Atom tmp = this.next_element;
				this.next_element = null;
				return tmp;
			}
			throw new NoSuchElementException();

		}

		public void computeNext() {

			List<Term> terms = new ArrayList<Term>();

			if(!this.it.hasNext()) {
				return;
			}
			Document d = this.it.next();
			
			int diff = filter_atom.getPredicate().getArity() - projection_paths.size(); 
			for(int i = 0; i < diff; i++) {
				terms.add(filter_atom.getTerm(i));
			}

			// create terms
			for(String path : projection_paths) {
				String[] keys =  path.split("\\.");
				for(int i = 0; i < keys.length -1; i++) {
					String key = keys[i];
					if(d.containsKey(key) && d.get(key) instanceof Document) {
						d = d.get(key, Document.class);
					} else {
						this.computeNext();
					}
				}
				String key = keys[keys.length-1];
				if (d.get(key) != null) {
					String value = d.get(key).toString();
					Term t = tf.createOrGetLiteral(value);
					terms.add(t);
				} else {
					this.computeNext();
				}
			}

			// filter
			for(int i = 0; i < filter_atom.getPredicate().getArity(); i++) {
				Term filter_term = filter_atom.getTerm(i);
				Term result_term = terms.get(i);

				if(!filter_term.isVariable() && !filter_term.equals(result_term)) {
					this.computeNext();
				}
			}
			
			// create atom
			this.next_element = new AtomImpl(this.filter_atom.getPredicate(), terms);
		}

	}
}
