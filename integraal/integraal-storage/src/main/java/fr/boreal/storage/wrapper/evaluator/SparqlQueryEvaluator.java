package fr.boreal.storage.wrapper.evaluator;

import java.util.List;
import java.util.Optional;

import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;

/**
 * Evaluates a SPARQL query using the associated repository.
 * This evaluator uses rdf4j {@link RepositoryConnection} to execute the query on the triple store
 * The result is a TupleQueryResult which represents the list of all resulting tuples.
 * 
 * This evaluator also provides additional methods for inserting and updating data on the triple store.
 */
public class SparqlQueryEvaluator implements NativeQueryEvaluator<String, TupleQueryResult> {
	
	private RepositoryConnection connection;
	
	/**
	 * Construct a new evaluator over the triple store represented by the given repository
	 * @param repo representing the triple store to connect to
	 * @throws RepositoryException if the initial connection to the triple store cannot be performed
	 */
	public SparqlQueryEvaluator(Repository repo) {
		this.connection = repo.getConnection();
	}

	@Override
	public Optional<TupleQueryResult> evaluate(String query) {
		try {
			TupleQuery prepared_query = this.connection.prepareTupleQuery(QueryLanguage.SPARQL, query);
			return Optional.of(prepared_query.evaluate());
		} catch (Exception e) {
			System.err.println(e);
			return Optional.empty();
		}
	}
	
	/**
	 * Insert the given statement
	 * @param statement to insert
	 * @return true iff the query has been executed without problem
	 */
	public boolean insert(Statement statement) {
		try {
			this.connection.add(statement);
			return true;
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
		
	}

	/**
	 * Insert the given statements, using a batch insert and parameters for the query
	 * @param statements to insert
	 * @return true iff the queries have been executed without problem
	 */
	public boolean insertBatch(List<Statement> statements) {
		try {
			this.connection.add(statements);
			return true;
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
	}
	
	/**
	 * Executes the given SPARQL update query
	 * @param query to evaluate
	 * @return true iff the query has been executed without problem
	 */
	public boolean update(String query) {
		try {
			Update prepared_query = this.connection.prepareUpdate(QueryLanguage.SPARQL, query);
			prepared_query.execute();
			return true;
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
	}

}
