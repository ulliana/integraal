package fr.boreal.storage.wrapper;

import java.util.Collection;

import fr.boreal.model.rule.api.FORule;

/**
 * This interface represents storage systems that can directly apply datalog rules
 * as part of their implementation.
 */
public interface DatalogRuleDelegatable {
	
	/**
	 * Apply all the given datalog rules.
	 * These are rules without existentials
	 * @throws an exception according to the implementation if a given rule is not datalog
	 */
	public boolean apply(Collection<FORule> rules) throws Exception;

}
