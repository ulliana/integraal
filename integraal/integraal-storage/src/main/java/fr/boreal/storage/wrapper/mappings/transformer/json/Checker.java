package fr.boreal.storage.wrapper.mappings.transformer.json;

import java.util.function.Predicate;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * A Checker is used to check if a given condition is true after applying a given query over a JSON object
 */
public class Checker {

	private String json_query;
	private Predicate<Object> condition;
	
	/**
	 * Creates a new Checker that will be valid if the condition is true after applying the json_query for a JSON object
	 */
	public Checker(String json_query, Predicate<Object> condition) {
		this.json_query = json_query;
		this.condition = condition;
	}
	
	/**
	 * @param json_object the JSON object (or array) in which to check the condition
	 * @return true iff the condition if true after applying the json_query on the json_object
	 */
	public boolean check(Object json_object) {
		Object o;
		if(json_object instanceof JSONArray) {
			JSONArray json_a = (JSONArray)json_object;
			o = json_a.query(json_query);
		} else if(json_object instanceof JSONObject) {
			JSONObject json_o = (JSONObject)json_object;
			o = json_o.query(json_query);
		} else {
			o = json_object;
		}
		return this.condition.test(o);
	}
	
	
}
