package fr.boreal.storage.wrapper.rdbms.driver;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.hsqldb.HsqlException;
import org.hsqldb.jdbc.JDBCDataSource;

/**
 * Driver for HSQLDB databases
 */
public class HSQLDBDriver implements RDBMSDriver {

	private JDBCDataSource ds;
	private Connection test_connection;
	private String JDBCString;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new driver using the given alias
	 * This represents a new memory database
	 * @param alias of the database
	 * @throws HsqlException iff some error occur
	 */
	public HSQLDBDriver(String alias) throws HsqlException {
		this.ds = new JDBCDataSource();
		this.ds.setUrl("jdbc:hsqldb:mem:" + alias);
		this.JDBCString = "jdbc:hsqldb:mem:" + alias;
		try {
			Connection c = this.ds.getConnection();
			c.createStatement().execute("SET DATABASE SQL SYNTAX MYS TRUE;");
			this.test_connection = c;
		} catch (SQLException e) {
			throw new HsqlException(e, "[HSQLDBDriver] An error occured while connecting to the HSQL database at " + this.ds.getUrl() + "\n"
					+ "Please make sure this connection path is correct and the database is accessible.", e.getSQLState(), e.getErrorCode());
		}
	}

	@Override
	public DataSource getDatasource() {
		return this.ds;
	}

	@Override
	public String getBaseSafeInsertQuery() {
		return "INSERT IGNORE INTO %t VALUES(%f);";
	}
	
	@Override
	public String getBaseSafeInsertSelectQuery() {
		return "INSERT IGNORE INTO %t %s;";
	}

	@Override
	public String getTextFieldName() {
		return "TEXT";
	}

	@Override
	public String getNumberFieldName() {
		return "INT";
	}

	@Override
	public Connection getConnection() {
		return this.test_connection;
	}

	@Override
	public String getJDBCString() {
		return this.JDBCString;
	}

}
