package fr.boreal.storage.wrapper.evaluator;

import java.util.Optional;

/**
 * A NativeQueryEvaluator can evaluate native queries over a datasource.
 * Implementations needs to specify which type of query can be evaluated and what is the result type.
 * 
 * The evaluator's job is to hide the use of jdbc or other libraries from a higher point of view.
 *
 * @param <NativeQueryType> the type used to represent the native query
 * @param <NativeResultType> the type used to represent the results as the native format
 */
public interface NativeQueryEvaluator<NativeQueryType, NativeResultType> {

	/**
	 * Evaluate the given query and returns the result
	 * This should not do anymore work
	 * @param query to evaluate
	 * @return an optional which contains the results of the query if everything went right, an empty optional otherwise
	 */
	public Optional<NativeResultType> evaluate(NativeQueryType query);
	
}
