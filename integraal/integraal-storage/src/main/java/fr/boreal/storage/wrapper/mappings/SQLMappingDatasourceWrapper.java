package fr.boreal.storage.wrapper.mappings;

import java.sql.SQLException;
import java.util.List;

import fr.boreal.model.kb.api.FactBaseType;
import fr.boreal.model.kb.impl.FactBaseDescription;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.storage.wrapper.evaluator.SQLQueryEvaluator;
import fr.boreal.storage.wrapper.mappings.specializer.NoSpecializer;
import fr.boreal.storage.wrapper.mappings.transformer.SQLTuplesTransformer;
import fr.boreal.storage.wrapper.rdbms.driver.RDBMSDriver;

public class SQLMappingDatasourceWrapper extends MappingDatasourceWrapper<String, List<Object[]>> {

	private String url;

	public SQLMappingDatasourceWrapper(RDBMSDriver driver) throws SQLException {
		super(new NoSpecializer<String>(), new SQLQueryEvaluator(driver), new SQLTuplesTransformer());
		this.url = driver.getJDBCString();
	}

	@Override
	public FactBaseDescription getDescription(Predicate viewPredicate) {
		return new FactBaseDescription(this.url, this.getQuery(viewPredicate));
	}

	@Override
	public FactBaseType getType(Predicate viewPredicate) {
		return FactBaseType.RDBMS;
	}

}
