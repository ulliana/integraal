package fr.boreal.storage.wrapper.mappings;

import java.util.List;

import fr.boreal.model.kb.impl.FactBaseDescription;

public class MongoDBDescription extends FactBaseDescription {

	private String database;
	private String collection;
	private List<String> projectionPaths;

	public MongoDBDescription(String url, String database, String collection, String query,
			List<String> projectionPaths) {
		super(url, query);
		this.database = database;
		this.collection = collection;
		this.projectionPaths = projectionPaths;		
	}

	public String getDatabase() {
		return this.database;
	}

	public String getCollection() {
		return this.collection;
	}

	public List<String> getProjectionPaths() {
		return this.projectionPaths;
	}

}
