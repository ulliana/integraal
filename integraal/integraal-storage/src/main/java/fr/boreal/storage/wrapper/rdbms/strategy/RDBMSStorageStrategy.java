package fr.boreal.storage.wrapper.rdbms.strategy;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.factory.api.PredicateFactory;
import fr.boreal.model.logicalElements.factory.api.TermFactory;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.storage.wrapper.rdbms.SQLParameterizedQuery;
import fr.boreal.storage.wrapper.rdbms.driver.RDBMSDriver;

/**
 * This represents how the RDBMS stores atoms into tables and rows.
 */
public interface RDBMSStorageStrategy {
	
	/**
	 * Filtering of the query is represented by the WHERE conditions in SQL.
	 * If the strategy can generate the conditions in every case, then this method should return true.
	 * Otherwise it should return false and additional filtering will be needed
	 * @return true iff this strategy can handle complete filtering of the query
	 */
	public boolean canHandleFiltering();
	
	/**
	 * If the table in which the given atom does not exist, this method creates it and return it's name
	 * @return The name of the table in which the given atom is stored
	 * @throws SQLException 
	 */
	public String getTableName(Atom atom) throws SQLException;
	
	/**
	 * Handle the given terms, storing them if needed
	 */
	public void handleTerms(Set<Term> all_terms) throws SQLException;
	
	/**
	 * @return The name of the column which store the term at the given index in the given table
	 */
	public String getColumnName(String table, int term_index);
	
	/**
	 * Add strategy specific conditions to the given query
	 * @return is not yet defined
	 */
	public SQLParameterizedQuery addSpecificConditions(String sql_query, List<String> arguments, FOQuery q);

	/**
	 * @return true iff the given driver's database have the correct schema of data for this strategy 
	 * @throws SQLException 
	 */
	public boolean hasCorrectDatabaseSchema(RDBMSDriver driver) throws SQLException;

	/**
	 * Create the schema associated to this strategy on the given driver's database
	 * @throws SQLException 
	 */
	public void createDatabaseSchema(RDBMSDriver driver) throws SQLException;

	/**
	 * @return the name of all the tables storing atoms
	 * @throws SQLException
	 */
	public Collection<String> getAllTableNames() throws SQLException;
	
	/**
	 * @return all the predicates stored in the database; even if no atom exists with this predicate
	 * @throws SQLException
	 */
	public Collection<Predicate> getAllPredicates(PredicateFactory factory) throws SQLException;

	/**
	 * This is the reverse operation of getRepresentation
	 * @param term string representation
	 * @param factory to instantiate the term
	 * @return the term associated with the given string representation
	 * @throws SQLException
	 */
	public Term createTerm(String term, TermFactory factory) throws SQLException;
	
	/**
	 * @param predicate string representation
	 * @param arity predicate arity
	 * @param factory to instantiate the predicate
	 * @return the predicate associated with the given string representation and arity
	 */
	public Predicate createPredicate(String predicate, int arity, PredicateFactory factory);

	/**
	 * This is the reverse operation of createTerm
	 * @param t the term
	 * @return the string representation according to the storage strategy of the given term
	 * @throws SQLException
	 */
	public String getRepresentation(Term t) throws SQLException;

}
