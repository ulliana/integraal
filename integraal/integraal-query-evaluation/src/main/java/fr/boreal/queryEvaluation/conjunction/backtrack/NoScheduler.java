package fr.boreal.queryEvaluation.conjunction.backtrack;

import java.util.List;
import java.util.stream.Collectors;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.query.api.FOQueryConjunction;

/**
 * This (static) scheduler gives the next part to evaluate by keeping the given order.
 */
public class NoScheduler implements Scheduler {
	
	private List<FOFormula<Atom>> order;

	/**
	 * Creates a new scheduler over the given query
	 */
	public NoScheduler(FOQueryConjunction query) {
		this.order = query.getFormula().getSubElements().stream().collect(Collectors.toList());
	}

	@Override
	public FOFormula<Atom> next(int level) {
		return order.get(level);
	}

	@Override
	public boolean hasNext(int level) {
		return order.size() >= level;
	}

}
