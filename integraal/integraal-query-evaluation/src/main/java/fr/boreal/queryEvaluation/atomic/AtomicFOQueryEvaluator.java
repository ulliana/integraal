package fr.boreal.queryEvaluation.atomic;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.ComputedAtom;
import fr.boreal.model.logicalElements.api.Predicate;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.AtomicFOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;

/**
 * Evaluates a {@link AtomicFOQuery} by directly giving the query to the {@link FactBase}
 * and converting the resulting {@link Atom} to {@link Substitution}.
 */
public class AtomicFOQueryEvaluator implements FOQueryEvaluator<AtomicFOQuery> {

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(AtomicFOQuery query, FactBase factbase) {
		Atom query_atom = query.getFormula().getElement();
		Substitution initial_substitution = query.getInitialSubstitution();

		// In case of a computed atom, we create it's image with the initial substitution to evaluated it
		if(query_atom instanceof ComputedAtom) {
			Atom substitut = initial_substitution.createImageOf(query_atom);
			return new AtomicFOQueryResultIterator(List.of(substitut).iterator(), null);
		} else {
			Iterator<Atom> matched_atoms = factbase.match(query_atom, initial_substitution);
			Map<Variable, Integer> variables_position = this.computeVariablesPosition(query);
			return new AtomicFOQueryResultIterator(matched_atoms, variables_position);
		}
	}

	/**
	 * Translates an Iterator<Atom> into a Iterator<Substitution> with respect to the given variables position
	 * This is used to given a lazy evaluation behaviour and avoid iterating over all the answers before returning a result.
	 */
	private class AtomicFOQueryResultIterator implements Iterator<Substitution> {

		private Iterator<Atom> atoms;
		private Map<Variable, Integer> variables_position;

		private Substitution next_result = null;

		public AtomicFOQueryResultIterator(Iterator<Atom> atoms, Map<Variable, Integer> variables_position) {
			this.atoms = atoms;
			this.variables_position = variables_position;
		}

		@Override
		public boolean hasNext() {
			if(this.next_result == null) {
				this.computeNextResult();
			}
			return this.next_result != null;
		}

		@Override
		public Substitution next() {
			if(this.next_result == null) {
				computeNextResult();
			}
			if(this.next_result == null) {
				throw new NoSuchElementException();
			}
			Substitution tmp = this.next_result;
			this.next_result = null;
			return tmp;
		}

		private void computeNextResult() {
			// for each atom matched from the factbase
			boolean found = false;
			while(!found && this.atoms.hasNext()) {
				Atom current_match = this.atoms.next();
				if(current_match.getPredicate().equals(Predicate.BOTTOM)) {
					continue;
				} else if(current_match.getPredicate().equals(Predicate.TOP)) {
					found = true;
					this.next_result = new SubstitutionImpl();
				} else {
					Substitution current_substitution = new SubstitutionImpl();					
					// fill the substitution with the terms found in the factbase
					for (Variable v : variables_position.keySet()) {
						int v_pos = variables_position.get(v);
						current_substitution.add(v, current_match.getTerm(v_pos));
					}

					found = true;
					this.next_result = current_substitution;
				}
			}
		}

	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * Compute and return the position of the variables in the query
	 * Variables must be present in the atom of the query and in its answer variables
	 * @param query
	 */
	private Map<Variable, Integer> computeVariablesPosition(AtomicFOQuery query) {
		Map<Variable, Integer> variables_position = new HashMap<Variable, Integer>();
		Atom query_atom = query.getFormula().getElement();

		for (int i = 0; i < query_atom.getPredicate().getArity(); i++) {
			Term term_i = query_atom.getTerm(i);
			if (term_i.isVariable() && query.getAnswerVariables().contains(term_i)) {
				variables_position.put((Variable) term_i, i);
			}
		}
		return variables_position;
	}

}
