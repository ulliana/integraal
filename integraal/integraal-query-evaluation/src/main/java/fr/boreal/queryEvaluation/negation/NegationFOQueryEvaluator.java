package fr.boreal.queryEvaluation.negation;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.FOQueryNegation;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;

/**
 * Evaluates a {@link FOQueryNegation} by negating the result of the sub query.
 */
public class NegationFOQueryEvaluator implements FOQueryEvaluator<FOQueryNegation> {

	/**
	 * Query evaluator used to evaluate the sub-queries
	 */
	private FOQueryEvaluator<FOQuery> evaluator;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////
	
	/**
	 * Creates a new evaluator using the given evaluator to evaluate sub-queries
	 * @param evaluator to evaluate sub-queries
	 */
	public NegationFOQueryEvaluator(FOQueryEvaluator<FOQuery> evaluator) {
		this.evaluator = evaluator;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(FOQueryNegation query, FactBase factbase) {
		Optional<? extends FOQuery> sub_query_opt = FOQueryFactory.instance().createOrGetQuery(query.getFormula().getElement(), query.getAnswerVariables(), query.getInitialSubstitution());
		if(sub_query_opt.isPresent()) {
			Iterator<Substitution> sub_result = this.evaluator.evaluate(sub_query_opt.get(), factbase);
			if(! sub_result.hasNext()) {
				return List.<Substitution>of(new SubstitutionImpl()).iterator();
			}
		}
		return Collections.emptyIterator();
	}

}
