package fr.boreal.queryEvaluation.conjunction.backtrack;

import java.util.List;
import java.util.stream.Collectors;

import fr.boreal.model.formula.api.AtomicFOFormula;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.ComputedAtom;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.query.api.FOQueryConjunction;

/**
 * This (static) scheduler gives the next part to evaluate by keeping the parts containing computed atoms and negation at the end.
 * This make it sure that the current solution contains the elements that are needed to evaluated the given part (iff the initial conjunction is correct)
 */
public class SimpleScheduler implements Scheduler {
	
	private List<FOFormula<Atom>> order;

	/**
	 * Creates a new scheduler over the given query
	 */
	@SuppressWarnings("unchecked")
	public SimpleScheduler(FOQueryConjunction query) {
		this.order = query.getFormula().getSubElements().stream().sorted((a,b) -> {
			
			boolean aNegation = a.isNegation();
			boolean aAtomic = a.isAtomic();
			boolean aComputed = aAtomic && ((AtomicFOFormula<Atom>)a).getElement() instanceof ComputedAtom;
			boolean aFunctions = false;
			if(aAtomic) {
				Atom aa = ((AtomicFOFormula<Atom>)a).getElement();
				for(Term t : aa.getTerms()) {
					if(t.isFunctionalTerm()) {
						aFunctions = true;
						break;
					}
				}
			}
			
			boolean bNegation = b.isNegation();
			boolean bAtomic = b.isAtomic();;
			boolean bComputed = bAtomic && ((AtomicFOFormula<Atom>)b).getElement() instanceof ComputedAtom;;
			boolean bFunctions = false;
			if(bAtomic) {
				Atom bb = ((AtomicFOFormula<Atom>)b).getElement();
				for(Term t : bb.getTerms()) {
					if(t.isFunctionalTerm()) {
						bFunctions = true;
						break;
					}
				}
			}
			
			if(aComputed && bComputed) {
				return 0;
			} else if(aComputed && !bComputed) {
				return 1;
			} else if(!aComputed && bComputed) {
				return -1;
			}
			
			if(aFunctions && bFunctions) {
				return 0;
			} else if(aFunctions && !bFunctions) {
				return 1;
			} else if(!aFunctions && bFunctions) {
				return -1;
			}
			
			if(aNegation && bNegation) {
				return 0;
			} else if(aNegation && !bNegation) {
				return 1;
			} else if(!aNegation && bNegation) {
				return -1;
			}
			
			if(aAtomic && bAtomic) {
				return 0;
			} else if(aAtomic && !bAtomic) {
				return -1;
			} else if(!aAtomic && bAtomic) {
				return 1;
			}
			
			return 0;
			
		}).collect(Collectors.toList());
	}

	@Override
	public FOFormula<Atom> next(int level) {
		return order.get(level);
	}

	@Override
	public boolean hasNext(int level) {
		return order.size() >= level;
	}

}
