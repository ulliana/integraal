package fr.boreal.queryEvaluation.conjunction.backtrack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;

/**
 * First simple implementation of the {@link SolutionManager} using maps to index results.
 */
public class SimpleSolutionManager implements SolutionManager {

	/**
	 * Store the current solution
	 */
	private Substitution current_substitution;
	
	/**
	 * Index solutions by level
	 */
	private Map<Integer, Iterator<Substitution> > substitutionsByLevel;
	
	/**
	 * Index assigned variables by level
	 */
	private Map<Integer, Collection<Variable> > variableByLevel;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////
	
	/**
	 * Creates a new solution manager with default objects
	 */
	public SimpleSolutionManager() {
		this.substitutionsByLevel = new HashMap<>();
		this.variableByLevel = new HashMap<>();
		this.current_substitution = new SubstitutionImpl();
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////
	
	public Substitution getCurrentSolution() {
		return this.current_substitution;
	}

	/**
	 * Try to use the next solution for the given level.
	 * Recursively iterate over the solutions for the given level if needed.
	 * 
	 * @return true iff there is a next solution for this level
	 */
	public boolean next(int level) {

		// remove entries from the current substitution for variables defined after the given level
		this.current_substitution = this.current_substitution.limitedTo(this.variablesBeforeLevel(level));

		// get the next solution for the given level
		Iterator<Substitution> next_result_it = this.substitutionsByLevel.get(level);
		while(next_result_it.hasNext()) {
			// merge the current solution with the new one
			Optional<Substitution> merged_substitutions = this.current_substitution.merged(next_result_it.next());
			if(merged_substitutions.isPresent()) {
				this.current_substitution = merged_substitutions.get();
				return true;
			}
		}
		return false;
	}

	/**
	 * Not ideal as we must update the indexes
	 * And therefore iterate over all the results in the iterator and recreate it.
	 */
	public void add(int level, Iterator<Substitution> results) {
		
		// delete old results from the given level
		Collection<Variable> current_vars = new HashSet<Variable>(this.current_substitution.keys());
		for(Variable v : current_vars) {
			if(! this.variablesBeforeLevel(level).contains(v)) {
				current_substitution.remove(v);
			}
		}

		// We need the first result to update the indexes
		// So we recreate the whole iterator.
		// This could be improved
		ArrayList<Substitution> tmp_substitutions = new ArrayList<Substitution>();
		while(results.hasNext()) {
			Substitution s = results.next();
			tmp_substitutions.add(s);
		}
		
		// update indexes
		this.variableByLevel.put(level, tmp_substitutions.get(0).keys());
		this.substitutionsByLevel.put(level, tmp_substitutions.iterator());
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////
	
	/**
	 * @return a collection of variables assigned before the given level
	 */
	private Collection<Variable> variablesBeforeLevel(int level) {
		Collection<Variable> vars = new ArrayList<Variable>();
		for(int i = 0; i<level; i++) {
			vars.addAll(this.variableByLevel.get(i));
		}
		return vars;
	}

}
