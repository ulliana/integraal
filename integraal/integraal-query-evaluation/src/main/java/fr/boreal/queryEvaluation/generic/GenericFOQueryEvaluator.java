package fr.boreal.queryEvaluation.generic;

import java.util.Iterator;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.AtomicFOQuery;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.FOQueryConjunction;
import fr.boreal.model.query.api.FOQueryDisjunction;
import fr.boreal.model.query.api.FOQueryNegation;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.queryEvaluation.atomic.AtomicFOQueryEvaluator;
import fr.boreal.queryEvaluation.conjunction.backtrack.BacktrackEvaluator;
import fr.boreal.queryEvaluation.negation.NegationFOQueryEvaluator;

/**
 * Evaluates a {@link FOQuery} by dispatching it to the correct evaluator.
 * This is used to abstract the query evaluation when the exact query type is not known
 * such as during a compound query (conjunction or disjunction) sub-queries evaluation.
 */
public class GenericFOQueryEvaluator implements FOQueryEvaluator<FOQuery> {

	/**
	 * How to evaluate an {@link AtomicFOQuery}
	 */
	private FOQueryEvaluator<AtomicFOQuery> atomicEvaluator;

	/**
	 * How to evaluate a {@link FOQueryConjunction}
	 */
	private FOQueryEvaluator<FOQueryConjunction> conjunctionEvaluator;

	/**
	 * How to evaluate a {@link FOQueryDisjunction}
	 */
	private FOQueryEvaluator<FOQueryDisjunction> disjunctionEvaluator;

	/**
	 * How to evaluate a {@link FOQueryNegation}
	 */
	private FOQueryEvaluator<FOQueryNegation> negationEvaluator;

	private static final FOQueryEvaluator<FOQuery> default_instance = createDefaultInstance();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * The default instance is used to give a default behavior to the evaluator when one doesn't want to configure it.
	 * It uses a {@link AtomicFOQueryEvaluator} for {@link AtomicFOQuery},
	 * a {@link BacktrackEvaluator} for {@link FOQueryConjunction},
	 * a {@link NegationFOQueryEvaluator} for {@link FOQueryNegation}
	 * It does not yet consider {@link FOQueryDisjunction}.
	 */
	public static FOQueryEvaluator<FOQuery> defaultInstance() {
		return default_instance;
	}

	private static FOQueryEvaluator<FOQuery> createDefaultInstance() {
		GenericFOQueryEvaluator instance = new GenericFOQueryEvaluator();

		// Evaluate Atomic queries directly on the factbase
		return instance
				.setAtomicEvaluator(new AtomicFOQueryEvaluator())
				.setConjunctionEvaluator(new BacktrackEvaluator(instance))
				.setNegationEvaluator(new NegationFOQueryEvaluator(instance));

	}

	/////////////////////////////////////////////////
	// Setters
	/////////////////////////////////////////////////

	/**
	 * Sets the evaluator for atomic queries
	 */
	public GenericFOQueryEvaluator setAtomicEvaluator(FOQueryEvaluator<AtomicFOQuery> evaluator) {
		this.atomicEvaluator = evaluator;
		return this;
	}

	/**
	 * Sets the evaluator for conjunction of queries
	 */
	public GenericFOQueryEvaluator setConjunctionEvaluator(FOQueryEvaluator<FOQueryConjunction> evaluator) {
		this.conjunctionEvaluator = evaluator;
		return this;
	}

	/**
	 * Sets the evaluator for disjunction of queries
	 */
	public GenericFOQueryEvaluator setDisjunctionEvaluator(FOQueryEvaluator<FOQueryDisjunction> evaluator) {
		this.disjunctionEvaluator = evaluator;
		return this;
	}

	/**
	 * Sets the evaluator for negation of queries
	 */
	public GenericFOQueryEvaluator setNegationEvaluator(FOQueryEvaluator<FOQueryNegation> evaluator) {
		this.negationEvaluator = evaluator;
		return this;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(FOQuery query, FactBase factbase) {

		if (query instanceof AtomicFOQuery) {
			return this.atomicEvaluator.evaluate((AtomicFOQuery)query, factbase);
		} else if (query instanceof FOQueryConjunction) {
			return this.conjunctionEvaluator.evaluate((FOQueryConjunction)query, factbase);
		} else if (query instanceof FOQueryDisjunction) {
			return this.disjunctionEvaluator.evaluate((FOQueryDisjunction)query, factbase);
		} else if (query instanceof FOQueryNegation) {
			return this.negationEvaluator.evaluate((FOQueryNegation)query, factbase);
		} else {
			return null;
		}
	}

}
