package fr.boreal.queryEvaluation.conjunction.backtrack;

import java.util.Iterator;

import fr.boreal.model.logicalElements.api.Substitution;

/**
 * A solution manager manages the solutions of the backtrack, keeping them by level.
 * This make it sure that we can go back in the backtrack and have a current solution retrived. 
 */
public interface SolutionManager {

	/**
	 * @return the current solution.
	 */
	public Substitution getCurrentSolution();

	/**
	 * Update the current solution to use the next correct solution by using the next substitution at the given level.
	 * @return true iff there is effectively a next solution for the given level.
	 */
	public boolean next(int level);

	/**
	 * Adds the given results at the given level.
	 * 
	 * Old results already stored at that level must be overridden by the new ones.
	 * The current solution must be updated accordingly when overriding the old results.
	 */
	public void add(int level, Iterator<Substitution> results);

}
