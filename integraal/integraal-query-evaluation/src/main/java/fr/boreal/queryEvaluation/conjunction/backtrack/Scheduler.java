package fr.boreal.queryEvaluation.conjunction.backtrack;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.logicalElements.api.Atom;

/**
 * A scheduler gives the next part of the conjunction to evaluate given the current level on the backtrack.
 * This order can either be static or dynamic and can change according to executions.
 */
public interface Scheduler {
	
	/**
	 * @param level
	 * @return the next element to evaluate at the given level
	 */
	public FOFormula<Atom> next(int level);
	
	/**
	 * @param level
	 * @return true iff there is a next element to evaluate at the given level
	 */
	public boolean hasNext(int level);

}
