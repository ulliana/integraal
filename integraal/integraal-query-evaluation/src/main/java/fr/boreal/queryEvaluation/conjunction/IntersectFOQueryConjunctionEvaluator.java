package fr.boreal.queryEvaluation.conjunction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Term;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.logicalElements.impl.SubstitutionImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.FOQueryConjunction;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;

/**
 * Evaluates a {@link FOQueryConjunction} by using intersection to aggregate the result of each sub-query
 * 
 * The intersection of two substitutions is a substitution from the variables of any of the two substitutions
 * An intersection is considered correct if for each variables that appear in both substitutions, the associated terms are equals
 */
public class IntersectFOQueryConjunctionEvaluator implements FOQueryEvaluator<FOQueryConjunction> {

	/**
	 * Query evaluator used to evaluate the sub-queries
	 */
	private FOQueryEvaluator<FOQuery> evaluator;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new evaluator using the given evaluator to evaluate sub-queries
	 * @param evaluator to evaluate sub-queries
	 */
	public IntersectFOQueryConjunctionEvaluator(FOQueryEvaluator<FOQuery> evaluator) {
		this.evaluator = evaluator;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(FOQueryConjunction query, FactBase factbase) {

		Iterator<Substitution> result_without_ans_var = null;

		// Compute all the substitutions
		for (FOFormula<Atom> subformula : query.getFormula().getSubElements()) {
			Collection<Variable> subquery_vars = query.getAnswerVariables();
			Substitution subquery_subst = query.getInitialSubstitution();
			Optional<? extends FOQuery> subquery_opt = FOQueryFactory.instance().createOrGetQuery(subformula, subquery_vars , subquery_subst);
			
			if(subquery_opt.isPresent()) {
				Iterator<Substitution> current_subst = evaluator.evaluate(subquery_opt.get(), factbase);
				result_without_ans_var = intersectAll(result_without_ans_var, current_subst);
			} else {
				return Collections.<Substitution>emptyIterator();
			}
			
		}

		// Filter the substitutions according to the answer variables of the query
		ArrayList<Substitution> result = new ArrayList<Substitution>();
		while (result_without_ans_var.hasNext()) {
			Substitution current_s = result_without_ans_var.next();
			Substitution correct_subst = new SubstitutionImpl();
			for (Variable ans_var : query.getAnswerVariables()) {
				correct_subst.add(ans_var, current_s.createImageOf(ans_var));
			}
			result.add(correct_subst);
		}

		return result.iterator();
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * Computes and returns the intersection of sets of substitutitons
	 */
	private Iterator<Substitution> intersectAll(Iterator<Substitution> a, Iterator<Substitution> b) {

		if (a == null) {
			return b;
		}
		if (b == null) {
			return a;
		}

		Collection<Substitution> results = new ArrayList<Substitution>();

		// because you can't start over the iterator
		Collection<Substitution> b_save = new ArrayList<Substitution>();
		while (b.hasNext()) {
			b_save.add(b.next());
		}

		while (a.hasNext()) {
			Substitution current_a = a.next();
			b = b_save.iterator();
			while (b.hasNext()) {
				Substitution current_b = b.next();
				Optional<Substitution> res = intersect(current_a, current_b);
				if (res.isPresent()) {
					results.add(res.get());
				}
			}
		}

		return results.iterator();
	}

	/**
	 * @return a {@link Substitution} being the intersection of the two substitution given
	 * or an empty optional if the two substitutions cannot be intersected
	 */
	private Optional<Substitution> intersect(Substitution a, Substitution b) {
		Substitution result = new SubstitutionImpl(a);
		for (Variable current_variable : b.keys()) {
			Term b_image = b.createImageOf(current_variable);
			if (result.keys().contains(current_variable)) {
				Term current_image = result.createImageOf(current_variable);
				if (!current_image.equals(b_image)) {
					return Optional.empty();
				}
			} else {
				result.add(current_variable, b_image);
			}
		}
		return Optional.of(result);
	}

}
