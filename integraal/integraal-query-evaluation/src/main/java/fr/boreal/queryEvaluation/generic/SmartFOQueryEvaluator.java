package fr.boreal.queryEvaluation.generic;

import java.sql.SQLException;
import java.util.Iterator;

import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;
import fr.boreal.storage.wrapper.rdbms.RDBMSWrapper;
import fr.boreal.storage.wrapper.triplestore.TripleStoreWrapper;

/**
 * Evaluates a {@link FOQuery} by dispatching it to the correct evaluator.
 * This is used to abstract the query evaluation when the exact query type is not known
 * such as during a compound query (conjunction or disjunction) sub-queries evaluation.
 */
public class SmartFOQueryEvaluator implements FOQueryEvaluator<FOQuery>{

	private static final FOQueryEvaluator<FOQuery> default_instance = createDefaultInstance();

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * @return the default configuration
	 */
	public static FOQueryEvaluator<FOQuery> defaultInstance() {
		return default_instance;
	}

	private static FOQueryEvaluator<FOQuery> createDefaultInstance() {
		return new SmartFOQueryEvaluator();
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	/**
	 * If using a SQL database, we ask the SQL storage to evaluate the whole query
	 * If anything throws an exception, we use the {@link GenericFOQueryEvaluator} as a fallback
	 */
	@Override
	public Iterator<Substitution> evaluate(FOQuery query, FactBase factbase) {
		try {
			if(factbase instanceof RDBMSWrapper) {
				RDBMSWrapper database = (RDBMSWrapper)factbase;
				return database.evaluate(query);
			}
			if(factbase instanceof TripleStoreWrapper) {
				TripleStoreWrapper endpoint = (TripleStoreWrapper)factbase;
				return endpoint.evaluate(query);
			}
		} catch (SQLException e) { 
			
		} // fallback
		return GenericFOQueryEvaluator.defaultInstance().evaluate(query, factbase);


		
	}

}
