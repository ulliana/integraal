package fr.boreal.queryEvaluation.conjunction.backtrack;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.logicalElements.api.Atom;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.logicalElements.api.Variable;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.FOQueryConjunction;
import fr.boreal.model.query.factory.FOQueryFactory;
import fr.boreal.model.queryEvaluation.api.FOQueryEvaluator;

/**
 * Evaluates a {@link FOQueryConjunction} by using homomorphism computing using backtrack algorithm to aggregate the result of each sub-query
 * 
 * To evaluate a given query, a sub-query is chosen and evaluated.
 * The first result is assumed correct and the next sub-query is evaluated.
 * If the results of the second sub-query are consistent with the first (assumed correct) result, the next sub-query is evaluated and so on.
 * When a sub-query result is not consistent with the previous results, a step of backtrack is used to go back on the assumed result and take the next answer.
 * If there is no more answer to go back to, it is also considered inconsistent.
 * 
 * This Backtrack algorithm is inspired by the Baget Jean-François Thesis (Chapter 5)
 * See http://www.lirmm.fr/~baget/publications/thesis.pdf for more information
 */
public class BacktrackEvaluator implements FOQueryEvaluator<FOQueryConjunction> {

	/**
	 * Query evaluator used to evaluate the sub-queries
	 */
	private FOQueryEvaluator<FOQuery> evaluator;

	/////////////////////////////////////////////////
	// Constructors
	/////////////////////////////////////////////////

	/**
	 * Creates a new evaluator using the given evaluator to evaluate sub-queries
	 */
	public BacktrackEvaluator(FOQueryEvaluator<FOQuery> evaluator) {
		this.evaluator = evaluator;
	}

	/////////////////////////////////////////////////
	// Public methods
	/////////////////////////////////////////////////

	@Override
	public Iterator<Substitution> evaluate(FOQueryConjunction query, FactBase factbase) {
		return new BacktrackIterator(evaluator, query, factbase);
	}

	/////////////////////////////////////////////////
	// Private methods
	/////////////////////////////////////////////////

	/**
	 * The BacktrackIterator is used to redefined the iterator return as an answer to the query.
	 * It compute results only when needed.
	 * Calling hasNext() effectively computes the next answer.
	 */
	private class BacktrackIterator implements Iterator<Substitution> {

		/////////////////////////////
		// Evaluator configuration //
		/////////////////////////////
		
		/**
		 * Query evaluator used to evaluate the sub-queries
		 */
		private FOQueryEvaluator<FOQuery> evaluator;
		
		/**
		 * Query to evaluate
		 */
		private FOQueryConjunction query;
		
		/**
		 * The factbase on which to evaluate the query
		 */
		private FactBase factbase;

		/////////////////////////////
		// Backtrack configuration //
		/////////////////////////////
		
		/**
		 * Object used to handle the results to the query.
		 * It is used as a sort of cache during the query evaluation
		 */
		private SolutionManager solutionManager = new SimpleSolutionManager();
		
		/**
		 * Object used to choose the next sub-query to evaluate
		 */
		private Scheduler scheduler;

		////////////////////////
		// Internal variables //
		////////////////////////
		
		// high level variable
		private boolean hasCurrentSolution = false; // true iff the current solution is already computed
		
		// low level variables
		private int level = 0;
		private boolean backtrack = false; // true iff going back up

		//////////////////
		// Constructors //
		//////////////////
		
		public BacktrackIterator(FOQueryEvaluator<FOQuery> evaluator, FOQueryConjunction query, FactBase factbase) {
			this.evaluator = evaluator;
			this.query = query;
			this.factbase = factbase;
			
			this.scheduler = new NoScheduler(this.query);
		}

		////////////////////
		// Public methods //
		////////////////////

		@Override
		/**
		 * This method effectively computes the next answer if it is not yet computed.
		 * @return true iff there is a next solution
		 */
		public boolean hasNext() {
			if(this.hasCurrentSolution) {
				return true;
			} else {
				this.computeNextSolution();
				return this.hasCurrentSolution;
			}
		}
		
		@Override
		/**
		 * If there is a current stored substitution, this method returns it.
		 * Else, it computes it before returning it.
		 * @throws NoSuchElementException is generated if there is no next solution as of java iterators standards.
		 */
		public Substitution next() {
			if(this.hasNext()) {
				this.hasCurrentSolution = false;
				return this.solutionManager.getCurrentSolution();
			}
			else {
				throw new NoSuchElementException("[backtrack] There is no more answer to the query");
			}
		}

		/////////////////////
		// Private methods //
		/////////////////////
		
		/**
		 * Compute the next solution using backtrack algorithm.
		 */
		private void computeNextSolution() {
			while(!this.hasCurrentSolution && this.level >= 0) {
				if(this.solutionFound()) {
					this.hasCurrentSolution = true;
					this.fail();
				} else {
					if(this.backtrack) {
						// going back up
						if(this.solutionManager.next(this.level)) {
							this.success();
						} else {
							// backtrack
							this.fail();
						}
					} else {
						// going down
						if(this.scheduler.hasNext(this.level)) {
							FOFormula<Atom> element = this.scheduler.next(this.level);
							Collection<Variable> subquery_vars = this.query.getAnswerVariables();
							Optional<Substitution> previous_step_subst_opt = this.query.getInitialSubstitution().merged(this.solutionManager.getCurrentSolution());
							if(previous_step_subst_opt.isPresent()) {
								Substitution subquery_subst = previous_step_subst_opt.get();
								Optional<? extends FOQuery> subquery_opt = FOQueryFactory.instance().createOrGetQuery(element, subquery_vars , subquery_subst);
								if(subquery_opt.isEmpty()) {
									this.fail();
								} else {
									Iterator<Substitution> sub_results = this.evaluator.evaluate(subquery_opt.get(), factbase);
									if(sub_results.hasNext()) {
										this.solutionManager.add(level, sub_results);
										if(this.solutionManager.next(level)) {
											this.success();
										} else {
											this.fail();
										}
									} else {
										this.fail();
									}
								}
							} else {
								this.fail();
							}
							
						} else {
							throw new NoSuchElementException("[backtrack] There is no more subquery in this query to evaluate");
						}
					}
				}
			}
		}
		
		/**
		 * Called when there is a success during the backtrack algorithm.
		 * A success is when a sub-query is evaluated and correct with regards to the previously computed answer.
		 */
		private void success() {
			this.backtrack = false;
			this.computeNextLevel();
		}
		
		/**
		 * Called when there is a failure during the backtrack algorithm.
		 * A failure is when a sub-query is evaluated and is incorrect with regards to the previously computed answer.
		 * A failure is also considered if the answer is found to be able to go to the next result if we want all the result of the query
		 */
		private void fail() {
			this.backtrack = true;
			this.computePreviousLevel();
		}

		/**
		 * Compute and update the previous level
		 */
		private void computePreviousLevel() {
			this.level--;
		}
		
		/**
		 * Compute and update the next level
		 */
		private void computeNextLevel() {
			this.level++;
		}

		/**
		 * @return true iff a solution is found
		 */
		private boolean solutionFound() {
			return this.level == this.query.getFormula().getSubElements().size();
		}
	}

}
