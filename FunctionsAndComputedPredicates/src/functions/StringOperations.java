package functions;

import java.util.ArrayList;
import java.lang.Math;

public class StringOperations {
	
	public static int sup = (int) Math.pow(10, 8);
	 
	public static String concat(String a, String b) {
		return a.concat(b);
	}
	
	public static String lower(String a) {
		return a.toLowerCase();
	}
	
	public static String upper(String a) {
		return a.toUpperCase();
	}
	
	public static String replace(String a, String b, String c) {
		return a.replace(b, c);
	}
	
	public static int length(String a) {
		return a.length();
	}
	
	public static int sum(int a, int b) {
		return a+b;
	}
	
	public static double div2(int a) {
		return (double) a/2;
	}
	
	public static double moyenne(Double... values) {
		double som = 0;
		double total = 0;
		for(int i=0; i<values.length-1; i = i+2) {
			if(values[i+1]<0) {
				som += (sup-values[i])*Math.abs(values[i+1]);
			}
			else {
				som += values[i]*values[i+1];
			}
			total += Math.abs(values[i+1]);
		}
		return som/total;
	}
	
	public static double une_valeur(double v, double s) {
		if (s < 0) {
			return sup-v;
		}
		else {
			return v;
		}
	}
	
	public static double mediane(Double... values) {
		ArrayList<Double> val = new ArrayList<>();
		for (int i = 0; i < values.length-1; i = i+2) {
			for (int j = 0; j < Math.abs(values[i+1]); j++) {
				val.add(values[i]);}
		}
	    int middle = val.size()/2;
	    if (val.size()%2 == 1) {
	        return val.get(middle);
	    } 
	    else {
	        return (val.get(middle-1) + val.get(middle)) / 2;
	    }
	}
	
	public static double maxi(Double... values) {
		double maxi = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] < 0) {
				values[i] = sup - values[i];
			}
			if (values[i] > maxi) {
				maxi = values[i];
			}
		}
		return maxi;
	}
	
	public static double agreg(String fct, Object... values) {
		Double[] int_values = new Double[values.length];
		for(int i = 0; i < values.length; i++) {
			int_values[i] = Double.parseDouble(values[i].toString());
		}
		if(fct.equals("max")) {
			return maxi(int_values);
		}
		if (fct.equals("moy")) {
			return moyenne(int_values);
		}
		else if (fct.equals("med")) {
			return mediane(int_values);
		}
		return (Double) null;
	}
}