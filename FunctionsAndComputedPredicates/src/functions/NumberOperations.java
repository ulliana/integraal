package functions;

public class NumberOperations {
	public static float sum(String a, String b) {
		float x=Float.parseFloat(a);
		float y=Float.parseFloat(b);
		return x+y;
	}
	public static float substract(String a, String b) {
		float x=Float.parseFloat(a);
		float y=Float.parseFloat(b);
		return x-y;
	}
	public static float multiply(String a, String b) {
		float x=Float.parseFloat(a);
		float y=Float.parseFloat(b);
		return x*y;
	}
	public static float divide(String a, String b) {
		float x=Float.parseFloat(a);
		float y=Float.parseFloat(b);
		if(y==0) return 0;
		return x/y;
	}
}
