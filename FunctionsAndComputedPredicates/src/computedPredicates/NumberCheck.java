package computedPredicates;

public class NumberCheck {
	
	public static boolean even(int a) {
		return a % 2 == 0;
	}
	
	public static boolean odd(int a) {
		return a % 2 == 1;
	}
	
	public static boolean isGreaterThan(int a, int b) {
		return a > b;
	}
	
	public static boolean isGreaterOrEqualsTo(int a, int b) {
		return a >= b;
	}
	
	public static boolean isSmallerThan(int a, int b) {
		return a < b;
	}
	
	public static boolean isSmallerOrEqualsTo(int a, int b) {
		return a <= b;
	}
	
	public static boolean isPrime(int n) {
        if (n <= 1)
            return false;
        if (n <= 3)
            return true;
        if (n % 2 == 0 || n % 3 == 0)
            return false;
        for (int i = 5; i * i <= n; i += 6)
            if (n % i == 0 || n % (i + 2) == 0)
                return false;
        return true;
	}
	
	public static boolean isEqualToString(String a, String b) {
		return a.equals(b);
	}

}
