# InteGraal : Storage module

This document presents the storage module from InteGraal and describe how to use it.

## Utility

This module is used to both store atoms and query sources to retrieve data.

## Module hierarchy

```txt
integraal\integraal-storage\src\main\java\
│
└── fr\
    └── boreal\
        └── storage\
            ├── builder\
            │   └── StorageBuilder.java
            │
            ├── inmemory\
            │   ├── DefaultInMemoryAtomSet.java
            │   ├── SimpleFOFormulaStore.java
            │   └── SimpleInMemoryGraphStore.java
            │
            └── wrapper\
                ├── evaluator\
                │   ├── HttpQueryEvaluator.java
                │   ├── MongoDBQueryEvaluator.java
                │   ├── NativeQueryEvaluator.java
                │   ├── SparqlQueryEvaluator.java
                │   └── SQLQueryEvaluator.java
                │
                ├── mappings\
                │   ├── specializer\
                │   │   ├── NoSpecializer.java
                │   │   ├── OrderedStringReplacementSpecializer.java
                │   │   └── Specializer.java
                │   │
                │   ├── transformer\
                │   │   ├── json\
                │   │   │   ├── Checker.java
                │   │   │   └── JSONStringTransformer.java
                │   │   │
                │   │   ├── MongoDocumentTransformer.java
                │   │   ├── SparqlTuplesTransformer.java
                │   │   ├── SQLTuplesTransformer.java
                │   │   └── Transformer.java
                │   │
                │   ├── MappingDatasourceWrapper.java
                │   └── RelationalViewSpecification.java
                │
                ├── rdbms\
                │   ├── driver\
                │   │   ├── HSQLDBDriver.java
                │   │   ├── MySQLDriver.java
                │   │   ├── PostgreSQLDriver.java
                │   │   ├── RDBMSDriver.java
                │   │   └── SQLiteDriver.java
                │   │
                │   ├── strategy\
                │   │   ├── AdHocSQLStrategy.java
                │   │   ├── EncodingAdHocSQLStrategy.java
                │   │   └── RDBMSStorageStrategy.java
                │   │
                │   ├── RDBMSWrapper.java
                │   └── SQLParameterizedQuery.java
                │
                ├── triplestore\
                │   └── TripleStoreWrapper.java
                │
                ├── AbstractStorageWrapper.java
                └── DatalogRuleDelegatable.java
```

## Import

You can either import this module using maven dependencies or adding the corresponding jar in your build path.

```xml
<dependency>
    <groupId>integraal</groupId>
    <artifactId>integraal-storage</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

## Usage

To use this module, you need to import it into your main application.

Then, you have access to the packages under `fr.boreal.storage` to get the needed interfaces or classes.

The easier storage to setup is the `SimpleInMemoryGraphStore` which keeps atoms in memory and uses a graph representation using

```java
FactBase fb = StorageBuilder.getSimpleInMemoryGraphStore();
```

## Configuration

You can easily configurate your factbase to use another storage system by using the `StorageBuilder`.

You can also connect to an existing relational database or triple store using the same builder as follow.

When creating the factbase using the `build()` method, an empty optional is returned iff the minimal configuration is not given.

Refer to the `StorageBuilder` class for more informations on this minimal configuration.

```java
FactBase database = StorageBuilder.defaultBuilder()
.useSPARQLEndpoint(null) // new endpoint in memory
.setTermFactory(termfactory)
.setPredicateFactory(predicatefactory)
.build()
.get();
```

## Modification

If you want to create your own implementations or approches, you can easily do so by implementing the `FactStorage` (or `FactBase`) interface.

Once this is done, you can use your new implementation in the same way as presented in the configuration part of this document.

## Explanations

We can describe the storages available in the two following kinds

### InteGraal storages

These storages are handled entierly by InteGraal. They are accessible using the StorageBuilder and give access to read, write and update queries.

Some algorithm exist to handle queries and even rules directly on the storage without retrieving data in memory. This is denoted by the `DatalogRuleDelegatable` interface.

### External datasources

These storages are not handled entierly by InteGraal. They are accessible using user-defined queries.

For the moment, we do not have a textual format for declaring these types of storages, so you'll need to write some Java, but this is planned for the future.

You can refer to the [examples available in the project](https://gitlab.inria.fr/rules/integraal/-/blob/develop/integraal/integraal-app/src/main/java/O2SolubilityINRAEUseCase.java) for more information on how to setup this storage system.

To use this type of storages, represented by the `MappingDatasourceWrapper`, multiple elements are needed :
* A native query to execute
* The connection to the datasource
* The object that will be used to execute the query at the lower level
* The predicate representing the relational view that will be instantiated
* Some optional way to specialize the inital query to take into account constants or (rdf-)literals
* Some way to transform the result from the query to relational.

We provide some generic solutions for SQL and SPARQL as well as some potential solution for JSON data.
