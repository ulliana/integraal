# InteGraal : I/O module - RDF files

This document presents the usage of the I/O module from InteGraal whith RDF files.

## Utility

This module is used to import data in a RDF format.

## Usage

To use this module, you need to import it into your main application.

Then, you have access to the packages under `fr.boreal.io` to get the needed interfaces or classes.

In it's most simple form, you can use it by creating the parser object
```java
RDFParser parser = new RDFParser(new File("path/to/file.ttl"), RDFFormat.TURTLE);
```

As you can see, to create the parser you need two information :
1. A file which contains the data
2. The format of the file
    - InteGraal can import data in all the RDF format supported by [org.eclipse.rdf4j.rio](https://rdf4j.org/documentation/programming/rio/)
    
Then you can iterate over the parser object to get the parsed objects one by one

```java
while(parser.hasNext()) {
    if(parser.next() instanceof Atom a) {
		atoms.add(a);
	}
}
```

Don't forget to close the parser at the end.

```java
parser.close();
```

## Configuration

You can give additional arguments when creating the parser to choose a translation method to convert from RDF triples to InteGraal atoms.

```java
RDFParser parser = new RDFParser(new File("path/to/file.ttl"), RDFFormat.TURTLE, RDFTranslationMode.NaturalFull);
```

Three modes are available :
1. Raw : All the triples are converted to the "triple" predicate of arity 3 and the 3 terms corresponding to (in order) the subject, the predicate and the object of the RDF triple.
2. Natural : All the triples are converted to an atom with the predicate corresponding to the predicate of the RDF triple, of arity 2 with the 2 terms corresponding to (in order) the subject and the object of the RDF triple.
3. NaturalFull : This method is used by default if none is specified. It is the same as the natural mode but additionally, if the predicate happen to be rdf:type then the object of the RDF triple is used as predicate for the atom, of arity 1 with the 1 term corresponding to the subject in the RDF triple.

For example, given the RDF triple `(s, rdf:type, o)`, the atom created will be :
1. With the Raw mode : `triple(s, rdf:type, o)`
1. With the Natural mode : `rdf:type(s, o)`
1. With the NaturalFull mode : `o(s)`

Please note that only the Raw translation is sound and complete from a reasoning point of view of the whole RDF(S) but the other translations are easier to understand.

## Modification

If you wish to add more translation methods, you can do so by extanding the `RDFTranslationMode` enumeration and adding the corresponding case and treatment to the `RDFParser.Producer.run()` method.
