# InteGraal : I/O module

This document presents the I/O module from InteGraal and describe how to use it.

## Utility

This module is used to import and export data, rules and queries.
For now, the existing implementations can import DLGP files and RDF data files.

## Module hierarchy

This is not fixed yet

## Import

You can either import this module using maven dependencies or adding the corresponding jar in your build path.

```xml
<dependency>
    <groupId>integraal</groupId>
    <artifactId>integraal-io</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

## Usage

To use this module, you need to import it into your main application.

Then, you have access to the packages under `fr.boreal.io` to get the needed interfaces or classes.

### Import

InteGraal provides objects and functionalities for import of DLGP and RDF files.

For example on import, please refer to the dedicated language documentation.

### Export

This is not yet available.

## Modification

If you want to add an additional language for import and/or export, you can do so by implementing the Parser<T> interface.
