# InteGraal : Query evaluation module

This document presents the query evaluation module from InteGraal and describe how to use it.

## Utility

This module is used to evaluate a given query over a given factbase.

## Module hierarchy

```txt
integraal\integraal-query-evaluation\src\main\java\
│
└── fr\
    └── boreal\
        └── queryEvaluation\
            ├── atomic\
            │   └── AtomicFOQueryEvaluator.java
            │
            ├── conjunction\
            │   ├── backtrack\
            │   │   ├── BacktrackEvaluator.java
            │   │   ├── NoScheduler.java
            │   │   ├── Scheduler.java
            │   │   ├── SimpleScheduler.java
            │   │   ├── SimpleSolutionManager.java
            │   │   └── SolutionManager.java
            │   │
            │   └── IntersectFOQueryConjunctionEvaluator.java
            │
            ├── generic\
            │   ├── GenericFOQueryEvaluator.java
            │   └── SmartFOQueryEvaluator.java
            │
            └── negation\
                └── NegationFOQueryEvaluator.java
```

## Import

You can either import this module using maven dependencies or adding the corresponding jar in your build path.

```xml
<dependency>
    <groupId>integraal</groupId>
  	<artifactId>integraal-query-evaluation</artifactId>
  	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

## Usage

To use this module, you need to import it into your main application.

Then, you have access to the packages under `fr.boreal.queryEvaluation` to get the needed interfaces or classes.

In it's most basic form, you can use the default evaluator defined by this module using
```java
FOQueryEvaluator<FOQuery> evaluator = GenericFOQueryEvaluator.defaultInstance();
```

To use the query evaluator, you then need to have a query and a factbase and call
```java
Iterator<Substitution> queryResults = evaluator.evaluate(query, factbase);
```

You can then iterate over the results to use them (for example, to print them)
```java
while (results.hasNext()) {
    Substitution result = results.next();
    System.out.println(result);
}
```

## Configuration

You can configurate your query evaluator to use another algorithm for certain type of queries.
For example, to use a different algorithm to evaluate a conjunction of queries, you can use
```java
GenericFOQueryEvaluator evaluator = new GenericFOQueryEvaluator();
evaluator.setConjunctionEvaluator(my_conjunction_evaluator);
```

## Modification

If you want to create your own query evaluator, you can easily do so by implementing the `FOQueryEvaluator<Q>` interface with the correct type of Query that you want to evaluate.
Once this is done, you can call it by using the steps from the configuration part of this document.

## Explanations

As we use first order formulas to represent our queries, we have a recursive evaluation which depends on the type of the given fomula.

The `GenericEvaluator` can take any type of formula (either a conjunction, or a disjunction, or a negation, or a atomic) and only redirect the evaluation on a specialized evaluator.

The `SmartEvaluator` also take into account the factbase type and if it is able to directly translate a given query in a native langage query.

### Atomic queries

Atomic queries are evauated by the `AtomicFOQueryEvaluator` by simply redirecting the atom on the factbase using the `match` operation.

### Negated queries

Negated queries are evaluated by the `NegationFOQueryEvaluator` by recursivly evaluating the sub-query using a generic evaluator and the `exist` operation and negating the result.

### Conjunction queries

Conjunction of queries are evaluated by the `BacktrackEvaluator` by recursivly evaluating the sub-queires using a generic evaluator and the `evaluate` operation and the backtrack algorithm.

This evaluator uses a lazy evaluation as the next answer of the query is only computed when calling the `hasNext` or `next` method.

### Disjunction queries

Disjunction of queries are not yet evaluated.
