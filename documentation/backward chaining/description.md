# InteGraal : Backward chaining module

This document presents the backward chaining module from InteGraal and describe how to use it.

## Utility

This module is used to saturate a given factbase with the given rules.
For now, the existing implementations can saturate a factbase using FORule and the chase method.

## Module hierarchy

```txt
integraal\integraal-backward-chaining\src\main\java\
│
└── fr\
    └── boreal\
        └── backward_chaining\
            ├── api\
            │   └── BackwardChainingAlgorithm.java
            │
            ├── old\
            │   └── RewritingBridge.java
            │
            └── pure\
                ├── cover\
                │   ├── CoverFunction.java
                │   ├── NoCover.java
                │   └── QueryCover.java
                │
                ├── rewriting_operator\
                │   ├── dlx\
                │   │   ├── data\
                │   │   │   ├── ColumnObject.java
                │   │   │   ├── DataObject.java
                │   │   │   └── DebugDataObject.java
                │   │   │
                │   │   ├── DLX.java
                │   │   ├── DLXException.java
                │   │   ├── DLXResult.java
                │   │   ├── DLXResultProcessor.java
                │   │   ├── NullResultProcessor.java
                │   │   └── SimpleDLXResultProcessor.java
                │   │
                │   ├── RewritingOperator.java
                │   ├── SingleRuleAggregator.java
                │   └── SourceTargetOperator.java
                │
                └── PureRewriter.java
```

## Import

You can either import this module using maven dependencies or adding the corresponding jar in your build path.

```xml
<dependency>
    <groupId>integraal</groupId>
  	<artifactId>integraal-backward-chaining</artifactId>
  	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

## Usage

To use this module, you need to import it into your main application.

Then, you have access to the packages under `fr.boreal.backward_chaining` to get the needed interfaces or classes.

In it's most basic form, you can use the given `PureRewriter` to get the prebuilt rewriting algorithm defined by this module using
```java
BackwardChainingAlgorithm rewriter = new PureRewriter();
```

You can then use the rewrite method to get the rewritings of a given query according to a given rulebase using :
```java
rewriter.rewrite(query, rules);
```

The query will be rewriten using piece unifications and the result will be a cover of the set of all rewritings of the query with the rules.

## Configuration

You can configurate your backward chaining algorithm to use another approches.

The main parts of the configuration are the operator and cover algorithm.

For example, if you are sure you have only source to target rules (in the case of mappings), you can use the SourceTargetOperator which will fully rewrite the query in one step.

Some of them will be added in the near future and the PureRewriter constructor can take them as parameters.

**Please note that no additional verification is done to assure that the algorithm will end or that the configuration is coherent.**

## Modification

If you want to create your own implementations or approches, you can easily do so by implementing the associated interface.
Once this is done, you can use your new implementation by using the given constructor in the same way as presented in the configuration part of this document. No additional modifications should be required.

If many approches emerge, a builder will be added but it doesn't seems necessary for the moment.

## Explanations

The work done for the theorical part of the backward chaining is mostly part of Melanie Konïg's thesis (2014).
