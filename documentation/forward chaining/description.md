# InteGraal : Forward chaining module

This document presents the forward chaining module from InteGraal and describe how to use it.

## Utility

This module is used to saturate a given factbase with the given rules.
For now, the existing implementations can saturate a factbase using FORule and the chase method.

## Module hierarchy

```txt
integraal\integraal-forward-chaining\src\main\java\
│
└── fr\
    └── boreal\
        └── forward_chaining\
            ├── api\
            │   └── ForwardChainingAlgorithm.java
            │
            └── chase\
                ├── halting_condition\
                │   ├── CreatedFactsAtPreviousStep.java
                │   ├── HaltingCondition.java
                │   ├── HasRulesToApply.java
                │   ├── LimitAtoms.java
                │   ├── LimitNumberOfStep.java
                │   └── Timeout.java
                │
                ├── rule_applier\
                │   ├── trigger_applier\
                │   │   ├── facts_handler\
                │   │   │   ├── DelegatedApplication.java
                │   │   │   ├── DirectApplication.java
                │   │   │   └── FactsHandler.java
                │   │   │
                │   │   ├── renamer\
                │   │   │   ├── BodySkolem.java
                │   │   │   ├── FreshRenamer.java
                │   │   │   ├── FrontierByPieceSkolem.java
                │   │   │   ├── FrontierSkolem.java
                │   │   │   └── TriggerRenamer.java
                │   │   │
                │   │   ├── TriggerApplier.java
                │   │   └── TriggerApplierImpl.java
                │   │
                │   ├── trigger_checker\
                │   │   ├── AlwaysTrueChecker.java
                │   │   ├── EquivalentChecker.java
                │   │   ├── MultiTriggerChecker.java
                │   │   ├── ObliviousChecker.java
                │   │   ├── RestrictedChecker.java
                │   │   ├── SemiObliviousChecker.java
                │   │   └── TriggerChecker.java
                │   │
                │   ├── trigger_computer\
                │   │   ├── NaiveTriggerComputer.java
                │   │   ├── SemiNaiveComputer.java
                │   │   ├── TriggerComputer.java
                │   │   └── TwoStepComputer.java
                │   │
                │   ├── AbstractRuleApplier.java
                │   ├── BreadthFirstTriggerRuleApplier.java
                │   ├── ParallelTriggerRuleApplier.java
                │   ├── RuleApplier.java
                │   └── SourceDelegatedDatalogRuleApplier.java
                │
                ├── rule_scheduler\
                │   ├── GRDScheduler.java
                │   ├── NaiveScheduler.java
                │   └── RuleScheduler.java
                │
                ├── treatment\
                │   ├── AddCreatedFacts.java
                │   ├── Debug.java
                │   ├── EndTreatment.java
                │   ├── Pretreatment.java
                │   ├── RuleSplit.java
                │   └── Treatment.java
                │
                ├── Chase.java
                ├── ChaseBuilder.java
                ├── ChaseImpl.java
                └── RuleApplicationStepResult.java
```

## Import

You can either import this module using maven dependencies or adding the corresponding jar in your build path.

```xml
<dependency>
    <groupId>integraal</groupId>
  	<artifactId>integraal-forward-chaining</artifactId>
  	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

## Usage

To use this module, you need to import it into your main application.

Then, you have access to the packages under `fr.boreal.forward_chaining` to get the needed interfaces or classes.

In it's most basic form, you can use the given builder `ChaseBuilder` to get the prebuilt chases defined by this module using
```java
ForwardChainingAlgorithm chase = ChaseBuilder.defaultChase(factbase, rulebase, termfactory);
```

As you can see, the chase need a factbase and a rulebase as well as a factory to create the new terms which should be the same factory used to create the factbase in a first step.
In future release, giving the knowledgebase will be enough.

To use the chase, you just need to execute it using :
```java
chase.execute();
```

The factbase with which the chase as been built will then be saturated with the given rules and you can use it to evaluate queries or do anything else.

## Configuration

You can configurate your forward chaining algorithm to use another approches.

Some are already available using the `ChaseBuilder`, but all combinations can be created using this builder and the setters associated.

When creating the chase using the `build()` method, an empty optional is returned iff the minimal configuration is not given.

Refer to the `ChaseBuilder` class for more informations on this minimal configuration.

**Please note that no additional verification is done to assure that the chase will end or that the configuration is coherent.**

For example, to use a restricted criteria with a GRD scheduler, you can use
```java
ChaseBuilder sb = ChaseBuilder.defaultBuilder(factbase, rb, termfactory)
    .useRestrictedChecker()
    .useGRDRuleScheduler()
    .useSemiNaiveComputer()
    .useDirectApplication() // default value that can be omitted
    .addStepEndTreatments(new Debug());
ForwardChainingAlgorithm chase = sb.build().get();
```

## Modification

If you want to create your own implementations or approches, you can easily do so by implementing the associated interface.
Once this is done, you can use your new implementation by setting it using the `ChaseBuilder` in the same way as presented in the configuration part of this document. No additional modifications should be required.

## Explanations

The work done for the modeling and implementation of the Chase is mostly part of Guillaume Pérution-Kihli internship's work (2020)
