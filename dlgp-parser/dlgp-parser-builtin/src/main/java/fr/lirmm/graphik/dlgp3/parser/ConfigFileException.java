package fr.lirmm.graphik.dlgp3.parser;

public class ConfigFileException extends Exception{

	public ConfigFileException(String message) {
		System.err.println(message);
	}
}
