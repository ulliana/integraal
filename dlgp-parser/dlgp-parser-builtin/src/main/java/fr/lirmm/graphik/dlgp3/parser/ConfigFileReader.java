package fr.lirmm.graphik.dlgp3.parser;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLClassLoader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ConfigFileReader {
	
	public ConfigFileReader() { }

	/*given a json Object this method return an instance of the class
	 * using informations in the json Objet*/

	public Class<?> getClassFromJsonObject(JSONObject jsonObject){
		Class<?> result = null;
		try {
			String className = this.getPackage(jsonObject) + "." + this.getClasse(jsonObject);
			if(!jsonObject.containsKey("path")) 
				result= Class.forName(className);
			else {
				
				String path = this.getPath(jsonObject);
				File file = new File(path);
				
				InvokeManager.classToPath.put(className, path);
				
				URL url = file.toURI().toURL();
				URL[] urls = new URL[] {url};
				URLClassLoader loader = new URLClassLoader(urls);
				
				result = loader.loadClass(className);

				loader.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;	
	}


	public JSONObject readJsonFile(String URI) {
		JSONObject jsonObj = null;
		try {
			Reader reader = new FileReader(URI);
			JSONParser parser = new JSONParser();
			jsonObj = (JSONObject) parser.parse(reader);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return jsonObj;

	}


	public JSONObject getDefault(JSONObject obj) {
		if(obj.containsKey("default"))
			return (JSONObject)obj.get("default");
		else
			return new JSONObject();
	}
	public JSONObject getTypes(JSONObject obj) {
		if(obj.containsKey("types"))
			return (JSONObject)obj.get("types");
		else
			return new JSONObject();
	}

	public JSONObject getPredicates(JSONObject obj) {
		if(obj.containsKey("predicates"))
			return (JSONObject)obj.get("predicates");
		else
			return new JSONObject();
	}

	public JSONObject getFunctions(JSONObject obj) {
		if(obj.containsKey("functions"))
			return (JSONObject)obj.get("functions");
		else
			return new JSONObject();
	}

	public JSONObject getElements(JSONObject obj) {
		if(obj.containsKey("elements"))
			return (JSONObject)obj.get("elements");
		else
			return new JSONObject();
	}

	public JSONObject getLocation(JSONObject obj) {
		if(obj.containsKey("location"))
			return (JSONObject)obj.get("location");
		else
			return new JSONObject();
	}

	public String getType(JSONObject obj) {
		return (String) obj.get("type");
	}

	public String getPath(JSONObject obj) {
		return (String) obj.get("path");
	}

	public String getPackage(JSONObject obj) {
		return (String) obj.get("package");
	}

	public String getClasse(JSONObject obj) {
		return (String) obj.get("class");
	}


}
