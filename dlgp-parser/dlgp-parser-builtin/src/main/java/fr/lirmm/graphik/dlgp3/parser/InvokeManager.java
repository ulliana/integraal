package fr.lirmm.graphik.dlgp3.parser;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;


public class InvokeManager {

	private ConfigFileReader confReader = new ConfigFileReader();
	public static HashMap<String,String> prefixUriMap=new HashMap<String,String>(); 	
	public static HashMap<Object,String> inversedTypesTable=new HashMap<Object,String>();
	public static HashMap<String,Object> typesTable=new HashMap<String,Object>();
	public static HashMap<String,Object> prefixTable=new HashMap<String,Object>();
	public static HashMap<String, String> classToPath = new HashMap<String, String>();

	/*default constructor which intilalizes the prefix table 
	 * with special predicates & functions and the typesTable with standards type*/
	
	public InvokeManager() {
		reverseMap();
	}
	
	/*this metod set the typesTable given the standardfilePath*/
	/*public void setTypesTable(String filePath){
		try {
			//substring to remove angle brackets <typesFilePath> --> typesFilePath
			if(filePath.startsWith("<"))
				filePath=filePath.substring(1,filePath.length()-1);
			JSONObject jsonObject=this.confReader.readJesonFile(filePath);
			for(Object key :jsonObject.keySet())
				typesTable.put((String)key,Class.forName((String)jsonObject.get(key)));
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}		
	}
*/
	
	/*this method adds a prefix and the object that allows as to call functions, in the prefixTable
	 * and add a (key,value) <prefix,uri> in prefixUriMap
	 * and add a usersTypes if there exists*/
	public void addPrefix(String prefix,String filePath) {
		//substring to remove angle brackets <prefixFilePath> --> prefixFilePath
		if(filePath.startsWith("<"))
			filePath=filePath.substring(1,filePath.length()-1);
		this.getObjectToInvoke(prefix,filePath);
		prefixUriMap.put(prefix, filePath+"#");
		
	}
	
	/*return the object that allows to invok given a prefix*/
	public Object getInvokerObject(String key) {
		if(prefixTable.containsKey(key))
			return prefixTable.get(key);
		else {
			int indexOf=key.indexOf("#");
			String prefix=key.substring(0,indexOf+1);
			return prefixTable.get(prefix);
		}
	}
	public String getFull(String prefix) {
		return prefixUriMap.get(prefix); 
	}
	/*return the java type given a graal type*/
	public Object getType(String key) {
		if(typesTable.containsKey(key))
			return typesTable.get(key);
		else {
			int indexOf=key.indexOf("#");
			String prefix=key.substring(0,indexOf);
			return typesTable.get(prefix);
		}
	}
	
	/*return the graal type given a java type*/
	public String getGraalType(Object key) {		
		return inversedTypesTable.get(key);
	}	
	
	/*given a path for a file config this method return an instance 
	 * of the classe where are functions*/
	
	public void getObjectToInvoke(String prefix,String prefixFilePath){
		JSONObject jsonObject=this.confReader.readJsonFile(prefixFilePath);
		JSONObject defaultJsonObject=this.confReader.getDefault(jsonObject);
		JSONObject elementsJsonObject=this.confReader.getElements(jsonObject);
		if(!elementsJsonObject.isEmpty()) {
			for(Object key :elementsJsonObject.keySet()) {
				JSONObject currentJsonObject=(JSONObject)elementsJsonObject.get(key);
				String type=this.confReader.getType(currentJsonObject);
				try {
					JSONObject locationJsonObject=this.confReader.getLocation(currentJsonObject); 
					switch(type) {
					case "literal":
						typesTable.put(prefix+((String) key), this.confReader.getClassFromJsonObject(locationJsonObject));
						break;
					case "predicate":
						prefixTable.put(prefix+((String) key), this.confReader.getClassFromJsonObject(locationJsonObject));
						break;
					case "function":
						prefixTable.put(prefix+((String) key), this.confReader.getClassFromJsonObject(locationJsonObject));
						break;
					default:
						throw new ConfigFileException(type+"not in <literal,predicate,function>");
						}
					}catch(ConfigFileException e) {
						e.printStackTrace();
					}
				}
		
		}
		if(!defaultJsonObject.isEmpty()) { 
			JSONObject typesJsonObject=this.confReader.getTypes(defaultJsonObject);
			JSONObject predicatesJsonObject=this.confReader.getPredicates(defaultJsonObject);
			JSONObject functionsJsonObject=this.confReader.getFunctions(defaultJsonObject);
			
			if(!typesJsonObject.isEmpty())
				typesTable.put(prefix, this.confReader.getClassFromJsonObject(typesJsonObject));
			if(!predicatesJsonObject.isEmpty()) 
				prefixTable.put(prefix, this.confReader.getClassFromJsonObject(predicatesJsonObject));
			
			if(!functionsJsonObject.isEmpty()) 
				prefixTable.put(prefix, this.confReader.getClassFromJsonObject(functionsJsonObject));	
			
		}
	}

	
	
	private void reverseMap() {
		for(Map.Entry<String,Object> entry:typesTable.entrySet())
			inversedTypesTable.put(entry.getValue(), entry.getKey());
	}
}
