# InteGraal

Copyright 2022 INRIA, LIRMM, CNRS, Univ. Montpellier, INRAe

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

## Java

InteGraal has been tested with Java version >= 11 up to the last java 17 LTS].

## How to work with InteGraal

If you just need to use InteGraal, you can import it as any library using the [jar available in the project repository](https://gitlab.inria.fr/rules/integraal/-/blob/develop/integraal-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar).
This jar should include all the dependencies for the project and is also executable with an example application already prepared.

## Examples availables

Some examples are available under the `integraal-app` module.

Please note that these examples are of two types :
- Feature example, presenting how to use a given feature of the library
- Use-case example wich are concrete examples that we used in applications. Thses can require some identification informations to access the datasources.

## Working on extending InteGraal

### Quick note

If you want to extend InteGraal you are welcome and we thank you in advance for your work with us.

### How to build InteGraal

#### Git

* Install [git](http://www.git-scm.com/)
* Clone this repository
```sh
git clone https://gitlab.inria.fr/rules/integraal.git
```

#### Temporary work-around with the DLGP parser

As we are still in early developpement phases, some of the annex projects are not yet published on the maven repository and you will need to compile them independently such as the DLGP parser.

To do so, you'll need to compile the grammar file `DLGP2Parser.jj` using [javaCC](https://javacc.github.io/javacc/).
You can find this file under [dlgp-parser/dlgp-parser-builtin/src/main/java/fr/lirmm/graphik/dlgp3/parser](https://gitlab.inria.fr/rules/integraal/-/tree/develop/dlgp-parser/dlgp-parser-builtin/src/main/java/fr/lirmm/graphik/dlgp3/parser)

Then you can build the parser project using maven.

#### Maven

* Install [maven](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)
* Build the project
```sh
mvn clean install
```

#### IDE

If you wish to extend the InteGraal library, you can open the project in your favorite IDE such as Eclipse or IntelliJ.

Please make sure that you are using a version that support java 11 or more and configure it accordingly.

## Contact

If you need more informations about the library or encounter any problem, please contact [Florent Tornil](mailto:florent.tornil@inria.fr).

If you encounter some unexpected behaviour, feel free to fill an issue on this Git an we will try our best to help you.
